﻿// API.Spectrum.cs created with MonoDevelop
// User: klose at 17:47 16.08.2018
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2012  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.Collections.Generic;
using System.IO;

using Hiperscan.Common;
using Hiperscan.Spectroscopy;
using Hiperscan.Spectroscopy.IO;
using Hiperscan.Unix;


namespace Hiperscan.CInterface.Finder
{

    public static partial class API
    {
        
        public static int ReadCsv(string fname, 
                                  ref double[] wavelength, 
                                  ref double[] intensity,
                                  ref double[] reflectance,
                                  ref long timestamp,
                                  ref string creator,
                                  ref string comment,
                                  ref string serial_number,
                                  ref string firmware_version,
                                  ref string label,
                                  ref double spectral_resolution,
                                  ref uint average_count,
                                  ref int probe_type,
                                  ref int spectrum_type)
        {
            try
            {
                Console.WriteLine("Invoking CInterface.Finder.API.ReadCsv()...");

                Spectrum spectrum = CSV.Read(fname);

                if (spectrum.SpectrumType != SpectrumType.ProcessedFluidSpecimen && 
                    spectrum.SpectrumType != SpectrumType.ProcessedSolidSpecimen &&
                    spectrum.SpectrumType != SpectrumType.ProcessedIntensitySpecimen)
                {
                    throw new InterfaceException(ExceptionType.InvalidSpectrumType, 
                                                 string.Format(Catalog.GetString("Invalid spectrum type: {0}"),
                                                               spectrum.SpectrumType.ToString()));
                }

                DataSet reflectance_ds = null;

                if (spectrum.HasAbsorbance)
                        reflectance_ds = 1.0 / DataSet.Pow(10.0, spectrum.Absorbance);

                wavelength = new double[reflectance_ds.Count];
                intensity  = new double[reflectance_ds.Count];

                if (reflectance_ds != null)
                {
                    reflectance = new double[reflectance_ds.Count];

                    for (int ix=0; ix < reflectance_ds.Count; ++ix)
                    {
                        wavelength[ix]  = reflectance_ds.X[ix];
                        reflectance[ix] = reflectance_ds.Y[ix];
                        intensity[ix]   = spectrum.Intensity.Y[ix];
                    }
                }
                else
                {
                    reflectance = new double[0];

                    for (int ix=0; ix < reflectance_ds.Count; ++ix)
                    {
                        wavelength[ix]  = spectrum.Intensity.X[ix];
                        intensity[ix]   = spectrum.Intensity.Y[ix];
                    }
                }

                timestamp           = Misc.GetEpochSeconds(spectrum.Timestamp);
                creator             = spectrum.Creator;
                comment             = spectrum.Comment;
                serial_number       = spectrum.Serial;
                firmware_version    = spectrum.FirmwareVersion;
                label               = spectrum.DefaultLabel ? null : spectrum.Label;
                spectral_resolution = spectrum.SpectralResolution;
                average_count       = (uint)spectrum.AverageCount;
                probe_type          = (int)spectrum.ProbeType;
                spectrum_type       = (int)spectrum.SpectrumType;

                return 0;
            }
            catch (InterfaceException ex)
            {
                API.Exception = ex;
                return ex.Result;
            }
            catch (Exception ex)
            {
                API.Exception = ex;
                return (int)ExceptionType.Unknown;
            }
        }
 
        public static int WriteCsv(string fname,
                                   int len,
                                   double[] wavelength,
                                   double[] intensity,
                                   double[] reflectance,
                                   long timestamp,
                                   string creator,
                                   string comment,
                                   string serial_number,
                                   string firmware_version,
                                   string label,
                                   double spectral_resolution,
                                   uint average_count,
                                   int probe_type,
                                   int spectrum_type,
                                   byte[] salt)
        {
            try
            {
                Console.WriteLine("Invoking CInterface.Finder.API.WriteCsv()...");

                if (wavelength.Length != len)
                {
                    throw new InterfaceException(ExceptionType.InvalidSpectrumFormat,
                                                 Catalog.GetString("Unexpected spectrum length."));
                }

                DataSet intensity_ds;
                DataSet reference_ds;
                if (intensity.Length == len)
                {
                    intensity_ds = new DataSet(wavelength, intensity);

                    if (reflectance.Length == len)
                        reference_ds = intensity_ds / new DataSet(wavelength, reflectance);
                    else
                        reference_ds = null;
                }
                else if (reflectance.Length == len)
                {
                    intensity_ds = new DataSet(wavelength, reflectance);
                    reference_ds = new DataSet(wavelength, 1.0);
                }
                else
                {
                    throw new InterfaceException(ExceptionType.InvalidSpectrumType,
                                                 Catalog.GetString("Spectrum must contain intensity or reflectance values."));
                }

                Spectrum spectrum = new Spectrum(intensity_ds, new WavelengthLimits(intensity_ds))
                {
                    Reference = reference_ds != null ? new Spectrum(reference_ds, new WavelengthLimits(reference_ds)) : null,
                    DarkIntensity = new DarkIntensity(double.Epsilon, 0),
                    WavelengthCorrection = new WavelengthCorrection(),

                    Timestamp = Misc.GetDateTime(timestamp)
                };

                if (string.IsNullOrEmpty(creator))
                {
                    throw new InterfaceException(ExceptionType.InvalidArgument,
                                                 Catalog.GetString("Argument Creator must not be empty."));
                }

                spectrum.Creator = creator;

                if (API.ENABLE_EXPERIMENTAL_API_CALLS)
                    spectrum.Comment = "WARNING: Instrument driver is running in EXPERIMENTAL mode. Experimental API calls are enabled.\n";
                else
                    spectrum.Comment = string.Empty;

                spectrum.Comment += comment;

                if (string.IsNullOrEmpty(serial_number))
                {
                    throw new InterfaceException(ExceptionType.InvalidArgument,
                                                 Catalog.GetString("Argument Serial Number must not be empty."));
                }

                spectrum.Serial  = serial_number;

                if (string.IsNullOrEmpty(firmware_version) == false)
                    spectrum.FirmwareVersion = firmware_version;

                if (string.IsNullOrEmpty(label) == false)
                    spectrum.Label = label;
                
                if (spectral_resolution <= 0.0)
                {
                    throw new InterfaceException(ExceptionType.InvalidArgument,
                                                 Catalog.GetString("Invalid value for Spectral Resolution."));
                }

                spectrum.SpectralResolution = spectral_resolution;

                if (average_count < 1)
                {
                    throw new InterfaceException(ExceptionType.InvalidArgument,
                                                 Catalog.GetString("Invalid value for Average Count."));
                }

                spectrum.AverageCount = (int)average_count;

                try
                {
                    spectrum.ProbeType = (ProbeType)probe_type;
                }
                catch
                {
                    throw new InterfaceException(ExceptionType.InvalidArgument, 
                                                 string.Format(Catalog.GetString("Unknown Probe type: {0}"), probe_type));
                }

                try
                {
                    spectrum.SpectrumType = (SpectrumType)spectrum_type;
                }
                catch
                {
                    throw new InterfaceException(ExceptionType.InvalidArgument, 
                                                 string.Format(Catalog.GetString("Unknown Spectrum type: {0}"), spectrum_type));
                }

                if (spectrum.HasAbsorbance == false && spectrum.SpectrumType != SpectrumType.ProcessedIntensitySpecimen)
                    throw new InterfaceException(ExceptionType.InvalidSpectrumType,
                                                 string.Format(Catalog.GetString("Inkonsistent Spectrum type: {0}"), spectrum_type));

                CSV.Write(spectrum, fname, CSV.DEFAULT_CULTURE, salt);

                return 0;
            }
            catch (InterfaceException ex)
            {
                API.Exception = ex;
                return ex.Result;
            }
            catch (Exception ex)
            {
                API.Exception = ex;
                return (int)ExceptionType.Unknown;
            }
        }

        public static int CheckIntegrity(string fname, byte[] salt, ref int result)
        {
            Console.WriteLine("Invoking CInterface.Finder.API.CheckIntegrity()...");

            try
            {
                if (salt == null || salt.Length == 0)
                    throw new InterfaceException(ExceptionType.InvalidArgument, 
                                                 Catalog.GetString("Cannot check integrity of a spectrum without a SHA salt."));

                if (fname.EndsWith(".csvc", StringComparison.InvariantCultureIgnoreCase))
                    result = CSV.CheckContainerIntegrety(fname, salt) ? 1 : 0;
                else
                    result = CSV.CheckIntegrity(fname, salt) ? 1 : 0;

                return 0;
            }
            catch (InterfaceException ex)
            {
                API.Exception = ex;
                return ex.Result;
            }
            catch (InvalidDataException ex)
            {
                API.Exception = ex;
                return (int)ExceptionType.MissingChecksum;
            }
            catch (Exception ex)
            {
                API.Exception = ex;
                return (int)ExceptionType.Unknown;
            }
        }

        public static int WriteContainer(List<string> tmp_files, string fname, byte[] salt)
        {
            Console.WriteLine("Invoking CInterface.Finder.API.WriteContainer()...");

            try
            {
                if (tmp_files == null || tmp_files.Count < 1)
                {
                    throw new InterfaceException(ExceptionType.InvalidArgument,
                                                 Catalog.GetString("Cannot write empty CSV Container file."));
                }

                List<Spectrum> spectra = new List<Spectrum>();

                foreach (string tmp_file in tmp_files)
                {
                    Spectrum spectrum = CSV.Read(tmp_file);
                    if (string.IsNullOrEmpty(spectrum.Label) || spectrum.DefaultLabel)
                    {
                        throw new InterfaceException(ExceptionType.InvalidSpectrumFormat,
                                                     Catalog.GetString("Spectra without defined Label cannot be included into a CSV Container file."));
                    }

                    spectra.Add(spectrum);
                }

                CSV.WriteContainer(spectra, fname, CSV.DEFAULT_CULTURE, salt);

                return 0;
            }
            catch (InterfaceException ex)
            {
                API.Exception = ex;
                return ex.Result;
            }
            catch (Exception ex)
            {
                API.Exception = ex;
                return (int)ExceptionType.Unknown;
            }
        }

        public static int ReadContainer(string fname, List<string> tmp_fnames)
        {
            Console.WriteLine("Invoking CInterface.Finder.API.ReadContainer()...");

            try
            {
                Spectrum[] spectra = CSV.ReadContainer(fname);

                foreach (Spectrum spectrum in spectra)
                {
                    string tmp_fname = Path.Combine(Path.GetTempPath(), Path.GetRandomFileName() + ".csv");
                    tmp_fnames.Add(tmp_fname);
                    CSV.Write(spectrum, tmp_fname);
                }

                return 0;
            }
            catch (InterfaceException ex)
            {
                API.Exception = ex;
                return ex.Result;
            }
            catch (Exception ex)
            {
                API.Exception = ex;
                return (int)ExceptionType.Unknown;
            }
        }
    }
}