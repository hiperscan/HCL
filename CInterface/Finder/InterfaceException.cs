// InterfaceException.cs created with MonoDevelop
// User: klose at 17:47 25.04.2013
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2012  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;


namespace Hiperscan.CInterface.Finder
{

    public class InterfaceException : System.Exception
    {
        public ExceptionType ExceptionType { get; protected set; }
        public int Result                  { get; protected set; }

        public InterfaceException(ExceptionType exception_type, string msg) : base(msg)
        {
            this.ExceptionType = exception_type;
            this.Result = (int)exception_type;
        }

        public InterfaceException(ExceptionType exception_type, string msg, Exception ex) : base(msg, ex)
        {
            this.ExceptionType = exception_type;
            this.Result = (int)exception_type;
        }
    }
}

