// CInterface.cs created with MonoDevelop
// User: klose at 17:47 25.04.2013
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2012  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using Hiperscan.Unix;


namespace Hiperscan.CInterface.Finder
{

    public static partial class API
    {
        public delegate void QueryExternalSampleHandler();
        public delegate void TemperatureStatusChangedHandler(string json_status);
        public delegate void FinderStatusChangedHandler(int status);


        public static event QueryExternalSampleHandler QueryExternalWhiteReference
        {
            add
            {
                if (Synchronous.Finder == null && Asynchronous.Finder == null)
                {
                    throw new InterfaceException(ExceptionType.NoSuchDevice,
                                                 Catalog.GetString("Cannot add handler for uninitialized device."));
                }

                if (API.has_temperature_stabilization)
                    Asynchronous.QueryExternalWhiteReference += value;
                else
                    Synchronous.QueryExternalWhiteReference  += value;
            }
            remove
            {
                if (API.has_temperature_stabilization)
                    Asynchronous.QueryExternalWhiteReference -= value;
                else
                    Synchronous.QueryExternalWhiteReference  -= value;
            }
        }

        public static event QueryExternalSampleHandler QueryExternalDarkReference
        {
            add
            {
                if (Synchronous.Finder == null && Asynchronous.Finder == null)
                {
                    throw new InterfaceException(ExceptionType.NoSuchDevice,
                                                 Catalog.GetString("Cannot add handler for uninitialized device."));
                }

                if (API.has_temperature_stabilization)
                    Asynchronous.QueryExternalDarkReference += value;
                else
                    Synchronous.QueryExternalDarkReference  += value;
            }
            remove
            {
                if (API.has_temperature_stabilization)
                    Asynchronous.QueryExternalDarkReference -= value;
                else
                    Synchronous.QueryExternalDarkReference  -= value;
            }
        }

        public static event QueryExternalSampleHandler QueryExternalTransflectanceReference
        {
            add
            {
                if (Synchronous.Finder == null && Asynchronous.Finder == null)
                {
                    throw new InterfaceException(ExceptionType.NoSuchDevice,
                                                 Catalog.GetString("Cannot add handler for uninitialized device."));
                }

                if (API.has_temperature_stabilization)
                    Asynchronous.QueryExternalTransflectanceReference += value;
                else
                    Synchronous.QueryExternalTransflectanceReference  += value;
            }
            remove
            {
                if (API.has_temperature_stabilization)
                    Asynchronous.QueryExternalTransflectanceReference -= value;
                else
                    Synchronous.QueryExternalTransflectanceReference  -= value;
            }
        }

        public static event QueryExternalSampleHandler QueryExternalFluidSpecimen
        {
            add
            {
                if (Synchronous.Finder == null && Asynchronous.Finder == null)
                {
                    throw new InterfaceException(ExceptionType.NoSuchDevice,
                                                 Catalog.GetString("Cannot add handler for uninitialized device."));
                }

                if (API.has_temperature_stabilization)
                    Asynchronous.QueryExternalFluidSample += value;
                else
                    Synchronous.QueryExternalFluidSample  += value;
            }
            remove
            {
                if (API.has_temperature_stabilization)
                    Asynchronous.QueryExternalFluidSample -= value;
                else
                    Synchronous.QueryExternalFluidSample  -= value;
            }
        }

        public static event TemperatureStatusChangedHandler TemperatureStatusChanged
        {
            add
            {
                if (Synchronous.Finder == null && Asynchronous.Finder == null)
                {
                    throw new InterfaceException(ExceptionType.NoSuchDevice,
                                                 Catalog.GetString("Cannot add handler for uninitialized device."));
                }

                if (API.has_temperature_stabilization)
                    Asynchronous.TemperatureStatusChanged += value;
            }
            remove
            {
                if (API.has_temperature_stabilization)
                    Asynchronous.TemperatureStatusChanged -= value;
            }
        }
    }
}