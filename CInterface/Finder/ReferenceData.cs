﻿// ReferenceData.cs created with MonoDevelop
// User: klose at 17:47 21.11.2017
// CVS release: $Id$
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2012  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

using Hiperscan.Spectroscopy;


namespace Hiperscan.CInterface.Finder
{

    [Serializable()]
    public class ReferenceData : System.Runtime.Serialization.ISerializable
    {
        public Spectrum ExternalWhite    { get; set; }
        public Spectrum ExternalEmpty    { get; set; }
        public Spectrum InternaleOxidRef { get; set; }
        public Spectrum InternalWhiteRef { get; set; }
        public Spectrum InternalDarkRef  { get; set; }

        public ReferenceData(Spectrum extwhite, Spectrum extempty, Spectrum intoxidref, Spectrum intwhiteref, Spectrum intdarkref)
        {
            this.ExternalWhite    = extwhite;
            this.ExternalEmpty    = extempty;
            this.InternaleOxidRef = intoxidref;
            this.InternalWhiteRef = intwhiteref;
            this.InternalDarkRef  = intdarkref;
        }

        private ReferenceData(SerializationInfo info, StreamingContext context)
        {
            this.ExternalWhite    = (Spectrum)info.GetValue("ExternalWhite",    typeof(Spectrum));
            this.ExternalEmpty    = (Spectrum)info.GetValue("ExternalEmpty",    typeof(Spectrum));
            this.InternaleOxidRef = (Spectrum)info.GetValue("InternalOxidRef",  typeof(Spectrum));
            this.InternalWhiteRef = (Spectrum)info.GetValue("InternalWhiteRef", typeof(Spectrum));
            this.InternalDarkRef  = (Spectrum)info.GetValue("InternalDarkRef",  typeof(Spectrum));
        }

        public static ReferenceData Deserialize(byte[] data)
        {
            BinaryFormatter bf = new BinaryFormatter();

            using (MemoryStream mstream = new MemoryStream(data))
            {
                return (ReferenceData)bf.Deserialize(mstream);
            }
        }

        public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            info.AddValue("ExternalWhite",    this.ExternalWhite);
            info.AddValue("ExternalEmpty",    this.ExternalEmpty);
            info.AddValue("InternalOxidRef",  this.InternaleOxidRef);
            info.AddValue("InternalWhiteRef", this.InternalWhiteRef);
            info.AddValue("InternalDarkRef",  this.InternalDarkRef);
        }

        public byte[] Serialize()
        {
            BinaryFormatter bf = new BinaryFormatter();

            using (MemoryStream mstream = new MemoryStream())
            {
                bf.Serialize(mstream, this);
                return mstream.GetBuffer();
            }
        }

        public bool IsComplete
        {
            get
            {
                return this.ExternalWhite    != null && 
                       this.ExternalEmpty    != null &&
                       this.InternaleOxidRef != null &&
                       this.InternalWhiteRef != null &&
                       this.InternalDarkRef  != null;
            }
        }
    }
}