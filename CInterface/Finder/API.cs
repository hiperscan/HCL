// API.cs created with MonoDevelop
// User: klose at 17:47 25.04.2013
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2012  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Reflection;

using Hiperscan.Common;
using Hiperscan.Common.LogWriter;
using Hiperscan.SGS;
using Hiperscan.SGS.Benchtop;
using Hiperscan.SGS.SerialInterface;
using Hiperscan.Spectroscopy.Math;
using Hiperscan.Unix;


namespace Hiperscan.CInterface.Finder
{

    public static partial class API
    {
        public const string NOT_AVAILABLE = "n/a";

        private static volatile bool _opening = false;
        private static volatile bool _require_transflectance_reference = true;

        public static readonly bool ENABLE_EXPERIMENTAL_API_CALLS =
            Environment.GetEnvironmentVariable(Env.HCL.EXPERIMENTAL) == "1";

        public static readonly string DEBUG_OUT_DIRECTORY =
            Path.Combine(Path.GetTempPath(), "HCL_Debug_" + DateTime.Now.ToString("yyyy-MM-dd"));


        private static Exception exception = null;

        internal static InstrumentType InstrumentType  { get; private set; } = InstrumentType.Unknown;
        public static bool DeactivateDebugLog          { get; set; } = false;
        public static CultureInfo Culture              { get; } = CultureInfo.InvariantCulture;
        public static List<double> StandardWavelengths { get; } = new List<double>();

        private static bool has_temperature_stabilization = false;


        private static DeviceList device_list;

        public static int Open(uint product_id)
        {
            return API.Open( product_id, string.Empty);
        }

        public static int Open(uint product_id, string wrapper_version)
        {
            int ires;
            uint vid;
            switch (product_id)
            {
                case DeviceList.PRODUCT_ID_APOIDENT:
                    vid = DeviceList.VENDOR_ID_HIPERSCAN;
                    break;
                default:
                    vid = DeviceList.VENDOR_ID_FTDI;
                    break;
            }
            ires = API.Open(vid, product_id, wrapper_version);
            if (ires != 0)
            {
                Console.WriteLine("Error opening VID/PID " + vid.ToString("X4") + "/" + product_id.ToString("X4"));
            }
            return ires;
        }



        public static int Open(uint vendor_id, uint product_id, string wrapper_version)
        {
            if (Synchronous.Finder != null || Asynchronous.Finder != null)
            {
                API.Exception = new InterfaceException(ExceptionType.Busy, Catalog.GetString("Instrument is already open."));
                return (int)ExceptionType.Busy;
            }

            API._opening = true;

            try
            {
                if (Environment.GetEnvironmentVariable(Env.HCL.DEBUG) == "1" && API.DeactivateDebugLog == false)
                {
                    try
                    {
                        if (Directory.Exists(DEBUG_OUT_DIRECTORY) == false)
                            Directory.CreateDirectory(DEBUG_OUT_DIRECTORY);

                        string logfile = string.Format("logfile_{0}.txt", DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"));
                        Timestamper log_writer = new Timestamper(Path.Combine(DEBUG_OUT_DIRECTORY, logfile));
                        Console.SetOut(log_writer);
                        Console.WriteLine(Assembly.GetExecutingAssembly());
                        Console.WriteLine("HCL C wrapper version: " + wrapper_version);
                    }
#pragma warning disable RECS0022
                    catch { }
#pragma warning restore RECS0022
                }

                device_list = new DeviceList(vendor_id, product_id, SerialInterfaceConfig.BackendType.FT245BL);
                //device_list = new DeviceList(DeviceList.DEFAULT_USB_CONFIGS);
                device_list.Update();
                if (device_list.Count < 1)
                    throw new InterfaceException(ExceptionType.NoSuchDevice, Catalog.GetString("No device found."));

                IDevice device = device_list.GetFirstDevice();

                device.NewException += ex => throw new InterfaceException(ExceptionType.InputOutput, ex.Message);

                API.ResetCallbacks();

                device.Open();
                if (device.IsFinder == false)
                {
                    device.Close();
                    throw new InterfaceException(ExceptionType.NoSuchDevice, Catalog.GetString("Unsupported device type."));
                }

                try
                {
                    device.ReadTemperatureControlInfo(out byte version, out byte control_value, out double target_temperature);

                    Console.WriteLine("Read temperature control info: Version={0}, Control Value={1}, Target Temperature={2}",
                                      version,
                                      control_value,
                                      target_temperature);

                    if (version != 0 && version != 255)
                    {
                        device.SetFinderFanSpeed(false);
                        API.has_temperature_stabilization = true;
                    }
                }
                catch (Exception ex) when (ex is NotSupportedException || ex is NotImplementedException)
                {
                    API.has_temperature_stabilization = false;
                }

                if ((vendor_id == DeviceList.VENDOR_ID_FTDI && product_id == DeviceList.PRODUCT_ID_SGS1900) ||
                    (vendor_id == DeviceList.VENDOR_ID_HIPERSCAN && product_id == DeviceList.PRODUCT_ID_APOIDENT))
                {
                    if (API.has_temperature_stabilization)
                        API.InstrumentType = InstrumentType.FinderSD;
                    else
                        API.InstrumentType = InstrumentType.ApoIdent;
                }
                else if (vendor_id == DeviceList.VENDOR_ID_FTDI && product_id == DeviceList.PRODUCT_ID_SGS1900B)
                {
                    API.InstrumentType = InstrumentType.PTIM100;
                }
                else
                {
                    throw new NotSupportedException($"Unsupported device: {vendor_id}:{product_id}.");
                }


                Console.WriteLine("Found instrument of type: " + API.InstrumentType.ToString());

                if (API.has_temperature_stabilization)
                {
                    Console.WriteLine("Enable temperature stabilized mode.");
                    return Asynchronous.Open(device);
                }
                else
                {
                    Console.WriteLine("Disable temperature stabilized mode.");
                    return Synchronous.Open(device);
                }
            }
            catch (InterfaceException ex)
            {
                Synchronous.Finder = null;
                Asynchronous.Finder = null;
                API.Exception = ex;
                API.SetStatus(AsyncStabilizedFinder.StateType.Unknown);
                return ex.Result;
            }
            catch (TimeoutException ex)
            {
                Synchronous.Finder = null;
                Asynchronous.Finder = null;
                API.Exception = ex;
                API.SetStatus(AsyncStabilizedFinder.StateType.Unknown);
                return (int)ExceptionType.Timeout;
            }
            catch (Exception ex)
            {
                Synchronous.Finder = null;
                Asynchronous.Finder = null;
                API.Exception = ex;
                API.SetStatus(AsyncStabilizedFinder.StateType.Unknown);
                return (int)ExceptionType.Unknown;
            }
            finally
            {
                API._opening = false;
            }
        }

        public static int OpenRemote( string server_name, uint port_number )
        {
            int ires;

            API._opening = true;

            ires = (int)ExceptionType.NoSuchDevice;
            try
            {
                if (Environment.GetEnvironmentVariable(Env.HCL.DEBUG) == "1" && API.DeactivateDebugLog == false)
                {
                    try
                    {
                        if (Directory.Exists(DEBUG_OUT_DIRECTORY) == false)
                            Directory.CreateDirectory(DEBUG_OUT_DIRECTORY);

                        string logfile = string.Format("logfile_{0}.txt", DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"));
                        Timestamper log_writer = new Timestamper(Path.Combine(DEBUG_OUT_DIRECTORY, logfile));
                        Console.SetOut(log_writer);
                        Console.WriteLine(Assembly.GetExecutingAssembly());
                        //Console.WriteLine("HCL C wrapper version: " + wrapper_version);
                    }
#pragma warning disable RECS0022
                    catch { }
#pragma warning restore RECS0022
                }

                if (string.IsNullOrEmpty(server_name))
                    throw new ArgumentException("A server address must be specified.");

                device_list.AddRemoteDevice(server_name, port_number);
                device_list.Update();

                if (device_list.Count < 1)
                    throw new InterfaceException(ExceptionType.NoSuchDevice, Catalog.GetString("No device found."));

                IDevice device = device_list.GetFirstDevice();

                device.NewException += ex => throw new InterfaceException(ExceptionType.InputOutput, ex.Message);

                // API.ResetCallbacks();
                API.ResetCallbacks();

                device.Open();
                if (device.IsFinder == false)
                {
                    device.Close();
                    throw new InterfaceException(ExceptionType.NoSuchDevice, Catalog.GetString("Unsupported device type."));
                }

                try
                {
                    device.ReadTemperatureControlInfo(out byte version, out byte control_value, out double target_temperature);

                    Console.WriteLine("Read temperature control info: Version={0}, Control Value={1}, Target Temperature={2}",
                                      version,
                                      control_value,
                                      target_temperature);

                    if (version != 0 && version != 255)
                    {
                        device.SetFinderFanSpeed(false);
                        API.has_temperature_stabilization = true;
                    }
                }
                catch (Exception ex) when (ex is NotSupportedException || ex is NotImplementedException)
                {
                    API.has_temperature_stabilization = false;
                }

                //} 
                //catch (Exception ex)
                //{
                //    Console.WriteLine("Cannot connect remote device: {0}", ex.Message);
                //}

                //if (ires != 0)
                //{
                //    Console.WriteLine("Error opening " + server_name + ":" + port_number.ToString());
                //}


                if (API.has_temperature_stabilization)
                    API.InstrumentType = InstrumentType.FinderSD;
                else
                    API.InstrumentType = InstrumentType.ApoIdent;


                Console.WriteLine("Found instrument of type: " + API.InstrumentType.ToString());

                if (API.has_temperature_stabilization)
                {
                    Console.WriteLine("Enable temperature stabilized mode.");
                    return Asynchronous.Open(device);
                }
                else
                {
                    Console.WriteLine("Disable temperature stabilized mode.");
                    return Synchronous.Open(device);
                }
            }
            catch (InterfaceException ex)
            {
                Synchronous.Finder = null;
                Asynchronous.Finder = null;
                API.Exception = ex;
                API.SetStatus(AsyncStabilizedFinder.StateType.Unknown);
                return ex.Result;
            }
            catch (TimeoutException ex)
            {
                Synchronous.Finder = null;
                Asynchronous.Finder = null;
                API.Exception = ex;
                API.SetStatus(AsyncStabilizedFinder.StateType.Unknown);
                return (int)ExceptionType.Timeout;
            }
            catch (Exception ex)
            {
                Synchronous.Finder = null;
                Asynchronous.Finder = null;
                API.Exception = ex;
                API.SetStatus(AsyncStabilizedFinder.StateType.Unknown);
                return (int)ExceptionType.Unknown;
            }
            finally
            {
                API._opening = false;
            }
        }

        public static int CloseRemote( string server_name, uint port_number ) 
        {
            int ires = 0;

            try 
            {
                device_list.RemoveRemoteDevice(null);
                return ires;
            }
            finally
            {
                //API._opening = false;

            }
        }

        public static int Init()
        {
            try
            {
                return API.has_temperature_stabilization 
                          ? Asynchronous.Init() 
                          : Synchronous.Init();
            }
            catch (InterfaceException ex)
            {
                API.Exception = ex;
                API.SetStatus(AsyncStabilizedFinder.StateType.Unknown);
                return ex.Result;
            }
            catch (TimeoutException ex)
            {
                API.Exception = ex;
                API.SetStatus(AsyncStabilizedFinder.StateType.Unknown);
                return (int)ExceptionType.Timeout;
            }
            catch (Exception ex)
            {
                API.Exception = ex;
                API.SetStatus(AsyncStabilizedFinder.StateType.Unknown);
                return (int)ExceptionType.Unknown;
            }
        }

        public static int Close()
        {
            try
            {
                return API.has_temperature_stabilization 
                          ? Asynchronous.Close() 
                          : Synchronous.Close();
            }
            catch (InterfaceException ex)
            {
                API.Exception = ex;
                return ex.Result;
            }
            catch (TimeoutException ex)
            {
                API.Exception = ex;
                return (int)ExceptionType.Timeout;
            }
            catch (Exception ex)
            {
                API.Exception = ex;
                return (int)ExceptionType.Unknown;
            }
            finally
            {
                Synchronous.Finder = null;
                Asynchronous.Finder = null;
            }
        }

        public static int SetParam(string param, string val)
        {
            Console.WriteLine("SetParam({0}, {1})", param, val);

            try
            {
                return API.has_temperature_stabilization
                    ? Asynchronous.SetParam(param, val)
                    : Synchronous.SetParam(param, val);
            }
            catch (InterfaceException ex)
            {
                API.Exception = ex;
                return ex.Result;
            }
            catch (Exception ex) when (ex is FormatException || ex is ArgumentException)
            {
                API.Exception = ex;
                return (int)ExceptionType.InvalidArgument;
            }
            catch (Exception ex)
            {
                API.Exception = ex;
                API.SetStatus(AsyncStabilizedFinder.StateType.Unknown);
                return (int)ExceptionType.Unknown;
            }
        }

        public static int GetParam(string param, out string val)
        {
            Console.WriteLine("GetParam({0})", param);

            try
            {
                return API.has_temperature_stabilization 
                              ? Asynchronous.GetParam(param, out val) 
                              : Synchronous.GetParam(param, out val);
            }
            catch (InterfaceException ex)
            {
                val = string.Empty;
                API.Exception = ex;
                return ex.Result;
            }
            catch (Exception ex) when (ex is FormatException || ex is ArgumentException)
            {
                val = string.Empty;
                API.Exception = ex;
                return (int)ExceptionType.InvalidArgument;
            }
            catch (Exception ex)
            {
                val = Catalog.GetString("n/a");
                API.Exception = ex;
                API.SetStatus(AsyncStabilizedFinder.StateType.Unknown);
                return (int)ExceptionType.Unknown;
            }
        }

        public static int GetReferenceData(int maxlen, ref byte[] data, ref int len)
        {
            Console.WriteLine("GetReferenceData()");

            try
            {
                byte[] _data = API.has_temperature_stabilization 
                                  ? Asynchronous.GetReferenceData() 
                                  : Synchronous.GetReferenceData();

                if (_data.Length > maxlen)
                    throw new InterfaceException(ExceptionType.InvalidArgument,
                                                 string.Format(Catalog.GetString("Data buffer ({0}B) is too small for serialized reference data ({1}B)."),
                                                               maxlen,
                                                               _data.Length));

                Array.Copy(_data, data, _data.Length);
                len = _data.Length;

                return 0;
            }
            catch (InterfaceException ex)
            {
                data = null; ;
                API.Exception = ex;
                return ex.Result;
            }
            catch (Exception ex)
            {
                data = null;
                API.Exception = ex;
                return (int)ExceptionType.Unknown;
            }
        }

        public static int SetReferenceData(byte[] data, int len)
        {
            Console.WriteLine("SetReferenceData()");

            try
            {
                byte[] _data = new byte[len];
                Array.Copy(data, _data, len);

                try
                {
                    if (API.has_temperature_stabilization)
                        Asynchronous.SetReferenceData(_data);
                    else
                        Synchronous.SetReferenceData(_data);
                }
                catch (Exception ex)
                {
                    throw new InterfaceException(ExceptionType.InvalidReferenceData, ex.Message, ex);
                }

                return 0;
            }
            catch (InterfaceException ex)
            {
                API.Exception = ex;
                return ex.Result;
            }
            catch (Exception ex)
            {
                API.Exception = ex;
                return (int)ExceptionType.Unknown;
            }
        }

        private static void SetStatus(AsyncStabilizedFinder.StateType state)
        {
            if (API.has_temperature_stabilization && Asynchronous.Finder != null)
                Asynchronous.Finder.State = state;
        }

        public static int GetStatus(out int status, out double progress, out double eta_seconds, out double temperature, out double temperature_gradient)
        {
            try
            {
                if (API.has_temperature_stabilization && API._opening == false)
                {
                    return Asynchronous.GetStatus(out status, out progress, out eta_seconds, out temperature, out temperature_gradient);
                }
                else
                {
                    status = (int)AsyncStabilizedFinder.StateType.Idle;
                    progress = -1.0;
                    eta_seconds = -1.0;
                    temperature = double.NaN;
                    temperature_gradient = double.NaN;
                    return 0;
                }
            }
            catch (InterfaceException ex)
            {
                status = (int)AsyncStabilizedFinder.StateType.Unknown;
                API.Exception = ex;
                progress = -1.0;
                eta_seconds = -1.0;
                temperature = double.NaN;
                temperature_gradient = double.NaN;
                API.SetStatus(AsyncStabilizedFinder.StateType.Unknown);
                return ex.Result;
            }
            catch (Exception ex)
            {
                status = (int)AsyncStabilizedFinder.StateType.Unknown;
                API.Exception = ex;
                progress = -1.0;
                eta_seconds = -1.0;
                temperature = double.NaN;
                temperature_gradient = double.NaN;
                API.SetStatus(AsyncStabilizedFinder.StateType.Unknown);
                return (int)ExceptionType.Unknown;
            }
        }

        public static int BeginAcquisitionSequence()
        {
            try
            {
                if (API.has_temperature_stabilization)
                    Asynchronous.BeginAcquisitionSequence();
                else
                    Synchronous.BeginAcquisitionSequence();

                return 0;
            }
            catch (InterfaceException ex)
            {
                API.Exception = ex;
                if (ex.ExceptionType != ExceptionType.Busy &&
                    ex.ExceptionType != ExceptionType.MissingReference &&
                    ex.ExceptionType != ExceptionType.NotPermitted)
                {
                    API.SetStatus(AsyncStabilizedFinder.StateType.Unknown);
                }

                return ex.Result;
            }
            catch (TimeoutException ex)
            {
                API.Exception = ex;
                API.SetStatus(AsyncStabilizedFinder.StateType.Unknown);
                return (int)ExceptionType.Timeout;
            }
            catch (Hiperscan.SGS.Benchtop.Finder.LightSourceDamagedException ex)
            {
                API.Exception = ex;
                return (int)ExceptionType.LightSourceDamaged;
            }
            catch (Exception ex)
            {
                API.Exception = ex;
                API.SetStatus(AsyncStabilizedFinder.StateType.Unknown);
                return (int)ExceptionType.Unknown;
            }
        }

        public static int EndAcquisitionSequence()
        {
            try
            {
                if (API.has_temperature_stabilization)
                    Asynchronous.EndAcquisitionSequence();
                else
                    Synchronous.EndAcquisitionSequence();

                return 0;
            }
            catch (InterfaceException ex)
            {
                API.Exception = ex;
                if (ex.ExceptionType != ExceptionType.Busy &&
                    ex.ExceptionType != ExceptionType.NotPermitted)
                {
                    API.SetStatus(AsyncStabilizedFinder.StateType.Unknown);
                }
                return ex.Result;
            }
            catch (TimeoutException ex)
            {
                API.Exception = ex;
                API.SetStatus(AsyncStabilizedFinder.StateType.Unknown);
                return (int)ExceptionType.Timeout;
            }
            catch (Hiperscan.SGS.Benchtop.Finder.LightSourceDamagedException ex)
            {
                API.Exception = ex;
                return (int)ExceptionType.LightSourceDamaged;
            }
            catch (Exception ex)
            {
                API.Exception = ex;
                API.SetStatus(AsyncStabilizedFinder.StateType.Unknown);
                return (int)ExceptionType.Unknown;
            }
        }

        public static int AcquireReflectanceSpectrum(int len, ref double[] wavelength, ref double[] reflectance)
        {
            double[] intensity = new double[len];
            return API.AcquireReflectanceSpectrum(len, ref wavelength, ref reflectance, ref intensity);
        }

        public static int AcquireReflectanceSpectrum(int len, ref double[] wavelength, ref double[] reflectance, ref double[] intensity)
        {
            try
            {
                return API.has_temperature_stabilization
                    ? Asynchronous.AcquireReflectanceSpectrum(len, ref wavelength, ref reflectance, ref intensity)
                    :  Synchronous.AcquireReflectanceSpectrum(len, ref wavelength, ref reflectance, ref intensity);
            }
            catch (InterfaceException ex)
            {
                API.Exception = ex;
                if (ex.ExceptionType != ExceptionType.Busy &&
                    ex.ExceptionType != ExceptionType.MissingReference)
                {
                    API.SetStatus(AsyncStabilizedFinder.StateType.Unknown);
                }
                return ex.Result;
            }
            catch (TimeoutException ex)
            {
                API.Exception = ex;
                API.SetStatus(AsyncStabilizedFinder.StateType.Unknown);
                return (int)ExceptionType.Timeout;
            }
            catch (Hiperscan.SGS.Benchtop.Finder.LightSourceDamagedException ex)
            {
                API.Exception = ex;
                return (int)ExceptionType.LightSourceDamaged;
            }
            catch (LevenbergMarquardt.MaxIterationNumberException ex)
            {
                API.Exception = ex;
                return (int)ExceptionType.InvalidRecalibrationSpectrum;
            }
            catch (Exception ex)
            {
                API.Exception = ex;
                API.SetStatus(AsyncStabilizedFinder.StateType.Unknown);
                return (int)ExceptionType.Unknown;
            }
        }

        public static int AcquireTransflectanceSpectrum(int len, ref double[] wavelength, ref double[] reflectance)
        {
            try
            {
                return API.has_temperature_stabilization
                    ? Asynchronous.AcquireTransflectanceSpectrum(len, ref wavelength, ref reflectance)
                    : Synchronous.AcquireTransflectanceSpectrum(len, ref wavelength, ref reflectance);
            }
            catch (InterfaceException ex)
            {
                Console.WriteLine("Returning " + ex.Result.ToString());
                API.Exception = ex;
                if (ex.ExceptionType != ExceptionType.Busy &&
                    ex.ExceptionType != ExceptionType.MissingReference)
                {
                    API.SetStatus(AsyncStabilizedFinder.StateType.Unknown);
                }
                return ex.Result;
            }
            catch (TimeoutException ex)
            {
                API.Exception = ex;
                API.SetStatus(AsyncStabilizedFinder.StateType.Unknown);
                return (int)ExceptionType.Timeout;
            }
            catch (Hiperscan.SGS.Benchtop.Finder.LightSourceDamagedException ex)
            {
                API.Exception = ex;
                API.ResetButtonLED();
                return (int)ExceptionType.LightSourceDamaged;
            }
            catch (LevenbergMarquardt.MaxIterationNumberException ex)
            {
                API.Exception = ex;
                API.ResetButtonLED();
                return (int)ExceptionType.InvalidRecalibrationSpectrum;
            }
            catch (Hiperscan.SGS.Benchtop.Finder.InvalidTransflectReferenceException ex)
            {
                API.Exception = ex;
                API.ResetButtonLED();
                return (int)ExceptionType.InvalidTransflectReference;
            }
            catch (Exception ex)
            {
                API.Exception = ex;
                API.SetStatus(AsyncStabilizedFinder.StateType.Unknown);
                return (int)ExceptionType.Unknown;
            }
        }

        public static int UpdateRecalibration()
        {
            try
            {
                return API.has_temperature_stabilization 
                              ? Asynchronous.UpdateRecalibration() 
                              : Synchronous.UpdateRecalibration();
            }
            catch (InterfaceException ex)
            {
                API.Exception = ex;
                if (ex.ExceptionType != ExceptionType.Busy)
                    API.SetStatus(AsyncStabilizedFinder.StateType.Unknown);
                return ex.Result;
            }
            catch (TimeoutException ex)
            {
                API.Exception = ex;
                API.SetStatus(AsyncStabilizedFinder.StateType.Unknown);
                return (int)ExceptionType.Timeout;
            }
            catch (Hiperscan.SGS.Benchtop.Finder.LightSourceDamagedException ex)
            {
                API.Exception = ex;
                return (int)ExceptionType.LightSourceDamaged;
            }
            catch (LevenbergMarquardt.MaxIterationNumberException ex)
            {
                API.Exception = ex;
                return (int)ExceptionType.InvalidRecalibrationSpectrum;
            }
            catch (Exception ex)
            {
                API.Exception = ex;
                API.SetStatus(AsyncStabilizedFinder.StateType.Unknown);
                return (int)ExceptionType.Unknown;
            }
        }
        
        public static int AcquireDarkSpectrum( bool intern = false )
        {
            try
            {
                return API.has_temperature_stabilization
                    ? Asynchronous.AcquireDarkSpectrum( intern )
                    : Synchronous.AcquireDarkSpectrum( intern );
            }
            catch (Exception e)
            {
                Console.WriteLine( "AcquireDarkSpectrum: " + e.Message );
                return 2;
            }
        }
        
        public static int AcquireWhiteSpectrum( bool intern = false )
        {
            try
            {
                return API.has_temperature_stabilization
                    ? Asynchronous.AcquireWhiteSpectrum( intern ) 
                    :  Synchronous.AcquireWhiteSpectrum( intern );
            }
            
            catch (Exception e)
            {
                Console.WriteLine( "AcquireWhiteSpectrum: " + e.Message );
                return 2;
            }
        }


        public static int AcquireExternalReferences( int mode = 0)
        {
            try
            {
                return API.has_temperature_stabilization
                    ? Asynchronous.AcquireExternalReferences( mode ) 
                    :  Synchronous.AcquireExternalReferences( mode );
            }
            catch (InterfaceException ex)
            {
                API.Exception = ex;
                if (ex.ExceptionType != ExceptionType.Busy &&
                    ex.ExceptionType != ExceptionType.ReferencingTimeout)
                {
                    API.SetStatus(AsyncStabilizedFinder.StateType.Unknown);
                }
                return ex.Result;
            }
            catch (TimeoutException ex)
            {
                API.Exception = ex;
                API.SetStatus(AsyncStabilizedFinder.StateType.Unknown);
                return (int)ExceptionType.Timeout;
            }
            catch (Hiperscan.SGS.Benchtop.Finder.LightSourceDamagedException ex)
            {
                API.Exception = ex;
                API.ResetButtonLED();
                return (int)ExceptionType.LightSourceDamaged;
            }
            catch (LevenbergMarquardt.MaxIterationNumberException ex)
            {
                API.Exception = ex;
                API.ResetButtonLED();
                return (int)ExceptionType.InvalidRecalibrationSpectrum;
            }
            catch (Hiperscan.SGS.Benchtop.Finder.BlackReferenceException ex)
            {
                API.Exception = ex;
                API.ResetButtonLED();
                return (int)ExceptionType.InvalidDarkReference;
            }
            catch (Hiperscan.SGS.Benchtop.Finder.WhiteReferenceException ex)
            {
                API.Exception = ex;
                API.ResetButtonLED();
                return (int)ExceptionType.InvalidWhiteReference;
            }
            catch (Hiperscan.SGS.Benchtop.Finder.ReferencingTimeoutException ex)
            {
                API.Exception = ex;
                API.ResetButtonLED();
                return (int)ExceptionType.ReferencingTimeout;
            }
            catch (Exception ex)
            {
                API.Exception = ex;
                return (int)ExceptionType.Unknown;
            }
        }

        public static int GetWhiteSpectrum(int len, ref double[] wavelength, ref double[] intensity)
        {
            try
            {
                return API.has_temperature_stabilization
                    ? Asynchronous.GetWhiteSpectrum(len, ref wavelength, ref intensity)
                    : Synchronous.GetWhiteSpectrum(len, ref wavelength, ref intensity);
            }
            catch (InterfaceException ex)
            {
                API.Exception = ex;
                return ex.Result;
            }
            catch (Exception ex)
            {
                API.Exception = ex;
                return (int)ExceptionType.Unknown;
            }
        }

        public static int GetDarkSpectrum(int len, ref double[] wavelength, ref double[] intensity)
        {
            try
            {
                return API.has_temperature_stabilization
                    ? Asynchronous.GetDarkSpectrum(len, ref wavelength, ref intensity)
                    : Synchronous.GetDarkSpectrum(len, ref wavelength, ref intensity);
            }
            catch (InterfaceException ex)
            {
                API.Exception = ex;
                return ex.Result;
            }
            catch (Exception ex)
            {
                API.Exception = ex;
                return (int)ExceptionType.Unknown;
            }
        }

        public static int GetTransflectanceReferenceSpectrum(int len, ref double[] wavelength, ref double[] intensity)
        {
            try
            {
                return API.has_temperature_stabilization
                    ? Asynchronous.GetTransflectanceReferenceSpectrum(len, ref wavelength, ref intensity)
                    : Synchronous.GetTransflectanceReferenceSpectrum(len, ref wavelength, ref intensity);
            }
            catch (InterfaceException ex)
            {
                API.Exception = ex;
                return ex.Result;
            }
            catch (Exception ex)
            {
                API.Exception = ex;
                return (int)ExceptionType.Unknown;
            }
        }

        public static string GetLastError()
        {
            if (API.Exception == null)
                return string.Empty;

            return API.Exception.Message;
        }

        public static string GetLastStacktrace()
        {
            if (API.Exception == null)
                return string.Empty;

            return API.Exception.ToString();
        }

        public static void ResetLastException()
        {
            API.Exception = null;
        }

        public static void ExperimentalApiCall(string arguments = "")
        {
            StackFrame frame = new StackFrame(1, true);
            MethodBase method = frame.GetMethod();

            if (API.ENABLE_EXPERIMENTAL_API_CALLS == false)
            {
                throw new InterfaceException(ExceptionType.NotImplemented,
                                             string.Format(Catalog.GetString("Cannot execute EXPERIMENTAL marked " +
                                                                             "function {0}({1}). Set environment " +
                                                                             "variable HCL_EXPERIMENTAL=1 to force " +
                                                                             "execution."),
                                                           method.Name,
                                                           arguments));
            }

            Console.WriteLine("WARNING: Executing as EXPERIMENTAL marked function {0}({1})", method.Name, arguments);
        }

        public static int ResetLightSourceStats()
        {
            Console.WriteLine("ResetLightSourceStats()");

            try
            {
                return API.has_temperature_stabilization
                    ? Asynchronous.ResetLightSourceStats()
                    : throw new InterfaceException(ExceptionType.NotImplemented, Catalog.GetString("Not supported by hardware."));
            }
            catch (InterfaceException ex)
            {
                API.Exception = ex;
                return ex.Result;
            }
            catch (Exception ex)
            {
                API.Exception = ex;
                API.SetStatus(AsyncStabilizedFinder.StateType.Unknown);
                return (int)ExceptionType.Unknown;
            }
        }

        public static int Sleep()
        {
            Console.WriteLine("Sleep()");

            try
            {
                return API.has_temperature_stabilization
                    ? Asynchronous.Sleep()
                    : throw new InterfaceException(ExceptionType.NotImplemented, Catalog.GetString("Sleep mode is not supported by device."));
            }
            catch (InterfaceException ex)
            {
                API.Exception = ex;
                return ex.Result;
            }
            catch (TimeoutException ex)
            {
                API.Exception = ex;
                API.SetStatus(AsyncStabilizedFinder.StateType.Unknown);
                return (int)ExceptionType.Timeout;
            }
            catch (Exception ex)
            {
                API.Exception = ex;
                return (int)ExceptionType.Unknown;
            }
        }

        internal static void ResetCallbacks()
        {
            if (API.has_temperature_stabilization)
                Asynchronous.ResetCallbacks();
            else
                Synchronous.ResetCallbacks();
        }

        private static void ResetButtonLED()
        {
            if (Synchronous.Finder != null)
                Synchronous.Finder.Device.FinderButtonPressed();
        }

        public static bool RequireTransflectanceReference
        {
            get { return API._require_transflectance_reference; }
            set { API._require_transflectance_reference = value; }
        }

        private static Exception Exception
        {
            get { return API.exception; }
            set
            {
                if (value != null)
                    Console.WriteLine(value);
                API.exception = value;
            }
        }
    }
}