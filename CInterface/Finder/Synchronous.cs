// Synchronous.cs created with MonoDevelop
// User: klose at 17:47 25.04.2013
// CVS release: $Id$
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2012  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.Globalization;
using System.IO;
using System.Runtime.CompilerServices;
using Hiperscan.Common;
using Hiperscan.Common.CommandLineArgsParser;
using Hiperscan.SGS;
using Hiperscan.SGS.Benchtop;
using Hiperscan.Spectroscopy;
using Hiperscan.Unix;


namespace Hiperscan.CInterface.Finder
{

    public static class Synchronous
    {
        public static event API.QueryExternalSampleHandler QueryExternalWhiteReference;
        public static event API.QueryExternalSampleHandler QueryExternalDarkReference;
        public static event API.QueryExternalSampleHandler QueryExternalTransflectanceReference;
        public static event API.QueryExternalSampleHandler QueryExternalFluidSample;


        internal static Hiperscan.SGS.Benchtop.Finder Finder { get; set; }

        static int extended_parameters = 0;
        static string remote_server = "";


        internal static int Open(IDevice device)
        {
            Synchronous.Finder = new Hiperscan.SGS.Benchtop.Finder(device,
                                                                   ReferenceWheelVersion.Auto,
                                                                   ExternalWhiteReferenceType.Unknown,
                                                                   "Hiperscan Class Library")
            {
                AutoReferencing = false,
                InternalReferenceAveraging = 501
            };

            if (Environment.GetEnvironmentVariable(Env.HCL.DEBUG) == "1")
            {
                try
                {
                    Synchronous.Finder.AutoSaveReferences = true;

                    if (Directory.Exists(API.DEBUG_OUT_DIRECTORY) == false)
                        Directory.CreateDirectory(API.DEBUG_OUT_DIRECTORY);

                    Synchronous.Finder.AutoSaveDirectory = API.DEBUG_OUT_DIRECTORY;
                }
#pragma warning disable RECS0022
                catch { }
#pragma warning restore RECS0022
            }

            Synchronous.Finder.WaitForExternalEmptyReference += () => Synchronous.QueryExternalDarkReference?.Invoke();
            Synchronous.Finder.WaitForExternalWhiteReference += () => Synchronous.QueryExternalWhiteReference?.Invoke();

            API.StandardWavelengths.Clear();
            for (int ix = 1000; ix <= 1900; ++ix)
            {
                API.StandardWavelengths.Add((double)ix);
            }

            return 0;
        }

        internal static int Close()
        {
            API.ResetCallbacks();

            if (Synchronous.Finder?.Device == null)
            {
                Console.WriteLine(Catalog.GetString("Cannot close uninitialized device."));
                return 0;
            }

            Synchronous.Finder.Device.Close();

            return 0;
        }

        internal static int Init()
        {
            return 0;
        }

        private static int ExtendedParameters
        {
            get { return extended_parameters;  }
            set { extended_parameters = value; }
        }

        private static string RemoteServer
        {
            get { return remote_server; }
            set { remote_server = value; }
        }

        internal static int SetParam(string param, string val)
        {
            if (Synchronous.Finder?.Device == null)
                throw new InterfaceException(ExceptionType.NoSuchDevice, Catalog.GetString("Cannot set parameter for uninitialized device."));
            
            string msg = string.Empty;
            SetParameterType p;
            try
            {
                p = (SetParameterType)Enum.Parse(typeof(SetParameterType), param, true);
            }
            catch (ArgumentException)
            {
                msg = string.Format(Catalog.GetString("Invalid parameter name: {0}"), param);
                throw new InterfaceException(ExceptionType.InvalidArgument, msg);
            }
            msg = string.Empty;

            switch (p)
            {

            case SetParameterType.Average:
                int average = int.Parse(val);
                if (average < Synchronous.Finder.Device.MinAverage || average > Synchronous.Finder.Device.MaxAverage)
                {
                    msg = string.Format(Catalog.GetString("Parameter \"Average\" ist out of range. Must have a value between {0} and {1}."),
                                               Synchronous.Finder.Device.MinAverage, Synchronous.Finder.Device.MaxAverage);
                    throw new InterfaceException(ExceptionType.InvalidArgument, msg);
                }
                if (average != Synchronous.Finder.Device.CurrentConfig.Average)
                {
                    Synchronous.Finder.Device.CurrentConfig.Average = (ushort)average;
                    Synchronous.Finder.Device.WriteConfig();
                    Synchronous.Finder.Device.FinderButtonPressed();
                    Synchronous.Finder.ExternalReferenceAveraging = (ushort)(4 * average > Synchronous.Finder.Device.MaxAverage
                                                                             ? Synchronous.Finder.Device.MaxAverage 
                                                                             : 4 * average);
                }
                break;

            case SetParameterType.ReferenceTimeoutMilliseconds:
                Synchronous.Finder.ReferencingTimeoutMilliseconds = uint.Parse(val);
                break;

            case SetParameterType.RecalibrationTimeoutMilliseconds:
                Synchronous.Finder.RecalibrationTimeoutMilliseconds = uint.Parse(val);
                break;

            case SetParameterType.AutoReferencing:
                Synchronous.Finder.AutoReferencing = bool.Parse(val.ToLower());
                break;

            case SetParameterType.AutoRecalibrationMode:
                Synchronous.Finder.RecalibrationMode = (RecalibrationMode)Enum.Parse(typeof(RecalibrationMode), val);
                break;

            case SetParameterType.PlausibilityChecks:
                Synchronous.Finder.PlausibilityChecks = bool.Parse(val.ToLower());
                break;

            case SetParameterType.AutoSaveReferences:
                Synchronous.Finder.AutoSaveReferences = bool.Parse(val.ToLower());
                break;

            case SetParameterType.AutoSaveDirectory:
                if (Directory.Exists(val) == false)
                    throw new InterfaceException(ExceptionType.NoSuchFileOrDirectory, string.Format(Catalog.GetString("Cannot open directory path: {0}"), val));
                Synchronous.Finder.AutoSaveDirectory = val;
                break;

            case SetParameterType.AutoAcquisitionSequence:
                Synchronous.Finder.AutoAcquisitionSequence = bool.Parse(val.ToLower());
                break;

            case SetParameterType.LightSourceWaitMinutes:
            case SetParameterType.IdleTimeoutMinutes:
                AsyncStabilizedFinder.LightSourceWaitMinutes = double.Parse(val, API.Culture);
                break;

            case SetParameterType.RegressionCount:
            case SetParameterType.LightSourceTimeoutMinutes:
            case SetParameterType.DisableConsoleOutput:
                break;
            
            // ALH 
                case SetParameterType.ExtendedParameters:
                    ExtendedParameters = int.Parse(val);
                    break;
                
                case SetParameterType.ReferenceWheelPosition:  // 
                    if( ExtendedParameters > 0 )
                        if( FinderWheelPosition.TryParse(val, true, out FinderWheelPosition fwp) )
                            Finder.Device.SetFinderWheel( fwp, null );
                    break;
                    
                case SetParameterType.StatusLED:
                    if( ExtendedParameters > 0 )
                        Finder.Device.SwitchFinderStatusLED( Boolean.Parse(val) );
                    break;
                case SetParameterType.LightSource:
                    if( ExtendedParameters > 0 )
                        Finder.Device.SetFinderLightSource( Boolean.Parse(val), null);
                    break;
                case SetParameterType.RemoteServer:
                    RemoteServer = val;
                    break;



                default:
                msg = string.Format(Catalog.GetString("Setting of parameter {0} not implemented yet."), param);
                throw new InterfaceException(ExceptionType.NotImplemented, msg);

            }

            return 0;
        }

        internal static int GetParam(string param, out string val)
        {
            if (Synchronous.Finder?.Device == null)
                throw new InterfaceException(ExceptionType.NoSuchDevice, Catalog.GetString("Cannot get parameter from uninitialized device."));

            GetParameterType p;
            try
            {
                p = (GetParameterType)Enum.Parse(typeof(GetParameterType), param, true);
            }
            catch (ArgumentException)
            {
                string msg = string.Format(Catalog.GetString("Invalid parameter name: {0}"), param);
                throw new InterfaceException(ExceptionType.InvalidArgument, msg);
            }

            switch (p)
            {

            case GetParameterType.AverageMin:
                val = Synchronous.Finder.Device.HardwareProperties.MinAverage.ToString();
                break;

            case GetParameterType.AverageMax:
                val = Synchronous.Finder.Device.HardwareProperties.MaxAverage.ToString();
                break;

            case GetParameterType.Average:
                val = Synchronous.Finder.Device.CurrentConfig.Average.ToString(API.Culture);
                break;

            case GetParameterType.WavelengthMin:
                val = Synchronous.Finder.Device.HardwareProperties.MinWavelength.ToString();
                break;

            case GetParameterType.WavelengthMax:
                val = Synchronous.Finder.Device.HardwareProperties.MaxWavelength.ToString();
                break;

            case GetParameterType.WavelengthInc:
                val = Synchronous.Finder.Device.HardwareProperties.WavelengthIncrement.ToString();
                break;

            case GetParameterType.ReferenceTimeoutMilliseconds:
                val = Synchronous.Finder.ReferencingTimeoutMilliseconds.ToString();
                break;

            case GetParameterType.RecalibrationTimeoutMilliseconds:
                val = Synchronous.Finder.RecalibrationTimeoutMilliseconds.ToString();
                break;

            case GetParameterType.AutoReferencing:
                val = Synchronous.Finder.AutoReferencing.ToString();
                break;

            case GetParameterType.AutoRecalibrationMode:
                val = Synchronous.Finder.RecalibrationMode.ToString();
                break;

            case GetParameterType.PlausibilityChecks:
                val = Synchronous.Finder.PlausibilityChecks.ToString();
                break;

            case GetParameterType.AutoSaveReferences:
                val = Synchronous.Finder.AutoSaveReferences.ToString();
                break;

            case GetParameterType.AutoSaveDirectory:
                val = Synchronous.Finder.AutoSaveDirectory;
                break;

            case GetParameterType.RegressionCount:
            case GetParameterType.LightSourceWaitMinutes:
            case GetParameterType.IdleTimeoutMinutes:
            case GetParameterType.LightSourceTimeoutMinutes:
                val = "0";
                break;

            case GetParameterType.SerialNumber:
                val = Synchronous.Finder.Device.Info.SerialNumber;
                break;

            case GetParameterType.OemSerialNumber:
                val = API.NOT_AVAILABLE;
                break;

            case GetParameterType.Name:
                val = Synchronous.Finder.Device.CurrentConfig.Name;
                break;

            case GetParameterType.FirmwareVersion:
                val = Synchronous.Finder.Device.FirmwareVersion;
                break;

            case GetParameterType.Peripheral:
                val = ((int)Synchronous.Finder.Device.FinderVersion.Peripheral).ToString();
                break;

            case GetParameterType.BoardVersion:
                val = Synchronous.Finder.Device.FinderVersion.Board.ToString();
                break;

            case GetParameterType.CasingVersion:
                val = Synchronous.Finder.Device.FinderVersion.Casing.ToString();
                break;

            case GetParameterType.LightSourceVersion:
                val = Synchronous.Finder.Device.FinderVersion.LightSource.ToString();
                break;

            case GetParameterType.ReferenceWheelVersion:
                val = Synchronous.Finder.Device.FinderVersion.ReferenceWheel.ToString();
                break;

            case GetParameterType.ExternalDarkReferenceTimestamp:
                if (Synchronous.Finder.CurrentExternalBlackSpectrum == null)
                    val = API.NOT_AVAILABLE;
                else
                    val = Misc.GetEpochSeconds(Synchronous.Finder.CurrentExternalBlackSpectrum.Timestamp).ToString();
                break;

            case GetParameterType.ExternalWhiteReferenceTimestamp:
                if (Synchronous.Finder.CurrentExternalWhiteSpectrum == null)
                    val = API.NOT_AVAILABLE;
                else
                    val = Misc.GetEpochSeconds(Synchronous.Finder.CurrentExternalWhiteSpectrum.Timestamp).ToString();
                break;

            case GetParameterType.RecalibrationTimestamp:
                if (Synchronous.Finder.RecalibrationTimestamp == DateTime.MinValue)
                    val = API.NOT_AVAILABLE;
                else
                    val = Misc.GetEpochSeconds(Synchronous.Finder.RecalibrationTimestamp).ToString();
                break;

            case GetParameterType.AutoAcquisitionSequence:
                val = Synchronous.Finder.AutoAcquisitionSequence.ToString();
                break;

            case GetParameterType.LightSourcePowerCycleCount:
            case GetParameterType.LightSourceOperatingTimeMinutes:
            case GetParameterType.SystemOperatingTimeMinutes:
            case GetParameterType.SampleSpinnerAcquisitionCount:
                val = API.NOT_AVAILABLE;
                break;

            case GetParameterType.InstrumentType:
                val = ((int)API.InstrumentType).ToString();
                break;

            case GetParameterType.SpectralResolution:
                val = Synchronous.Finder.SpectralResolution.ToString("f1", CultureInfo.InvariantCulture);
                break;

            case GetParameterType.ReleaseTemperature:
                val = API.NOT_AVAILABLE;
                break;

            // ALH
            case GetParameterType.ExtendedParameters:
                val = Synchronous.ExtendedParameters.ToString();
                break;
            
            case GetParameterType.ButtonState:
                val = Synchronous.Finder.Device.FinderButtonPressed().ToString();
                break;
            
            case GetParameterType.ReferenceWheelPosition:
                val = Synchronous.Finder.Device.FinderWheelPosition.ToString();
                break;
            
            case GetParameterType.SpectrumWidth:
                val = (
                    (Synchronous.Finder.Device.HardwareProperties.MaxWavelength 
                     - Synchronous.Finder.Device.HardwareProperties.MinWavelength )
                     / Synchronous.Finder.Device.HardwareProperties.WavelengthIncrement + 1).ToString();
                break;

            case GetParameterType.RemoteServer:
                val = Synchronous.RemoteServer;
                break;
              

            default:
                string msg = string.Format(Catalog.GetString("Getting of parameter {0} not implemented (yet)."), param);
                throw new InterfaceException(ExceptionType.NotImplemented, msg);

            }

            Synchronous.Finder.Device.FinderButtonPressed();

            return 0;
        }

        internal static byte[] GetReferenceData()
        {
            ReferenceData ref_data = new ReferenceData(Synchronous.Finder.CurrentExternalWhiteSpectrum,
                                                       Synchronous.Finder.CurrentExternalBlackSpectrum,
                                                       Synchronous.Finder.CurrentInternalOxidRefSpectrum,
                                                       Synchronous.Finder.CurrentInternalWhiteRefSpectrum,
                                                       Synchronous.Finder.CurrentInternalDarkRefSpectrum);

            if (ref_data.IsComplete == false)
                throw new InterfaceException(ExceptionType.InvalidReferenceData, Catalog.GetString("Reference spectrum data is not complete."));

            return ref_data.Serialize();
        }

        internal static void SetReferenceData(byte[] data)
        {
            ReferenceData ref_data = ReferenceData.Deserialize(data);

            if (ref_data.IsComplete == false)
                throw new InterfaceException(ExceptionType.InvalidReferenceData, Catalog.GetString("Reference spectrum data is not complete."));

            Synchronous.Finder.__SetSpectra(null, null, null, null, ref_data.ExternalWhite, null, 
                                            ref_data.ExternalEmpty, ref_data.InternaleOxidRef, 
                                            ref_data.InternalWhiteRef, ref_data.InternalDarkRef);
        }

        internal static void BeginAcquisitionSequence()
        {
            if (Synchronous.Finder?.Device == null)
                throw new InterfaceException(ExceptionType.NoSuchDevice, Catalog.GetString("Cannot begin acquisition sequence with uninitialized device."));

            if (Synchronous.Finder.AutoAcquisitionSequence == true)
                throw new InterfaceException(ExceptionType.NotPermitted, "Cannot begin acquistion sequence with AutoAcquisitonSequence set to True.");

            if (Synchronous.Finder.ReferencingIsPending && Synchronous.Finder.AutoReferencing == false)
                throw new InterfaceException(ExceptionType.MissingReference, Catalog.GetString("Cannot begin acquisition sequence without external reference measurements."));

            if (Synchronous.Finder.AcquisitionSequenceRunning)
                throw new InterfaceException(ExceptionType.NotPermitted, "Cannot begin acquisition sequence while acquisition sequence is already running.");
            
            Synchronous.Finder.BeginAcquisitionSequence(null);
        }

        internal static void EndAcquisitionSequence()
        {
            if (Synchronous.Finder?.Device == null)
                throw new InterfaceException(ExceptionType.NoSuchDevice, Catalog.GetString("Cannot end acquisition sequence with uninitialized device."));

            if (Synchronous.Finder.AutoAcquisitionSequence == true)
                throw new InterfaceException(ExceptionType.NotPermitted, "Cannot end acquistion sequence with AutoAcquisitonSequence set to True.");

            if (Synchronous.Finder.AcquisitionSequenceRunning == false)
                throw new InterfaceException(ExceptionType.NotPermitted, "Cannot end acquisition sequence while no acquisition sequence is running.");

            Synchronous.Finder.EndAcquisitionSequence(null);
            Synchronous.Finder.Device.FinderButtonPressed();
        }

        internal static int AcquireReflectanceSpectrum(int len, ref double[] wavelength, ref double[] reflectance, ref double[] intensity)
        {
            if (Synchronous.Finder?.Device == null)
                throw new InterfaceException(ExceptionType.NoSuchDevice, Catalog.GetString("Cannot acquire reflectance or intensity spectrum with uninitialized device."));

            if (Synchronous.Finder.ReferencingIsPending && Synchronous.Finder.AutoReferencing == false)
                throw new InterfaceException(ExceptionType.MissingReference, Catalog.GetString("Cannot acquire reflectance or intensity spectrum without external reference measurements."));
          
            Synchronous.Finder.Acquire(false);
            
            Synchronous.Finder.SaveReferenceSpectrum(Synchronous.Finder.CurrentSampleSpectrum, SpectrumType.SolidSpecimen, false, string.Empty);

            if (Synchronous.Finder.AutoAcquisitionSequence)
                Synchronous.Finder.Device.FinderButtonPressed();

            Spectrum spectrum = Synchronous.Finder.Process(false);
            Synchronous.Finder.SaveReferenceSpectrum(spectrum, SpectrumType.ProcessedSolidSpecimen, false, string.Empty);

            DataSet reflectance_ds = 1.0 / DataSet.Pow(10.0, spectrum.Absorbance);

            if (reflectance_ds.Count != wavelength.Length)
                throw new InterfaceException(ExceptionType.InvalidArgument, Catalog.GetString("Invalid wavelength array length."));

            if (reflectance_ds.Count != reflectance.Length)
                throw new InterfaceException(ExceptionType.InvalidArgument, Catalog.GetString("Invalid reflectance array length."));

            if (spectrum.Intensity.Count != intensity.Length)
                throw new InterfaceException(ExceptionType.InvalidArgument, Catalog.GetString("Invalid intensity array length."));

            for (int ix=0; ix < reflectance_ds.Count; ++ix)
            {
                wavelength[ix]  = reflectance_ds.X[ix];
                reflectance[ix] = reflectance_ds.Y[ix];
                intensity[ix]   = spectrum.Intensity.Y[ix];
            }

            return 0;
        }

        internal static int UpdateRecalibration()
        {
            if (Synchronous.Finder?.Device == null)
                throw new InterfaceException(ExceptionType.NoSuchDevice, Catalog.GetString("Cannot acquire internal referencing with uninitialized device."));

            Synchronous.Finder.RecalibrateInternal(false);
            Synchronous.Finder.Device.FinderButtonPressed();

            return 0;
        }

        internal static int AcquireTransflectanceSpectrum(int len, ref double[] wavelength, ref double[] reflectance)
        {
            if (Synchronous.Finder?.Device == null)
                throw new InterfaceException(ExceptionType.NoSuchDevice, Catalog.GetString("Cannot acquire transflectance spectrum with uninitialized device."));

            if (Synchronous.QueryExternalTransflectanceReference == null || Synchronous.QueryExternalFluidSample == null)
                throw new InterfaceException(ExceptionType.NotImplemented, Catalog.GetString("Cannot acquire transflectance spectrum without properly set function callbacks."));

            if (Synchronous.Finder.ReferencingIsPending && Synchronous.Finder.AutoReferencing == false)
                throw new InterfaceException(ExceptionType.MissingReference, Catalog.GetString("Cannot acquire transflectance spectrum without external reference measurements."));

            Synchronous.Finder.Device.SwitchFinderStatusLED(false);

            if (API.RequireTransflectanceReference)
            {
                Synchronous.QueryExternalTransflectanceReference();
                Synchronous.Finder.AcquireExternalTransflectanceReference(true);
                Synchronous.QueryExternalFluidSample();
            }

            if (Synchronous.Finder.CurrentExternalBlackSpectrum == null)
                throw new InterfaceException(ExceptionType.MissingReference, Catalog.GetString("Cannot acquire transflectance spectrum without transflectance reference measurement."));

            Synchronous.Finder.Acquire(true);
            Synchronous.Finder.SaveReferenceSpectrum(Synchronous.Finder.CurrentSampleSpectrum, SpectrumType.FluidSpecimen, false, string.Empty);

            if (Synchronous.Finder.AutoAcquisitionSequence)
                Synchronous.Finder.Device.FinderButtonPressed();

            Spectrum spectrum = Synchronous.Finder.Process(true);
            Synchronous.Finder.SaveReferenceSpectrum(spectrum, SpectrumType.ProcessedFluidSpecimen, false, string.Empty);

            DataSet reflectance_ds = 1.0 / DataSet.Pow(10.0, spectrum.Absorbance);

            if (reflectance_ds.Count != wavelength.Length)
                throw new InterfaceException(ExceptionType.InvalidArgument, Catalog.GetString("Invalid wavelength array length."));

            if (reflectance_ds.Count != reflectance.Length)
                throw new InterfaceException(ExceptionType.InvalidArgument, Catalog.GetString("Invalid reflectance array length."));

            for (int ix=0; ix < reflectance_ds.Count; ++ix)
            {
                wavelength[ix]  = reflectance_ds.X[ix];
                reflectance[ix] = reflectance_ds.Y[ix];
            }

            return 0;
        }
        
        internal static int AcquireDarkSpectrum( bool intern = false)
        {
            try
            {
                return Synchronous.Finder.AcquireDarkSpectrum( intern );
            }
            catch (Exception e)
            {
                Console.WriteLine( "AcquireDarkSpectrum: " + e.Message );
                return 2;
            }
        }
        
        internal static int AcquireWhiteSpectrum( bool intern = false)
        {
            try
            {
                return Synchronous.Finder.AcquireWhiteSpectrum(intern);
            }
            catch (Exception e)
            {
                Console.WriteLine( "AcquireWhiteSpectrum: " + e.Message );
                return 2;
            }
        }
        
        internal static int AcquireExternalReferences(int mode = 0 )
        {
            switch (mode)
            {
                case 1 : // "Timed" :
                    return AcquireExternalReferences();
                    break;
                case 2 : // "PreAcquired" :
                    return AcquireExternalReferences_PreAcquired( 2 );
                    break;
                case 3 : // "PreAcquired" :
                    return AcquireExternalReferences_PreAcquired( 3 );
                    break;
                default :
                    return AcquireExternalReferences();
                    break;
            }
        }

        internal static int AcquireExternalReferences()
        {
            if (Synchronous.Finder?.Device == null)
                throw new InterfaceException(ExceptionType.NoSuchDevice, Catalog.GetString("Cannot acquire external reference spectra with uninitialized device."));

            Synchronous.Finder.ResetReferences();
            Synchronous.Finder.Recalibrate(false);
            Synchronous.Finder.Device.FinderButtonPressed();

            return 0;
        }

        private static int AcquireExternalReferences_PreAcquired(int mode)
        {
            if (Synchronous.Finder?.Device == null)
                throw new InterfaceException(ExceptionType.NoSuchDevice,
                    Catalog.GetString("Cannot acquire external reference spectra with uninitialized device."));

            // ALH CInterface.finder.ResetReferences();
            switch (mode) {
                case 2 : 
                    Synchronous.Finder.Recalibrate_PreAcquired( false );
                    break;
                default :
                    Synchronous.Finder.Recalibrate_PreAcquired( true );
                    break;
            }
            Synchronous.Finder.Device.FinderButtonPressed(); //.ResetButtonLED();
            return 0;
        }

        internal static int GetWhiteSpectrum(int len, ref double[] wavelength, ref double[] intensity)
        {
            if (Synchronous.Finder?.Device == null)
                throw new InterfaceException(ExceptionType.NoSuchDevice, Catalog.GetString("Cannot get white reference spectrum with uninitialized device."));

            if (Synchronous.Finder.CurrentExternalWhiteSpectrum == null)
                throw new InterfaceException(ExceptionType.NoData, Catalog.GetString("No white spectrum available."));

            DataSet intensity_ds = Synchronous.Finder.CurrentExternalWhiteSpectrum.Intensity.Interpolate(API.StandardWavelengths);

            if (intensity_ds.Count != wavelength.Length)
                throw new InterfaceException(ExceptionType.InvalidArgument, Catalog.GetString("Invalid wavelength array length."));

            if (intensity_ds.Count != intensity.Length)
                throw new InterfaceException(ExceptionType.InvalidArgument, Catalog.GetString("Invalid intensity array length."));

            for (int ix=0; ix < intensity_ds.Count; ++ix)
            {
                wavelength[ix]  = intensity_ds.X[ix];
                intensity[ix]   = intensity_ds.Y[ix];
            }

            Synchronous.Finder.Device.FinderButtonPressed();

            return 0;
        }

        internal static int GetDarkSpectrum(int len, ref double[] wavelength, ref double[] intensity)
        {
            if (Synchronous.Finder?.Device == null)
                throw new InterfaceException(ExceptionType.NoSuchDevice, Catalog.GetString("Cannot get dark reference spectrum with uninitialized device."));

            if (Synchronous.Finder.CurrentExternalBlackSpectrum == null)
                throw new InterfaceException(ExceptionType.NoData, Catalog.GetString("No dark spectrum available."));

            DataSet intensity_ds = Synchronous.Finder.CurrentExternalBlackSpectrum.Intensity.Interpolate(API.StandardWavelengths);

            if (intensity_ds.Count != wavelength.Length)
                throw new InterfaceException(ExceptionType.InvalidArgument, Catalog.GetString("Invalid wavelength array length."));

            if (intensity_ds.Count != intensity.Length)
                throw new InterfaceException(ExceptionType.InvalidArgument, Catalog.GetString("Invalid intensity array length."));

            for (int ix=0; ix < intensity_ds.Count; ++ix)
            {
                wavelength[ix]  = intensity_ds.X[ix];
                intensity[ix]   = intensity_ds.Y[ix];
            }

            Synchronous.Finder.Device.FinderButtonPressed();

            return 0;
        }

        internal static int GetTransflectanceReferenceSpectrum(int len, ref double[] wavelength, ref double[] intensity)
        {
            if (Synchronous.Finder?.Device == null)
                throw new InterfaceException(ExceptionType.NoSuchDevice, Catalog.GetString("Cannot get transflectance reference spectrum with uninitialized device."));

            if (Synchronous.Finder.CurrentTransflectanceReferenceSpectrum == null)
                throw new InterfaceException(ExceptionType.NoData, Catalog.GetString("No transflectance reference spectrum available."));

            DataSet intensity_ds =
                Synchronous.Finder.CurrentTransflectanceReferenceSpectrum.Intensity.Interpolate(API.StandardWavelengths);

            if (intensity_ds.Count != wavelength.Length)
                throw new InterfaceException(ExceptionType.InvalidArgument, Catalog.GetString("Invalid wavelength array length."));

            if (intensity_ds.Count != intensity.Length)
                throw new InterfaceException(ExceptionType.InvalidArgument, Catalog.GetString("Invalid intensity array length."));

            for (int ix=0; ix < intensity_ds.Count; ++ix)
            {
                wavelength[ix] = intensity_ds.X[ix];
                intensity[ix]  = intensity_ds.Y[ix];
            }

            Synchronous.Finder.Device.FinderButtonPressed();

            return 0;
        }

        internal static void ResetCallbacks()
        {
            Synchronous.QueryExternalDarkReference           = null;
            Synchronous.QueryExternalWhiteReference          = null;
            Synchronous.QueryExternalFluidSample             = null;
            Synchronous.QueryExternalTransflectanceReference = null;
        }
        
        
        /*
         * internal static int ReferenceExternal_PreAcquired()
        
		{
			Console.WriteLine("\n=== ReferenceExternal_PreAcquired start");
			bool success = false;
			if ( Finder.Device == null)
				throw new Exception(Catalog.GetString("No device available."));
			
			if (this.internal_dark_spectrum  == null || 
			    this.internal_white_spectrum == null ||
			    this.internal_oxid_spectrum  == null)
				throw new Exception(Catalog.GetString("Cannot recalibrate externally without current internal reference spectra."));
			
			if ( this.external_black_spectrum == null )
				throw new Exception("Dark reference not acquired");
					
			if ( this.external_white_spectrum == null )
				throw new Exception("White reference not acquired");
			
			if (true) //this.ReferencingIsPending == true)
			{
				if (this.ExternalCalibrationStarted != null)
					this.ExternalCalibrationStarted();
				
				try
				{
					this.current_ref_spectra.Clear();
					this.current_ref_spectra.Add(new Spectrum(this.internal_oxid_spectrum));
					this.current_ref_spectra.Add(new Spectrum(this.internal_oxid_spectrum.Reference));
					this.current_ref_spectra.Add(new Spectrum(this.internal_oxid_spectrum.Reference.RawIntensity.X,
					                                          this.internal_oxid_spectrum.GetCorrectedDarkIntensity(this.correction_mask).Intensity,
					                                          this.internal_oxid_spectrum.WavelengthLimits));

					TimeSpan span = DateTime.Now - this.recalibration_timestamp;
					if (span > this.referencing_max_duration)
					{
						this.ResetReferences();
						throw new ReferencingTimeoutException(Catalog.GetString("The reference procedure must be finished a short time period (max. 3 Minutes) after " +
                                                                                "acquisition of the first spectrum. This condition was not fulfilled. Please repeat " +
                                                                                "measurement."));
					}
					
					this.referencing_time = DateTime.Now;
					success = true;
				}
				catch (Exception ex)
				{
                    this.ExternalCalibrationFailed?.Invoke();

                    this.ResetReferences();
					success = false;
					throw ex;
				}
				finally
				{
					
					if (this.ExternalCalibrationFinished != null)
						ExternalCalibrationFinished(success);
					Console.WriteLine( "\n === ReferenceExternal_PreAcquired finish");
				}
			}
		}
		*/
    }
}