// CInterfaceA.cs created with MonoDevelop
// User: klose at 16:37 10.04.2014
// CVS release: $Id$
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2014  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.IO;

using Hiperscan.Common;
using Hiperscan.SGS;
using Hiperscan.SGS.Benchtop;
using Hiperscan.SGS.Benchtop.FinderSetup;
using Hiperscan.Spectroscopy;
using Hiperscan.Unix;


namespace Hiperscan.CInterface.Finder
{
    public static class Asynchronous
    {
        public static event API.QueryExternalSampleHandler QueryExternalWhiteReference;
        public static event API.QueryExternalSampleHandler QueryExternalDarkReference;
        public static event API.QueryExternalSampleHandler QueryExternalTransflectanceReference;
        public static event API.QueryExternalSampleHandler QueryExternalFluidSample;

        public static event API.TemperatureStatusChangedHandler TemperatureStatusChanged;

        private static AsyncStabilizedFinder.InfoType info = AsyncStabilizedFinder.InfoType.None;
        private static Spectrum processed_spectrum = null;

        private static Exception _exception = null;
        private static volatile bool _initialized = false;

        private static TextWriter __sav_console_out = Console.Out;

        internal static AsyncStabilizedFinder Finder { get; set; }

        static int extended_parameters = 0;


        internal static int Open(IDevice device)
        {
            Asynchronous.Finder = new AsyncStabilizedFinder(device, 
                                                            ReferenceWheelVersion.Auto,
                                                            ExternalWhiteReferenceType.Unknown,
                                                            "Hiperscan Class Library")
            {
                AutoSaveReferences = false,
                InternalReferenceAveraging = 499
            };

            Asynchronous.Finder.TemperatureChanged += Asynchronous.OnTemperatureChanged;

            Asynchronous.Finder.IdleTimeoutMinutes = (double)Asynchronous.Finder.Device.ReadFinderIdleTime() / 60.0;

            Asynchronous._initialized = false;

            try
            {
                if (Environment.GetEnvironmentVariable(Env.Finder.NO_SESSION_TIMEOUT) == "1")
                {
                    Console.WriteLine("Disabling USB session timeout.");
                    lock (Asynchronous.Finder)
                        Asynchronous.Finder.Device.WriteSessionTimeout(0);
                }
                else
                {
                    lock (Asynchronous.Finder)
                        Asynchronous.Finder.Device.WriteSessionTimeout(10);
                }
            }
            catch (NotImplementedException) { }
            catch (NotSupportedException)   { }

            if (Environment.GetEnvironmentVariable(Env.HCL.NO_TEMPERATURE_STABILIZATION) == "1")
            {
                Console.WriteLine("Disabling temperature stabilization.");
                Asynchronous.Finder.StabilizationIsActive = false;
            }

            if (Environment.GetEnvironmentVariable(Env.HCL.NO_LIGHT_SOURCE_STABILIZATION) == "1")
            {
                Console.WriteLine("Disabling light source stabilization.");
                Asynchronous.Finder.StabilizedLightSource = false;
            }

            Asynchronous.Finder.Start();

            Asynchronous.Finder.Exception += ex =>
            {
                Console.WriteLine(ex);
                Asynchronous.Exception = ex;
            };

            Asynchronous.Finder.NewInfo += info =>
            {
                Console.WriteLine($"New Info: {info.ToString()}");
                Asynchronous.info = info;
            };

            Asynchronous.Finder.NewProcessedSpectrum += spectrum => Asynchronous.processed_spectrum = spectrum;

            if (Environment.GetEnvironmentVariable(Env.HCL.DEBUG) == "1")
            {
                try
                {
                    Asynchronous.Finder.AutoSaveReferences = true;
                    Asynchronous.Finder.AutoSaveSpectra    = true;

                    if (Directory.Exists(API.DEBUG_OUT_DIRECTORY) == false)
                        Directory.CreateDirectory(API.DEBUG_OUT_DIRECTORY);

                    Asynchronous.Finder.OutputDirPath = API.DEBUG_OUT_DIRECTORY;
                }
#pragma warning disable RECS0022
                catch { }
#pragma warning restore RECS0022
            }

            API.StandardWavelengths.Clear();

            for (int ix = 1000; ix <= 1900; ++ix)
            {
                API.StandardWavelengths.Add((double)ix);
            }

            Asynchronous.Finder.WaitForIdleOrWarmup(60000, Asynchronous.OnIdle);

            return 0;
        }

        private static void OnTemperatureChanged(AsyncStabilizedFinder.TemperatureInfo temp_info)
        {
            if (Asynchronous.TemperatureStatusChanged == null)
                return;

            Asynchronous.TemperatureStatusChanged(temp_info.ToJson());
        }

        internal static int Init()
        {
            Asynchronous.Finder.WaitForIdleOrWarmup(60000, Asynchronous.OnIdle);
            Asynchronous.Finder.AcquisitionRequest = AsyncStabilizedFinder.AcquisitionType.IntDark;

            Asynchronous._initialized = true;

            return 0;
        }

        private static int ExtendedParameters
        {
            get { return extended_parameters; }
            set { extended_parameters = value; }
        }

        internal static int Close()
        {
            Asynchronous._initialized = false;

            Asynchronous.ResetCallbacks();

            if (Asynchronous.Finder?.Device == null)
            {
                Console.WriteLine(Catalog.GetString("Cannot close uninitialized device."));
                return 0;
            }

            Asynchronous.Finder.Stop();
            Asynchronous.Finder.Device.Close();

            return 0;
        }

        internal static int SetParam(string param, string val)
        {
            if (Asynchronous.Finder?.Device == null)
                throw new InterfaceException(ExceptionType.NoSuchDevice, Catalog.GetString("Cannot set parameter for uninitialized device."));

            string msg = string.Empty;

            SetParameterType p;
            try
            {
                p = (SetParameterType)Enum.Parse(typeof(SetParameterType), param, true);
            }
            catch (ArgumentException)
            {
                msg = string.Format(Catalog.GetString("Invalid parameter name: {0}"), param);
                throw new InterfaceException(ExceptionType.InvalidArgument, msg);
            }

            AsyncStabilizedFinder.StateType state = Asynchronous.Finder.State;
            if (state != AsyncStabilizedFinder.StateType.Idle && 
                state != AsyncStabilizedFinder.StateType.Busy &&
                state != AsyncStabilizedFinder.StateType.Init)
            {
                msg = string.Format(Catalog.GetString("Cannot set parameter: Device is busy. (State is {0}.)"), state.ToString());
                throw new InterfaceException(ExceptionType.Busy, msg);
            }

            switch (p)
            {

            case SetParameterType.Average:
                if (Asynchronous.Finder.AcquisitionSequenceRunning)
                {
                    throw new InterfaceException(ExceptionType.NotPermitted,
                                                 Catalog.GetString("Cannot set Average while acquisition sequence is running."));
                }

                int average = int.Parse(val);
                if (average < Asynchronous.Finder.Device.MinAverage || average > Asynchronous.Finder.Device.MaxAverage)
                {
                    msg = string.Format(Catalog.GetString("Parameter \"Average\" ist out of range. Must have a value between {0} and {1}."),
                                        Asynchronous.Finder.Device.MinAverage, Asynchronous.Finder.Device.MaxAverage);
                    throw new InterfaceException(ExceptionType.InvalidArgument, msg);
                }

                bool changed = false;
                lock (Asynchronous.Finder)
                {
                    changed = Asynchronous.Finder.AverageCount != (ushort)average;

                    if (changed == false)
                        break;

                    Asynchronous.Finder.AverageCount = (ushort)average;
                    if (Asynchronous.Finder.AutoReferencing)
                    {
                        Asynchronous.Finder.ExternalReferenceAveraging = (ushort)(4*average > Asynchronous.Finder.Device.MaxAverage 
                                                                                  ? Asynchronous.Finder.Device.MaxAverage 
                                                                                  : 4*average);
                        if (Asynchronous._initialized == false)
                            break;

                        if (Asynchronous.Finder.RecalibrationMode != RecalibrationMode.Never)
                            Asynchronous.Finder.AcquisitionRequest = AsyncStabilizedFinder.AcquisitionType.InternalReferences;
                    }
                }
                break;

            case SetParameterType.ReferenceTimeoutMilliseconds:
                Asynchronous.Finder.ReferencingTimeoutMilliseconds = uint.Parse(val);
                break;

            case SetParameterType.RecalibrationTimeoutMilliseconds:
                Asynchronous.Finder.RecalibrationTimeoutMilliseconds = uint.Parse(val);
                break;

            case SetParameterType.AutoReferencing:
                Asynchronous.Finder.AutoReferencing = bool.Parse(val.ToLower());
                break;

            case SetParameterType.AutoRecalibrationMode:
                Asynchronous.Finder.RecalibrationMode = (RecalibrationMode)Enum.Parse(typeof(RecalibrationMode), val);
                break;

            case SetParameterType.PlausibilityChecks:
                Asynchronous.Finder.PlausibilityChecks = bool.Parse(val.ToLower());
                break;

            case SetParameterType.AutoSaveReferences:
                Asynchronous.Finder.AutoSaveSpectra = bool.Parse(val.ToLower());
                break;

            case SetParameterType.AutoSaveDirectory:
                if (Directory.Exists(val) == false)
                {
                    throw new InterfaceException(ExceptionType.NoSuchFileOrDirectory, 
                                                 string.Format(Catalog.GetString("Cannot open directory path: {0}"), val));
                }
                Asynchronous.Finder.OutputDirPath = val;
                break;

            case SetParameterType.RegressionCount:
                Asynchronous.Finder.RegressionCount = int.Parse(val);
                break;

            case SetParameterType.DisableConsoleOutput:
                if (Environment.GetEnvironmentVariable(Env.HCL.DEBUG) != "1")
                    Console.SetOut(bool.Parse(val.ToLower()) ? TextWriter.Null : Asynchronous.__sav_console_out);
                break;

            case SetParameterType.IdleTimeoutMinutes:
            case SetParameterType.LightSourceTimeoutMinutes:
                double idle_timeout_minutes = double.Parse(val, API.Culture);
                lock (Asynchronous.Finder)
                    Asynchronous.Finder.IdleTimeoutMinutes = idle_timeout_minutes;
                break;

            case SetParameterType.AutoAcquisitionSequence:
                if (Asynchronous.Finder.AcquisitionSequenceRunning)
                {
                    throw new InterfaceException(ExceptionType.NotPermitted,
                                  Catalog.GetString("Cannot set AutoAcquisitionSequence while acquisition sequence is running."));
                }
                Asynchronous.Finder.AutoAcquisitionSequence = bool.Parse(val.ToLower());
                break;

            // ALH 
            case SetParameterType.ExtendedParameters:
                ExtendedParameters = int.Parse(val);
                break;

            case SetParameterType.ReferenceWheelPosition:  // 
                if (ExtendedParameters > 0)
                    if (Enum.TryParse(val, true, out FinderWheelPosition fwp))
                            Asynchronous.Finder.Device.SetFinderWheel(fwp, null);
                break;

            case SetParameterType.StatusLED:
                if (ExtendedParameters > 0)
                        Asynchronous.Finder.Device.SwitchFinderStatusLED(Boolean.Parse(val));
                break;
            case SetParameterType.LightSource:
                if (ExtendedParameters > 0)
                        Asynchronous.Finder.Device.SetFinderLightSource(Boolean.Parse(val), null);
                break;
                

            default:
                msg = string.Format(Catalog.GetString("Setting of parameter {0} not implemented (yet)."), param);
                throw new InterfaceException(ExceptionType.NotImplemented, msg);

            }
            return 0;
        }

        internal static int GetParam(string param, out string val)
        {
            if (Asynchronous.Finder?.Device == null)
            {
                throw new InterfaceException(ExceptionType.NoSuchDevice, 
                                             Catalog.GetString("Cannot get parameter from uninitialized device."));
            }

            GetParameterType p;
            try
            {
                p = (GetParameterType)Enum.Parse(typeof(GetParameterType), param, true);
            }
            catch (ArgumentException)
            {
                string msg = string.Format(Catalog.GetString("Invalid parameter name: {0}"), param);
                throw new InterfaceException(ExceptionType.InvalidArgument, msg);
            }

            AsyncStabilizedFinder.StateType state = Asynchronous.Finder.State;
            if (state != AsyncStabilizedFinder.StateType.Idle && 
                state != AsyncStabilizedFinder.StateType.Busy &&
                state != AsyncStabilizedFinder.StateType.Init)
            {
                string msg = string.Format(Catalog.GetString("Cannot get parameter: Device is busy. (State is {0}.)"),
                                           state.ToString());
                throw new InterfaceException(ExceptionType.Busy, msg);
            }

            switch (p)
            {

            case GetParameterType.AverageMin:
                val = Asynchronous.Finder.Device.HardwareProperties.MinAverage.ToString(API.Culture);
                break;

            case GetParameterType.AverageMax:
                val = Asynchronous.Finder.Device.HardwareProperties.MaxAverage.ToString(API.Culture);
                break;

            case GetParameterType.Average:
                val = (Asynchronous.Finder.AverageCount).ToString(API.Culture);
                break;

            case GetParameterType.WavelengthMin:
                val = Asynchronous.Finder.Device.HardwareProperties.MinWavelength.ToString(API.Culture);
                break;

            case GetParameterType.WavelengthMax:
                val = Asynchronous.Finder.Device.HardwareProperties.MaxWavelength.ToString(API.Culture);
                break;

            case GetParameterType.WavelengthInc:
                val = Asynchronous.Finder.Device.HardwareProperties.WavelengthIncrement.ToString(API.Culture);
                break;

            case GetParameterType.ReferenceTimeoutMilliseconds:
                val = Asynchronous.Finder.ReferencingTimeoutMilliseconds.ToString(API.Culture);
                break;

            case GetParameterType.RecalibrationTimeoutMilliseconds:
                val = Asynchronous.Finder.RecalibrationTimeoutMilliseconds.ToString(API.Culture);
                break;

            case GetParameterType.AutoReferencing:
                val = Asynchronous.Finder.AutoReferencing.ToString();
                break;

            case GetParameterType.AutoRecalibrationMode:
                val = Asynchronous.Finder.RecalibrationMode.ToString();
                break;

            case GetParameterType.PlausibilityChecks:
                val = Asynchronous.Finder.PlausibilityChecks.ToString();
                break;

            case GetParameterType.AutoSaveReferences:
                val = Asynchronous.Finder.AutoSaveReferences.ToString();
                break;

            case GetParameterType.AutoSaveDirectory:
                val = Asynchronous.Finder.AutoSaveDirectory;
                break;

            case GetParameterType.RegressionCount:
                val = Asynchronous.Finder.RegressionCount.ToString(API.Culture);
                break;

            case GetParameterType.LightSourceWaitMinutes:
                val = AsyncStabilizedFinder.LightSourceWaitMinutes.ToString(API.Culture);
                break;

            case GetParameterType.IdleTimeoutMinutes:
            case GetParameterType.LightSourceTimeoutMinutes:
                val = Asynchronous.Finder.IdleTimeoutMinutes.ToString(API.Culture);
                break;

            case GetParameterType.SerialNumber:
                val = Asynchronous.Finder.Device.Info.SerialNumber;
                break;

            case GetParameterType.OemSerialNumber:
                lock (Asynchronous.Finder)
                    val = Asynchronous.Finder.Device.ReadOemSerialNumber();
                break;

            case GetParameterType.Name:
                val = Asynchronous.Finder.Device.CurrentConfig.Name;
                break;

            case GetParameterType.FirmwareVersion:
                val = Asynchronous.Finder.Device.FirmwareVersion;
                break;

            case GetParameterType.Peripheral:
                val = ((byte)Asynchronous.Finder.Device.FinderVersion.Peripheral).ToString();
                break;

            case GetParameterType.BoardVersion:
                val = Asynchronous.Finder.Device.FinderVersion.Board.ToString();
                break;

            case GetParameterType.CasingVersion:
                val = Asynchronous.Finder.Device.FinderVersion.Casing.ToString();
                break;

            case GetParameterType.LightSourceVersion:
                val = Asynchronous.Finder.Device.FinderVersion.LightSource.ToString();
                break;

            case GetParameterType.ReferenceWheelVersion:
                val = Asynchronous.Finder.Device.FinderVersion.ReferenceWheel.ToString();
                break;

            case GetParameterType.ExternalDarkReferenceTimestamp:
                if (Asynchronous.Finder.CurrentExternalBlackSpectrum == null)
                    val = API.NOT_AVAILABLE;
                else
                    val = Misc.GetEpochSeconds(Asynchronous.Finder.CurrentExternalBlackSpectrum.Timestamp).ToString();
                break;

            case GetParameterType.ExternalWhiteReferenceTimestamp:
                if (Asynchronous.Finder.CurrentExternalWhiteSpectrum == null)
                    val = API.NOT_AVAILABLE;
                else
                    val = Misc.GetEpochSeconds(Asynchronous.Finder.CurrentExternalWhiteSpectrum.Timestamp).ToString();
                break;

            case GetParameterType.RecalibrationTimestamp:
                if (Asynchronous.Finder.RecalibrationTimestamp == DateTime.MinValue)
                    val = API.NOT_AVAILABLE;
                else
                    val = Misc.GetEpochSeconds(Asynchronous.Finder.RecalibrationTimestamp).ToString();
                break;

            case GetParameterType.LightSourcePowerCycleCount:
                lock (Asynchronous.Finder)
                    val = Asynchronous.Finder.Device.ReadSystemStats().LightSourcePowerCycles.ToString();
                break;

            case GetParameterType.ReleaseTemperature:
                double release_temp;
                lock (Asynchronous.Finder)
                    Asynchronous.Finder.Device.ReadTemperatureControlInfo(out byte v, out byte c, out release_temp);
                val = release_temp.ToString("f1", API.Culture);
                break;

            case GetParameterType.LightSourceOperatingTimeMinutes:
                lock (Asynchronous.Finder)
                    val = Asynchronous.Finder.Device.ReadSystemStats().LightSourceOperatingTime.TotalMinutes.ToString("f0");
                break;

            case GetParameterType.SystemOperatingTimeMinutes:
                lock (Asynchronous.Finder)
                    val = Asynchronous.Finder.Device.ReadSystemStats().SystemOperatingTime.TotalMinutes.ToString("f0");
                break;

            case GetParameterType.SampleSpinnerAcquisitionCount:
                try
                {
                    lock (Asynchronous.Finder)
                        val = Asynchronous.Finder.Device.ReadSampleSpinnerCounter().ToString();
                }
                catch (NotImplementedException)
                {
                    val = API.NOT_AVAILABLE;
                }
                break;

            case GetParameterType.AutoAcquisitionSequence:
                val = Asynchronous.Finder.AutoAcquisitionSequence.ToString();
                break;

            case GetParameterType.InstrumentType:
                val = ((int)API.InstrumentType).ToString();
                break;

            case GetParameterType.SpectralResolution:
                val = Asynchronous.Finder.SpectralResolution.ToString("f1", API.Culture);
                break;

            // ALH
            case GetParameterType.ExtendedParameters:
                val = Asynchronous.ExtendedParameters.ToString();
                break;

            case GetParameterType.ButtonState:
                val = Asynchronous.Finder.Device.FinderButtonPressed().ToString();
                break;

            case GetParameterType.ReferenceWheelPosition:
                val = Asynchronous.Finder.Device.FinderWheelPosition.ToString();
                break;

            case GetParameterType.SpectrumWidth:
                val = (
                    (Asynchronous.Finder.Device.HardwareProperties.MaxWavelength
                     - Asynchronous.Finder.Device.HardwareProperties.MinWavelength)
                     / Asynchronous.Finder.Device.HardwareProperties.WavelengthIncrement + 1).ToString();
                break;

            default:
                string msg = string.Format(Catalog.GetString("Getting of parameter {0} not implemented yet."), param);
                throw new InterfaceException(ExceptionType.NotImplemented, msg);

            }

            return 0;
        }

        internal static byte[] GetReferenceData()
        {
            ReferenceData ref_data = new ReferenceData(Asynchronous.Finder.CurrentExternalWhiteSpectrum,
                                                       Asynchronous.Finder.CurrentExternalBlackSpectrum,
                                                       Asynchronous.Finder.CurrentInternalOxidRefSpectrum,
                                                       Asynchronous.Finder.CurrentInternalWhiteRefSpectrum,
                                                       Asynchronous.Finder.CurrentInternalDarkRefSpectrum);

            if (ref_data.IsComplete == false)
            {
                throw new InterfaceException(ExceptionType.InvalidReferenceData,
                                             Catalog.GetString("Reference spectrum data is not complete."));
            }

            return ref_data.Serialize();
        }

        internal static void SetReferenceData(byte[] data)
        {
            ReferenceData ref_data = ReferenceData.Deserialize(data);

            if (ref_data.IsComplete == false)
            {
                throw new InterfaceException(ExceptionType.InvalidReferenceData,
                                             Catalog.GetString("Reference spectrum data is not complete."));
            }

            Asynchronous.Finder.SetReferenceData(ref_data.ExternalWhite,
                                                 ref_data.ExternalEmpty, 
                                                 ref_data.InternaleOxidRef,
                                                 ref_data.InternalWhiteRef,
                                                 ref_data.InternalDarkRef);
        }

        internal static int GetStatus(out int status, out double progress, out double eta_seconds, out double temperature, out double temperature_gradient)
        {
            if (Asynchronous.Finder?.Device == null)
            {
                throw new InterfaceException(ExceptionType.NoSuchDevice,
                                             Catalog.GetString("Cannot get status for uninitialized device."));
            }

            status = (int)Asynchronous.Finder.State;
            AsyncStabilizedFinder.TemperatureInfo temp_info = Asynchronous.Finder.Temperatures;

            progress = -1;
            eta_seconds = -1.0;
            temperature = temp_info.SgsTemperature;
            temperature_gradient = temp_info.SgsTemperatureGradient;

            if (Asynchronous.Finder.LightSourceState == AsyncStabilizedFinder.LightSourceStateType.Damaged)
            {
                throw new InterfaceException(ExceptionType.LightSourceDamaged,
                                             Catalog.GetString("Light source is not operating correctly."));
            }

            if (Asynchronous.Finder.State == AsyncStabilizedFinder.StateType.WaitForLightSource)
            {
                progress    = Asynchronous.Finder.Progress;
                eta_seconds = Asynchronous.Finder.LightSourceEtaWaitSeconds;
            }

            if (status == (int)AsyncStabilizedFinder.StateType.Unknown)
                throw new InterfaceException(ExceptionType.Unknown, Catalog.GetString("Device state is unknown.")); 

            return 0;
        }

        internal static void BeginAcquisitionSequence()
        {
            if (Asynchronous.Finder?.Device == null)
            {
                throw new InterfaceException(ExceptionType.NoSuchDevice,
                                             Catalog.GetString("Cannot begin acquisition sequence with uninitialized device."));
            }

            if (Asynchronous.Finder.AutoAcquisitionSequence == true)
            {
                throw new InterfaceException(ExceptionType.NotPermitted,
                                             "Cannot begin acquistion sequence with AutoAcquisitonSequence set to true.");
            }

            if (Asynchronous.Finder.ExternalReferencingIsPending)
            {
                throw new InterfaceException(ExceptionType.MissingReference,
                                             Catalog.GetString("Cannot begin acquisition sequence without external reference measurements."));
            }

            if (Asynchronous.Finder.AcquisitionSequenceRunning)
            {
                throw new InterfaceException(ExceptionType.NotPermitted,
                                             "Cannot begin acquisition sequence while acquisition sequence is already running.");
            }

            lock (Asynchronous.Finder)
            {
                AsyncStabilizedFinder.StateType state = Asynchronous.Finder.State;
                if (state != AsyncStabilizedFinder.StateType.Idle)
                {
                    string msg = string.Format(Catalog.GetString("Cannot begin acquisition sequence: Device is busy. (State is {0}.)"),
                                               state.ToString());
                    throw new InterfaceException(ExceptionType.Busy, msg);
                }

                Asynchronous.Finder.BeginAcquisitionSequence();
            }
        }

        internal static void EndAcquisitionSequence()
        {
            if (Asynchronous.Finder?.Device == null)
            {
                throw new InterfaceException(ExceptionType.NoSuchDevice,
                                             Catalog.GetString("Cannot end acquisition sequence with uninitialized device."));
            }

            if (Asynchronous.Finder.AutoAcquisitionSequence == true)
            {
                throw new InterfaceException(ExceptionType.NotPermitted,
                                             "Cannot end acquistion sequence with AutoAcquisitonSequence set to true.");
            }

            if (Asynchronous.Finder.AcquisitionSequenceRunning == false)
            {
                throw new InterfaceException(ExceptionType.NotPermitted,
                                             "Cannot end acquisition sequence while no acquisition sequence is running.");
            }

            lock (Asynchronous.Finder)
            {
                AsyncStabilizedFinder.StateType state = Asynchronous.Finder.State;
                if (state != AsyncStabilizedFinder.StateType.Idle)
                {
                    string msg = string.Format(Catalog.GetString("Cannot end acquisition sequence: Device is busy. (State is {0}.)"),
                                               state.ToString());
                    throw new InterfaceException(ExceptionType.Busy, msg);
                }

                Asynchronous.Finder.EndAcquisitionSequence();
            }
        }

        internal static int AcquireReflectanceSpectrum(int len, ref double[] wavelength, ref double[] reflectance, ref double[] intensity)
        {
            Console.WriteLine("Invoking CInterface.Finder.Asynchronous.AcquireReflectanceSpectrum()...");

            if (Asynchronous.Finder?.Device == null)
            {
                throw new InterfaceException(ExceptionType.NoSuchDevice,
                                             Catalog.GetString("Cannot acquire reflectance or intensity spectrum with uninitialized device."));
            }

            if (Asynchronous.Finder.ExternalReferencingIsPending)
            {
                throw new InterfaceException(ExceptionType.MissingReference,
                                             Catalog.GetString("Cannot acquire reflectance or intensity spectrum without external reference measurements."));
            }

            AsyncStabilizedFinder.StateType state = Asynchronous.Finder.State;
            if (state != AsyncStabilizedFinder.StateType.Idle)
            {
                string msg = string.Format(Catalog.GetString("Cannot acquire reflectance or intensity spectrum: Device is busy. (State is {0}.)"),
                                           state.ToString());
                throw new InterfaceException(ExceptionType.Busy, msg);
            }

            Asynchronous.CheckDeviceStatus();

            Asynchronous.processed_spectrum = null;
            Asynchronous.Exception = null;

            Asynchronous.Finder.TransflectanceMode = false;
            Asynchronous.Finder.AcquisitionRequest = AsyncStabilizedFinder.AcquisitionType.Sample;
            Asynchronous.Finder.WaitForIdle(60000, Asynchronous.OnIdle);

            if (Asynchronous.Exception != null)
            {
                Exception ex = Asynchronous.Exception;
                Asynchronous.Exception = null;
                throw ex;
            }

            if (Asynchronous.processed_spectrum == null)
                throw new InterfaceException(ExceptionType.NoData, Catalog.GetString("The instrument did not return data."));

            Spectrum spectrum = Asynchronous.processed_spectrum;
            Asynchronous.processed_spectrum = null;

            DataSet reflectance_ds = 1.0 / DataSet.Pow(10.0, spectrum.Absorbance);

            if (reflectance_ds.Count != wavelength.Length)
                throw new InterfaceException(ExceptionType.InvalidArgument, Catalog.GetString("Invalid wavelength array length."));

            if (reflectance_ds.Count != reflectance.Length)
                throw new InterfaceException(ExceptionType.InvalidArgument, Catalog.GetString("Invalid reflectance array length."));

            if (spectrum.Intensity.Count != intensity.Length)
                throw new InterfaceException(ExceptionType.InvalidArgument, Catalog.GetString("Invalid intensity array length."));

            for (int ix=0; ix < reflectance_ds.Count; ++ix)
            {
                wavelength[ix]  = reflectance_ds.X[ix];
                reflectance[ix] = reflectance_ds.Y[ix];
                intensity[ix]   = spectrum.Intensity.Y[ix];
            }

            return 0;
        }

        internal static int UpdateRecalibration()
        {
            if (Asynchronous.Finder?.Device == null)
            {
                throw new InterfaceException(ExceptionType.NoSuchDevice,
                                             Catalog.GetString("Cannot acquire internal references with uninitialized device."));
            }

            AsyncStabilizedFinder.StateType state = Asynchronous.Finder.State;
            if (state != AsyncStabilizedFinder.StateType.Idle)
            {
                string msg = string.Format(Catalog.GetString("Cannot acquire internal references: Device is busy. (State is {0}.)"),
                                           state.ToString());
                throw new InterfaceException(ExceptionType.Busy, msg);
            }

            Asynchronous.CheckDeviceStatus();
            Asynchronous.Finder.AcquisitionRequest = AsyncStabilizedFinder.AcquisitionType.InternalReferences;

            System.Threading.Thread.Sleep(200);

            return 0;
        }

        internal static int AcquireTransflectanceSpectrum(int len, ref double[] wavelength, ref double[] reflectance)
        {
            if (Asynchronous.Finder?.Device == null)
            {
                throw new InterfaceException(ExceptionType.NoSuchDevice,
                                             Catalog.GetString("Cannot acquire transflectance spectrum with uninitialized device."));
            }

            if (Asynchronous.QueryExternalTransflectanceReference == null || Asynchronous.QueryExternalFluidSample == null)
            {
                throw new InterfaceException(ExceptionType.NotImplemented,
                                             Catalog.GetString("Cannot acquire transflectance spectrum without properly set function callbacks."));
            }

            if (Asynchronous.Finder.ExternalReferencingIsPending)
            {
                throw new InterfaceException(ExceptionType.MissingReference,
                                             Catalog.GetString("Cannot acquire transflectance spectrum without external reference measurements."));
            }

            AsyncStabilizedFinder.StateType state = Asynchronous.Finder.State;
            if (state != AsyncStabilizedFinder.StateType.Idle)
            {
                string msg = string.Format(Catalog.GetString("Cannot acquire transflectance spectrum: Device is busy. (State is {0}.)"),
                                           state.ToString());
                throw new InterfaceException(ExceptionType.Busy, msg);
            }

            if (Asynchronous.Finder.Device.FinderVersion.Peripheral == FinderPeripheralType.SampleSpinner)
            {
                string msg = Catalog.GetString("Cannot acquire transflectance reference spectrum while using Sample Spinner.");
                throw new Hiperscan.SGS.Benchtop.Finder.InvalidTransflectReferenceException(msg);
            }

            Asynchronous.CheckDeviceStatus();

            Asynchronous.Finder.TransflectanceMode = true;
            Asynchronous.Exception = null;

            if (API.RequireTransflectanceReference)
            {
                Asynchronous.QueryExternalTransflectanceReference();
                Asynchronous.Finder.AcquisitionRequest = AsyncStabilizedFinder.AcquisitionType.ExtTransflectanceInsert;
                Asynchronous.Finder.WaitForIdle(60000, Asynchronous.OnIdle);

                if (Asynchronous.Exception != null)
                {
                    Exception ex = Asynchronous.Exception;
                    Asynchronous.Exception = null;
                    throw ex;
                }

                Asynchronous.QueryExternalFluidSample();

                if (Asynchronous.Finder.AutoAcquisitionSequence == false)
                    Asynchronous.Finder.BeginAcquisitionSequence();
            }

            if (Asynchronous.Finder.CurrentExternalBlackSpectrum == null)
            {
                throw new InterfaceException(ExceptionType.MissingReference,
                                             Catalog.GetString("Cannot acquire transflectance spectrum " +
                                                               "without transflectance reference measurement."));
            }

            Asynchronous.processed_spectrum = null;
            Asynchronous.Exception = null;

            Asynchronous.Finder.AcquisitionRequest = AsyncStabilizedFinder.AcquisitionType.Sample;
            Asynchronous.Finder.WaitForIdle(60000, Asynchronous.OnIdle);

            if (Asynchronous.Exception != null)
            {
                Exception ex = Asynchronous.Exception;
                Asynchronous.Exception = null;
                throw ex;
            }

            if (Asynchronous.processed_spectrum == null)
            {
                throw new InterfaceException(ExceptionType.NoData,
                                             Catalog.GetString("The device did not return data."));
            }

            Spectrum spectrum = Asynchronous.processed_spectrum;
            Asynchronous.processed_spectrum = null;

            DataSet reflectance_ds = 1.0 / DataSet.Pow(10.0, spectrum.Absorbance);

            if (reflectance_ds.Count != wavelength.Length)
            {
                throw new InterfaceException(ExceptionType.InvalidArgument,
                                             Catalog.GetString("Invalid wavelength array length."));
            }

            if (reflectance_ds.Count != reflectance.Length)
            {
                throw new InterfaceException(ExceptionType.InvalidArgument,
                                             Catalog.GetString("Invalid reflectance array length."));
            }

            for (int ix=0; ix < reflectance_ds.Count; ++ix)
            {
                wavelength[ix]  = reflectance_ds.X[ix];
                reflectance[ix] = reflectance_ds.Y[ix];
            }

            return 0;
        }
        
        internal static int AcquireDarkSpectrum( bool intern = false)
        {
            try
            {
                //return Asynchronous.Finder.AcquireDarkSpectrum(intern);
                if ( intern )
                    Asynchronous.Finder.AcquisitionRequest = AsyncStabilizedFinder.AcquisitionType.InternalEmpty;
                else
                    Asynchronous.Finder.AcquisitionRequest = AsyncStabilizedFinder.AcquisitionType.ExtEmpty;
                Asynchronous.Finder.WaitForIdle(60000, Asynchronous.OnIdle);
                return 0;
            }
            catch (Exception e)
            {
                Console.WriteLine( "AcquireDarkSpectrum: " + e.Message );
                return 2;
            }
            //return 1;
        }
        
        internal static int AcquireWhiteSpectrum( bool intern = false)
        {
            try
            {
                //return Finder.AcquireWhiteSpectrum(intern);
                if (intern)
                    Asynchronous.Finder.AcquisitionRequest = AsyncStabilizedFinder.AcquisitionType.InternalWhite;
                else
                    Asynchronous.Finder.AcquisitionRequest = AsyncStabilizedFinder.AcquisitionType.ExtWhite;
                Asynchronous.Finder.WaitForIdle(60000, Asynchronous.OnIdle);
                return 0;
            }
            catch (Exception e)
            {
                Console.WriteLine( "AcquireWhiteSpectrum: " + e.Message );
                return 2;
            }
            return 1;
        }

        private static int AcquireExternalReferences_PreAcquired(int mode)
        {
            if (Asynchronous.Finder?.Device == null)
                throw new InterfaceException(ExceptionType.NoSuchDevice,
                    Catalog.GetString("Cannot acquire external reference spectra with uninitialized device."));

            // ALH CInterface.finder.ResetReferences();
            switch (mode)
            {
                case 2:
                    //Asynchronous.Finder.Recalibrate_PreAcquired(false);
                    break;
                default:
                    //Asynchronous.Finder.Recalibrate_PreAcquired(true);
                    break;
            }
            Synchronous.Finder.Device.FinderButtonPressed(); //.ResetButtonLED();
            return 0;
        }

        //internal static int AcquireExternalReferences(int mode = 0 )
        //{
        //    switch (mode)
        //    {
        //        case 1 : //"Timed" :
        //            return AcquireExternalReferences();
        //            break;

        //        case 2: // "PreAcquired" :
        //            return AcquireExternalReferences_PreAcquired(2);
        //            break;
        //        case 3: // "PreAcquired" :
        //            return AcquireExternalReferences_PreAcquired(3);
        //            break;
              
        //        default :
        //            return AcquireExternalReferences();
        //            break;
        //    }
        //}
        
        internal static int AcquireExternalReferences( int mode = 0 )
        {
            if (Asynchronous.Finder?.Device == null)
            {
                throw new InterfaceException(ExceptionType.NoSuchDevice,
                                             Catalog.GetString("Cannot acquire external reference spectra with uninitialized device."));
            }

            var start_time = DateTime.Now;
            var timeout_error_msg = Catalog.GetString("The reference procedure must be finished a short time " +
                                                             "period (max. 3 Minutes) after acquisition of the first " +
                                                             "spectrum. This condition was not fulfilled. Please repeat " +
                                                             "measurement.");

            if (mode == 0)
            {

                if (Asynchronous.QueryExternalDarkReference == null ||
                    Asynchronous.QueryExternalWhiteReference == null)
                {
                    throw new InterfaceException(ExceptionType.NotImplemented,
                                                 Catalog.GetString("Cannot acquire external reference spectrum " +
                                                                   "without properly set function callbacks."));
                }

                AsyncStabilizedFinder.StateType state = Asynchronous.Finder.State;
                if (state != AsyncStabilizedFinder.StateType.Idle &&
                    state != AsyncStabilizedFinder.StateType.AcquireExternalSpectrum)
                {
                    string msg = string.Format(Catalog.GetString("Cannot acquire external reference spectra: Device is busy. (State is {0}.)"),
                                               state.ToString());
                    throw new InterfaceException(ExceptionType.Busy, msg);
                }

                Asynchronous.CheckDeviceStatus();

                Asynchronous.Finder.ResetReferences();
                Asynchronous.Exception = null;

                start_time = DateTime.Now;


                if (Asynchronous.Finder.Device.FinderVersion.Peripheral != FinderPeripheralType.SampleSpinner)
                    Asynchronous.QueryExternalDarkReference();

                if (DateTime.Now - start_time > Asynchronous.Finder.ReferencingMaxDuration)
                    throw new InterfaceException(ExceptionType.ReferencingTimeout, timeout_error_msg);

                Asynchronous.Finder.AcquisitionRequest = AsyncStabilizedFinder.AcquisitionType.ExtEmpty;
                Asynchronous.Finder.WaitForIdle(60000, Asynchronous.OnIdle);

                if (Asynchronous.Exception != null)
                {
                    var ex = Asynchronous.Exception;
                    Asynchronous.Exception = null;
                    throw ex;
                }

                if (DateTime.Now - start_time > Asynchronous.Finder.ReferencingMaxDuration)
                    throw new InterfaceException(ExceptionType.ReferencingTimeout, timeout_error_msg);

                if (Asynchronous.Finder.Device.FinderVersion.Peripheral != FinderPeripheralType.SampleSpinner)
                    Asynchronous.QueryExternalWhiteReference();

                if (DateTime.Now - start_time > Asynchronous.Finder.ReferencingMaxDuration)
                    throw new InterfaceException(ExceptionType.ReferencingTimeout, timeout_error_msg);

                Asynchronous.Finder.AcquisitionRequest = AsyncStabilizedFinder.AcquisitionType.ExtWhite;
                Asynchronous.Finder.WaitForIdle(60000, Asynchronous.OnIdle);

                if (Asynchronous.Exception != null)
                {
                    var ex = Asynchronous.Exception;
                    Asynchronous.Exception = null;
                    throw ex;
                }
            }  // mode == 0

            if (DateTime.Now - start_time > Asynchronous.Finder.ReferencingMaxDuration)
                throw new InterfaceException(ExceptionType.ReferencingTimeout, timeout_error_msg);

            Asynchronous.Finder.SetExternalReferencingTimestamp(DateTime.Now);

            return 0;
        }
        

        internal static int GetWhiteSpectrum(int len, ref double[] wavelength, ref double[] intensity)
        {
            if (Asynchronous.Finder?.Device == null)
            {
                throw new InterfaceException(ExceptionType.NoSuchDevice, 
                                             Catalog.GetString("Cannot get white reference spectrum with uninitialized device."));
            }

            AsyncStabilizedFinder.StateType state = Asynchronous.Finder.State;
            if (state != AsyncStabilizedFinder.StateType.Idle)
            {
                string msg = string.Format(Catalog.GetString("Cannot get white reference spectrum: Device is busy. (State is {0}.)"),
                                           state.ToString());
                throw new InterfaceException(ExceptionType.Busy, msg);
            }

            Asynchronous.CheckDeviceStatus();

            if (Asynchronous.Finder.CurrentExternalWhiteSpectrum == null)
                throw new InterfaceException(ExceptionType.NoData, Catalog.GetString("No white reference spectrum available."));

            DataSet intensity_ds = Asynchronous.Finder.CurrentExternalWhiteSpectrum.Intensity.Interpolate(API.StandardWavelengths);

            if (intensity_ds.Count != wavelength.Length)
                throw new InterfaceException(ExceptionType.InvalidArgument, Catalog.GetString("Invalid wavelength array length."));

            if (intensity_ds.Count != intensity.Length)
                throw new InterfaceException(ExceptionType.InvalidArgument, Catalog.GetString("Invalid intensity array length."));

            for (int ix=0; ix < intensity_ds.Count; ++ix)
            {
                wavelength[ix] = intensity_ds.X[ix];
                intensity[ix]  = intensity_ds.Y[ix];
            }

            return 0;
        }

        internal static int GetDarkSpectrum(int len, ref double[] wavelength, ref double[] intensity)
        {
            if (Asynchronous.Finder?.Device == null)
            {
                throw new InterfaceException(ExceptionType.NoSuchDevice,
                                             Catalog.GetString("Cannot get dark reference spectrum with uninitialized device."));
            }

            AsyncStabilizedFinder.StateType state = Asynchronous.Finder.State;
            if (state != AsyncStabilizedFinder.StateType.Idle)
            {
                string msg = string.Format(Catalog.GetString("Cannot get dark reference spectrum: Device is busy. (State is {0}.)"),
                                           state.ToString());
                throw new InterfaceException(ExceptionType.Busy, msg);
            }

            Asynchronous.CheckDeviceStatus();

            if (Asynchronous.Finder.CurrentExternalBlackSpectrum == null)
            {
                throw new InterfaceException(ExceptionType.NoData,
                                             Catalog.GetString("No dark reference spectrum available."));
            }

            DataSet intensity_ds = Asynchronous.Finder.CurrentExternalBlackSpectrum.Intensity.Interpolate(API.StandardWavelengths);

            if (intensity_ds.Count != wavelength.Length)
                throw new InterfaceException(ExceptionType.InvalidArgument, Catalog.GetString("Invalid wavelength array length."));

            if (intensity_ds.Count != intensity.Length)
                throw new InterfaceException(ExceptionType.InvalidArgument, Catalog.GetString("Invalid intensity array length."));

            for (int ix=0; ix < intensity_ds.Count; ++ix)
            {
                wavelength[ix] = intensity_ds.X[ix];
                intensity[ix]  = intensity_ds.Y[ix];
            }

            return 0;
        }

        internal static int GetTransflectanceReferenceSpectrum(int len, ref double[] wavelength, ref double[] intensity)
        {
            if (Asynchronous.Finder?.Device == null)
            {
                throw new InterfaceException(ExceptionType.NoSuchDevice,
                                             Catalog.GetString("Cannot get transflectance reference spectrum with uninitialized device."));
            }

            AsyncStabilizedFinder.StateType state = Asynchronous.Finder.State;
            if (state != AsyncStabilizedFinder.StateType.Idle)
            {
                string msg = string.Format(Catalog.GetString("Cannot get transflectance reference spectrum: Device is busy. (State is {0}.)"),
                                           state.ToString());
                throw new InterfaceException(ExceptionType.Busy, msg);
            }

            Asynchronous.CheckDeviceStatus();

            if (Asynchronous.Finder.CurrentTransflectanceReferenceSpectrum == null)
                throw new InterfaceException(ExceptionType.NoData, Catalog.GetString("No transflectance reference spectrum available."));

            DataSet intensity_ds =
                Asynchronous.Finder.CurrentTransflectanceReferenceSpectrum.Intensity.Interpolate(API.StandardWavelengths);

            if (intensity_ds.Count != wavelength.Length)
                throw new InterfaceException(ExceptionType.InvalidArgument, Catalog.GetString("Invalid wavelength array length."));

            if (intensity_ds.Count != intensity.Length)
                throw new InterfaceException(ExceptionType.InvalidArgument, Catalog.GetString("Invalid intensity array length."));

            for (int ix=0; ix < intensity_ds.Count; ++ix)
            {
                wavelength[ix] = intensity_ds.X[ix];
                intensity[ix]  = intensity_ds.Y[ix];
            }

            return 0;
        }

        private static void OnIdle()
        {
            if (Asynchronous.Exception != null)
            {
                Exception ex = Asynchronous.Exception;
                Asynchronous.Exception = null;

                if (ex is InterfaceException iex)
                {
                    throw new InterfaceException(iex.ExceptionType, iex.Message, iex);
                }

                throw new Exception(ex.Message, ex);
            }
        }

        internal static void ResetCallbacks()
        {
            Asynchronous.QueryExternalDarkReference           = null;
            Asynchronous.QueryExternalWhiteReference          = null;
            Asynchronous.QueryExternalFluidSample             = null;
            Asynchronous.QueryExternalTransflectanceReference = null;
            Asynchronous.TemperatureStatusChanged             = null;
        }

        private static Exception Exception
        {
            get { return Asynchronous._exception; }
            set
            {
                if (value != null)
                {
                    Console.WriteLine("********* Setting Exception:");
                    Console.WriteLine(value);
                }
                Asynchronous._exception = value;
            }
        }

        private static void CheckDeviceStatus()
        {
            if (Asynchronous.Finder.Device is Hiperscan.SGS.Sgs1900.BaseDevice == false)
            {
                throw new InterfaceException(ExceptionType.NotImplemented,
                                             "Only devices of type SGS1900 support firmware status.");
            }

            Hiperscan.SGS.Sgs1900.SGSFirmwareStatusType status = ((Hiperscan.SGS.Sgs1900.BaseDevice)Asynchronous.Finder.Device).SGSFirmwareStatus;

            if (status != Hiperscan.SGS.Sgs1900.SGSFirmwareStatusType.SGS_ERROR_NONE)
            {
                try
                {
                    Asynchronous.Finder.Device.SetFinderStatus(FinderStatusType.Error);
                }
#pragma warning disable RECS0022
                catch { }
#pragma warning restore RECS0022

                throw new Exception(string.Format(Catalog.GetString("SGS firmware status: {0}"), status.ToString()));
            }

            if (Asynchronous.Finder.LightSourceState == AsyncStabilizedFinder.LightSourceStateType.Damaged)
                throw new Hiperscan.SGS.Benchtop.Finder.LightSourceDamagedException();
        }

        internal static int ResetLightSourceStats()
        {
            if (Asynchronous.Finder?.Device == null)
            {
                throw new InterfaceException(ExceptionType.NoSuchDevice,
                                             Catalog.GetString("Cannot reset light source stats for uninitialized device."));
            }

            AsyncStabilizedFinder.StateType state = Asynchronous.Finder.State;
            if (state != AsyncStabilizedFinder.StateType.Idle &&
                state != AsyncStabilizedFinder.StateType.Busy &&
                state != AsyncStabilizedFinder.StateType.Init)
            {
                string msg = string.Format(Catalog.GetString("Cannot reset light source stats: Device is busy. (State is {0}.)"),
                                           state.ToString());
                throw new InterfaceException(ExceptionType.Busy, msg);
            }

            lock (Asynchronous.Finder)
                Asynchronous.Finder.Device.ResetLightSourceStats();

            return 0;
        }

        internal static int Sleep()
        {
            if (Asynchronous.Finder?.Device == null)
            {
                throw new InterfaceException(ExceptionType.NoSuchDevice,
                                             Catalog.GetString("Cannot set Sleep mode for uninitialized device."));
            }

            Asynchronous.Finder.Device.WaitForIdle(5000);
            lock (Asynchronous.Finder)
            {
                Asynchronous.Finder.Device.SetFinderStatus(FinderStatusType.Sleep);
                Asynchronous.Finder.IgnoreStateChanges = true;
            }

            return 0;
        }
    }
}