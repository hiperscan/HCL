﻿// CInterface.cs created with MonoDevelop
// User: klose at 17:47 25.04.2013
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2012  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

namespace Hiperscan.CInterface.Finder
{

    public enum InstrumentType : int
    {
        Unknown  = 0,
        ApoIdent = 1,
        PTIM100  = 2,
        FinderSD = 3
    }

    public enum ExceptionType : int
    {
        NotPermitted = 1,
        NoSuchFileOrDirectory = 2,
        InputOutput = 5,
        Access = 13,
        Busy = 16,
        NoSuchDevice = 19,
        InvalidArgument = 22,
        NotImplemented = 38,
        NoData = 61,
        Timeout = 62,
        Unknown = 1000,
        LightSourceDamaged = 1001,
        InvalidRecalibrationSpectrum = 1002,
        InvalidDarkReference = 1003,
        InvalidWhiteReference = 1004,
        InvalidTransflectReference = 1005,
        MissingReference = 1006,
        ReferencingTimeout = 1007,
        UnknownDeviceStatus = 1008,
        InvalidReferenceData = 1009,
        InvalidSpectrumType = 1010,
        InvalidSpectrumFormat = 1011,
        MissingChecksum = 1012,
        InvalidChecksum = 1013,
    }

    public enum SetParameterType
    {
        Average,
        ReferenceTimeoutMilliseconds,
        RecalibrationTimeoutMilliseconds,
        AutoReferencing,
        AutoRecalibrationMode,
        PlausibilityChecks,
        AutoSaveReferences,
        AutoSaveDirectory,
        RegressionCount,
        LightSourceWaitMinutes,
        DisableConsoleOutput,
        LightSourceTimeoutMinutes,
        IdleTimeoutMinutes,
        AutoAcquisitionSequence,
        // ALH
        ExtendedParameters,
        ReferenceWheelPosition,
        StatusLED,
        LightSource, 
        RemoteServer
        
    }

    public enum GetParameterType
    {
        Average,
        AverageMin,
        AverageMax,
        WavelengthMin,
        WavelengthMax,
        WavelengthInc,
        ReferenceTimeoutMilliseconds,
        RecalibrationTimeoutMilliseconds,
        AutoReferencing,
        AutoRecalibrationMode,
        PlausibilityChecks,
        AutoSaveReferences,
        AutoSaveDirectory,
        RegressionCount,
        LightSourceWaitMinutes,
        LightSourceTimeoutMinutes,
        IdleTimeoutMinutes,
        SerialNumber,
        OemSerialNumber,
        FirmwareVersion,
        BoardVersion,
        CasingVersion,
        LightSourceVersion,
        Peripheral,
        ReferenceWheelVersion,
        ExternalDarkReferenceTimestamp,
        ExternalWhiteReferenceTimestamp,
        RecalibrationTimestamp,
        LightSourcePowerCycleCount,
        LightSourceOperatingTimeMinutes,
        SystemOperatingTimeMinutes,
        SampleSpinnerAcquisitionCount,
        AutoAcquisitionSequence,
        InstrumentType,
        SpectralResolution,
        Name,
        ReleaseTemperature,
        // ALH 
        ExtendedParameters,
        ButtonState,
        ReferenceWheelPosition,
        SpectrumWidth,
        RemoteServer
    }
}