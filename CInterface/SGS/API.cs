// CInterfaceSGS.cs created with MonoDevelop
// User: klose at 16:11 10.10.2014
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2014  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.Globalization;
using System.IO;

using Hiperscan.Common;
using Hiperscan.SGS;
using Hiperscan.SGS.SerialInterface;
using Hiperscan.Spectroscopy;
using Hiperscan.Unix;


namespace Hiperscan.CInterface.SGS
{

    public static class API
    {

        public const string NOT_AVAILABLE = "n/a";

        private delegate void QueryHandler();

        private static IDevice Device = null;
        private static Exception last_exception = null;

        private static readonly string DEBUG_OUT_DIRECTORY =
            Path.Combine(Path.GetTempPath(), "HCL_Debug_" + DateTime.Now.ToString("yyyy-MM-dd"));


        public static int Open(uint product_id)
        {
            return API.Open(DeviceList.VENDOR_ID_FTDI, product_id);
        }

        public static int Open(uint vendor_id, uint product_id)
        {
            try
            {
                if (Environment.GetEnvironmentVariable(Env.HCL.DEBUG) == "1")
                {
                    try
                    {
                        if (Directory.Exists(DEBUG_OUT_DIRECTORY) == false)
                            Directory.CreateDirectory(DEBUG_OUT_DIRECTORY);
                        StreamWriter writer = new StreamWriter(Path.Combine(DEBUG_OUT_DIRECTORY, "logfile.txt"))
                        {
                            AutoFlush = true
                        };
                        Console.SetOut(writer);
                    }
#pragma warning disable RECS0022
                    catch { }
#pragma warning restore RECS0022
                }

                if (API.Device != null)
                    throw new InterfaceException(ExceptionType.Busy, Catalog.GetString("Device is already in use."));
                
                DeviceList device_list = new DeviceList(vendor_id, product_id, SerialInterfaceConfig.BackendType.FT245BL);
                device_list.Update();
                
                if (device_list.Count < 1)
                    throw new InterfaceException(ExceptionType.NoSuchDevice, Catalog.GetString("No device found."));
                
                API.Device = device_list.GetFirstDevice();
                
                API.Device.NewException += ex => throw new InterfaceException(ExceptionType.InputOutput, ex.Message);

                API.ResetCallbacks();

                API.Device.Open();

                if (API.Device.IsFinder)
                {
                    API.Device.Close();
                    throw new InterfaceException(ExceptionType.NoSuchDevice, Catalog.GetString("Unsupported device type."));
                }

                return 0;
            }
            catch (InterfaceException ex)
            {
                API.Device = null;
                API.last_exception = ex;
                return ex.Result;
            }
            catch (TimeoutException ex)
            {
                API.Device = null;
                API.last_exception = ex;
                return (int)ExceptionType.Timeout;
            }
            catch (Exception ex)
            {
                API.Device = null;
                API.last_exception = ex;
                return (int)ExceptionType.Unknown;
            }
        }

        public static int Close()
        {
            try
            {
                API.ResetCallbacks();

                if (API.Device == null)
                {
                    Console.WriteLine(Catalog.GetString("Warning: Cannot close uninitialized device."));
                    return 0;
                }

                API.Device.Close();

                return 0;
            }
            catch (InterfaceException ex)
            {
                API.last_exception = ex;
                return ex.Result;
            }
            catch (TimeoutException ex)
            {
                API.last_exception = ex;
                return (int)ExceptionType.Timeout;
            }
            catch (Exception ex)
            {
                API.last_exception = ex;
                return (int)ExceptionType.Unknown;
            }
            finally
            {
                API.Device = null;
            }
        }

        public static int SetParam(string param, string val)
        {
            try
            {
                if (API.Device == null)
                    throw new InterfaceException(ExceptionType.NoSuchDevice, Catalog.GetString("Cannot set parameter for uninitialized device."));

                string msg = string.Empty;
                SetParameter p;
                try
                {
                    p = (SetParameter)Enum.Parse(typeof(SetParameter), param, true);
                }
                catch (ArgumentException)
                {
                    msg = string.Format(Catalog.GetString("Invalid parameter name: {0}"), param);
                    throw new InterfaceException(ExceptionType.InvalidArgument, msg);
                }
                msg = string.Empty;

                switch (p)
                {

                case SetParameter.Average:
                    int average = int.Parse(val);
                    if (average < API.Device.MinAverage || average > API.Device.MaxAverage)
                    {
                        msg = string.Format(Catalog.GetString("Parameter \"Average\" ist out of range. Must have a value between {0} and {1}."),
                                            API.Device.MinAverage,
                                            API.Device.MaxAverage);
                        throw new InterfaceException(ExceptionType.InvalidArgument, msg);
                    }
                    API.Device.CurrentConfig.Average = (ushort)average;
                    API.Device.WriteConfig();
                    break;

                default:
                    msg = string.Format(Catalog.GetString("Setting of parameter {0} not implemented yet."), param);
                    throw new InterfaceException(ExceptionType.NotImplemented, msg);

                }

                return 0;
            }
            catch (InterfaceException ex)
            {
                API.last_exception = ex;
                return ex.Result;
            }
            catch (FormatException ex)
            {
                API.last_exception = ex;
                return (int)ExceptionType.InvalidArgument;
            }
            catch (Exception ex)
            {
                API.last_exception = ex;
                return (int)ExceptionType.Unknown;
            }
        }

        public static int GetParam(string param, out string val)
        {
            try
            {
                if (API.Device == null)
                {
                    throw new InterfaceException(ExceptionType.NoSuchDevice,
                                                 Catalog.GetString("Cannot get parameter from uninitialized device."));
                }

                GetParameter p;
                try
                {
                    p = (GetParameter)Enum.Parse(typeof(GetParameter), param, true);
                }
                catch (ArgumentException)
                {
                    string msg = string.Format(Catalog.GetString("Invalid parameter name: {0}"), param);
                    throw new InterfaceException(ExceptionType.InvalidArgument, msg);
                }

                switch (p)
                {

                case GetParameter.AverageMin:
                    val = API.Device.HardwareProperties.MinAverage.ToString();
                    break;

                case GetParameter.AverageMax:
                    val = API.Device.HardwareProperties.MaxAverage.ToString();
                    break;

                case GetParameter.Average:
                    val = API.Device.CurrentConfig.Average.ToString(CultureInfo.InvariantCulture);
                    break;

                case GetParameter.WavelengthMin:
                    val = API.Device.HardwareProperties.MinWavelength.ToString();
                    break;

                case GetParameter.WavelengthMax:
                    val = API.Device.HardwareProperties.MaxWavelength.ToString();
                    break;

                case GetParameter.WavelengthInc:
                    val = API.Device.HardwareProperties.WavelengthIncrement.ToString();
                    break;

                default:
                    string msg = string.Format(Catalog.GetString("Getting of parameter {0} not implemented yet."), param);
                    throw new InterfaceException(ExceptionType.NotImplemented, msg);

                }

                return 0;
            }
            catch (InterfaceException ex)
            {
                val = Catalog.GetString(NOT_AVAILABLE);
                API.last_exception = ex;
                return ex.Result;
            }
            catch (Exception ex)
            {
                val = Catalog.GetString(NOT_AVAILABLE);
                API.last_exception = ex;
                return (int)ExceptionType.Unknown;
            }
        }

        public static int AcquireIntensitySpectrum(int len, ref double[] wavelength, ref double[] intensity)
        {
            try
            {
                if (API.Device == null)
                {
                    throw new InterfaceException(ExceptionType.NoSuchDevice,
                                                 Catalog.GetString("Cannot acquire reflectance spectrum with uninitialized device."));
                }

                if (API.Device.CurrentConfig.DarkIntensity == null)
                {
                    throw new InterfaceException(ExceptionType.MissingReference,
                                                 Catalog.GetString("Cannot acquire intensity spectrum without dark intensity."));
                }

                Spectrum spectrum = API.Device.Single();
                spectrum.DarkIntensity = new DarkIntensity(API.Device.CurrentConfig.DarkIntensity);

                DataSet intensity_ds = spectrum.Intensity;

                if (intensity_ds.Count != wavelength.Length)
                {
                    throw new InterfaceException(ExceptionType.InvalidArgument, 
                                                 Catalog.GetString("Invalid wavelength array length."));
                }

                if (intensity_ds.Count != intensity.Length)
                {
                    throw new InterfaceException(ExceptionType.InvalidArgument, 
                                                 Catalog.GetString("Invalid intensity array length."));
                }

                for (int ix=0; ix < intensity_ds.Count; ++ix)
                {
                    wavelength[ix] = intensity_ds.X[ix];
                    intensity[ix]  = intensity_ds.Y[ix];
                }

                return 0;
            }
            catch (InterfaceException ex)
            {
                API.last_exception = ex;
                return ex.Result;
            }
            catch (TimeoutException ex)
            {
                API.last_exception = ex;
                return (int)ExceptionType.Timeout;
            }
            catch (Exception ex)
            {
                API.last_exception = ex;
                return (int)ExceptionType.Unknown;
            }
        }

        public static int AcquireReflectanceSpectrum(int len, ref double[] wavelength, ref double[] reflectance)
        {
            try
            {
                if (API.Device == null)
                {
                    throw new InterfaceException(ExceptionType.NoSuchDevice,
                                                 Catalog.GetString("Cannot acquire reflectance spectrum with uninitialized device."));
                }

                if (API.ReferencingIsPending)
                {
                    throw new InterfaceException(ExceptionType.MissingReference,
                                                 Catalog.GetString("Cannot acquire reflectance spectrum without reference measurements."));
                }

                Spectrum spectrum = API.Device.Single();
                API.Device.SetAbsorbance(spectrum);

                DataSet reflectance_ds = 1.0 / DataSet.Pow(10.0, spectrum.Absorbance);

                if (reflectance_ds.Count != wavelength.Length)
                {
                    throw new InterfaceException(ExceptionType.InvalidArgument,
                                                 Catalog.GetString("Invalid wavelength array length."));
                }

                if (reflectance_ds.Count != reflectance.Length)
                {
                    throw new InterfaceException(ExceptionType.InvalidArgument,
                                                 Catalog.GetString("Invalid reflectance array length."));
                }

                for (int ix=0; ix < reflectance_ds.Count; ++ix)
                {
                    wavelength[ix]  = reflectance_ds.X[ix];
                    reflectance[ix] = reflectance_ds.Y[ix];
                }

                return 0;
            }
            catch (InterfaceException ex)
            {
                API.last_exception = ex;
                return ex.Result;
            }
            catch (TimeoutException ex)
            {
                API.last_exception = ex;
                return (int)ExceptionType.Timeout;
            }
            catch (Exception ex)
            {
                API.last_exception = ex;
                return (int)ExceptionType.Unknown;
            }
        }

        public static int AcquireAbsorbanceSpectrum(int len, ref double[] wavelength, ref double[] absorbance)
        {
            try
            {
                if (API.Device == null)
                {
                    throw new InterfaceException(ExceptionType.NoSuchDevice,
                                                 Catalog.GetString("Cannot acquire absorbance spectrum with uninitialized device."));
                }

                if (API.ReferencingIsPending)
                {
                    throw new InterfaceException(ExceptionType.MissingReference,
                                                 Catalog.GetString("Cannot acquire absorbance spectrum without reference measurements."));
                }

                Spectrum spectrum = API.Device.Single();
                API.Device.SetAbsorbance(spectrum);

                DataSet absorbance_ds = spectrum.Absorbance;

                if (absorbance_ds.Count != wavelength.Length)
                {
                    throw new InterfaceException(ExceptionType.InvalidArgument,
                                                 Catalog.GetString("Invalid wavelength array length."));
                }

                if (absorbance_ds.Count != absorbance.Length)
                {
                    throw new InterfaceException(ExceptionType.InvalidArgument,
                                                 Catalog.GetString("Invalid absorbance array length."));
                }

                for (int ix=0; ix < absorbance_ds.Count; ++ix)
                {
                    wavelength[ix] = absorbance_ds.X[ix];
                    absorbance[ix] = absorbance_ds.Y[ix];
                }

                return 0;
            }
            catch (InterfaceException ex)
            {
                API.last_exception = ex;
                return ex.Result;
            }
            catch (TimeoutException ex)
            {
                API.last_exception = ex;
                return (int)ExceptionType.Timeout;
            }
            catch (Exception ex)
            {
                API.last_exception = ex;
                return (int)ExceptionType.Unknown;
            }
        }

        public static int AcquireDarkIntensity()
        {
            try
            {
                if (API.Device == null)
                {
                    throw new InterfaceException(ExceptionType.NoSuchDevice,
                                                 Catalog.GetString("Cannot acquire dark intensity with uninitialized device."));
                }

                Spectrum spectrum = API.Device.Single();
                API.Device.CurrentConfig.DarkIntensity = new DarkIntensity(spectrum);

                return 0;
            }
            catch (InterfaceException ex)
            {
                API.last_exception = ex;
                return ex.Result;
            }
            catch (TimeoutException ex)
            {
                API.last_exception = ex;
                return (int)ExceptionType.Timeout;
            }
            catch (Exception ex)
            {
                API.last_exception = ex;
                return (int)ExceptionType.Unknown;
            }
        }

        public static int AcquireReferenceSpectrum()
        {
            try
            {
                if (API.Device == null)
                {
                    throw new InterfaceException(ExceptionType.NoSuchDevice,
                                                 Catalog.GetString("Cannot acquire reference spectrum with uninitialized device."));
                }

                Spectrum spectrum = API.Device.Single();
                API.Device.CurrentConfig.ReferenceSpectrum = spectrum;

                return 0;
            }
            catch (InterfaceException ex)
            {
                API.last_exception = ex;
                return ex.Result;
            }
            catch (TimeoutException ex)
            {
                API.last_exception = ex;
                return (int)ExceptionType.Timeout;
            }
            catch (Exception ex)
            {
                API.last_exception = ex;
                return (int)ExceptionType.Unknown;
            }
        }

        public static int GetReferenceSpectrum(int len, ref double[] wavelength, ref double[] intensity)
        {
            try
            {
                if (API.Device == null)
                {
                    throw new InterfaceException(ExceptionType.NoSuchDevice,
                                                 Catalog.GetString("Cannot get reference spectrum with uninitialized device."));
                }

                if (API.Device.CurrentConfig.HasReferenceSpectrum == false)
                    throw new InterfaceException(ExceptionType.NoData, Catalog.GetString("No reference spectrum available."));

                DataSet intensity_ds = API.Device.CurrentConfig.ReferenceSpectrum.Intensity;

                if (intensity_ds.Count != wavelength.Length)
                    throw new InterfaceException(ExceptionType.InvalidArgument, Catalog.GetString("Invalid wavelength array length."));

                if (intensity_ds.Count != intensity.Length)
                    throw new InterfaceException(ExceptionType.InvalidArgument, Catalog.GetString("Invalid intensity array length."));

                for (int ix=0; ix < intensity_ds.Count; ++ix)
                {
                    wavelength[ix] = intensity_ds.X[ix];
                    intensity[ix]  = intensity_ds.Y[ix];
                }

                return 0;
            }
            catch (InterfaceException ex)
            {
                API.last_exception = ex;
                return ex.Result;
            }
            catch (Exception ex)
            {
                API.last_exception = ex;
                return (int)ExceptionType.Unknown;
            }
        }

        public static int GetDarkIntensity(ref double intensity, ref double sigma)
        {
            try
            {
                if (API.Device == null)
                {
                    throw new InterfaceException(ExceptionType.NoSuchDevice,
                                                 Catalog.GetString("Cannot get dark intensity with uninitialized device."));
                }

                if (API.Device.CurrentConfig.DarkIntensity == null)
                {
                    throw new InterfaceException(ExceptionType.NoData,
                                                 Catalog.GetString("No dark intensity available."));
                }

                intensity = API.Device.CurrentConfig.DarkIntensity.Intensity;
                sigma     = API.Device.CurrentConfig.DarkIntensity.Sigma;

                return 0;
            }
            catch (InterfaceException ex)
            {
                API.last_exception = ex;
                return ex.Result;
            }
            catch (Exception ex)
            {
                API.last_exception = ex;
                return (int)ExceptionType.Unknown;
            }
        }

        public static string GetLastError()
        {
            if (API.last_exception == null)
                return string.Empty;
            else
                return API.last_exception.Message;
        }
        
        public static string GetLastStacktrace()
        {
            return API.last_exception?.ToString() ?? string.Empty;
        }
        
        public static void ResetLastException()
        {
            API.last_exception = null;
        }

        private static void ResetCallbacks()
        {
        }

        private static bool ReferencingIsPending
        {
            get { return API.Device.CurrentConfig.DarkIntensity == null || API.Device.CurrentConfig.HasReferenceSpectrum == false; }
        }
    }
}