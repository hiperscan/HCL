// Created with MonoDevelop
//
//    Copyright (C) 2010 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Drawing;
using System.Drawing.Drawing2D;

using Hiperscan.Spectroscopy;


namespace Hiperscan.NPlotEngine.Overlay.Annotation
{

    public class Ellipse : Rectangle
    {
        public Ellipse(IPlotEngine plot, float width, Color color, LineStyle line_style)
            : base(plot, width, color, line_style)
        {
            this.Type = AnnotationType.Ellipse;
        }
        
        public Ellipse(IPlotEngine plot, Annotation a) : base(plot, a)
        {
        }

        public override void Draw (Graphics g)
        {
            if (this.Visible == false || this.PlotTypeId != this.plot.PlotTypeId)
                return;
            
            float x1 = (float)this.plot.GetPhysicalXCoord(this.X, false);
            float y1 = (float)this.plot.GetPhysicalYCoord(this.Y, false);
            float x2 = (float)this.plot.GetPhysicalXCoord(this.X + this.Width, false);
            float y2 = (float)this.plot.GetPhysicalYCoord(this.Y + this.Height, false);

            Pen pen = new Pen(this.Color, this.LineWidth)
            {
                DashStyle = (DashStyle)Enum.Parse(typeof(DashStyle), this.LineStyle.ToString())
            };

            Region clip = g.Clip;
            g.Clip = this.ClippingRegion;
            g.DrawEllipse(pen, Math.Min(x1, x2), Math.Min(y1, y2), Math.Abs(x2 - x1), Math.Abs(y2 - y1));
            g.Clip = clip;
        }
    }
}