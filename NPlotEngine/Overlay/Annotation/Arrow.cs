// Created with MonoDevelop
//
//    Copyright (C) 2010 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Drawing;
using System.Drawing.Drawing2D;

using Hiperscan.Spectroscopy;


namespace Hiperscan.NPlotEngine.Overlay.Annotation
{

    public class Arrow : Line
    {
        public Arrow(IPlotEngine plot, float width, Color color, LineStyle line_style, CapStyle cap_style) 
            : base(plot, width, color, line_style)
        {
            this.CapStyle = cap_style;
            this.Type = AnnotationType.Arrow;
        }
        
        public Arrow(IPlotEngine plot, Annotation a) : base(plot, a)
        {
        }

        public override void Draw(Graphics g)
        {
            if (this.Visible == false || this.PlotTypeId != this.plot.PlotTypeId)
                return;
            
            float x1 = (float)this.plot.GetPhysicalXCoord(this.X, false);
            float y1 = (float)this.plot.GetPhysicalYCoord(this.Y, false);
            float x2 = (float)this.plot.GetPhysicalXCoord(this.X + this.Width, false);
            float y2 = (float)this.plot.GetPhysicalYCoord(this.Y + this.Height, false);

            Pen pen = new Pen(this.Color, this.LineWidth)
            {
                DashStyle = (DashStyle)Enum.Parse(typeof(DashStyle), this.LineStyle.ToString())
            };

            Region clip = g.Clip;
            g.Clip = this.ClippingRegion;
            g.DrawLine(pen, x1, y1, x2, y2);
            Pen cap_pen = new Pen(this.Color, this.LineWidth)
            {
                CustomStartCap = LineCaps.GetLineCap(this.CapStyle, this.LineWidth)
            };
            float k = 0.5f / Math.Max(Math.Abs(x2-x1), Math.Abs(y2-y1));
            g.DrawLine(cap_pen, x1, y1, (x1 + (x2-x1)*k), (y1 + (y2-y1)*k));
            g.Clip = clip;
        }
    }
}