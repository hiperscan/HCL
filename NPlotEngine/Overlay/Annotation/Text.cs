// Created with MonoDevelop
//
//    Copyright (C) 2010 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.Drawing;


namespace Hiperscan.NPlotEngine.Overlay.Annotation
{

    public class Text : Annotation
    {
        
        public Text(IPlotEngine plot, string text, Font font, Color color) 
            : base(plot, AnnotationType.Text)
        {
            this.Text  = text;
            this.FontFamily = font.OriginalFontName;
            this.FontSize = font.Size;
            this.FontStyle = font.Style;
            this.ColorArgb = color.ToArgb();
            this.Interactive = true;
            this.Visible = true;
        }
        
        public Text(IPlotEngine plot, string text) : 
            this(plot, text, new Font(System.Drawing.FontFamily.GenericSansSerif, 12f, GraphicsUnit.Pixel), Color.Black)
        {
        }
        
        public Text(IPlotEngine plot, Annotation a) : base(plot, a)
        {
        }
        
        public override void Draw(Graphics g)
        {
            if (this.Visible == false || this.PlotTypeId != this.plot.PlotTypeId)
                return;
            
            float x = (float)this.plot.GetPhysicalXCoord(this.X, false);
            float y = (float)this.plot.GetPhysicalYCoord(this.Y, false);
            
            Brush brush = new SolidBrush(this.Color);
            Region clip = g.Clip;
            g.Clip = this.ClippingRegion;
            g.DrawString(this.Text, this.Font, brush, x, y);
            g.Clip = clip;
        }
        
        public override void Motion(int x, int y, Gdk.ModifierType modifier)
        {
            this.X = this.plot.GetWorldXCoord(x, false);
            this.Y = this.plot.GetWorldYCoord(y, false);
        }
        
        public override void ButtonPressed(int x, int y, Gdk.ModifierType modifier)
        {
            this.X = this.plot.GetWorldXCoord(x, false);
            this.Y = this.plot.GetWorldYCoord(y, false);
            this.Interactive = false;
        }
    }
}