// Created with MonoDevelop
//
//    Copyright (C) 2010 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Drawing;
using System.Drawing.Drawing2D;


namespace Hiperscan.NPlotEngine.Overlay
{

    public enum CapStyle : int
    {
        ArrowAnchor,
        DiamondAnchor,
        RoundAnchor,
        SquareAnchor
    }

    public static class LineCaps
    {
        public static CustomLineCap GetLineCap(CapStyle style, float line_width)
        {
            GraphicsPath path = new GraphicsPath();
            float k;

            switch (style)
            {

            case CapStyle.ArrowAnchor:
                k = (float)Math.Sqrt(line_width*0.3f);
                path.AddLine(0f, -5f*k, -2f*k, -5f*k);
                path.AddLine(-2f*k, -5f*k, 0f, 0f);
                path.AddLine(0f, 0f, 2f*k, -5f*k);
                path.AddLine(2f*k, -5f*k, 0f, -5f*k);
                path.AddLine(0f, -4.5f*k, -1.5f*k, -4.5f*k);
                path.AddLine(-1.5f*k, -4.5f*k, 0f, -0.5f);
                path.AddLine(0f, -0.5f, 1.5f*k, -4.5f*k);
                path.AddLine(1.5f*k, -4.5f*k, 0f, -4.5f*k);
                path.AddLine(0f, -4f*k, -1f*k, -4f*k);
                path.AddLine(-1f*k, -4f*k, 0f, -1f);
                path.AddLine(0f, -1f, 1f*k, -4f*k);
                path.AddLine(1f*k, -4f*k, 0f, -4f*k);
                path.AddLine(0f, -3.5f*k, -0.5f*k, -3.5f*k);
                path.AddLine(-0.5f*k, -3.5f*k, 0f, -1.5f);
                path.AddLine(0f, -1.5f, 0.5f*k, -3.5f*k);
                path.AddLine(0.5f*k, -3.5f*k, 0f, -3.5f*k);
                break;

            case CapStyle.DiamondAnchor:
                k = (float)Math.Sqrt(line_width*0.5f);
                path.AddLine(0f, -2f*k, -2f*k, 0f);
                path.AddLine(-2f*k, 0f, 0f, 2f);
                path.AddLine(0f, 2f, 2f*k, 0f);
                path.AddLine(2f*k, 0f, 0f, -2f*k);
                path.AddLine(0f, -1.5f*k, -1.5f*k, 0f);
                path.AddLine(-1.5f*k, 0f, 0f, 1.5f);
                path.AddLine(0f, 1.5f, 1.5f*k, 0f);
                path.AddLine(1.5f*k, 0f, 0f, -1.5f*k);
                path.AddLine(0f, -1f*k, -1f*k, 0f);
                path.AddLine(-1f*k, 0f, 0f, 1f);
                path.AddLine(0f, 1f, 1f*k, 0f);
                path.AddLine(1f*k, 0f, 0f, -1f*k);
                path.AddLine(0f, -0.5f*k, -0.5f*k, 0f);
                path.AddLine(-0.5f*k, 0f, 0f, 0.5f);
                path.AddLine(0f, 0.5f, 0.5f*k, 0f);
                path.AddLine(0.5f*k, 0f, 0f, -0.5f*k);
                break;

            case CapStyle.RoundAnchor:
                k = (float)Math.Sqrt (line_width * 0.2f);
                path.AddEllipse(-2f, -2f, 4f, 4f);
                path.AddEllipse(-1.5f, -1.5f, 3f, 3f);
                path.AddEllipse(-1f, -1f, 2f, 2f);
                path.AddEllipse(-0.5f, -0.5f, 1f, 1f);
                break;

            case CapStyle.SquareAnchor:
                k = (float)Math.Sqrt (line_width * 0.2f);
                path.AddRectangle(new RectangleF(-2f, -2f, 4f, 4f));
                path.AddRectangle(new RectangleF(-1.5f, -1.5f, 3f, 3f));
                path.AddRectangle(new RectangleF(-1f, -1f, 2f, 2f));
                path.AddRectangle(new RectangleF(-0.5f, -0.5f, 1f, 1f));
                break;

            }

            CustomLineCap line_cap = new CustomLineCap(null, path);
            line_cap.SetStrokeCaps(LineCap.Round, LineCap.Round);
            return line_cap;
        }
    }
}