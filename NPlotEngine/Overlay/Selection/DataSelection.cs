// Created with MonoDevelop
//
//    Copyright (C) 2010 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Drawing;

using Hiperscan.Spectroscopy;


namespace Hiperscan.NPlotEngine.Overlay.Selection
{

    public class DataSetSelection
    {
        readonly IPlotEngine plot;
        DataSet ds;
        
        public DataSetSelection(IPlotEngine plot, Spectrum spectrum)
        {
            this.plot = plot;
            this.ds = this.plot.DataSets[spectrum.Id];
            this.ds.Color  = spectrum.Color;
            this.ds.Label  = spectrum.Label;
            this.ds.ZOrder = spectrum.ZOrder;
        }
        
        public void Draw(Graphics g)
        {
            if (ds.IsVisible == false)
                return;
            
            int x_min = this.plot.PhysicalXMin;
            int x_max = this.plot.PhysicalXMax;
            int y_min = this.plot.PhysicalYMax;
            int y_max = this.plot.PhysicalYMin;
            
            Brush brush = new SolidBrush(this.ds.Color);

            float last_x = float.MinValue, last_y = float.MinValue;
            for (int ix = 0; ix < this.ds.Count; ++ix)
            {
                float x = (float)this.plot.GetPhysicalXCoord(this.ds.X[ix]);
                float y = (float)this.plot.GetPhysicalYCoord(this.ds.Y[ix]);
                
                if (Math.Pow(x - last_x, 2f) + Math.Pow(y - last_y, 2f) < 120f)
                    continue;
                
                if (x <= x_min || x >= x_max ||
                    y <= y_min || y >= y_max)
                    continue;

                g.FillRectangle(brush, x-3f, y-3f, 7f, 7f);
                last_x = x;
                last_y = y;
            }
        }
    }
}