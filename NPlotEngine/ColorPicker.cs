// Created with MonoDevelop
//
//    Copyright (C) 2010 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Drawing;


namespace Hiperscan.NPlotEngine
{
    
    public static class ColorPicker
    {
        private static Color[] colors = 
        {
            Color.Blue,       Color.Green,      Color.Red,
            Color.DarkOrange, Color.Black,      Color.DeepPink,
            Color.DarkRed,    Color.Chartreuse, Color.BurlyWood
        };
        
        private static ulong[] counts = new ulong[colors.Length];
        
        public static Color New()
        {
            ulong min_count = ulong.MaxValue;
            foreach (ulong count in counts)
            {
                if (count < min_count)
                    min_count = count;
            }
            
            Color color = colors[0];
            for (int ix=0; ix < colors.Length; ++ix)
            {
                Console.WriteLine(colors[ix]);
                if (counts[ix] == min_count)
                {
                    color = colors[ix];
                    ++counts[ix];
                    break;
                }
            }
            
            return color;
        }
        
        public static void Clear()
        {
            for (int ix=0; ix < counts.Length; ++ix)
            {
                counts[ix] = 0;
            }
        }
        
        public static void Existing(Color color)
        {
            for (int ix=0; ix < colors.Length; ++ix)
            {
                if (color != colors[ix])
                    continue;

                ++counts[ix];
                break;
            }
        }
    }
}