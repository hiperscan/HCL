// Created with MonoDevelop
//
//    Copyright (C) 2010 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;


namespace Hiperscan.NPlotEngine
{

    public partial class NPlotEngine
    {
        
        private class DrawableInfoList : Dictionary<Guid,DrawableInfo>
        {
            public DrawableInfoList() : base()
            {
            }
            
            public DrawableInfoList(DrawableInfoList list) : base(list)
            {
            }
            
            public List<Guid> GetSortedGuids()
            {
                List<KeyValuePair<string,int>> bg_z_orders = new List<KeyValuePair<string,int>>();
                List<KeyValuePair<string,int>> z_orders    = new List<KeyValuePair<string,int>>();
                Dictionary<string,Guid> bg_dict = new Dictionary<string,Guid>();
                Dictionary<string,Guid> dict    = new Dictionary<string,Guid>();

                foreach (DrawableInfo info in this.Values)
                {
                    if (info.IsBackground)
                    {
                        bg_z_orders.Add(new KeyValuePair<string,int>(info.Id, info.ZOrder));
                        bg_dict.Add(info.Id, info.Guid);
                    }
                    else
                    {
                        z_orders.Add(new KeyValuePair<string,int>(info.Id, info.ZOrder));
                        dict.Add(info.Id, info.Guid);
                    }
                }

                bg_z_orders.Sort((lhs, rhs) =>
                {
                    return lhs.Value.CompareTo(rhs.Value);
                });

                z_orders.Sort((lhs, rhs) =>
                {
                    return lhs.Value.CompareTo(rhs.Value);
                });
                
                List<string> bg_ids = new List<string>();
                List<string> ids    = new List<string>();
                List<Guid> guids    = new List<Guid>();
                
                foreach (KeyValuePair<string,int> kv in bg_z_orders)
                {
                    if (kv.Value != int.MaxValue)
                        guids.Add(bg_dict[kv.Key]);
                    else
                        bg_ids.Add(kv.Key);
                }
                
                bg_ids.Sort();
                foreach (string id in bg_ids)
                {
                    guids.Add(bg_dict[id]);
                }

                foreach (KeyValuePair<string,int> kv in z_orders)
                {
                    if (kv.Value != int.MaxValue)
                        guids.Add(dict[kv.Key]);
                    else
                        ids.Add(kv.Key);
                }
                
                ids.Sort();
                foreach (string id in ids)
                {
                    guids.Add(dict[id]);
                }
                
                return guids;
            }
        }
    }
}