// Created with MonoDevelop
//
//    Copyright (C) 2010 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;

using Hiperscan.Spectroscopy;

using NPlot;


namespace Hiperscan.NPlotEngine
{

    public partial class NPlotEngine
    {
        
        private class DrawableInfo
        {
            public DrawableInfo(IDrawable drawable, DataSet ds)
            {
                this.Drawable     = drawable;
                this.Guid         = ds.Guid;
                this.Id           = ds.Id;
                this.ZOrder       = ds.ZOrder;
                this.IsBackground = ds.IsBackground;
            }

            public IDrawable Drawable { get; }
            public Guid Guid          { get; }
            public string Id          { get; }
            public int ZOrder         { get; set; }
            public bool IsBackground  { get; }
        }
    }
}