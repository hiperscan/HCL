// Created with MonoDevelop
//
//    Copyright (C) 2010 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.Collections.Generic;

using Hiperscan.Spectroscopy;


namespace Hiperscan.Extensions
{
    public delegate void CurrentSpectraChangedHandler(ICurrentSpectra current_spectra, bool ds_changed);

    public interface ICurrentSpectra
    {
        event CurrentSpectraChangedHandler Changed;
        
        void AddHistoryEntry();
        void NotifyChanges();
        
        bool Contains(string id);
        bool ContainsLabel(string label);
        Spectrum GetByLabel(string label);
            
        int Count { get; }

        Dictionary<string,Spectrum>.KeyCollection Keys     { get; }
        Dictionary<string,Spectrum>.ValueCollection Values { get; }

        List<string> ZOrderKeys { get; }

        Spectrum GetById(string id);
    }
}
