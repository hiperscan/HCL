// Created with MonoDevelop
//
//    Copyright (C) 2010 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using Gtk;


namespace Hiperscan.Extensions
{

    public class ToolbarInfo : PluginInfo
    {
        private readonly ToolItem tool_item;

        public ToolbarInfo(ToolItem tool_item, string label, string tooltip)
        {
            this.tool_item = tool_item;
            this.label     = label;
            this.Tooltip   = tooltip;
        }
        
        public Widget ToolItem
        {
            get { return this.tool_item; }
        }

        public string Tooltip { get; }
    }
}