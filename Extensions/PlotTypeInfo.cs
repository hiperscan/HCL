// Created with MonoDevelop
//
//    Copyright (C) 2010 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using Hiperscan.Spectroscopy;


namespace Hiperscan.Extensions
{

    abstract public class PlotTypeInfo
    {
        private string name;
        private string xlabel = "x";
        private string ylabel = "y";
        
        private double lower_limit = -2f;
        private double upper_limit = 5f;
        private double standard_min = -0.1f;
        private double standard_max = 2.6f;
        
        private bool need_absorbance;
        private bool active;
        private bool interdependent;


        protected PlotTypeInfo(string name)
        {
            this.name = name;
            this.active = true;
            this.interdependent = false;
        }
        
        public virtual int GetPlotTypeId()
        {
            return this.GetType().ToString().GetHashCode();
        }
        
        public virtual DataSet ComputeDataSet(Spectrum spectrum, DataSet.XConstraints x_constraints)
        {
            return spectrum.Intensity;
        }
        
        public virtual bool QueryInformation()
        {
            return true;
        }
        
        public virtual void Reset()
        {
        }
        
        public virtual string Name
        {
            get { return this.name;  }
            set { this.name = value; }
        }
        
        public virtual string XLabel
        {
            get { return this.xlabel;  }
            set { this.xlabel = value; }
        }
        
        public virtual string YLabel
        {
            get { return this.ylabel;  }
            set { this.ylabel = value; }
        }
        
        public virtual double LowerLimit
        {
            get { return this.lower_limit;  }
            set { this.lower_limit = value; }
        }
        
        public virtual double UpperLimit
        {
            get { return this.upper_limit;  }
            set { this.upper_limit = value; }
        }
        
        public virtual double StandardMin
        {
            get { return this.standard_min;  }
            set { this.standard_min = value; }
        }
        
        public virtual double StandardMax
        {
            get { return this.standard_max;  }
            set { this.standard_max = value; }
        }
        
        public virtual bool NeedAbsorbance
        {
            get { return this.need_absorbance;  }
            set { this.need_absorbance = value; }
        }
        
        public virtual bool Active
        {
            get { return this.active;  }
            set { this.active = value; }
        }
        
        public virtual bool Interdependent
        {
            get { return this.interdependent;  }
            set { this.interdependent = value; }
        }
    }
}