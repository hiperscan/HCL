// Created with MonoDevelop
//
//    Copyright (C) 2010 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Globalization;

using Hiperscan.Spectroscopy;
using Hiperscan.Spectroscopy.IO;


namespace Hiperscan.Extensions
{

    public enum FileRwType
    {
        ReadOnly,
        WriteOnly,
        ReadWrite
    }
    
    abstract public class FileTypeInfo
    {
        protected bool is_container  = false;
        protected FileRwType rw_type = FileRwType.ReadWrite;


        protected FileTypeInfo(string name, IEnumerable<string> extensions)
        {
            this.Name = name;
            this.Extensions = extensions;
        }
        
        public virtual Spectrum Read(string fname)
        {
            throw new NotImplementedException();
        }
        
        public virtual void Write(Spectrum spectrum, string fname, CultureInfo culture)
        {
            throw new NotImplementedException();
        }    
        
        public virtual Spectrum[] ReadContainer(string fname, out List<MetaData> meta_data)
        {
            throw new NotImplementedException();
        }
        
        public virtual void WriteContainer(List<Spectrum> spectra, List<MetaData> meta_data, string fname, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public string Name { get; }

        public IEnumerable<string> Extensions { get; }

        public bool IsContainer
        {
            get { return this.is_container; }
        }
        
        public FileRwType RwType
        {
            get { return this.rw_type; }
        }
    }
}