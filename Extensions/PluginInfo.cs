// Created with MonoDevelop
//
//    Copyright (C) 2010 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;


namespace Hiperscan.Extensions
{

    public abstract class PluginInfo
    {
        protected string label;
        protected bool watch_cursor = false;
        protected bool need_idle_device = false;
        protected bool need_unprogrammed_or_idle_device = false;
        protected bool need_single_device = false;
        protected bool is_toggle = false;
        protected bool is_sensitive = true;

        public event EventHandler<EventArgs> Invoked;
        
        protected PluginInfo() {}

        public void Invoke(object sender, EventArgs e)
        {
            this.Invoked?.Invoke(sender, e);
        }

        public string Label
        {
            get { return this.label; }
        }
        
        public bool WatchCursor
        {
            get { return this.watch_cursor;  }
            set { this.watch_cursor = value; }
        }

        public bool NeedIdleDevice
        {
            get { return this.need_idle_device;  }
            set { this.need_idle_device = value; }
        }
        
        public bool NeedUnprogrammedOrIdleDevice
        {
            get { return this.need_unprogrammed_or_idle_device;  }
            set { this.need_unprogrammed_or_idle_device = value; }
        }

        public bool NeedSingleDevice
        {
            get { return this.need_single_device;  }
            set { this.need_single_device = value; }
        }
        
        public bool IsToggle
        {
            get { return this.is_toggle;  }
            set { this.is_toggle = value; }
        }
        
        public bool IsSensitive
        {
            get { return this.is_sensitive;  }
            set { this.is_sensitive = value; }
        }
    }
}