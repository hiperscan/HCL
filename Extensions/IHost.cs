// Created with MonoDevelop
//
//    Copyright (C) 2010 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Configuration;

using Gtk;

using Hiperscan.SGS;
using Hiperscan.Spectroscopy;


namespace Hiperscan.Extensions
{

    public interface IHost
    {
        event EventHandler<EventArgs> Exit;
        event EventHandler<EventArgs> UnhandledException;
        event EventHandler<EventArgs> InfoMessageShown;
        event EventHandler<EventArgs> SpectrometerSelectionChanged;

        void ShowInfoMessage(Window parent, string message, params object[] args);
        void ShowInfoMessage(Window parent, string add_button, EventHandler add_button_clicked, string message, params object[] args);
        void ShowInfoMessage(Window parent, ref bool dont_show_again, string message, params object[] args);

        void ShowInfoMessage(Window parent, bool use_markup, string message, params object[] args);
        void ShowInfoMessage(Window parent, string add_button, EventHandler add_button_clicked, bool use_markup, string message, params object[] args);
        void ShowInfoMessage(Window parent, ref bool dont_show_again, bool use_markup, string message, params object[] args);

        void ProcessEvents();
        void OpenFileBrowser(string path);
        void SaveAll(ICurrentSpectra current_spectra, string first_fname, bool save_all);
        void SetCreator(Spectrum spectrum);
        void ShowQuickStream(IDevice device);
        void Quit();
        void Quit(bool force);
        
        void InitConfigurationSection(string section_name, ConfigurationSection section);
        ConfigurationSection GetConfigurationSection(string section_name);
        void SaveConfiguration();
        
        void AddToCurrentPlot(Spectrum spectrum);
        void AddToCurrentPlot(List<Spectrum> spectra);
        string GetCurrentPlotType();
        bool TrySetCurrentPlotType(string plot_type_name);
        void ClearPlot();
        void UpdateCurrentPlot();
        void FitPlot();
        void UpdateProperties();
        void UpdatePluginInfos();
        void UpdateDeviceControls();
        
        bool CheckForUnsavedSpectra();
        
        ICurrentSpectra CurrentSpectra { get; }
        IActiveDevices  ActiveDevices  { get; }
        DeviceList      DeviceList     { get; }
        
        double SnvLowerBoundary { get; }
        double SnvUpperBoundary { get; }
        
        bool ExhibitionMode         { get; }
        bool TouchScreenMode        { get; }
        bool IsRelease              { get; }
        bool InfoMessageIsShown     { get; }
        Gdk.Color BackgroundColor   { get; }
        Gtk.Window MainWindow       { get; }
        string ConfigurationDirPath { get; }
        string VersionInfo          { get; }

        string[] Args { get; }

        bool ProfileChangedMode         { get; }
        string MainWindowTitelExtension { get; }
    }
}
