// Created with MonoDevelop
//
//    Copyright (C) 2010 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Reflection;


namespace Hiperscan.Extensions
{

    public interface IPlugin
    {
        void Initialize(IHost host);
        void ReplaceGui();
        
        MenuInfo[] MenuInfos         { get; }
        ToolbarInfo[] ToolbarInfos   { get; }
        PlotTypeInfo[] PlotTypeInfos { get; }
        FileTypeInfo[] FileTypeInfos { get; }

        string Name        { get; }
        string Version     { get; }
        string Vendor      { get; }
        Assembly Assembly  { get; }
        string Description { get; }
        Guid Guid          { get; }
        bool Active        { get; set; }
        bool CanReplaceGui { get; }
    }
}