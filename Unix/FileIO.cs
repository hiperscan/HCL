﻿// Created with MonoDevelop
//
//    Copyright (C) 2010 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;


namespace Hiperscan.Unix
{
    public static class FileIO
    {
        public static string CacheDir
        {
            get
            {
                string path = string.Empty;

                switch (Environment.OSVersion.Platform)
                {

                case PlatformID.Unix:
                    path = "/var/cache";
                    break;

                case PlatformID.Win32NT:
                case PlatformID.Win32S:
                case PlatformID.Win32Windows:
                case PlatformID.WinCE:
                case PlatformID.Xbox:
                    path = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
                    break;

                default:
                    throw new NotSupportedException(string.Format(Catalog.GetString("Platform {0} is not supported."),
                                                                  Environment.OSVersion.Platform));
                }

                return path;
            }
        }
    }
}
