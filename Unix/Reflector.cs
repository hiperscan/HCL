﻿// Created with MonoDevelop
//
//    Copyright (C) 2010 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Reflection;


namespace Hiperscan.Unix
{

    internal class Reflector
    {
        private Reflector()
        {
            Assembly assemby;

            try
            {
#pragma warning disable CS0618 // Type or member is obsolete
                assemby = Assembly.LoadWithPartialName("Mono.Posix");
#pragma warning restore CS0618 // Type or member is obsolete
            }
            catch
            {
                assemby = null;
            }

            if (assemby == null)
            {
                try
                {
                    assemby = Assembly.Load("Mono.Posix.NETStandard");
                }
                catch
                {
                    assemby = null;
                }               
            }

            if (assemby == null)
                return;

            try
            {
                this.InitCatalogMethods(assemby);
                this.InitSyscallMethods(assemby);
                this.InitStdlibMethods(assemby);
            }
#pragma warning disable RECS0022
            catch { }
#pragma warning restore RECS0022
        }

        private void InitCatalogMethods(Assembly assembly)
        {
            try
            {
                Type catalog_type = assembly.GetType("Mono.Unix.Catalog");

                this.CatalogInitMethod            = catalog_type.GetMethod("Init", BindingFlags.Public | BindingFlags.Static);
                this.CatalogGetStringMethod       = catalog_type.GetMethod("GetString", BindingFlags.Public | BindingFlags.Static);
                this.CatalogGetPluralStringMethod = catalog_type.GetMethod("GetPluralString", BindingFlags.Public | BindingFlags.Static);
            }
            catch
            {
                this.CatalogInitMethod            = null;
                this.CatalogGetStringMethod       = null;
                this.CatalogGetPluralStringMethod = null;
            }
        }

        private void InitSyscallMethods(Assembly assembly)
        {
            try
            {
                Type syscall_type = assembly.GetType("Mono.Unix.Native.Syscall");

                this.SyscallsymlinkMethod = syscall_type.GetMethod("symlink", BindingFlags.Public | BindingFlags.Static);
            }
            catch
            {
                this.SyscallsymlinkMethod = null;
            }
        }

        private void InitStdlibMethods(Assembly assembly)
        {
            try
            {
                Type stdlib_type = assembly.GetType("Mono.Unix.Native.Stdlib");

                this.StdlibGetLastErrorMethod = stdlib_type.GetMethod("GetLastError", BindingFlags.Public | BindingFlags.Static);
            }
            catch
            {
                this.StdlibGetLastErrorMethod = null;
            }
        }

        public MethodInfo CatalogInitMethod            { get; private set; }
        public MethodInfo CatalogGetStringMethod       { get; private set; }
        public MethodInfo CatalogGetPluralStringMethod { get; private set; }
        public MethodInfo SyscallsymlinkMethod         { get; private set; }
        public MethodInfo StdlibGetLastErrorMethod     { get; private set; }

        public static Reflector Instance { get; private set; } = new Reflector();
    }
}

