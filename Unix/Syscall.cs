﻿// Created with MonoDevelop
//
//    Copyright (C) 2019 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;


namespace Hiperscan.Unix.Native
{

    public static class Syscall
    {
        private volatile static bool is_unix = true;

        public static int symlink(string oldpath, string newpath)
        {
            try
            {
                if (Syscall.is_unix)
                    return Syscall._symlink(oldpath, newpath);
            }
#pragma warning disable RECS0022
            catch { }
#pragma warning restore RECS0022

            Syscall.is_unix = false;
            throw new NotSupportedException("Creating Symlinks is not supported.");
        }

        private static int _symlink(string oldpath, string newpath)
        {
            object[] parameters = new object[] { oldpath, newpath };
            return (int)Reflector.Instance.SyscallsymlinkMethod.Invoke(null, parameters);
        }
    }
}
