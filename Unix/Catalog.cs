// Created with MonoDevelop
//
//    Copyright (C) 2010 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;


namespace Hiperscan.Unix
{

    public static class Catalog
    {
        private volatile static bool i18n = true;

        public static void Init(string package, string localedir)
        {
            try
            {
                Catalog._Init(package, localedir);
            }
            catch
            {
                Catalog.i18n = false;
            }
        }
        
        public static string GetString(string s)
        {
            if (string.IsNullOrEmpty(s) == true)
                return s;

            if (i18n)
            {
                try
                {
                    return Catalog._GetString(s);
                }
                catch
                {
                    i18n = false;
                    return Catalog.GetString(s);
                }
            }
            else
            {
                return s;
            }
        }
        
        public static string GetPluralString(string s, string p, int n)
        {
            if (i18n)
            {
                try
                {
                    return Catalog._GetPluralString(s, p, n);
                }
                catch
                {
                    i18n = false;
                    return Catalog.GetPluralString(s, p, n);
                }
            }
            else
            {
                if (n == 1)
                    return s;
                else
                    return p;
            }
        }
        
        private static void _Init(string package, string localedir)
        {
            object[] parameters = new object[] { package, localedir };
            Reflector.Instance.CatalogInitMethod.Invoke(null, parameters);
        }

        private static string _GetString(string s)
        {
            object[] parameters = new object[] { s };
            return (string)Reflector.Instance.CatalogGetStringMethod.Invoke(null, parameters);
        }
        
        private static string _GetPluralString(string s, string p, int n)
        {
            object[] parameters = new object[] { s, p, n };
            return (string)Reflector.Instance.CatalogGetPluralStringMethod.Invoke(null, parameters);
        }
    }
}
