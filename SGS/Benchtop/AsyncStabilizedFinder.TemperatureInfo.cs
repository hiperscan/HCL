﻿// Created with MonoDevelop
//
//    Copyright (C) 2014 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;

using Hiperscan.Common;


namespace Hiperscan.SGS.Benchtop
{

    public partial class AsyncStabilizedFinder
    {

        [DataContract]
        public class TemperatureInfo
        {
            [DataMember] public long Timestamp                        { get; private set; }
            [DataMember] public long DataCount                        { get; private set; }
            [DataMember] public double SgsTemperature                 { get; private set; }
            [DataMember] public double FinderTemperature              { get; private set; }
            [DataMember] public double SgsTemperatureGradient         { get; private set; }
            [DataMember] public double FinderTemperatureGradient      { get; private set; }
            [DataMember] public double SgsTemperatureGradientGradient { get; private set; }

            public TemperatureInfo(int data_count, double sgs_temperature, double finder_temperature, 
                                   double sgs_temperature_gradient, double finder_temperature_gradient, double sgs_temperature_gradient_gradient)
            {
                this.Timestamp = Misc.GetEpochSeconds(DateTime.Now);
                this.DataCount = data_count;
                this.SgsTemperature = sgs_temperature;
                this.FinderTemperature = finder_temperature;
                this.SgsTemperatureGradient = sgs_temperature_gradient;
                this.FinderTemperatureGradient = finder_temperature_gradient;
                this.SgsTemperatureGradientGradient = sgs_temperature_gradient_gradient;
            }

            public TemperatureInfo(TemperatureInfo temperature_info)
            {
                this.Timestamp = temperature_info.Timestamp;
                this.DataCount = temperature_info.DataCount;
                this.SgsTemperature = temperature_info.SgsTemperature;
                this.FinderTemperature = temperature_info.FinderTemperature;
                this.SgsTemperatureGradient = temperature_info.SgsTemperatureGradient;
                this.FinderTemperatureGradient = temperature_info.FinderTemperatureGradient;
                this.SgsTemperatureGradientGradient = temperature_info.SgsTemperatureGradientGradient;
            }

            public override string ToString()
            {
                return string.Format("[TemperatureInfo: Timestamp={0}, DataCount={1}, SgsTemperature={2:f2}, FinderTemperature={3:f2}, SgsTemperatureGradient={4:f2}, FinderTemperatureGradient={5:f2}, SgsTemperatureGradientGradient={6:f2}]",
                                     this.Timestamp,
                                     this.DataCount,
                                     this.SgsTemperature, 
                                     this.FinderTemperature,
                                     this.SgsTemperatureGradient         > 1000.0 ? double.NaN : this.SgsTemperatureGradient,
                                     this.FinderTemperatureGradient      > 1000.0 ? double.NaN : this.FinderTemperatureGradient,
                                     this.SgsTemperatureGradientGradient > 1000.0 ? double.NaN : this.SgsTemperatureGradientGradient);
            }

            public string ToJson()
            {
                TemperatureInfo data = new TemperatureInfo(this)
                {
                    SgsTemperatureGradient         = this.SgsTemperatureGradient         > 1000.0 ? double.NaN : this.SgsTemperatureGradient,
                    FinderTemperatureGradient      = this.FinderTemperatureGradient      > 1000.0 ? double.NaN : this.FinderTemperatureGradient,
                    SgsTemperatureGradientGradient = this.SgsTemperatureGradientGradient > 1000.0 ? double.NaN : this.SgsTemperatureGradientGradient
                };

                DataContractJsonSerializer serializer = new DataContractJsonSerializer(data.GetType());
                using (var stream = new MemoryStream())
                {
                    serializer.WriteObject(stream, data);
                    string json_data = Encoding.UTF8.GetString(stream.ToArray(), 0, (int)stream.Length);
                    return json_data;
                }
            }
        }
    }
}