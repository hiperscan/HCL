﻿// Created with MonoDevelop
//
//    Copyright (C) 2019 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;

using Version = Hiperscan.SGS.Common.Version;


namespace Hiperscan.SGS.Benchtop.FinderSetup
{

    public enum FinderPeripheralType : byte
    {
        None = 0,
        SampleSpinner = 1
    }

    public class FinderVersion
    {
        public byte Board          { private set; get; }
        public byte ReferenceWheel { private set; get; }
        public byte LightSource    { private set; get; }
        public byte Casing         { private set; get; }

        public Version ProtocolVersion { private set; get; }
        public Version FirmwareVersion { private set; get; }

        public FinderPeripheralType Peripheral { private set; get; }

        public FinderVersion(byte board, byte reference_wheel, byte light_source, byte housing, byte protocol_major, byte protocol_minor, byte firmware_major, byte firmware_minor)
        {
            this.Board = board;
            this.ReferenceWheel = reference_wheel;
            this.LightSource = light_source;
            this.Casing = housing;

            this.ProtocolVersion = new Version(protocol_major, protocol_minor);
            this.FirmwareVersion = new Version(firmware_major, firmware_minor);

            this.Peripheral = FinderPeripheralType.None;
        }

        public FinderVersion(byte board, byte reference_wheel, byte light_source, byte housing, byte protocol_major, byte protocol_minor, byte firmware_major, byte firmware_minor, byte peripheral)
            : this(board, reference_wheel, light_source, housing, protocol_major, protocol_minor, firmware_major, firmware_minor)
        {
            this.Peripheral = (FinderPeripheralType)peripheral;
        }

        public FinderVersion() : this(0, 0, 0, 0, 0, 0, 0, 0)
        {
        }

        public FinderVersion(byte[] versions)
        {
            if (versions == null || versions.Length < 4)
            {
                this.Board = 0;
                this.ReferenceWheel = 0;
                this.LightSource = 0;
                this.Casing = 0;
                this.ProtocolVersion = new Version(0, 0);
                this.FirmwareVersion = new Version(0, 0);
                return;
            }

            this.Board = versions[0];
            this.ReferenceWheel = versions[1];
            this.LightSource = versions[2];
            this.Casing = versions[3];

            if (versions == null || versions.Length < 8)
            {
                this.ProtocolVersion = new Version(0, 0);
                this.FirmwareVersion = new Version(0, 0);
                return;
            }

            this.ProtocolVersion = new Version(versions[4], versions[5]);
            this.FirmwareVersion = new Version(versions[6], versions[7]);

            if (versions == null || versions.Length < 9)
            {
                this.Peripheral = FinderPeripheralType.None;
                return;
            }

            this.Peripheral = (FinderPeripheralType)versions[8];
        }

        public byte[] ToArray()
        {
            return new byte[4] { this.Board, this.ReferenceWheel, this.LightSource, this.Casing };
        }
    }
}