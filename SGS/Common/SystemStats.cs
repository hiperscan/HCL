﻿// Created with MonoDevelop
//
//    Copyright (C) 2019 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using Hiperscan.Unix;


namespace Hiperscan.SGS.Common
{

    [Serializable()]
    public class SystemStats
    {
        public TimeSpan LightSourceOperatingTime { private set; get; }
        public TimeSpan SystemOperatingTime      { private set; get; }

        public int LightSourcePowerCycles { private set; get; }

        public SystemStats(uint[] stats)
        {
            if (stats == null || stats.Length != 3)
                throw new ArgumentException(Catalog.GetString("Invalid system stats array."));

            this.SystemOperatingTime = new TimeSpan(0, 0, (int)stats[0]);
            this.LightSourceOperatingTime = new TimeSpan(0, 0, (int)stats[1]);
            this.LightSourcePowerCycles = (int)stats[2];
        }

        public override string ToString()
        {
            return string.Format("[SystemStats: LightSourceOperatingTime={0}, SystemOperatingTime={1}, LightSourcePowerCycles={2}, Gradient={3}]", LightSourceOperatingTime, SystemOperatingTime, LightSourcePowerCycles);
        }
    }
}