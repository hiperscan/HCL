﻿// Created with MonoDevelop
//
//    Copyright (C) 2009 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Text;

using Hiperscan.Spectroscopy;


namespace Hiperscan.SGS.Common
{

    [Serializable()]
    public class TemperatureHistory
    {
        public DateTime Timestamp    { get; private set; }
        public uint Interval         { get; private set; }
        public double[] Temperatures { get; private set; }

        public TemperatureHistory(uint interval, double[] temperatures)
        {
            this.Timestamp = DateTime.Now;
            this.Interval = interval;
            if (temperatures == null)
                this.Temperatures = new double[0];
            else
                this.Temperatures = (double[])temperatures.Clone();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder(string.Format("[TemperatureHistory: Timestamp={0}, Interval={1}]", this.Timestamp, this.Interval));
            for (int ix = 0; ix < this.Temperatures.Length; ++ix)
            {
                sb.Append(string.Format("\n[  Index={0}, Value={1}]", ix, this.Temperatures[ix]));
            }
            return sb.ToString();
        }

        public double Gradient
        {
            get
            {
                if (this.Temperatures.Length < 10)
                    return double.NaN;

                List<double> times = new List<double>();
                for (int ix = 0; ix < this.Temperatures.Length; ++ix)
                {
                    times.Add((double)ix * (double)this.Interval / 1000.0 / 60.0 / 60.0);
                }
                DataSet ds = new DataSet(times.ToArray(), this.Temperatures);
                return ds.LinearRegression().Slope;
            }
        }
    }
}