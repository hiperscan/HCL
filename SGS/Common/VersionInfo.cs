// Created with MonoDevelop
//
//    Copyright (C) 2009 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Runtime.Serialization;


namespace Hiperscan.SGS.Common
{

    [Serializable()]
    public class VersionInfo : System.Runtime.Serialization.ISerializable
    {
        public Version ProtocolVersion   { get; private set; }
        public Version FirmwareVersion   { get; private set; }
        public Version ScanEngineVersion { get; private set; }
        public Version HardwareVersion   { get; private set; }

        public VersionInfo(Version protocol_version)
        {
            this.ProtocolVersion = protocol_version;
            this.FirmwareVersion = new Version(0);
            this.ScanEngineVersion = new Version(0);
            this.HardwareVersion = new Version(0);
        }

        public VersionInfo(Version protocol_version, Version firmware_version, Version scan_engine_version, Version hardware_version)
        {
            this.ProtocolVersion = protocol_version;
            this.FirmwareVersion = firmware_version;
            this.ScanEngineVersion = scan_engine_version;
            this.HardwareVersion = hardware_version;
        }

        public VersionInfo(SerializationInfo info, StreamingContext context)
        {
            this.ProtocolVersion   = (Version)info.GetValue("ProtocolVersion",   typeof(Version));
            this.FirmwareVersion   = (Version)info.GetValue("FirmwareVersion",   typeof(Version));
            this.ScanEngineVersion = (Version)info.GetValue("ScanEngineVersion", typeof(Version));
            this.HardwareVersion   = (Version)info.GetValue("HardwareVersion",   typeof(Version));
        }

        public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            info.AddValue("ProtocolVersion",   this.ProtocolVersion);
            info.AddValue("FirmwareVersion",   this.FirmwareVersion);
            info.AddValue("ScanEngineVersion", this.ScanEngineVersion);
            info.AddValue("HardwareVersion",   this.HardwareVersion);
        }
    }
}
