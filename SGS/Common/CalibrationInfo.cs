﻿// Created with MonoDevelop
//
//    Copyright (C) 2019 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;


namespace Hiperscan.SGS.Common
{

    public class CalibrationInfo
    {
        public CalibrationInfo(DateTime dt, double freq, double interval, double deviation)
        {
            this.CalibrationTime = dt;
            this.CalibrationFreq = freq;
            this.TimeInterval    = interval;
            this.FreqDeviation   = deviation;
        }

        public DateTime CalibrationTime { get; }
        public double CalibrationFreq   { get; }
        public double TimeInterval      { get; }
        public double FreqDeviation     { get; }
    }
}