﻿// Created with MonoDevelop
//
//    Copyright (C) 2019 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections;

using Hiperscan.Spectroscopy;
using Hiperscan.Unix;


namespace Hiperscan.SGS.Common
{

    public class SpectrumQueue
    {
        private readonly Queue queue;

        public SpectrumQueue()
        {
            this.queue = Queue.Synchronized(new Queue(this.MaxQueueCount));
        }

        public SpectrumQueue(int max_queue_count)
        {
            this.MaxQueueCount = max_queue_count;
            this.queue = Queue.Synchronized(new Queue(this.MaxQueueCount));
        }

        public void Enqueue(Spectrum spectrum)
        {
            if (this.IsActive == false)
                return;

            if (this.Count >= this.MaxQueueCount)
            {
                string s = string.Format(Catalog.GetString("The maximum queue length of {0} would be exceeded."),
                                         this.MaxQueueCount);

                throw new OverflowException(s);
            }

            this.queue.Enqueue(spectrum);
        }

        public Spectrum Dequeue()
        {
            return (Spectrum)this.queue.Dequeue();
        }

        public void TrimToSize()
        {
            this.queue.TrimToSize();
        }

        public int Count
        {
            get { return this.queue.Count; }
        }

        public int MaxQueueCount { get; set; } = 20;
        public bool IsActive     { get; set; } = false;
    }
}