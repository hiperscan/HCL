﻿// Created with MonoDevelop
//
//    Copyright (C) 2019 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;


namespace Hiperscan.SGS.Common
{

    public class AcquisitionInfo
    {
        internal AcquisitionInfo(double mirror_freq, int samples, double sample_rate)
        {
            this.MirrorFreq = mirror_freq;
            this.Samples    = samples;
            this.SampleRate = sample_rate;
        }

        public double MirrorFreq { get; }
        public int Samples       { get; }
        public double SampleRate { get; }
    }
}