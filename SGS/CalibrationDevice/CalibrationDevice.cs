﻿// Created with MonoDevelop
//
//    Copyright (C) 2019 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;

using Hiperscan.Common;
using Hiperscan.SGS.Benchtop;
using Hiperscan.SGS.Common;
using Hiperscan.SGS.SerialInterface;
using Hiperscan.SGS.Sgs1900;
using Hiperscan.Spectroscopy;


namespace Hiperscan.SGS.CalibrationDevice
{
    public enum CalibrationFilterPosition : int
    {
        [StringValue("Initial")] Initial = 0,
        [StringValue("Dark/Laser")] DarkOrLaser = 1,
        [StringValue("White reference")] White = 2,
        [StringValue("Interference filter")] InterferenceFilter = 3,
        [StringValue("Rare Earth oxides")] RareEarthOxides = 4,
        [StringValue("Reserved1")] Reserved1 = 5,
        [StringValue("Reserved2")] Reserved2 = 6,
        [StringValue("Moving")] Moving = int.MaxValue - 1,
        [StringValue("Unknown")] Unknown = int.MaxValue
    }

    internal class CalibrationDevice : Device0_21
    {
        protected new class Protocol : Device0_21.Protocol
        {
        }

        public DateTime LightSourceActivationTime { get; protected set; }
        public DateTime LaserActivationTime       { get; protected set; }

        protected CalibrationDevice(DeviceInfo info, VersionInfo version_info) : base(info, version_info)
        {
        }

        public virtual void SetFilterPosition(CalibrationFilterPosition position, IdleTaskHandler idle_task)
        {
        }

        public virtual void SetLightSource(bool on, IdleTaskHandler idle_task)
        {
            base.SetFinderLightSource(on, idle_task);
        }

        public virtual void SetLaser(bool on)
        {
            base.SetFinderFanSpeed(on);
        }

        public virtual void SetStatusLed(bool green)
        {
            base.SwitchFinderStatusLED(green);
        }

        public override void SwitchFinderStatusLED(bool green)
        {
            throw new NotSupportedException($"{nameof(SwitchFinderStatusLED)} is not supported by device type {this.Info.DeviceType}.");
        }

        public override void SetFinderLightSource(bool on, IdleTaskHandler idle_task)
        {
            throw new NotSupportedException($"{nameof(SetFinderLightSource)} is not supported by device type {this.Info.DeviceType}.");
        }

        public override void SetFinderWheel(FinderWheelPosition position, IdleTaskHandler idle_task, bool no_wait = false)
        {
            throw new NotSupportedException($"{nameof(SetFinderWheel)} is not supported by device type {this.Info.DeviceType}.");
        }

        public override Spectrum Single(bool auto_finder_light, IdleTaskHandler idle_task)
        {
            throw new NotSupportedException($"{nameof(Single)} is not supported by device type {this.Info.DeviceType}.");
        }

        public override bool FinderButtonPressed()
        {
            throw new NotSupportedException($"{nameof(FinderButtonPressed)} is not supported by device type {this.Info.DeviceType}.");
        }

        public override Spectrum AcquireAdcBufferSpectrum(byte[] key, ushort[] buf)
        {
            throw new NotSupportedException($"{nameof(AcquireAdcBufferSpectrum)} is not supported by device type {this.Info.DeviceType}.");
        }

        public override int ReadAcquisitionOffset()
        {
            throw new NotSupportedException($"{nameof(ReadAcquisitionOffset)} is not supported by device type {this.Info.DeviceType}.");
        }

        public override DataSet ReadAdcCorrection()
        {
            throw new NotSupportedException($"{nameof(ReadAdcCorrection)} is not supported by device type {this.Info.DeviceType}.");
        }

        public override bool FinderWheelOnInitPosition()
        {
            throw new NotSupportedException($"{nameof(FinderWheelOnInitPosition)} is not supported by device type {this.Info.DeviceType}.");
        }

        public override byte ReadAdcResolution()
        {
            throw new NotSupportedException($"{nameof(ReadAdcResolution)} is not supported by device type {this.Info.DeviceType}.");
        }

        public override ushort ReadAverageCount()
        {
            throw new NotSupportedException($"{nameof(ReadAverageCount)} is not supported by device type {this.Info.DeviceType}.");
        }

        public override uint ReadClockFrequency()
        {
            throw new NotSupportedException($"{nameof(ReadClockFrequency)} is not supported by device type {this.Info.DeviceType}.");
        }

        public override uint ReadFinderIdleTime()
        {
            throw new NotSupportedException($"{nameof(ReadFinderIdleTime)} is not supported by device type {this.Info.DeviceType}.");
        }

        public override double ReadFinderTemperature()
        {
            throw new NotSupportedException($"{nameof(ReadFinderTemperature)} is not supported by device type {this.Info.DeviceType}.");
        }

        public override double ReadScanEngineMsaFactor()
        {
            throw new NotSupportedException($"{nameof(ReadScanEngineMsaFactor)} is not supported by device type {this.Info.DeviceType}.");
        }

        public override void SetAdcCount(byte[] key, ushort samples)
        {
            throw new NotSupportedException($"{nameof(SetAdcCount)} is not supported by device type {this.Info.DeviceType}.");
        }

        public override void SetFinderFanSpeed(bool on)
        {
            throw new NotSupportedException($"{nameof(SetFinderFanSpeed)} is not supported by device type {this.Info.DeviceType}.");
        }

        public override double ReadStatus()
        {
            throw new NotSupportedException($"{nameof(ReadStatus)} is not supported by device type {this.Info.DeviceType}.");
        }

        public override void SetOscillationMode(byte[] key, ushort factor)
        {
            throw new NotSupportedException($"{nameof(SetOscillationMode)} is not supported by device type {this.Info.DeviceType}.");
        }

        public override void StartQuickStream()
        {
            throw new NotSupportedException($"{nameof(StartQuickStream)} is not supported by device type {this.Info.DeviceType}.");
        }

        public override void StartSingle(bool auto_finder_light)
        {
            throw new NotSupportedException($"{nameof(StartSingle)} is not supported by device type {this.Info.DeviceType}.");
        }

        public override void StartStream()
        {
            throw new NotSupportedException($"{nameof(StartStream)} is not supported by device type {this.Info.DeviceType}.");
        }

        public override void WriteTemperatureControlInput(byte[] key, byte val)
        {
            throw new NotSupportedException($"{nameof(WriteTemperatureControlInput)} is not supported by device type {this.Info.DeviceType}.");
        }

        public override void WriteTemperatureControlInfo(byte[] key, byte version, byte control_value, double target_temperature)
        {
            throw new NotSupportedException($"{nameof(WriteTemperatureControlInfo)} is not supported by device type {this.Info.DeviceType}.");
        }

        public override void WriteScanEngineMsaFactor(byte[] key, double msa_factor)
        {
            throw new NotSupportedException($"{nameof(WriteScanEngineMsaFactor)} is not supported by device type {this.Info.DeviceType}.");
        }

        public override void WriteFinderPcbHardwareVersion(byte[] key, byte[] versions)
        {
            throw new NotSupportedException($"{nameof(WriteFinderPcbHardwareVersion)} is not supported by device type {this.Info.DeviceType}.");
        }

        public override void WriteFinderIdleTime(uint idle_seconds)
        {
            throw new NotSupportedException($"{nameof(WriteFinderIdleTime)} is not supported by device type {this.Info.DeviceType}.");
        }

        public override void WriteFinderPCBCommand(byte[] command)
        {
            throw new NotSupportedException($"{nameof(WriteFinderPCBCommand)} is not supported by device type {this.Info.DeviceType}.");
        }
    }
}