// Created with MonoDevelop
//
//    Copyright (C) 2009 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.



using System.Reflection;
using System.Runtime.CompilerServices;

// Information about this assembly is defined by the following attributes. 
// Change them to the values specific to your project.

[assembly: AssemblyTitle("SGS")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Hiperscan GmbH")]
[assembly: AssemblyProduct("QuickStep")]
[assembly: AssemblyCopyright("Copyright © 2009-2020 Hiperscan GmbH")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// The assembly version has the format "{Major}.{Minor}.{Build}.{Revision}".
// If the build and revision are set to '*' they will be updated automatically.

[assembly: AssemblyVersion("2.2.0.0")]

// The following attributes are used to specify the signing key for the assembly, 
// if desired. See the Mono documentation for more information about signing.


[assembly: InternalsVisibleTo("SGSFirmwareTest, PublicKey=002400000480000094000000060200000024000052534131000400001100000093c94bfa6f9a723bba92b30bd1b536abad857a6a347e43680ba63b17247c36d984c53e392a0dac664f9bb725fac64384d7c2e10c1ebd715c8a6af4e21b41a469729b03bd297f2c774bfb18f905eeb0c741e9d6135f59855c81113d2fdee44eeadef57968deeafb5dc9fd3961374e99ffe3e0ab994315de634de5b004d0b1d6dc")]
[assembly: InternalsVisibleTo("SpectralEngines, PublicKey=002400000480000094000000060200000024000052534131000400001100000093c94bfa6f9a723bba92b30bd1b536abad857a6a347e43680ba63b17247c36d984c53e392a0dac664f9bb725fac64384d7c2e10c1ebd715c8a6af4e21b41a469729b03bd297f2c774bfb18f905eeb0c741e9d6135f59855c81113d2fdee44eeadef57968deeafb5dc9fd3961374e99ffe3e0ab994315de634de5b004d0b1d6dc")]
[assembly: InternalsVisibleTo("DeviceCalibration, PublicKey=002400000480000094000000060200000024000052534131000400001100000093c94bfa6f9a723bba92b30bd1b536abad857a6a347e43680ba63b17247c36d984c53e392a0dac664f9bb725fac64384d7c2e10c1ebd715c8a6af4e21b41a469729b03bd297f2c774bfb18f905eeb0c741e9d6135f59855c81113d2fdee44eeadef57968deeafb5dc9fd3961374e99ffe3e0ab994315de634de5b004d0b1d6dc")]

