﻿// Created with MonoDevelop
//
//    Copyright (C) 2017-2019 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Runtime.Serialization;

using LibUsbDotNet.Main;


namespace Hiperscan.SGS.SerialInterface.USB
{

    public abstract partial class LibUsbDevice
    {

        [Serializable()]
        public class UsbException : System.Exception, System.Runtime.Serialization.ISerializable
        {
            public UsbException(ErrorCode error_code, string message) : base(message)
            {
                this.Status = Enum.GetName(typeof(ErrorCode), error_code);
            }

            public UsbException(Exception ex) : base(ex.Message, ex)
            {
            }

            public UsbException(SerializationInfo info, StreamingContext context) : base(info, context)
            {
                this.Status = (string)info.GetValue("Status", typeof(string));
            }

            public new void GetObjectData(SerializationInfo info, StreamingContext ctxt)
            {
                base.GetObjectData(info, ctxt);
                info.AddValue("Status", this.Status);
            }

            public string Status { get; }
        }
    }
}
