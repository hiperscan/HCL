﻿// Created with MonoDevelop
//
//    Copyright (C) 2017-2019 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

using Hiperscan.SGS.Common;
using Hiperscan.Unix;

using LibUsbDotNet;
using LibUsbDotNet.Main;

using Version = Hiperscan.SGS.Common.Version;


namespace Hiperscan.SGS.SerialInterface.USB
{
    public class STM32 : Hiperscan.SGS.SerialInterface.USB.LibUsbDevice, Hiperscan.SGS.SerialInterface.ISerialInterface
    {

        private const int READBUFFER_SIZE = 16384; //16384;
        /* From libftdi 1.4 sources:
         * We can't set readbuffer_chunksize larger than MAX_BULK_BUFFER_LENGTH,
         * which is defined in libusb-1.0. Otherwise, each USB read request will
         * be divided into multiple URBs. This will cause issues on Linux kernel
         * older than 2.6.32.  
         *
         * A readbuffer size greater than 16384 seems to cause still trouble on
         * ARM hardware. See tickets #15643, #33562        
         */

        private const int WRITE_CHUNKSIZE = 16384;
        private const int MAX_PACKET_SIZE = 64;
        private const int VERSION_READ_TIMEOUT_MS = 1000;
        private const int FRAME_END_TIMEOUT_MS = 20;

        private int readbuffer_remaining;
        private int readbuffer_offset;
        private readonly byte[] readbuffer = new byte[READBUFFER_SIZE];


        public STM32(IEnumerable<Tuple<uint,uint>> vendor_product_ids)
        {
            this.vendor_product_ids = vendor_product_ids.ToArray();
        }

        public STM32(IEnumerable<SerialInterfaceConfig> usb_configs) : this(usb_configs.Select(c => Tuple.Create(c.VendorId, c.ProductId)))
        {
        }

        public STM32(uint vendor_id, uint product_id) : this(new Tuple<uint,uint>[] { Tuple.Create(vendor_id, product_id) })
        {
        }

        protected override void OpenUsbDevice(string serial)
        {
            this.usb_device = UsbDevice.OpenUsbDevice((dev) =>
                                                      this.vendor_product_ids.Any(v => v.Item1 == dev.Vid && v.Item2 == (uint)dev.Pid) &&
                                                      LibUsbDevice.GetSerialNumber(dev) == serial);

            if (this.usb_device == null)
                throw new Exception(Catalog.GetString("Cannot open device."));

            if (this.usb_device is IUsbDevice iusb_device)
            {
                if (iusb_device.ClaimInterface(0) == false)
                    throw new Exception(Catalog.GetString("Cannot claim interface."));
            }

            UsbReset();
        }

        private void UsbReset()
        {
            const byte REQUEST_TYPE_HOST_TO_DEVICE_VENDOR_REQUEST = 0x40;

            const byte COMMAND_REQ_CONTROL = 0xAA;
            const int COMMAND_IDX_RESET_BUFFER = 0x01;

            UsbSetupPacket reset_buffer = new UsbSetupPacket(REQUEST_TYPE_HOST_TO_DEVICE_VENDOR_REQUEST,  //class request
                                                             COMMAND_REQ_CONTROL,
                                                             0x0000,
                                                             COMMAND_IDX_RESET_BUFFER,
                                                             0x0000);

            byte[] empty = new byte[0];
            bool success = this.usb_device.ControlTransfer(ref reset_buffer, empty, empty.Length, out int transferred);

            if (success == false || transferred != empty.Length)
                throw new UsbException(ErrorCode.None, Catalog.GetString("Cannot execute control transfer:") + " " + nameof(reset_buffer));
        }

        public void ClearReadbuffer()
        {
            try
            {
                // workaround for clearing readbuffer
                // when device was innetrrupted while transfering data
                ReadBlocking(READBUFFER_SIZE, 10, null);
            }
            catch (TimeoutException) { }
        }

        protected override void InitUsbDevice()
        {

            this.SetTimeouts(100, 100);
            this.SetLatency();

            this.endpoint_reader = this.usb_device.OpenEndpointReader(ReadEndpointID.Ep01);
            this.endpoint_writer = this.usb_device.OpenEndpointWriter(WriteEndpointID.Ep01);

            ClearReadbuffer();
        }

        public override void ResetDevice()
        {
            if (string.IsNullOrEmpty(this.serial))
                throw new Exception(Catalog.GetString("Cannot reset a USB device which has not been opened."));

            this.usb_device.Close();
            Thread.Sleep(1000);
            this.OpenUsbDevice(this.serial);
            this.InitUsbDevice();
        }

        //public override uint Read(out byte[] rx_buf, uint size)
        //{
        //    ErrorCode error_code;
        //    rx_buf = new byte[size];

        //    error_code = this.endpoint_reader.Transfer(rx_buf,
        //                                               0,
        //                                               rx_buf.Length,
        //                                               this.config.ReadTimeout,
        //                                               out int read_transferred);

        //    if (error_code != ErrorCode.None)
        //        throw new UsbException(error_code, Catalog.GetString("Cannot execute bulk transfer (read):") + " " + UsbDevice.LastErrorString);

        //    Thread.Sleep(20);

        //    return (uint)read_transferred;
        //}

        public override uint Read(out byte[] rx_buf, uint size)
        {
            int offset = 0;
            int transferred = 1;
            rx_buf = new byte[size];

            // everything we want is still in the readbuffer?
            if (size <= this.readbuffer_remaining)
            {
                Buffer.BlockCopy(this.readbuffer, this.readbuffer_offset, rx_buf, 0, (int)size);

                this.readbuffer_remaining -= (int)size;
                this.readbuffer_offset += (int)size;

                return size;
            }

            // something still in the readbuffer, but not enough to satisfy 'size'?
            if (this.readbuffer_remaining != 0)
            {
                Buffer.BlockCopy(this.readbuffer, this.readbuffer_offset, rx_buf, 0, this.readbuffer_remaining);

                offset += this.readbuffer_remaining;
            }

            // do the actual USB read
            while (offset < size && transferred > 0)
            {
                this.readbuffer_remaining = 0;
                this.readbuffer_offset = 0;

                ErrorCode error_code = this.endpoint_reader.Transfer(this.readbuffer,
                                                                     0,
                                                                     this.readbuffer.Length,
                                                                     this.config.ReadTimeout,
                                                                     out transferred);

                if (error_code != ErrorCode.None && error_code != ErrorCode.IoTimedOut)
                    throw new UsbException(error_code, Catalog.GetString("Cannot execute bulk transfer (read):") + " " + UsbDevice.LastErrorString);

               // Thread.Sleep(20);

                if (transferred == 0)
                {
                    // no more data to read?
                    rx_buf = rx_buf.Take(offset).ToArray();
                    return (uint)offset;
                }
                else
                {
                    // data still fits in buf?
                    if (offset + transferred <= size)
                    {
                        Buffer.BlockCopy(this.readbuffer, this.readbuffer_offset, rx_buf, offset, transferred);
                        offset += transferred;

                        // did we read exactly the right amount of bytes?
                        if (offset == size)
                        {
                            rx_buf = rx_buf.Take(offset).ToArray();
                            return (uint)offset;
                        }
                    }
                    else
                    {
                        // only copy part of the data or size <= readbuffer_chunksize
                        int part_size = (int)size - offset;
                        Buffer.BlockCopy(this.readbuffer, this.readbuffer_offset, rx_buf, offset, part_size);

                        this.readbuffer_offset += part_size;
                        this.readbuffer_remaining = transferred - part_size;
                        offset += part_size;

                        rx_buf = rx_buf.Take(offset).ToArray();
                        return (uint)offset;
                    }
                }
            }

            throw new Exception("This should be never reached.");
        }

        public uint Read(out byte[] data)
        {
            List<byte> rx_buf = new List<byte>();

            while (true)
            {
                byte[] buf = null;
                try
                {
                    this.Read(out buf, READBUFFER_SIZE);
                }
                catch (UsbException ex)
                {
                    if (ex.Status != ErrorCode.IoTimedOut.ToString())
                        throw new UsbException(ex);
                }

                if (buf?.Length > 0)
                    rx_buf.AddRange(buf);
                else
                    break;
            }

            data = rx_buf.ToArray();

            return (uint)rx_buf.Count;
        }

        public UInt32 Write(byte[] data)
        {
            int offset = 0;
            int size = data.Length;

            if (size > WRITE_CHUNKSIZE)
                throw new UsbException(ErrorCode.Overflow, Catalog.GetString("Cannot execute bulk transfer (write buffer overflow)"));


            ErrorCode error_code = this.endpoint_writer.Transfer(data,
                                                                     offset,
                                                                     size,
                                                                     this.config.WriteTimeout,
                                                                     out int transferred);

            if (error_code != ErrorCode.None || transferred != size)
                throw new UsbException(error_code, Catalog.GetString("Cannot execute bulk transfer (write):") + " " + UsbDevice.LastErrorString);

            // add zero length packet for mark frame end
            if ((size > 0) && (size % 64 == 0))
            {
                error_code = this.endpoint_writer.Transfer(data,
                                                         offset,
                                                         0,
                                                         this.config.WriteTimeout,
                                                         out transferred);

                if (error_code != ErrorCode.None || transferred != 0)
                    throw new UsbException(error_code, Catalog.GetString("Cannot execute bulk transfer (write):") + " " + UsbDevice.LastErrorString);
            }

            return (UInt32)(transferred);
        }

        internal static VersionInfo  GetVersionInfo(DeviceInfo info, bool disable_timeout)
        {
            STM32 usb = new STM32(info.VendorId, info.ProductId);

            usb.OpenBySerialNumber(info.SerialNumber);

            int timeout = 100;
            if (disable_timeout)
                timeout = 0;

            // protocol version
            byte[] pv_buf = new byte[] { 0, 0 };
            for (int count = 0; count < 20; ++count)
            {
                usb.ClearReadbuffer();

                try
                {
                    usb.Write(new byte[] { 0 });
                    pv_buf = usb.ReadBlocking(2, timeout, null);
                    break;
                }
                catch (Exception ex) when (ex is TimeoutException || (ex as UsbException)?.Status == ErrorCode.IoTimedOut.ToString())
                {
                    if (timeout == 0)
                        break;
                }
            }

            Version protocol_version = new Version(pv_buf[0], pv_buf[1]);

            byte[] fw_buf = new byte[] { 0, 0 };
            byte[] se_buf = new byte[] { 0, 0 };
            byte[] hw_buf = new byte[] { 0, 0 };

            // firmware version
            usb.Write(new byte[] { 1 });
            fw_buf = usb.ReadBlocking(2, timeout, null);

            Version cur_version = new Version(fw_buf[0], fw_buf[1]);

            // scan engine version
            usb.Write(new byte[] { 2 });
            se_buf = usb.ReadBlocking(2, timeout, null);

            // hardware version
            usb.Write(new byte[] { 4 });
            hw_buf = usb.ReadBlocking(2, timeout, null);

            usb.Close();

            Console.WriteLine("Protocol version: {0}.{1}", pv_buf[0], pv_buf[1]);
            Console.WriteLine("Firmware version: {0}.{1}", fw_buf[0], fw_buf[1]);
            Console.WriteLine("Scan Engine version: {0}.{1}", se_buf[0], se_buf[1]);
            Console.WriteLine("Hardware version: {0}.{1}", hw_buf[0], hw_buf[1]);

            Version firmware_version = new Version(fw_buf[0], fw_buf[1]);
            Version scan_engine_version = new Version(se_buf[0], se_buf[1]);
            Version hardware_version = new Version(hw_buf[0], hw_buf[1]);

            return new VersionInfo(protocol_version, firmware_version, scan_engine_version, hardware_version);
        }

        protected override void ResetUsbDevice()
        {
        }
    }
}
