﻿// Created with MonoDevelop
//
//    Copyright (C) 2020 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Runtime.Serialization;


namespace Hiperscan.SGS.SerialInterface
{

    [Serializable()]
    public class SerialInterfaceConfig : System.Runtime.Serialization.ISerializable
    {
        public enum BackendType
        {
            FT245BL,
            CP2130,
            STM32
        }

        public uint VendorId  { get; private set; }
        public uint ProductId { get; private set; }

        public DeviceType DeviceType { get; private set; }
        public BackendType Backend   { get; private set; }

        public SerialInterfaceConfig(uint vendor_id, uint product_id, BackendType backend, DeviceType device_type)
        {
            this.VendorId   = vendor_id;
            this.ProductId  = product_id;
            this.Backend    = backend;
            this.DeviceType = device_type;
        }

        public SerialInterfaceConfig(SerializationInfo info, StreamingContext context)
        {
            this.VendorId   = (uint)info.GetValue("VendorId", typeof(uint));
            this.ProductId  = (uint)info.GetValue("ProductId", typeof(uint));
            this.DeviceType = (DeviceType)info.GetValue("DeviceType", typeof(DeviceType));
            this.Backend    = (SerialInterfaceConfig.BackendType)info.GetValue("Backend", typeof(SerialInterfaceConfig.BackendType));
        }

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("VendorId",   this.VendorId);
            info.AddValue("ProductId",  this.ProductId);
            info.AddValue("DeviceType", this.DeviceType);
            info.AddValue("Backend",    this.Backend);
        }

        public override int GetHashCode()
        {
            return (int)this.VendorId ^ (int)this.ProductId ^ (int)this.Backend;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            SerialInterfaceConfig usb_config = obj as SerialInterfaceConfig;

            return this.VendorId.Equals(usb_config?.VendorId)   && 
                   this.ProductId.Equals(usb_config?.ProductId) &&
                   this.Backend.Equals(usb_config?.Backend);
        }
    }
}