// Created with MonoDevelop
//
//    Copyright (C) 2010 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.Collections.Generic;

using Hiperscan.Common;


namespace Hiperscan.SGS.SerialInterface
{
    public delegate void IdleTaskHandler();
    
    public enum DriverType
    {
        FTDI,
        LibFTDI,
        LibUSB,
        WinUSB,
        None
    }

    public enum DeviceType : ulong
    {
        [StringValue("Unknown")] Unknown,
        [StringValue("SGS1900")] Sgs1900,
        [StringValue("PT-IM100")] PTIM100,
        [StringValue("GerberScan")] GerberScan,
        [StringValue("Spectral Engines NSeries")] NSeries,
        [StringValue("SGS-NT")] SgsNt,
        [StringValue("Calibration Device")] CalibrationDevice,
        [StringValue("ApoIdent")] ApoIdent,
        [StringValue("ES20")] ES20,
    }

    public interface ISerialInterface
    {
        uint GetNumberOfDevices();
        IEnumerable<DeviceInfo> GetDeviceList();
        void OpenBySerialNumber(string serial);
        void Close();
        void ResetDevice();
        void SetTimeouts(int read_timeout, int write_timeout);
        void SetTimeouts();
        void SetLatency();
        void Purge();
        
        uint Write(byte[] data);
        uint Read(out byte[] data);
        uint Read(out byte[] data, uint len);
        byte[] ReadBlocking(uint len, int timeout, IdleTaskHandler idle_task);
        
        DriverType DriverType { get; }
    }
}
