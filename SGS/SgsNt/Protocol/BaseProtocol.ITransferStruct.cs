﻿// Created with MonoDevelop
//
//    Copyright (C) 2019 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.Runtime.InteropServices;


namespace Hiperscan.SGS.SgsNt.Protocol
{
    abstract partial class BaseProtocol
    {
        internal interface ITransferStruct
        {
        }

        internal static class TransferStruct
        {
            public static object FromBuffer(byte[] buffer, Type type)
            {
                object obj = null;

                if (buffer != null && buffer.Length > 0)
                {
                    IntPtr ptr_obj = IntPtr.Zero;
                    try
                    {
                        int obj_size = Marshal.SizeOf(type);
                        if (obj_size > 0)
                        {
                            if (buffer.Length < obj_size)
                                throw new Exception(string.Format("Unexpected buffer size for creation of object of type {0}.", type));

                            ptr_obj = Marshal.AllocHGlobal(obj_size);
                            if (ptr_obj != IntPtr.Zero)
                            {
                                Marshal.Copy(buffer, 0, ptr_obj, obj_size);
                                obj = Marshal.PtrToStructure(ptr_obj, type);
                            }
                            else
                            {
                                throw new Exception(string.Format("Cannot allocate memory to create object of type {0}", type));
                            }
                        }
                    }
                    finally
                    {
                        if (ptr_obj != IntPtr.Zero)
                            Marshal.FreeHGlobal(ptr_obj);
                    }
                }

                return obj;
            }

            public static byte[] GetBytes(ITransferStruct transfer_struct)
            {
                int len = Marshal.SizeOf(transfer_struct);
                byte[] buf = new byte[len];

                IntPtr ptr = IntPtr.Zero;

                try
                {
                    ptr = Marshal.AllocHGlobal(len);
                    Marshal.StructureToPtr(transfer_struct, ptr, true);
                    Marshal.Copy(ptr, buf, 0, len);
                }
                finally
                {
                    if (ptr != IntPtr.Zero)
                        Marshal.FreeHGlobal(ptr);
                }

                return buf;
            }
        }
    }
}