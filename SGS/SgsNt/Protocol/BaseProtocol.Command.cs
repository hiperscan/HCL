﻿// Created with MonoDevelop
//
//    Copyright (C) 2019 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;


namespace Hiperscan.SGS.SgsNt.Protocol
{

    abstract partial class BaseProtocol
    {

        public class Command : Hiperscan.SGS.SgsNt.Protocol.BaseProtocol.Frame
        {
            public Type ResponseDataType { get; internal set; }

            public Command(AddressType addr, char c1, char c2, Type payload_data_type, UInt16 payload_data_len, Type response_data_type)
                : base(addr, c1, c2, payload_data_type, payload_data_len)
            {
                this.ResponseDataType = response_data_type;
            }

            public Command(Command command) : base(command)
            {
                this.ResponseDataType = command.ResponseDataType;
            }

            public override string ToString()
            {
                return "[Command]" + base.ToString();
            }

            public void UpdateChecksum()
            {
                this.Checksum = this.ComputeCrc32();
            }

            public bool IsDataRequest
            {
                get { return this.ResponseDataType != null; }
            }
        }
    }
}
