﻿// Created with MonoDevelop
//
//    Copyright (C) 2019 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;

using Hiperscan.Unix;


namespace Hiperscan.SGS.SgsNt.Protocol
{

    abstract partial class BaseProtocol
    {
        public enum Errno : UInt16
        {
            Success = 0,
            NotPermitted = 1,
            NoSuchFileOrDirectory = 2,
            InputOutput = 5,
            Access = 13,
            Busy = 16,
            NoSuchDevice = 19,
            InvalidArgument = 22,
            NotImplemented = 38,
            NoData = 61,
            Timeout = 62,
            Unknown = 1000,
            LightSourceDamaged = 1001,
            InvalidRecalibrationSpectrum = 1002,
            InvalidDarkReference = 1003,
            InvalidWhiteReference = 1004,
            InvalidTransflectReference = 1005,
            MissingReference = 1006,
            ReferencingTimeout = 1007,
            UnknownDeviceStatus = 1008,
            InvalidReferenceData = 1009,
            InvalidSpectrumType = 1010,
            InvalidSpectrumFormat = 1011,
            MissingChecksum = 1012
        }

        public enum AddressType : byte
        {
            Master     = 0,
            Peripheral = 1
        }

        public enum HeaderStartType : byte
        {
            FrameStart = 0xAA
        }

        [Flags]
        public enum ChannelType : byte
        {
            Channel1    = 0b0000_0001,
            Channel2    = 0b0000_0010,
            Channel3    = 0b0000_0100,
            Channel4    = 0b0000_1000,
            ReverseScan = 0b1000_0000
        }
    }
}