﻿// Created with MonoDevelop
//
//    Copyright (C) 2019 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Linq;
using System.Threading;

using Hiperscan.SGS.Benchtop;
using Hiperscan.SGS.Common;
using Hiperscan.SGS.SgsNt.Protocol;
using Hiperscan.SGS.USB;
using Hiperscan.Spectroscopy;
using Hiperscan.Unix;


namespace Hiperscan.SGS.SgsNt
{

    abstract class BaseDevice : Hiperscan.SGS.Sgs1900.BaseDevice
    {

        public new const int LUT_LENGTH = 2800;

        protected class Protocol : Hiperscan.SGS.SgsNt.Protocol.BaseProtocol
        {
        }

        private readonly string serial;


        protected BaseDevice(DeviceInfo info, VersionInfo version) : base(info, version)
        {
            try
            {
                this.serial_interface = new CP2130(info.VendorId, info.ProductId);
                this.SetState(Spectrometer.State.Disconnected);
                this.serial = info.SerialNumber;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception while initializing device {0}: {1}", info.SerialNumber, ex.Message);
                this.SetState(Spectrometer.State.Unknown);
            }
        }

        public override void Open(bool auto_init)
        {
            lock (this.opening_mutex)
            {
                this.IgnoreSgsExceptions = false;

                try
                {
                    if (this.GetState() == Spectrometer.State.Disconnected)
                    {
                        // initialize usb device
                        try
                        {
                            this.serial_interface.OpenBySerialNumber(this.info.SerialNumber);
                        }
                        catch (System.Runtime.Remoting.RemotingException ex)
                        {
                            throw ex;
                        }

                        this.SetState(Spectrometer.State.Idle);

                        if (auto_init == false)
                            return;

                        try
                        {
                            this.Connect();
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Cannot connect device: {0}", ex.Message);
                            Console.WriteLine("Trying to reset and reopen device...");
                            this.serial_interface.ResetDevice();
                            this.Connect();
                        }

                        this.firmware_version  = this.ReadFirmwareVersion();
                        this.firmware_revision = this.ReadFirmwareRevision();
                        this.lut_timestamp     = this.ReadLutTimestamp();
                    }
                }
                catch (System.Runtime.Remoting.RemotingException ex)
                {
                    this.SetState(Spectrometer.State.Disconnected);
                    throw ex;
                }
                catch
                {
                    this.SetState(Spectrometer.State.Disconnected);
                    try
                    {
                        this.serial_interface.Close();
                    }
                    catch { }

                    throw;
                }
                finally
                {
                    this.is_initialized = true;
                }
            }
        }

        public override void Close()
        {
            try
            {
                this.serial_interface?.Close();
            }
            catch { }

            this.SetState(Spectrometer.State.Disconnected);
        }

        protected override void Connect()
        {
            if (this.WriteCommand(Protocol.Connect, 1000) == null)
                throw new SgsException(Catalog.GetString("Cannot connect device."), this);
        }

        protected Protocol.ChannelType AcquireSpectrum(UInt16 average_count, Protocol.ChannelType channel, IdleTaskHandler idle_task = null)
        {
            Protocol.Command command = new Protocol.Command(Protocol.AcquireSpectrum);

            byte[] payload_buf = BitConverter.GetBytes(average_count);
            payload_buf = payload_buf.Concat(new byte[] { (byte)channel }).ToArray();

            command.ByteArrayValue = payload_buf;

            Protocol.Response response = this.WriteCommand(command, 100, idle_task);

            if (response == null)
                throw new SgsException(Catalog.GetString("Cannot aquire spectrum."), this);

            return (Protocol.ChannelType)response.ByteValue;
        }

        protected float[] ReadIntensity(Protocol.ChannelType channel, IdleTaskHandler idle_task = null)
        {
            Protocol.Command command = new Protocol.Command(Protocol.ReadIntensity)
            {
                ByteValue = (byte)channel
            };

            Protocol.Response response = this.WriteCommand(command, 100, idle_task);

            if (response == null)
                throw new SgsException(Catalog.GetString("Cannot read intensity."), this);

            return response.FloatArrayValue;
        }

        internal override Spectrometer.Config ReadConfig(bool throw_if_invalid)
        {
            throw new System.NotImplementedException();
        }

        internal override void WriteConfig(Spectrometer.Config config)
        {
            this.SetState(Spectrometer.State.Idle);
        }

        public override void WriteEEPROM()
        {
            throw new System.NotImplementedException();
        }

        public override void StartSingle(bool auto_finder_light)
        {
            Thread run_single = new Thread(new ParameterizedThreadStart(this.DoRunSingle));
            run_single.Start(auto_finder_light);
        }

        public override Spectrum Single(bool auto_finder_light, IdleTaskHandler idle_task)
        {
            try
            {
                this.AcquireSpectrum(100, Protocol.ChannelType.Channel1, idle_task);
                float[] intensities = this.ReadIntensity(Protocol.ChannelType.Channel1, idle_task);

                return new Spectrum(Enumerable.Range(1, intensities.Length).Select(w => (double)w).ToList(),
                                    (double)intensities[0],
                                    new WavelengthLimits(1, intensities.Length));
            }
            catch (Exception ex)
            {
                if ((ex is InvalidSpectrumException) && (this.DisableInternalExceptionHandling || this.IgnoreSgsExceptions))
                    this.SetState(Spectrometer.State.Idle);

                this.ExceptionHandler(Catalog.GetString("Cannot read single spectrum:") + " " + ex.Message, ex);
                return null;
            }
        }

        public override void StartQuickStream()
        {
            Thread run_quick = new Thread(new ThreadStart(this.DoRunQuick));
            run_quick.Start();
        }

        public override void StartStream()
        {
            this.quick_stream_running = false;

            switch (this.CurrentConfig.StreamType)
            {

            case Spectrometer.StreamType.Continuous:
                Thread run_continuous = new Thread(new ThreadStart(this.DoRunContinuous));
                run_continuous.Start();
                break;

            case Spectrometer.StreamType.Timed:
                Thread run_timed = new Thread(new ThreadStart(this.DoRunTimed));
                run_timed.Start();
                break;

            default:
                throw new Exception("Unknown StreamType");

            }
        }

        public override void StopStream()
        {
            if (this.quick_stream_running)
            {
                this.SetState(Spectrometer.State.IdleRequest);
                return;
            }

            switch (this.CurrentConfig.StreamType)
            {

            case Spectrometer.StreamType.Continuous:
                throw new NotImplementedException();

            case Spectrometer.StreamType.Timed:
                this.SetState(Spectrometer.State.IdleRequest);
                break;

            default:
                throw new Exception("Unknown StreamType");

            }
        }

        public override double ReadStatus()
        {
            this.LastTemperature = double.NaN;
            return 0.0;
        }

        public override void SetFinderLightSource(bool on, IdleTaskHandler idle_task)
        {
            return;
        }

        public override void SetFinderWheel(FinderWheelPosition position, IdleTaskHandler idle_task, bool no_wait = false)
        {
            throw new NotSupportedException("Not supported by hardware.");
        }

        public override void SetFinderFanSpeed(bool on)
        {
            throw new NotSupportedException("Not supported by hardware.");
        }

        public override byte ReadDimmerParameter()
        {
            throw new NotSupportedException("Not supported by hardware.");
        }

        public override bool FinderButtonPressed()
        {
            throw new NotSupportedException("Not supported by hardware.");
        }

        public override double ReadFinderTemperature()
        {
            throw new NotSupportedException("Not supported by hardware.");
        }

        public override byte[] ReadFinderHardwareVersion()
        {
            throw new NotSupportedException("Not supported by hardware.");
        }

        public override void WriteSerialNumber(byte[] key, string serial)
        {
            throw new NotSupportedException("Not supported by hardware.");
        }

        private void DoRunSingle(object o)
        {
            bool auto_finder_light = (bool)o;

            try
            {
                Spectrum spectrum = this.Single(auto_finder_light);
                this.spectrum_queue.Enqueue(spectrum);
            }
            catch (System.Exception ex)
            {
                this.ExceptionHandler(Catalog.GetString("Cannot read single spectrum:") + " " + ex.Message, ex);
            }
        }

        private void DoRunTimed()
        {
            try
            {
                if (this.GetState() == Spectrometer.State.QuickStream)
                {
                    this.StopStream();
                    this.WaitForIdle(5 * this.CurrentConfig.Timeout);
                    this.StartStream();
                    return;
                }

                this.SetFinderLightSource(true, null);

                if (this.SetState(Spectrometer.State.Stream) == false)
                    throw new Exception(Catalog.GetString("Device is not ready."));

                DateTime then = DateTime.Now;
                this.SpectrumCount = 0;

                while ((this.GetState() == Spectrometer.State.Stream) &&
                       ((this.current_config.TimedCount == 0) || (this.SpectrumCount < this.CurrentConfig.TimedCount)))
                {
                    try
                    {
                        //double[] lambda;
                        //double[] intensity;
                        //uint[] add_data;
                        //this.ReadSpectrum(out add_data, out lambda, out intensity, null);

                        //WavelengthLimits limits = new WavelengthLimits(this.HardwareProperties.MinWavelength, this.HardwareProperties.MaxWavelength);
                        //Spectrum spectrum = new Spectrum(new DataSet(lambda, intensity), limits);

                        //spectrum.AddData = add_data;
                        //spectrum.Timestamp = DateTime.Now;
                        //spectrum.Serial = this.Info.SerialNumber;
                        //spectrum.FirmwareVersion = this.FirmwareVersion;
                        //spectrum.LutTimestamp = this.LutTimestamp.ToString();
                        //spectrum.SpectralResolution = this.HardwareProperties.SpectralResolution;
                        //spectrum.AverageCount = this.CurrentConfig.Average;
                        //spectrum.SpectrumType = SpectrumType.Stream;

                        //if (this.IsFinder)
                        //    spectrum.ProbeType = ProbeType.FinderStandard;

                        //spectrum.Id = "5_stream" + this.info.SerialNumber;
                        //spectrum.Label = Enum.GetName(typeof(State), State.Stream) + " " + this.CurrentConfig.Name;
                        //spectrum.DefaultLabel = true;
                        //spectrum.Comment = this.CurrentConfig.DefaultComment;
                        //spectrum.IsFromFile = false;
                        //spectrum.Number = (long)(++this.SpectrumCount);
                        //spectrum.LimitBandwidth = this.LimitBandwith;

                        //spectrum.HardwareInfo = this.FinderVersion.ToArray();

                        //if (this.CurrentConfig.HasAdcCorrection)
                        //    spectrum.AdcCorrection = this.CurrentConfig.AdcCorrection;

                        //if (this.StateNotification)
                        //    this.NewStreamNotify(spectrum.Clone());

                        //this.spectrum_queue.Enqueue(spectrum);

                        //lock (this.current_spectrum_mutex)
                            //this.current_spectrum = spectrum;

                    }
                    catch (Exception ex)
                    {
                        if ((ex is InvalidSpectrumException) &&
                            (this.DisableInternalExceptionHandling || this.IgnoreSgsExceptions))
                        {
                            this.SetState(Spectrometer.State.Idle);
                        }

                        this.ExceptionHandler(Catalog.GetString("Cannot read in timed mode:") +
                                              " " + ex.Message, ex);
                        break;
                    }

                    TimeSpan span;
                    ulong mseconds = 0;
                    ulong maxtime = 0;
                    bool light_is_on = true;

                    while (this.GetState() == Spectrometer.State.Stream)
                    {
                        if (this.CurrentConfig.TimedSpan == 0)
                            break;

                        span = DateTime.Now - then;
                        mseconds = (ulong)span.TotalMilliseconds % (this.CurrentConfig.TimedSpan * 1000);

                        if (mseconds < maxtime)
                            break;
                        else
                            maxtime = mseconds;

                        if ((this.CurrentConfig.TimedCount != 0) &&
                            (this.SpectrumCount >= this.CurrentConfig.TimedCount))
                            break;

                        // switch finder light off if there are more than 3.5 seconds to wait
                        if (this.IsFinder && light_is_on &&
                            this.CurrentConfig.TimedSpan * 1000 - mseconds > 3500)
                        {
                            Console.WriteLine("Next acquisition in {0} ms.",
                                              this.CurrentConfig.TimedSpan * 1000 - mseconds);
                            lock (this)
                            {
                                this.SetState(Spectrometer.State.Idle);
                                this.SetFinderLightSource(false, null);
                                this.SetState(Spectrometer.State.Stream);
                            }
                            light_is_on = false;
                        }

                        Thread.Sleep(50);
                    }

                    if (light_is_on == false && 
                        this.GetState() == Spectrometer.State.Stream &&
                        (this.SpectrumCount < this.CurrentConfig.TimedCount || this.CurrentConfig.TimedCount == 0))
                    {
                        lock (this)
                        {
                            this.SetState(Spectrometer.State.Idle);
                            this.SetFinderLightSource(true, null);
                            this.SetState(Spectrometer.State.Stream);
                        }
                    }
                }
                Console.WriteLine("Queue contains still " + this.spectrum_queue.Count + " spectra.");

                Spectrometer.State state = this.GetState();

                if (state == Spectrometer.State.Unknown)
                    return;

                if (state != Spectrometer.State.Disconnected)
                    this.SetState(Spectrometer.State.Idle);

                this.SetFinderLightSource(false, null);
            }
            catch (Exception ex)
            {
                this.ExceptionHandler(Catalog.GetString("Cannot read in timed mode:") +
                                      " " + ex.Message, ex);
            }
        }

        private void DoRunQuick()
        {
            try
            {
                if (this.GetState() == Spectrometer.State.Continuous ||
                    this.GetState() == Spectrometer.State.Stream)
                {
                    this.StopStream();
                    this.WaitForIdle(this.CurrentConfig.Timeout);

                    this.StartQuickStream();
                    return;
                }

                Console.WriteLine("Write QuickStream config");
                this.old_config = new Spectrometer.Config(this.CurrentConfig);
                this.CurrentConfig.Average = this.CurrentConfig.QuickAverage;
                this.CurrentConfig.Samples = this.CurrentConfig.QuickSamples;
                this.quick_stream_running = true;
                this.WriteConfig(this.CurrentConfig);

                this.SetFinderLightSource(true, null);

                if (this.SetState(Spectrometer.State.QuickStream) == false)
                    throw new Exception(Catalog.GetString("Device is not ready."));

                while (this.GetState() == Spectrometer.State.QuickStream)
                {
                    try
                    {
                        //double[] lambda;
                        //double[] intensity;
                        //uint[] add_data;
                        //this.ReadSpectrum(out add_data, out lambda, out intensity, null);

                        //WavelengthLimits limits = new WavelengthLimits(this.HardwareProperties.MinWavelength, this.HardwareProperties.MaxWavelength);
                        //Spectrum spectrum = new Spectrum(new DataSet(lambda, intensity), limits);

                        //spectrum.AddData = add_data;
                        //spectrum.Timestamp = DateTime.Now;
                        //spectrum.Serial = this.Info.SerialNumber;
                        //spectrum.FirmwareVersion = this.FirmwareVersion;
                        //spectrum.LutTimestamp = this.LutTimestamp.ToString();
                        //spectrum.SpectralResolution = this.HardwareProperties.SpectralResolution;
                        //spectrum.AverageCount = this.CurrentConfig.Average;
                        //spectrum.SpectrumType = SpectrumType.QuickStream;

                        //spectrum.Id = "5_stream" + this.info.SerialNumber;
                        //spectrum.Label = Enum.GetName(typeof(State), State.QuickStream) + " " + this.CurrentConfig.Name;
                        //spectrum.DefaultLabel = true;
                        //spectrum.Comment = this.CurrentConfig.DefaultComment;
                        //spectrum.IsFromFile = false;
                        ////spectrum.Number       = ++this.SpectrumCount;
                        //spectrum.LimitBandwidth = this.LimitBandwith;

                        //spectrum.HardwareInfo = this.FinderVersion.ToArray();

                        //if (this.CurrentConfig.HasAdcCorrection)
                        //    spectrum.AdcCorrection = this.CurrentConfig.AdcCorrection;

                        //if (this.StateNotification)
                        //    this.NewQuickStreamNotify(spectrum.Clone());

                        //this.spectrum_queue.Enqueue(spectrum);

                        //lock (this.current_spectrum_mutex)
                            //this.current_spectrum = spectrum;

                    }
                    catch (Exception ex)
                    {
                        if ((ex is InvalidSpectrumException) && (this.DisableInternalExceptionHandling || this.IgnoreSgsExceptions))
                            this.SetState(Spectrometer.State.Idle);

                        this.ExceptionHandler(Catalog.GetString("Cannot read in quick stream mode:") + " " + ex.Message, ex);
                        break;
                    }
                }

                Console.WriteLine("Queue contains still " + this.spectrum_queue.Count + " spectra.");

                Spectrometer.State state = this.GetState();

                if (state == Spectrometer.State.Unknown)
                    return;

                if (state != Spectrometer.State.Disconnected)
                    this.SetState(Spectrometer.State.Idle);

                this.SetFinderLightSource(false, null);

                this.CurrentConfig = this.old_config;
                this.old_config = null;
                this.WriteConfig(this.CurrentConfig);
            }
            catch (Exception ex)
            {
                this.ExceptionHandler(Catalog.GetString("Cannot read in quick stream mode:") +
                                      " " + ex.ToString(), ex);
            }
        }

        private void DoRunContinuous()
        {
            throw new NotImplementedException();
        }

        protected Protocol.Response WriteCommand(Protocol.Command cmd, int timeout, IdleTaskHandler idle_task = null)
        {
            try
            {
                Spectrometer.State sav_state = this.GetState();

                cmd.UpdateChecksum();

                Console.WriteLine("Writing {0}", cmd);
                uint result = this.Write(cmd.Buffer);

                this.SetState(sav_state);

                if (result != cmd.Buffer.Length)
                    throw new SgsException(Catalog.GetString("Transmission interrupted."), this);

                Thread.Sleep(10);

                return this.ReadResponse(timeout, cmd, idle_task);
            }
            catch (Exception ex)
            {
                this.ExceptionHandler(Catalog.GetString("Cannot transmit parameter:") + " " + ex.Message, ex);
                return null;
            }
        }

        protected Protocol.Response ReadResponse(int timeout, Protocol.Command cmd, IdleTaskHandler idle_task, bool throw_on_error = true)
        {
            Spectrometer.State sav_state = this.GetState();

            byte[] header = this.ReadBlocking(BaseProtocol.HEADER_LENGTH + 
                                              BaseProtocol.PAYLOAD_CMD_LENGTH + 
                                              BaseProtocol.PAYLOAD_LEN_LENGTH, 
                                              timeout, idle_task);

            UInt16 payload_data_len = BitConverter.ToUInt16(header, (int)BaseProtocol.PAYLOAD_LEN_OFFSET);

            if (payload_data_len > BaseProtocol.PAYLOAD_MAX_LENGTH)
                throw new Exception(string.Format(Catalog.GetString("Payload length exceeds transfer limit: {0} > {1}"),
                                                  payload_data_len,
                                                  BaseProtocol.PAYLOAD_MAX_LENGTH));

            int count = BaseProtocol.HEADER_LENGTH + 
                        BaseProtocol.PAYLOAD_CMD_LENGTH + 
                        BaseProtocol.PAYLOAD_LEN_LENGTH + 
                        payload_data_len + 
                        BaseProtocol.CHECKSUM_LENGTH;

            int try_count = 0;

            while (true)
            {
                try
                {
                    byte[] buf = this.ReadBlocking(count, timeout, idle_task);
                    Protocol.Response response = new BaseProtocol.Response(buf, cmd.ResponseDataType);

                    BaseProtocol.Errno errno = response.Check(cmd);

                    if (throw_on_error && errno != BaseProtocol.Errno.Success)
                        throw new SgsException(string.Format(Catalog.GetString("Device returned Error {0} ({1})."),
                                                             errno, (int)errno), this);

                    this.SetState(sav_state);

                    Console.WriteLine("Reading {0}", response);
                    return response;
                }
                catch (ChecksumException ex)
                {
                    if (++try_count > BaseProtocol.INVALID_CHECKSUM_RETRY_COUNT)
                        throw;

                    Console.WriteLine(string.Format(Catalog.GetString("Cannot read device response (try again): {0}"), ex.Message));
                }
            }
        }

        public override FinderWheelPosition FinderWheelPosition
        {
            get { throw new NotImplementedException("Not supported by hardware."); }
        }

        public override bool FinderLightSourceState
        {
            get { throw new NotImplementedException("Not supported by hardware."); }
        }
    }
}

