﻿// Created with MonoDevelop
//
//    Copyright (C) 2019 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;


namespace Hiperscan.SGS
{
    public class SgsException : System.Exception
    {
        internal SgsException(string message, IDevice device) : base(message)
        {
            this.Device = device;
            Console.WriteLine("SGSException: " + message);
        }

        internal SgsException(string message, IDevice device, Exception ex) : base(message, ex)
        {
            this.Device = device;
            Console.WriteLine("SGSException: " + message);
        }

        public IDevice Device { get; private set; }
    }

    public class InvalidSpectrumException : SgsException
    {
        internal InvalidSpectrumException(string msg, IDevice dev) : base(msg, dev)
        {
        }
    }

    public class InvalidLUTException : SgsException
    {
        internal InvalidLUTException(string msg, IDevice dev) : base(msg, dev)
        {
        }
    }
}