// application.cpp created with Microsoft Visual Studio
// User: klose at 11:02 06.05.2013
// CVS release: $Id$
//
//    Example application for HCL's C interface
//    Copyright (C) 2014-2017 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the				
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <stdio.h>
#include <tchar.h>
#include <windows.h>

#include "hcl.h"

#define MAX_PARAM_LEN      100
#define MAX_MSG_LEN        2000
#define MAX_STACKTRACE_LEN 2000
#define SPECTRUM_LEN       901
#define SEQUENCE_COUNT     10
//#define CLOSE_AND_SLEEP

#define CHKRESULT(_FUNC)         \
{                                \
	int _result = _FUNC;         \
	if (_result != 0)            \
    {                            \
	  if (_result != HCL_ENODEV) \
	    finder_close();          \
	  print_error_msg(_result);  \
	  return _result;            \
    }                            \
}

#if defined HIPERSCAN
#	define PID PID_HIPERSCAN_SGS1900
#elif defined PHARMATEST
#   define PID PID_PHARMATEST_PTIM100
#else
#   define PID PID_PHARMATEST_PTIM100
#endif



void external_white_callback()
{
	fprintf(stderr, "Please apply external white reference.\n");
	fprintf(stderr, "Press ENTER...\n");
	getchar();
}

void external_dark_callback()
{
	fprintf(stderr, "Please apply external dark reference.\n");
	fprintf(stderr, "Press ENTER...\n");
	getchar();
}

void external_transflectance_callback()
{
	fprintf(stderr, "Please apply external transflectance reference.\n");
	fprintf(stderr, "Press ENTER...\n");
	getchar();
}

void external_fluid_callback()
{
	fprintf(stderr, "Please apply liquid sample.\n");
	fprintf(stderr, "Press ENTER...\n");
	getchar();
}

void temperature_status_changed_callback(char* json_string)
{
	fprintf(stderr, json_string);
	fprintf(stderr, "\n");
}

void print_error_msg(int result)
{
	char msg[MAX_MSG_LEN];
	finder_get_last_error(msg, MAX_MSG_LEN);
	fprintf(stderr, "Error code: %i\n", result);
	fprintf(stderr, "Error message from HCL: %s\n", msg);
	char stacktrace[MAX_STACKTRACE_LEN];
	finder_get_last_stacktrace(stacktrace, MAX_STACKTRACE_LEN);
	fprintf(stderr, "Stacktrace from HCL: %s\n", stacktrace);
	fprintf(stderr, "Press ENTER...\n");
	getchar();
}

int wait_for_device()
{
	int device_status = HCL_FINDER_STATUS_UNKNOWN;
	double progress, eta_seconds, temperature, temperature_gradient;
	int result;
	while (true)
	{
		CHKRESULT(result = finder_get_status(&device_status, &progress, &eta_seconds, &temperature, &temperature_gradient));

		if (temperature_gradient > 1e10)
			fprintf(stderr, "Waiting for instrument. Status: %i, Progress: %.1f, ETA: %.1f, Temp: %.1f\n", device_status, progress, eta_seconds, temperature);
		else
			fprintf(stderr, "Waiting for instrument. Status: %i, Progress: %.1f, ETA: %.1f, Temp: %.1f/%.1f\n", device_status, progress, eta_seconds, temperature, temperature_gradient);

		if (device_status == HCL_FINDER_STATUS_IDLE)
			break;

		Sleep(3000);
	}
	return result;
}

void save_spectrum(char fname[], double wavelength[], double reflectance[])
{
	FILE* file = fopen(fname, "w");
	for (int ix = 0; ix < SPECTRUM_LEN; ++ix)
	{
		fprintf(file, "%f,%f\n", wavelength[ix], reflectance[ix]);
	}
	fclose(file);
}

void save_reference_data(char fname[], unsigned char data[], int len)
{
	FILE* file = fopen(fname, "wb");
	fwrite(data, sizeof(unsigned char), len, file);
	fclose(file);
}

unsigned char* load_reference_data(char fname[], int* len)
{
	struct stat info;
	stat(fname, &info);
	unsigned char* data = (unsigned char*)malloc(info.st_size * sizeof(unsigned char));
	FILE* file = fopen(fname, "rb");
	fread(data, sizeof(unsigned char), info.st_size, file);
	fclose(file);
	*len = info.st_size;
	return data;
}
	
int _tmain(int argc, _TCHAR* argv[])
{
	fprintf(stderr, "Instrument driver name: %s\n", hcl_get_driver_name());
	fprintf(stderr, "Instrument driver version: %s\n", hcl_get_driver_version());
	fprintf(stderr, "Instrument driver location: %s\n", hcl_get_driver_location());

		CHKRESULT(finder_open(PID));

	finder_set_query_external_white_reference_callback(external_white_callback);
	finder_set_query_external_dark_reference_callback(external_dark_callback);
	finder_set_query_external_transflectance_reference_callback(external_transflectance_callback);
	finder_set_query_external_transflectance_specimen_callback(external_fluid_callback);

	char json_buffer[10000];
	finder_set_temperature_status_callback(temperature_status_changed_callback, json_buffer, 10000);

	/*	
	CHKRESULT(finder_sleep());

	Sleep(10000);

	CHKRESULT(finder_close());
	CHKRESULT(finder_open(PID));
	*/

	// ************************************************************
	// use this settings for debugging/testing only!
	CHKRESULT(finder_set_param("Average", "50"));
	CHKRESULT(finder_set_param("RegressionCount", "50"));
	CHKRESULT(finder_set_param("AutoReferencing", "true"));
	CHKRESULT(finder_set_param("AutoRecalibrationMode", "Never"));
	//CHKRESULT(finder_set_param("RecalibrationTimeoutMilliseconds", "50000"));
	CHKRESULT(finder_set_param("ReferenceTimeoutMilliseconds", "600000"));
	//CHKRESULT(finder_set_param("AutoAcquisitionSequence", "false"));
	// ************************************************************

	char param[MAX_PARAM_LEN];	
	CHKRESULT(finder_get_param("ReleaseTemperature", MAX_PARAM_LEN, param));
	fprintf(stderr, "Read parameter \"ReleaseTemperature\": %s\n", param);

	CHKRESULT(finder_get_param("Average", MAX_PARAM_LEN, param));
	fprintf(stderr, "Read parameter \"Average\": %s\n", param);

	CHKRESULT(finder_get_param("WavelengthMin", MAX_PARAM_LEN, param));
	fprintf(stderr, "Read parameter \"WavelengthMin\": %s\n", param);

	CHKRESULT(finder_get_param("WavelengthMax", MAX_PARAM_LEN, param));
	fprintf(stderr, "Read parameter \"WavelengthMax\": %s\n", param);

	CHKRESULT(finder_get_param("AverageMin", MAX_PARAM_LEN, param));
	fprintf(stderr, "Read parameter \"AverageMin\": %s\n", param);

	CHKRESULT(finder_get_param("AverageMax", MAX_PARAM_LEN, param));
	fprintf(stderr, "Read parameter \"AverageMax\": %s\n", param);

	CHKRESULT(finder_get_param("WavelengthInc", MAX_PARAM_LEN, param));
	fprintf(stderr, "Read parameter \"WavelengthInc\": %s\n", param);

	CHKRESULT(finder_get_param("AutoReferencing", MAX_PARAM_LEN, param));
	fprintf(stderr, "Read parameter \"AutoReferencing\": %s\n", param);

	CHKRESULT(finder_get_param("AutoRecalibrationMode", MAX_PARAM_LEN, param));
	fprintf(stderr, "Read parameter \"AutoRecalibrationMode\": %s\n", param);

	CHKRESULT(finder_get_param("ReferenceTimeoutMilliseconds", MAX_PARAM_LEN, param));
	fprintf(stderr, "Read parameter \"ReferenceTimeoutMilliseconds\": %s\n", param);

	CHKRESULT(finder_get_param("RecalibrationTimeoutMilliseconds", MAX_PARAM_LEN, param));
	fprintf(stderr, "Read parameter \"RecalibrationTimeoutMilliseconds\": %s\n", param);

	CHKRESULT(finder_get_param("PlausibilityChecks", MAX_PARAM_LEN, param));
	fprintf(stderr, "Read parameter \"PlausibilityChecks\": %s\n", param);

	CHKRESULT(finder_get_param("AutoSaveReferences", MAX_PARAM_LEN, param));
	fprintf(stderr, "Read parameter \"AutoSaveReferences\": %s\n", param);

	CHKRESULT(finder_get_param("AutoSaveDirectory", MAX_PARAM_LEN, param));
	fprintf(stderr, "Read parameter \"AutoSaveDirectory\": %s\n", param);

	CHKRESULT(finder_get_param("LightSourceTimeoutMinutes", MAX_PARAM_LEN, param));
	fprintf(stderr, "Read parameter \"LightSourceTimeoutMinutes\": %s\n", param);

	char serial[MAX_PARAM_LEN];
	CHKRESULT(finder_get_param("SerialNumber", MAX_PARAM_LEN, serial));
	fprintf(stderr, "Read parameter \"SerialNumber\": %s\n", serial);

	CHKRESULT(finder_get_param("OemSerialNumber", MAX_PARAM_LEN, param));
	fprintf(stderr, "Read parameter \"OemSerialNumber\": %s\n", param);

	CHKRESULT(finder_get_param("Name", MAX_PARAM_LEN, param));
	fprintf(stderr, "Read parameter \"Name\": %s\n", param);

	char firmware_version[MAX_PARAM_LEN];
	CHKRESULT(finder_get_param("FirmwareVersion", MAX_PARAM_LEN, firmware_version));
	fprintf(stderr, "Read parameter \"FirmwareVersion\": %s\n", firmware_version);

	float spectral_resolution;
	CHKRESULT(finder_get_param("SpectralResolution", MAX_PARAM_LEN, param));
	fprintf(stderr, "Read parameter \"SpectralResolution\": %s\n", param);
	sscanf(param, "%f", &spectral_resolution);

	CHKRESULT(finder_get_param("Peripheral", MAX_PARAM_LEN, param));
	fprintf(stderr, "Read parameter \"Peripheral\": %s\n", param);
	bool sample_spinner = strcmp(param, "SampleSpinner") == 0;

	CHKRESULT(finder_get_param("BoardVersion", MAX_PARAM_LEN, param));
	fprintf(stderr, "Read parameter \"BoardVersion\": %s\n", param);

	CHKRESULT(finder_get_param("CasingVersion", MAX_PARAM_LEN, param));
	fprintf(stderr, "Read parameter \"CasingVersion\": %s\n", param);

	CHKRESULT(finder_get_param("LightSourceVersion", MAX_PARAM_LEN, param));
	fprintf(stderr, "Read parameter \"LightSourceVersion\": %s\n", param);

	CHKRESULT(finder_get_param("ReferenceWheelVersion", MAX_PARAM_LEN, param));
	fprintf(stderr, "Read parameter \"ReferenceWheelVersion\": %s\n", param);

	CHKRESULT(finder_get_param("ExternalDarkReferenceTimestamp", MAX_PARAM_LEN, param));
	fprintf(stderr, "Read parameter \"ExternalDarkReferenceTimestamp\": %s\n", param);

	CHKRESULT(finder_get_param("ExternalWhiteReferenceTimestamp", MAX_PARAM_LEN, param));
	fprintf(stderr, "Read parameter \"ExternalWhiteReferenceTimestamp\": %s\n", param);

	CHKRESULT(finder_get_param("RecalibrationTimestamp", MAX_PARAM_LEN, param));
	fprintf(stderr, "Read parameter \"RecalibrationTimestamp\": %s\n", param);

	CHKRESULT(finder_get_param("LightSourcePowerCycleCount", MAX_PARAM_LEN, param));
	fprintf(stderr, "Read parameter \"LightSourcePowerCycleCount\": %s\n", param);

	CHKRESULT(finder_get_param("LightSourceOperatingTimeMinutes", MAX_PARAM_LEN, param));
	fprintf(stderr, "Read parameter \"LightSourceOperatingTimeMinutes\": %s\n", param);

	CHKRESULT(finder_get_param("SystemOperatingTimeMinutes", MAX_PARAM_LEN, param));
	fprintf(stderr, "Read parameter \"SystemOperatingTimeMinutes\": %s\n", param);

	CHKRESULT(finder_get_param("SampleSpinnerAcquisitionCount", MAX_PARAM_LEN, param));
	fprintf(stderr, "Read parameter \"SampleSpinnerAcquisitionCount\": %s\n", param);

	CHKRESULT(finder_get_param("AutoAcquisitionSequence", MAX_PARAM_LEN, param));
	fprintf(stderr, "Read parameter \"AutoAcquisitionSequence\": %s\n", param);

	CHKRESULT(finder_get_param("InstrumentType", MAX_PARAM_LEN, param));
	fprintf(stderr, "Read parameter \"InstrumentType\": %s\n", param);

	CHKRESULT(finder_init());
	CHKRESULT(wait_for_device());

	fprintf(stderr, "Press ENTER to start update recalibration...\n");
	getchar();
	CHKRESULT(finder_update_recalibration());
	CHKRESULT(wait_for_device());

	fprintf(stderr, "Press ENTER to start external reference measurements...\n");
	getchar();

	CHKRESULT(finder_acquire_external_references());

	fprintf(stderr, "Getting spectral reference data...\n");
	unsigned char ref_data[HCL_MAX_REFERENCE_DATA_LEN];
	int ref_data_len;
	CHKRESULT(finder_get_reference_data(HCL_MAX_REFERENCE_DATA_LEN, ref_data, &ref_data_len));
	fprintf(stderr, "Read %i bytes.\n", ref_data_len);
	fprintf(stderr, "Saving reference data...\n");
	save_reference_data("reference-data.bin", ref_data, ref_data_len);

	int saved_ref_data_len;
	fprintf(stderr, "Loading reference data...\n");
	unsigned char* saved_ref_data = load_reference_data("reference-data.bin", &saved_ref_data_len);

	fprintf(stderr, "Setting spectral reference data...\n");
	CHKRESULT(finder_set_reference_data(saved_ref_data, saved_ref_data_len));

	fprintf(stderr, "Press ENTER to start reflectance measurement...\n");
	getchar();

	double wavelength[SPECTRUM_LEN];
	double reflectance[SPECTRUM_LEN];
	CHKRESULT(finder_acquire_reflectance_spectrum(SPECTRUM_LEN, wavelength, reflectance));
	save_spectrum("reflectance.csv", wavelength, reflectance);

	fprintf(stderr, "Press ENTER to start intensity measurement...\n");
	getchar();

	double intensity[SPECTRUM_LEN];
	CHKRESULT(finder_acquire_reflectance_intensity_spectrum(SPECTRUM_LEN, wavelength, reflectance, intensity));
	save_spectrum("reflectance2.csv", wavelength, reflectance);
	save_spectrum("intensity2.csv", wavelength, intensity);

	if (sample_spinner == false)
	{
		fprintf(stderr, "Press ENTER to start transflectance measurement...\n");
		getchar();

		double transflectance[SPECTRUM_LEN];
		CHKRESULT(finder_acquire_transflectance_spectrum(SPECTRUM_LEN, wavelength, transflectance));
		save_spectrum("transflectance1.csv", wavelength, transflectance);

		Sleep(1000);

		fprintf(stderr, "Repeating measurement using the same transflectance reference...\n");
		finder_require_transflectance_reference(0);
		CHKRESULT(finder_acquire_transflectance_spectrum(SPECTRUM_LEN, wavelength, transflectance));
		save_spectrum("transflectance2.csv", wavelength, transflectance);

		CHKRESULT(finder_get_transflectance_reference_spectrum(SPECTRUM_LEN, wavelength, intensity));
		save_spectrum("transflectance_reference.csv", wavelength, intensity);
	}

	CHKRESULT(finder_get_white_spectrum(SPECTRUM_LEN, wavelength, intensity));
	save_spectrum("white_reference.csv", wavelength, intensity);

	CHKRESULT(finder_get_dark_spectrum(SPECTRUM_LEN, wavelength, intensity));
	save_spectrum("dark_reference.csv", wavelength, intensity);

	fprintf(stderr, "Press ENTER to start reflectance sequence measurement...\n");
	getchar();

	CHKRESULT(finder_set_param("AutoAcquisitionSequence", "false"));
	CHKRESULT(finder_get_param("AutoAcquisitionSequence", MAX_PARAM_LEN, param));
	fprintf(stderr, "Read parameter \"AutoAcquisitionSequence\": %s\n", param);

	CHKRESULT(finder_begin_acquisition_sequence());
	for (int acquisition_count = 0; acquisition_count < SEQUENCE_COUNT; ++acquisition_count)
	{
		fprintf(stderr, "Acquiring spectrum %i of %i...\n", acquisition_count + 1, SEQUENCE_COUNT);
		CHKRESULT(finder_acquire_reflectance_spectrum(SPECTRUM_LEN, wavelength, reflectance));
		char fname[30];
		sprintf(fname, "reflectance-sequence-%i.csv", acquisition_count);
		save_spectrum(fname, wavelength, reflectance);
	}
	CHKRESULT(finder_end_acquisition_sequence());

	CHKRESULT(finder_get_param("ExternalDarkReferenceTimestamp", MAX_PARAM_LEN, param));
	fprintf(stderr, "Read parameter \"ExternalDarkReferenceTimestamp\": %s\n", param);

	CHKRESULT(finder_get_param("ExternalWhiteReferenceTimestamp", MAX_PARAM_LEN, param));
	fprintf(stderr, "Read parameter \"ExternalWhiteReferenceTimestamp\": %s\n", param);

	CHKRESULT(finder_get_param("RecalibrationTimestamp", MAX_PARAM_LEN, param));
	fprintf(stderr, "Read parameter \"RecalibrationTimestamp\": %s\n", param);

	fprintf(stderr, "Press ENTER to close device...\n");
	getchar();

#ifdef CLOSE_AND_SLEEP
	CHKRESULT(finder_close_and_sleep());
#else
	CHKRESULT(finder_close());
#endif

	fprintf(stderr, "Writing CSV spectrum file...\n");
	hcl_spectrum_t spectrum;
	hcl_spectrum_init(&spectrum, SPECTRUM_LEN);
	spectrum.average_count = 500;

	spectrum.creator = (char*)malloc(strlen("Test application") + 1);
	sprintf(spectrum.creator, "Test application");

	spectrum.label = (char*)malloc(strlen("Test spectrum") + 1);
	sprintf(spectrum.label, "Test spectrum");

	spectrum.probe_type = HCL_SPECTRUM_PROBE_TYPE_FINDER_STANDARD;
	spectrum.spectrum_type = HCL_SPECTRUM_TYPE_PROCESSED_SOLID;

	spectrum.serial_number = (char*)malloc(strlen(serial) + 1);
	sprintf(spectrum.serial_number, "%s", serial);

	spectrum.firmware_version = (char*)malloc(strlen(firmware_version) + 1);
	sprintf(spectrum.firmware_version, "%s", firmware_version);

	spectrum.timestamp = 123456;
	spectrum.spectral_resolution = spectral_resolution;

	spectrum.wavelength = (double*)malloc(sizeof(double) * spectrum.length);
	memcpy(spectrum.wavelength, wavelength, spectrum.length * sizeof(double));

	spectrum.reflectance = (double*)malloc(sizeof(double) * spectrum.length);
	memcpy(spectrum.reflectance, reflectance, spectrum.length * sizeof(double));

	CHKRESULT(hcl_spectrum_write_csv("spectrum.csv", &spectrum, NULL, 0));

	fprintf(stderr, "Writing checksumed CSV spectrum file...\n");
	byte salt[5];
	salt[0] = 2;
	salt[1] = 6;
	salt[2] = 56;
	salt[3] = 123;
	salt[4] = 34;

	CHKRESULT(hcl_spectrum_write_csv("spectrum_checksumed.csv", &spectrum, salt, 5));

	fprintf(stderr, "Checking integrity...\n");
	int check_result;
	CHKRESULT(hcl_spectrum_check_integrity("spectrum_checksumed.csv", salt, 5, &check_result));

	fprintf(stderr, "Result of integrity check: %i\n", check_result);

	fprintf(stderr, "Reading CSV spectrum file...\n");
	hcl_spectrum_t spectrum2;
	CHKRESULT(hcl_spectrum_read_csv("spectrum.csv", &spectrum2));

	hcl_spectrum_t* spectra[2];
	spectra[0] = &spectrum;
	spectra[1] = &spectrum2;

	fprintf(stderr, "Writing Container file...\n");
	CHKRESULT(hcl_spectrum_write_container("spectra.csvc", spectra, 2, NULL, 0));

	fprintf(stderr, "Checking integrity...\n");
	if (hcl_spectrum_check_integrity("spectra.csvc", salt, 5, &check_result) != 0)
		fprintf(stderr, "Check failed with error, which is expected due to the missing checksum.\n");
	else
		fprintf(stderr, "Result of integrity check: %i\n", check_result);

	fprintf(stderr, "Writing checksumed Container file...\n");
	CHKRESULT(hcl_spectrum_write_container("spectra_checksumed.csvc", spectra, 2, salt, 5));

	fprintf(stderr, "Checking integrity...\n");
	CHKRESULT(hcl_spectrum_check_integrity("spectra_checksumed.csvc", salt, 5, &check_result));
	fprintf(stderr, "Result of integrity check: %i\n", check_result);

	fprintf(stderr, "Reading Container file...\n");
	int spectrum_count;
	hcl_spectrum_t** spectra2;
	CHKRESULT(hcl_spectrum_read_container("spectra_checksumed.csvc", &spectra2, &spectrum_count));
	fprintf(stderr, "Read Container with %i spectra.\n", spectrum_count);

	fprintf(stderr, "Freeing resources claimed by spectral data...\n");
	hcl_spectrum_free(&spectrum);
	hcl_spectrum_free(&spectrum2);

	for (int ix = 0; ix < spectrum_count; ++ix)
	{
		hcl_spectrum_free(spectra2[ix]);
	}


	fprintf(stderr, "Done.\nPress ENTER...\n");
	getchar();
	return 0;
}
