// application_sgs.cpp created with Microsoft Visual Studio
// User: klose at 17:21 10.10.2014
// CVS release: $Id$
//
//    Example application for HCL's C interface
//    Copyright (C) 2014 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the				
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <stdio.h>
#include <tchar.h>
#include <windows.h>

#include "hcl.h"

#define CHKRESULT(_FUNC)         \
{                                \
	int _result = _FUNC;         \
	if (_result != 0)            \
    {                            \
	  if (_result != HCL_ENODEV) \
	    sgs_close();             \
	  print_error_msg(_result);  \
	  return _result;            \
    }                            \
}

#define CHKSCAN(_FUNC)               \
{                                    \
	fprintf(stderr, "Scanning..."); \
	int _result = _FUNC;              \
	fprintf(stderr, " Done.\n\n");     \
	if (_result != 0)                \
    {                                \
	  if (_result != HCL_ENODEV)     \
	    sgs_close();                 \
	  print_error_msg(_result);      \
	  return _result;                \
    }                                \
}


void print_error_msg(int result)
{
	char msg[HCL_MAX_MSG_LEN];
	sgs_get_last_error(msg, HCL_MAX_MSG_LEN);
	fprintf(stderr, "Error code: %i\n", result);
	fprintf(stderr, "Error message from HCL: %s\n", msg);
	char stacktrace[HCL_MAX_STACKTRACE_LEN];
	sgs_get_last_stacktrace(stacktrace, HCL_MAX_STACKTRACE_LEN);
	fprintf(stderr, "Stacktrace from HCL: %s\n", stacktrace);
	fprintf(stderr, "Press ENTER...\n");
	getchar();
}

void save_spectrum(char fname[], double wavelength[], double reflectance[])
{
	FILE* file = fopen(fname, "w");
	for (int ix = 0; ix < HCL_DEFAULT_SPECTRUM_LEN; ++ix)
	{
		fprintf(file, "%f,%f\n", wavelength[ix], reflectance[ix]);
	}
	fclose(file);
}
	
int _tmain(int argc, _TCHAR* argv[])		
{
	CHKRESULT(sgs_open(PID_HIPERSCAN_SGS1900));

	CHKRESULT(sgs_set_param("Average", "500"));

	char param[HCL_MAX_PARAM_LEN];	
	CHKRESULT(sgs_get_param("Average", HCL_MAX_PARAM_LEN, param));
	fprintf(stderr, "Read parameter \"Average\": %s\n", param);

	CHKRESULT(sgs_get_param("WavelengthMin", HCL_MAX_PARAM_LEN, param));
	fprintf(stderr, "Read parameter \"WavelengthMin\": %s\n", param);

	CHKRESULT(sgs_get_param("WavelengthMax", HCL_MAX_PARAM_LEN, param));
	fprintf(stderr, "Read parameter \"WavelengthMax\": %s\n", param);

	CHKRESULT(sgs_get_param("AverageMin", HCL_MAX_PARAM_LEN, param));
	fprintf(stderr, "Read parameter \"AverageMin\": %s\n", param);

	CHKRESULT(sgs_get_param("AverageMax", HCL_MAX_PARAM_LEN, param));
	fprintf(stderr, "Read parameter \"AverageMax\": %s\n", param);

	CHKRESULT(sgs_get_param("WavelengthInc", HCL_MAX_PARAM_LEN, param));
	fprintf(stderr, "Read parameter \"WavelengthInc\": %s\n", param);

	fprintf(stderr, "Press ENTER...\n");
	getchar();

	fprintf(stderr, "Please prepare device for dark intensity scan (i.e. deactivate light source).\n");
	fprintf(stderr, "Press ENTER...");
	getchar();
	CHKSCAN(sgs_acquire_dark_intensity());

	fprintf(stderr, "Please prepare device for reference scan (i.e. light source only).\n");
	fprintf(stderr, "Press ENTER...");
	getchar();
	CHKSCAN(sgs_acquire_reference_spectrum());

	double wavelength[HCL_DEFAULT_SPECTRUM_LEN];

	fprintf(stderr, "Please prepare device for intensity scan.\n");
	fprintf(stderr, "Press ENTER...");
	getchar();
	double intensity[HCL_DEFAULT_SPECTRUM_LEN];
	CHKSCAN(sgs_acquire_intensity_spectrum(HCL_DEFAULT_SPECTRUM_LEN, wavelength, intensity));
	save_spectrum("intensity.csv", wavelength, intensity);

	fprintf(stderr, "Please prepare device for reflectance scan.\n");
	fprintf(stderr, "Press ENTER...");
	getchar();
	double reflectance[HCL_DEFAULT_SPECTRUM_LEN];
	CHKSCAN(sgs_acquire_reflectance_spectrum(HCL_DEFAULT_SPECTRUM_LEN, wavelength, reflectance));
	save_spectrum("reflectance.csv", wavelength, reflectance);

	fprintf(stderr, "Please prepare device for absorbance scan.\n");
	fprintf(stderr, "Press ENTER...");
	getchar();
	double absorbance[HCL_DEFAULT_SPECTRUM_LEN];
	CHKSCAN(sgs_acquire_absorbance_spectrum(HCL_DEFAULT_SPECTRUM_LEN, wavelength, absorbance));
	save_spectrum("absorbance.csv", wavelength, absorbance);

	sgs_get_reference_spectrum(HCL_DEFAULT_SPECTRUM_LEN, wavelength, intensity);
	save_spectrum("reference.csv", wavelength, intensity);

	double dark_intensity;
	double dark_intensity_sigma;
	sgs_get_dark_intensity(&dark_intensity, &dark_intensity_sigma);
	fprintf(stderr, "Dark intensity: %f\n", dark_intensity);
	fprintf(stderr, "Dark intensity sigma: %f\n", dark_intensity_sigma);

	CHKRESULT(sgs_close());;

	fprintf(stderr, "\nDone.\nPress ENTER to quit program...");
	getchar();
	return 0;
}
