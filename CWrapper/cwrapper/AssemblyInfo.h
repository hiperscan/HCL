#pragma once

#define ASSEMBLY_COPYRIGHT    L"Copyright (c) Hiperscan GmbH 2014-2020"
#define ASSEMBLY_DESCRIPTION  L"C-Wrapper for Hiperscan Class Library (HCL)"
#define ASSEMBLY_COMPANY      L"Hiperscan GmbH"
#define ASSEMBLY_PRODUCT_NAME L"Hiperscan Class Library"
#define INTERNAL_NAME         L"cwrapper"

//CLR assembly version
#define FILE_VERSION     3,1,0,0
#define FILE_VERSION_STR L"3.1.0"
#define ASSEMBLY_VERSION L"3.1.0.*"
