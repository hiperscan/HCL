#include "stdafx.h"

using namespace System;
using namespace System::Reflection;
using namespace System::Runtime::CompilerServices;
using namespace System::Runtime::InteropServices;
using namespace System::Security::Permissions;

#include "AssemblyInfo.h"

//
// Allgemeine Informationen �ber eine Assembly werden �ber die folgenden
// Attribute gesteuert. �ndern Sie diese Attributwerte, um die Informationen zu �ndern,
// die mit einer Assembly verkn�pft sind.
//

[assembly:AssemblyTitleAttribute(INTERNAL_NAME)];
[assembly:AssemblyDescriptionAttribute(ASSEMBLY_DESCRIPTION)];
[assembly:AssemblyConfigurationAttribute(L"")];
[assembly:AssemblyCompanyAttribute(ASSEMBLY_COMPANY)];
[assembly:AssemblyProductAttribute(ASSEMBLY_PRODUCT_NAME)];
[assembly:AssemblyCopyrightAttribute(ASSEMBLY_COPYRIGHT)];
[assembly:AssemblyTrademarkAttribute(L"")];
[assembly:AssemblyCultureAttribute(L"")];

[assembly:AssemblyVersionAttribute(ASSEMBLY_VERSION)];

[assembly:ComVisible(false)];

[assembly:CLSCompliantAttribute(true)];
