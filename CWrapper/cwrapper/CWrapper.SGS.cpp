#include "stdafx.h"
#include "hcl.h"

#using <mscorlib.dll>
#using "CInterface.dll"

using namespace Hiperscan::CInterface;


int sgs_open(unsigned int product_id)
{
	return sgs_open2(VID_FTDI, product_id);
}

int sgs_open2(unsigned int vendor_id, unsigned int product_id)
{
	return SGS::API::Open(vendor_id, product_id);
}

int sgs_close()
{
	return SGS::API::Close();
}

int sgs_set_param(char param[], char val[])
{
	return SGS::API::SetParam(gcnew System::String(param), gcnew System::String(val));
}

int sgs_get_param(char param[], int maxlen, char val[])
{
	System::String ^_val;
	int result = SGS::API::GetParam(gcnew System::String(param), _val);

	if (_val->Length >= maxlen)
	{
		SGS::API::ResetLastException();
		return HCL_EINVAL;
	}

	int ix;
	for (ix=0; ix < _val->Length; ++ix)
	{
		val[ix] = _val[ix];
	}
	val[ix] = '\0';

	return result;
}

int sgs_acquire_intensity_spectrum(int len, double wavelength[], double intensity[])
{
	cli::array<double,1> ^w = gcnew cli::array<double,1>(len);
	cli::array<double,1> ^i = gcnew cli::array<double,1>(len);

	int result = SGS::API::AcquireIntensitySpectrum(len, w, i);
	if (result != 0)
		return result;

	for (int ix=0; ix < len; ++ix)
	{
		wavelength[ix] = w[ix];
		intensity[ix]  = i[ix];
	}

	return 0;
}

int sgs_acquire_reflectance_spectrum(int len, double wavelength[], double reflectance[])
{
	cli::array<double,1> ^w = gcnew cli::array<double,1>(len);
	cli::array<double,1> ^r = gcnew cli::array<double,1>(len);

	int result = SGS::API::AcquireReflectanceSpectrum(len, w, r);
	if (result != 0)
		return result;

	for (int ix=0; ix < len; ++ix)
	{
		wavelength[ix]  = w[ix];
		reflectance[ix] = r[ix];
	}

	return 0;
}

int sgs_acquire_absorbance_spectrum(int len, double wavelength[], double absorbance[])
{
	cli::array<double,1> ^w = gcnew cli::array<double,1>(len);
	cli::array<double,1> ^a = gcnew cli::array<double,1>(len);

	int result = SGS::API::AcquireAbsorbanceSpectrum(len, w, a);
	if (result != 0)
		return result;

	for (int ix=0; ix < len; ++ix)
	{
		wavelength[ix] = w[ix];
		absorbance[ix] = a[ix];
	}

	return 0;
}

int sgs_acquire_reference_spectrum()
{
	return SGS::API::AcquireReferenceSpectrum();
}

int sgs_acquire_dark_intensity()
{
	return SGS::API::AcquireDarkIntensity();
}

int sgs_get_reference_spectrum(int len, double wavelength[], double intensity[])
{
	cli::array<double,1> ^w = gcnew cli::array<double,1>(len);
	cli::array<double,1> ^r = gcnew cli::array<double,1>(len);

	int result = SGS::API::GetReferenceSpectrum(len, w, r);
	if (result != 0)
		return result;

	for (int ix=0; ix < len; ++ix)
	{
		wavelength[ix] = w[ix];
		intensity[ix] = r[ix];
	}

	return 0;
}


int sgs_get_dark_intensity(double *intensity, double *sigma)
{
	System::Double _val1;
	System::Double _val2;
	int result = SGS::API::GetDarkIntensity(_val1, _val2);
	*intensity = _val1;
	*sigma     = _val2;

	return result;
}
	

void sgs_get_last_error(char msg[], int maxlen)
{
	System::String ^result = SGS::API::GetLastError();

	int ix;
	for (ix=0; ix < result->Length && ix < maxlen-1; ++ix)
	{
		msg[ix] = result[ix];
	}
	msg[ix] = '\0';
}

void sgs_get_last_stacktrace(char stacktrace[], int maxlen)
{
	System::String ^result = SGS::API::GetLastStacktrace();

	int ix;
	for (ix=0; ix < result->Length && ix < maxlen-1; ++ix)
	{
		stacktrace[ix] = result[ix];
	}
	stacktrace[ix] = '\0';
}