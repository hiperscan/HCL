// cwrapper.cpp created with Microsoft Visual Studio
// User: klose at 11:02 06.05.2013
// CVS release: $Id$
//
//    HCL: Hiperscan Class Library
//    Copyright (C) 2009-2014 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include "stdafx.h"
#include "hcl.h"

#include <stdio.h>
#include <windows.h>

#using <mscorlib.dll>
#using <System.dll>
#using "CInterface.dll"

using namespace System;
using namespace System::Reflection;
using namespace System::Runtime::InteropServices;
using namespace System::IO;
using namespace System::Collections::Generic;

using namespace Hiperscan::CInterface;

static QUERY_CALLBACK finder_query_external_white_reference_callback;
static QUERY_CALLBACK finder_query_external_dark_reference_callback;
static QUERY_CALLBACK finder_query_external_transflectance_reference_callback;
static QUERY_CALLBACK finder_query_external_fluid_specimen_callback;
static TEMPERATURE_STATUS_CALLBACK finder_temperature_status_changed_callback;

static char* finder_temperature_status_buffer;
static int finder_temperature_status_json_string_maxlen;


void on_query_external_white_reference()
{
	finder_query_external_white_reference_callback();
}

int finder_set_query_external_white_reference_callback(QUERY_CALLBACK callback)
{
	finder_query_external_white_reference_callback = callback;
	Finder::API::QueryExternalWhiteReference += gcnew Finder::API::QueryExternalSampleHandler(on_query_external_white_reference);
	return 0;
}

void on_query_external_dark_reference()
{
	finder_query_external_dark_reference_callback();
}

int finder_set_query_external_dark_reference_callback(QUERY_CALLBACK callback)
{
	finder_query_external_dark_reference_callback = callback;
	Finder::API::QueryExternalDarkReference += gcnew Finder::API::QueryExternalSampleHandler(on_query_external_dark_reference);
	return 0;
}

void on_query_external_transflectance_reference()
{
	finder_query_external_transflectance_reference_callback();
}

int finder_set_query_external_transflectance_reference_callback(QUERY_CALLBACK callback)
{
	finder_query_external_transflectance_reference_callback = callback;
	Finder::API::QueryExternalTransflectanceReference += gcnew Finder::API::QueryExternalSampleHandler(on_query_external_transflectance_reference);
	return 0;
}
	
void on_query_external_fluid_specimen()
{
	finder_query_external_fluid_specimen_callback();
}

int finder_set_query_external_transflectance_specimen_callback(QUERY_CALLBACK callback)
{
	finder_query_external_fluid_specimen_callback = callback;
	Finder::API::QueryExternalFluidSpecimen += gcnew Finder::API::QueryExternalSampleHandler(on_query_external_fluid_specimen);
	return 0;
}


void on_temperature_status_changed(String^ json_status)
{
	int ix;
	for (ix = 0; ix < json_status->Length && ix < finder_temperature_status_json_string_maxlen - 1; ++ix)
	{
		finder_temperature_status_buffer[ix] = json_status[ix];
	}
	finder_temperature_status_buffer[ix] = '\0';

	finder_temperature_status_changed_callback(finder_temperature_status_buffer);
}

int finder_set_temperature_status_callback(TEMPERATURE_STATUS_CALLBACK callback, char temperature_status_buf[], int maxlen)
{
	finder_temperature_status_changed_callback = callback;
	finder_temperature_status_buffer = temperature_status_buf;
	finder_temperature_status_json_string_maxlen = maxlen;
	Finder::API::TemperatureStatusChanged += gcnew Finder::API::TemperatureStatusChangedHandler(on_temperature_status_changed);
	return 0;
}

int finder_open(unsigned int product_id)
{
	return finder_open2(VID_FTDI, product_id);
}

int finder_open2(unsigned int vendor_id, unsigned int product_id)
{
	char* version = hcl_get_driver_version();
	int result = Finder::API::Open(vendor_id, product_id, gcnew String(version));
	if (result == 0)
		SetThreadExecutionState(ES_CONTINUOUS | ES_SYSTEM_REQUIRED | ES_AWAYMODE_REQUIRED);
	return result;
}

int finder_init()
{
	return Finder::API::Init();
}

int finder_close()
{
	SetThreadExecutionState(ES_CONTINUOUS);
	return Finder::API::Close();
}

int finder_sleep()
{
	return Finder::API::Sleep();
}

int finder_set_param(char param[], char val[])
{
	return Finder::API::SetParam(gcnew String(param), gcnew String(val));
}

int finder_get_param(char param[], int maxlen, char val[])
{
	String ^_val;
	int result = Finder::API::GetParam(gcnew String(param), _val);

	if (_val->Length >= maxlen)
	{
		Finder::API::ResetLastException();
		return HCL_EINVAL;
	}

	int ix;
	for (ix=0; ix < _val->Length; ++ix)
	{
		val[ix] = _val[ix];
	}
	val[ix] = '\0';

	return result;
}

int finder_reset_light_source_stats()
{
	return Finder::API::ResetLightSourceStats();
}

int finder_get_status(int *status, double *progress, double *eta_seconds, double *temperature, double *temperature_gradient)
{
	Int32 _val1;
	Double _val2;
	Double _val3;
	Double _val4;
	Double _val5;

	int result = Finder::API::GetStatus(_val1, _val2, _val3, _val4, _val5);

	*status = _val1;
	*progress = _val2;
	*eta_seconds = _val3;
	*temperature = _val4;
	*temperature_gradient = _val5;
	return result;
}

int finder_update_recalibration()
{
	return Finder::API::UpdateRecalibration();
}

int finder_get_reference_data(int maxlen, unsigned char data[], int *len)
{
	cli::array<unsigned char, 1> ^d = gcnew cli::array<unsigned char, 1>(maxlen);
	Int32 _len;
	
	int result = Finder::API::GetReferenceData(maxlen, d, _len);
	if (result != 0)
		return result;

	*len = _len;

	for (int ix = 0; ix < maxlen; ++ix)
	{
		data[ix] = d[ix];
	}

	return result;
}

int finder_set_reference_data(unsigned char data[], int len)
{
	cli::array<unsigned char, 1> ^d = gcnew cli::array<unsigned char, 1>(len);

	for (int ix = 0; ix < len; ++ix)
	{
		d[ix] = data[ix];
	}

	return Finder::API::SetReferenceData(d, len);
}

int finder_begin_acquisition_sequence()
{
	return Finder::API::BeginAcquisitionSequence();
}

int finder_end_acquisition_sequence()
{
	return Finder::API::EndAcquisitionSequence();
}

int finder_acquire_reflectance_spectrum(int len, double wavelength[], double reflectance[])
{
	cli::array<double,1> ^w = gcnew cli::array<double,1>(len);
	cli::array<double,1> ^r = gcnew cli::array<double,1>(len);

	int result = Finder::API::AcquireReflectanceSpectrum(len, w, r);
	if (result != 0)
		return result;

	for (int ix=0; ix < len; ++ix)
	{
		wavelength[ix] = w[ix];
		reflectance[ix] = r[ix];
	}

	return result;
}

int finder_acquire_reflectance_intensity_spectrum(int len, double wavelength[], double reflectance[], double intensity[])
{
	cli::array<double, 1> ^w = gcnew cli::array<double,1>(len);
	cli::array<double, 1> ^r = gcnew cli::array<double,1>(len);
	cli::array<double, 1> ^i = gcnew cli::array<double,1>(len);

	int result = Finder::API::AcquireReflectanceSpectrum(len, w, r, i);
	if (result != 0)
		return result;

	for (int ix = 0; ix < len; ++ix)
	{
		wavelength[ix]  = w[ix];
		reflectance[ix] = r[ix];
		intensity[ix]   = i[ix];
	}

	return result;
}

int finder_acquire_transflectance_spectrum(int len, double wavelength[], double transflectance[])
{
	cli::array<double,1> ^w = gcnew cli::array<double,1>(len);
	cli::array<double,1> ^r = gcnew cli::array<double,1>(len);

	int result = Finder::API::AcquireTransflectanceSpectrum(len, w, r);
	if (result != 0)
		return result;

	for (int ix=0; ix < len; ++ix)
	{
		wavelength[ix] = w[ix];
		transflectance[ix] = r[ix];
	}

	return 0;
}

int finder_acquire_external_references()
{
	return Finder::API::AcquireExternalReferences();
}

int finder_get_white_spectrum(int len, double wavelength[], double intensity[])
{
	cli::array<double,1> ^w = gcnew cli::array<double,1>(len);
	cli::array<double,1> ^r = gcnew cli::array<double,1>(len);

	int result = Finder::API::GetWhiteSpectrum(len, w, r);
	if (result != 0)
		return result;

	for (int ix=0; ix < len; ++ix)
	{
		wavelength[ix] = w[ix];
		intensity[ix] = r[ix];
	}

	return 0;
}

int finder_get_dark_spectrum(int len, double wavelength[], double intensity[])
{
	cli::array<double,1> ^w = gcnew cli::array<double,1>(len);
	cli::array<double,1> ^r = gcnew cli::array<double,1>(len);

	int result = Finder::API::GetDarkSpectrum(len, w, r);
	if (result != 0)
		return result;

	for (int ix=0; ix < len; ++ix)
	{
		wavelength[ix] = w[ix];
		intensity[ix] = r[ix];
	}

	return 0;
}

int finder_get_transflectance_reference_spectrum(int len, double wavelength[], double intensity[])
{
	cli::array<double,1> ^w = gcnew cli::array<double,1>(len);
	cli::array<double,1> ^r = gcnew cli::array<double,1>(len);

	int result = Finder::API::GetTransflectanceReferenceSpectrum(len, w, r);
	if (result != 0)
		return result;

	for (int ix=0; ix < len; ++ix)
	{
		wavelength[ix] = w[ix];
		intensity[ix] = r[ix];
	}

	return 0;
}

void finder_get_last_error(char msg[], int maxlen)
{
	String ^result = Finder::API::GetLastError();

	int ix;
	for (ix=0; ix < result->Length && ix < maxlen-1; ++ix)
	{
		msg[ix] = result[ix];
	}
	msg[ix] = '\0';
}

void finder_get_last_stacktrace(char stacktrace[], int maxlen)
{
	String ^result = Finder::API::GetLastStacktrace();

	int ix;
	for (ix=0; ix < result->Length && ix < maxlen-1; ++ix)
	{
		stacktrace[ix] = result[ix];
	}
	stacktrace[ix] = '\0';
}

void finder_require_transflectance_reference(int param)
{
	Boolean require_transflectance_reference = (param != 0);
	Finder::API::RequireTransflectanceReference = require_transflectance_reference;
}

char* hcl_get_driver_version()
{
	String^ version = Assembly::GetExecutingAssembly()->GetName()->Version->ToString();
	IntPtr ptr = Marshal::StringToHGlobalAnsi(version);
	char* driver_version = static_cast<char*>(ptr.ToPointer());
	return driver_version;
}

char* hcl_get_driver_name()
{
	String^ version = Assembly::GetExecutingAssembly()->GetName()->ToString();
	IntPtr ptr = Marshal::StringToHGlobalAnsi(version);
	return static_cast<char*>(ptr.ToPointer());
}

char* hcl_get_driver_location()
{
	String^ version = Assembly::GetExecutingAssembly()->Location->ToString();
	IntPtr ptr = Marshal::StringToHGlobalAnsi(version);
	return static_cast<char*>(ptr.ToPointer());
}

void hcl_spectrum_init(hcl_spectrum_t* spectrum, int length)
{
	spectrum->length = length;
	
	spectrum->wavelength       = NULL;
	spectrum->intensity        = NULL;
	spectrum->reflectance      = NULL;
	spectrum->creator          = NULL;
	spectrum->comment          = NULL;
	spectrum->serial_number    = NULL;
	spectrum->firmware_version = NULL;
	spectrum->label            = NULL;
}

void hcl_spectrum_free(hcl_spectrum_t* spectrum)
{
	try
	{
		if (spectrum == NULL)
			return;

		if (spectrum->wavelength != NULL)
			free(spectrum->wavelength);
		if (spectrum->intensity != NULL)
			free(spectrum->intensity);
		if (spectrum->reflectance != NULL)
			free(spectrum->reflectance);
		if (spectrum->creator != NULL)
			free(spectrum->creator);
		if (spectrum->comment != NULL)
			free(spectrum->comment);
		if (spectrum->serial_number != NULL)
			free(spectrum->serial_number);
		if (spectrum->firmware_version != NULL)
			free(spectrum->firmware_version);
		if (spectrum->label != NULL)
			free(spectrum->label);
	}
	catch (...) {}
}

int hcl_spectrum_read_csv(char fname[], hcl_spectrum_t* spectrum)
{
	cli::array<double, 1> ^w;
	cli::array<double, 1> ^i;
	cli::array<double, 1> ^r;

	Int64 _timestamp;
	String ^_creator;
	String ^_comment;
	String ^_serial_number;
	String ^_firmware_version;
	String ^_label;
	Double _spectral_resolution;
	UInt32 _average_count;
	Int32  _probe_type;
	Int32  _spectrum_type;

	int result = Finder::API::ReadCsv(gcnew String(fname), w, i, r, _timestamp, _creator, _comment, _serial_number, _firmware_version, _label,
													 _spectral_resolution, _average_count, _probe_type, _spectrum_type);

	if (result != 0)
		return result;

	hcl_spectrum_init(spectrum, w->Length);

	spectrum->wavelength = new double[spectrum->length];

	if (i->Length == spectrum->length)
		spectrum->intensity = new double[spectrum->length];
	else
		spectrum->intensity = NULL;

	if (r->Length == spectrum->length)
		spectrum->reflectance = new double[spectrum->length];
	else
		spectrum->reflectance = NULL;

	for (int ix = 0; ix < spectrum->length; ++ix)
	{
		spectrum->wavelength[ix] = w[ix];

		if (spectrum->intensity != NULL)
			spectrum->intensity[ix] = i[ix];

		if (spectrum->reflectance != NULL)
			spectrum->reflectance[ix] = r[ix];
	}

	if (r->Length != spectrum->length)
		spectrum->reflectance = NULL;

	spectrum->timestamp = _timestamp;

	int ix;
	spectrum->creator = new char[_creator->Length + 1];
	for (ix = 0; ix < _creator->Length; ++ix)
	{
		spectrum->creator[ix] = _creator[ix];
	}
	spectrum->creator[ix] = '\0';

	spectrum->comment = new char[_comment->Length + 1];
	for (ix = 0; ix < _comment->Length; ++ix)
	{
		spectrum->comment[ix] = _comment[ix];
	}
	spectrum->comment[ix] = '\0';

	spectrum->serial_number = new char[_serial_number->Length + 1];
	for (ix = 0; ix < _serial_number->Length; ++ix)
	{
		spectrum->serial_number[ix] = _serial_number[ix];
	}
	spectrum->serial_number[ix] = '\0';

	spectrum->firmware_version = new char[_firmware_version->Length + 1];
	for (ix = 0; ix < _firmware_version->Length; ++ix)
	{
		spectrum->firmware_version[ix] = _firmware_version[ix];
	}
	spectrum->firmware_version[ix] = '\0';

	spectrum->label = new char[_label->Length + 1];
	for (ix = 0; ix < _label->Length; ++ix)
	{
		spectrum->label[ix] = _label[ix];
	}
	spectrum->label[ix] = '\0';

	spectrum->spectral_resolution = _spectral_resolution;
	spectrum->average_count = _average_count;

	spectrum->probe_type = _probe_type;
	spectrum->spectrum_type = _spectrum_type;

	return 0;
}

int hcl_spectrum_write_csv(char fname[], hcl_spectrum_t* spectrum, unsigned char salt[], int salt_len)
{
	cli::array<double, 1> ^w = gcnew cli::array<double, 1>(spectrum->length);
	cli::array<double, 1> ^i = gcnew cli::array<double, 1>(spectrum->intensity == NULL   ? 0 : spectrum->length);
	cli::array<double, 1> ^r = gcnew cli::array<double, 1>(spectrum->reflectance == NULL ? 0 : spectrum->length);
	
	for (int ix = 0; ix < spectrum->length; ++ix)
	{
		w[ix] = spectrum->wavelength[ix];

		if (spectrum->intensity != NULL)
			i[ix] = spectrum->intensity[ix];

		if (spectrum->reflectance != NULL)
			r[ix] = spectrum->reflectance[ix];
	}

	cli::array<byte, 1> ^s = gcnew cli::array<byte, 1>(salt == NULL ? 0 : salt_len);
	if (salt != NULL)
	{
		for (int ix = 0; ix < salt_len; ++ix)
		{
			s[ix] = salt[ix];
		}
	}

	Int64 _timestamp            = spectrum->timestamp;
	Double _spectral_resolution = spectrum->spectral_resolution;
	UInt32 _average_count       = spectrum->average_count;

	String ^_creator          = gcnew String(spectrum->creator);
	String ^_comment          = gcnew String(spectrum->comment);
	String ^_serial_number    = gcnew String(spectrum->serial_number);
	String ^_firmware_version = gcnew String(spectrum->firmware_version);
	String ^_label            = gcnew String(spectrum->label);

	UInt32 _probe_type    = spectrum->probe_type;
	UInt32 _spectrum_type = spectrum->spectrum_type;

	int result = Finder::API::WriteCsv(gcnew String(fname), spectrum->length, w, i, r, _timestamp, _creator, _comment, _serial_number, _firmware_version, _label, _spectral_resolution, _average_count, _probe_type, _spectrum_type, s);

	if (result != 0)
		return result;

	return 0;
}

int hcl_spectrum_check_integrity(char fname[], unsigned char salt[], int salt_len, int* result)
{
	cli::array<byte, 1> ^s = gcnew cli::array<byte, 1>(salt == NULL ? 0 : salt_len);
	if (salt != NULL)
	{
		for (int ix = 0; ix < salt_len; ++ix)
		{
			s[ix] = salt[ix];
		}
	}
	Int32 _val;

	int _result = Finder::API::CheckIntegrity(gcnew String(fname), s, _val);
	*result = _val;

	return _result;
}

int hcl_spectrum_write_container(char fname[], hcl_spectrum_t* spectra[], int count, unsigned char salt[], int salt_len)
{
	List<String^>^ tmp_fnames = gcnew List<String^>();
	int result;

	for (int ix = 0; ix < count; ++ix)
	{
		String^ tmp_fname = Path::GetTempFileName();
		tmp_fnames->Add(tmp_fname);

		char* _tmp_fname = new char[tmp_fname->Length + 1];
		
		int jx;
		for (jx = 0; jx < tmp_fname->Length; ++jx)
		{
			_tmp_fname[jx] = tmp_fname[jx];
		}
		_tmp_fname[jx] = '\0';

		result = hcl_spectrum_write_csv(_tmp_fname, spectra[ix], NULL, 0);

		free(_tmp_fname);

		if (result != 0)
			return result;
	}

	cli::array<byte, 1> ^s = gcnew cli::array<byte, 1>(salt == NULL ? 0 : salt_len);
	if (salt != NULL)
	{
		for (int ix = 0; ix < salt_len; ++ix)
		{
			s[ix] = salt[ix];
		}
	}

	result = Finder::API::WriteContainer(tmp_fnames, gcnew String(fname), s);

	for (int ix = 0; ix < tmp_fnames->Count; ++ix)
	{
		try
		{
			File::Delete(tmp_fnames[ix]);
		}
		catch (...) {}
	}

	return result;
}

int hcl_spectrum_read_container(char fname[], hcl_spectrum_t** spectra[], int* count)
{
	List<String^>^ tmp_fnames = gcnew List<String^>();

	int result = Finder::API::ReadContainer(gcnew String(fname), tmp_fnames);
	if (result != 0)
		return result;

	*spectra = new hcl_spectrum_t*[tmp_fnames->Count];

	for (int ix = 0; ix < tmp_fnames->Count; ++ix)
	{
		char* tmp_fname = new char[tmp_fnames[ix]->Length + 1];
		int jx;
		for (jx = 0; jx < tmp_fnames[ix]->Length; ++jx)
		{
			tmp_fname[jx] = (*tmp_fnames[ix])[jx];
		}
		tmp_fname[jx] = '\0';

		(*spectra)[ix] = new hcl_spectrum_t;
		result = hcl_spectrum_read_csv(tmp_fname, (*spectra)[ix]);
	}

	*count = tmp_fnames->Count;

	for (int ix = 0; ix < tmp_fnames->Count; ++ix)
	{
		try
		{
			File::Delete(tmp_fnames[ix]);
		}
		catch (...) {}
	}

	return result;
}
