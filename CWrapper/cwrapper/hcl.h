// hcl.h created with Microsoft Visual Studio
// User: klose at 11:02 06.05.2013
//
//    HCL: Hiperscan Class Library
//    Copyright (C) 2009-2020 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

/**
    \mainpage HCL C-wrapper API documentation

    Wrapper library for Hiperscan Class Library (HCL).

	HCL is a class library for accessing Hiperscan's Scanning Grating Spectrometers (SGS). It also provides means for handling spectral data (including file I/O) and basic functions for chemometrical applications.

    The library is easy to use. Have a look at this short example: application.cpp

	\see application_sgs.cpp
*/
/** \defgroup cwrapper C-wrapper
* This is a simple C-wrapper for Hiperscan Class Library (HCL). HCL is a class library for accessing Hiperscan's Scanning Grating Spectrometers (SGS). It also provides means for handling spectral data (including file I/O) and basic functions for chemometrical applications.
* This wrapper contains only a very small subset of HCL's capabilities. It can be extended by additional functions if necessary.
*
* Unlike the C# interface of HCL, this C-wrapper does not support USB hotplugging. Also only one device is supported by the current implementation.
* @{ */

#ifdef __cplusplus
extern "C" {
#endif

/** \defgroup errorcodes Error codes
* HCL Error codes are based on POSIX error codes. Custom extensions have values >= 1000.
* \see finder_get_last_error
* \see finder_get_last_stacktrace
* \see sgs_get_last_error
* \see sgs_get_last_stacktrace
* @{ */
#define HCL_EPERM		           1		/**< Operation not permitted */
#define HCL_ENOENT		           2		/**< No such file or directory */
#define HCL_EIO			           5		/**< I/O error */
#define HCL_EACCESS		           13		/**< Permission denied */
#define HCL_EBUSY		           16		/**< Device busy */
#define HCL_ENODEV		           19		/**< No such device */
#define HCL_EINVAL		           22		/**< Invalid argument */
#define HCL_ENOSYS		           38		/**< Function not implemented */
#define HCL_ENODATA		           61		/**< No data available */
#define HCL_ETIME		           62		/**< Timeout error */
#define HCL_EUNKNOWN               1000		/**< Unknown error */
#define HCL_ELIGHTSOURCE           1001		/**< Light source is not functioning properly */
#define HCL_ERECALIBRATION         1002		/**< Recalibration error (internal wavelength standard) */
#define HCL_EDARKREFERENCE         1003		/**< Invalid external dark reference */
#define HCL_EWHITEREFERENCE        1004		/**< Invalid external white reference */
#define HCL_ETRANSFLECTREFERENCE   1005		/**< Invalid external transflectance standard (measurement of liquids) */
#define HCL_EMISSINGREFERENCE      1006		/**< External reference measurements are missing */
#define HCL_EREFERENCETIMEOUT      1007		/**< External reference measurement timeout (overall duration exceeded 3min) */
#define HCL_ESTATUSUNKONWN         1008     /**< Device status is unknown */
#define HCL_EINVALIDREFDATA        1009     /**< Spectral reference data is invalid or incomplete */
#define HCL_EINVALIDSPECTRUMTYPE   1010     /**< Spectrum type is invalid or not allowed */
#define HCL_EINVALIDSPECTRUMFORMAT 1011     /**< Spectrum format is invalid or inconsistent */
#define HCL_EMISSING_CHECKSUM      1012		/**< Spectrum file or Container file is missing a checksum */
#define HCL_EINVALID_CHECKSUM      1013     /**< Data has invalid checksum/is inconsistent */
/* @} end of doxygen errorcodes group */

/** \defgroup finderinstrumenttype Finder instrument type
* Determine the type of the Finder
* \see finder_get_param
* @{ */
#define HCL_FINDER_TYPE_UNKNOWN   0 /**< The instrument type is unknown */
#define HCL_FINDER_TYPE_APO_IDENT 1 /**< The instrument is a Hiperscan Apo-Ident */
#define HCL_FINDER_TYPE_PTIM100   2 /**< The instrument is a PharmaTest PT-IM100 */
#define HCL_FINDER_TYPE_FINDER_SD 3 /**< The instrument is a Hiperscan Finder SD */
/* @} end of doxygen finderinstrumenttype group */

/** \defgroup finderstatus Finder status
* Finder status codes determine the current status of a Finder device.
* \see finder_get_status
* @{ */
#define HCL_FINDER_STATUS_UNKNOWN                 0   /**< Device status is undefined: communication error, hardware error, driver error? */
#define HCL_FINDER_STATUS_INIT                    1   /**< Device is initializing (driver is determining hardware state, hardware state is resetting) */
#define HCL_FINDER_STATUS_WARMUP                  2   /**< Device temperature is too low or yet unknown -> wait for IDLE status*/
#define HCL_FINDER_STATUS_IDLE                    3   /**< Device is IDLE (ready for operation) */
#define HCL_FINDER_STATUS_ACQUISITION_REQUEST     4   /**< A data acquisition operation has been requested */
#define HCL_FINDER_ACQUIRE_EXTERNAL_SPECTRUM      5   /**< Device is acquiring an external (sample/specimen) spectrum */
#define HCL_FINDER_INTERNAL_WAVELENGTH_REFERENCE  6   /**< Device is acquiring an internal wavelength reference spectrum */
#define HCL_FINDER_INTERNAL_WHITE_REFERENCE       7   /**< Device is acquiring an internal white reference spectrum */
#define HCL_FINDER_DARK_INTENSITY                 8   /**< Device is acquiring an internal dark reference spectrum */
#define HCL_FINDER_RANDOM_DELAY                   9   /**< Device is delaying a acquisition by random time interval (testing/debugging) */
#define HCL_FINDER_WAIT_FOR_LIGHT_SOURCE         10   /**< Stabilizing light source */
#define HCL_FINDER_CLOSE                         11   /**< Device is closing (deinitializing) */
/* @} end of doxygen finderstatus group */

/** \defgroup finderperipheral Finder peripheral
* Finder peripheral IDs identify the current periphery attached to a Finder device.
* \see finder_get_param
* @{ */
#define HCL_FINDER_PERIPHERAL_NONE           0 /**< No periperal is attached to the instrument */
#define HCL_FINDER_PERIPHERAL_SAMPLE_SPINNER 1 /**< A Sample Spinner is attached to the instrument */
/* @} end of doxygen finderperipheral group */

/** \defgroup spectrumprobetypes Spectrum probe types
* Spectrum probe type IDs identify the type of the probe a spectrum was acquired with.
* \see hcl_spectrum_t::probe_type
* @{ */
#define HCL_SPECTRUM_PROBE_TYPE_UNKNOWN						0 /**< Unknown or not specified probe type */
#define HCL_SPECTRUM_PROBE_TYPE_FINDER_STANDARD				1 /**< Finder setup, including Apo-Ident and PT-IM100 */
#define HCL_SPECTRUM_PROBE_TYPE_FINDER_INSERT				2 /**< Finder setup, including Apo-Ident and PT-IM100 with Sample Insert */
#define HCL_SPECTRUM_PROBE_TYPE_FINDER_SD_STANDARD			3 /**< Finder SD setup */
#define HCL_SPECTRUM_PROBE_TYPE_FINDER_SD_INSERT			4 /**< Finder SD setup with Sample Insert */
#define HCL_SPECTRUM_PROBE_TYPE_FINDER_SD_SAMPLE_SPINNER	5 /**< Finder SD setup with Sample Spinner (Rotator) */

/* @} end of doxygen spectrumprobetype group */

/** \defgroup spectrumtypes Spectrum types
* Spectrum type IDs identify the type of a spectrum.
* \see hcl_spectrum_t::probe_type
* @{ */
#define HCL_SPECTRUM_TYPE_PROCESSED_SOLID		12 /**< Processed diffuse reflectance spectrum (solids) */
#define HCL_SPECTRUM_TYPE_PROCESSED_FLUID		13 /**< Processed diffuse transflectance spectrum (fluids) */
#define HCL_SPECTRUM_TYPE_PROCESSED_INTENSITY	18 /**< Processed absolute intensity spectrum */
/* @} end of doxygen spectrumtype group */


/** \defgroup arraylength Array length definitions
* Definitions of default buffer lengths for return buffers.
* \see finderfunction
* \see sgsfunction
* @{ */
#define HCL_DEFAULT_SPECTRUM_LEN   901    /**< Default count of spectral data containt in a spectrum -> array length for sgs_aquire_... functions */
#define HCL_MAX_PARAM_LEN          100    /**< Max length of a parameter string returned by ..._get_param() */
#define HCL_MAX_MSG_LEN            2048   /**< Max length of a HCL error message */
#define HCL_MAX_STACKTRACE_LEN     2048   /**< Max length of a HCL stacktrace message */
#define HCL_MAX_REFERENCE_DATA_LEN 307200 /**< Max length of a binary spectral reference data \see finder_get_reference_data*/
/* @} end of doxygen arraylength group */

/** \defgroup vendorid Vendor ID definitions
* Definition of known USB vendor IDs for Hiperscan/OEM devices
* \see finder_open
* \see finder_open2
* \see sgs_open
* \see sgs_open2
* @{ */
#define VID_FTDI      0x0403	/**< Vendor ID (USB device) of Hiperscan SGS1900 microspectrometers prior Apo-Ident 2.0 */
#define VID_HIPERSCAN 0x3111	/**< Vendor ID (USB device) of Hiperscan microspectrometers */
/* @} end of doxygen vendorid group */

/** \defgroup productid Product ID definitions
* Definition of known USB product IDs for Hiperscan/OEM devices
* \see finder_open
* \see finder_open2
* \see sgs_open
* \see sgs_open2
* @{ */
#define PID_HIPERSCAN_SGS1900  0x9c70	/**< Product ID (USB device) of Hiperscan SGS1900 microspectrometers */
#define PID_PHARMATEST_PTIM100 0x9c72	/**< Product ID (USB device) of Pharma Test PT-IM100 microspectrometers */
#define PID_HIPERSCAN_APOIDENT 0x0027	/**< Product ID (USB device) of Hiperscan Version >= 2 */
/* @} end of doxygen productid group */

/**
	Generic definition for callback functions
	\see finder_set_query_external_white_reference_callback
	\see finder_set_query_external_dark_reference_callback
	\see finder_set_query_external_transflectance_reference_callback
	\see finder_set_query_external_transflectance_specimen_callback
*/
typedef void (__cdecl *QUERY_CALLBACK)();


/**
	Definition for temperature status callback function
	\see finder_set_temperature_status_callback
*/
typedef void(__cdecl *TEMPERATURE_STATUS_CALLBACK)(char*);

/**
A structure to represent a HCL spectrum.
\see hcl_spectrum_init
\see hcl_spectrum_free
\see spectrumprobetypes
\see spectrumtypes
\remark The structure must manage dynamically allocated arrays only. It is not allowed to set a reference to a char/double array which is constant or located on the Stack.
*/
typedef struct
{
	int          length;				/**< Spectrum length (count of wavelength, intensity, reflectance values) */
	double*      wavelength;			/**< Pointer to array of wavelength values with defined length */
	double*      intensity;				/**< Pointer to array of intensity values with defined length or NULL if not defined */
	double*      reflectance;			/**< Pointer to array of reflectance values with defined length or NULL if not defined */
	long         timestamp;				/**< Spectrum timestamp (creation time) as Epoch Seconds, must be set to write a spectrum file*/
	char*        creator;				/**< Creator of the spectrum (instrument/software) without newlines, must be set to write a spectrum file */
	char*        comment;				/**< Spectrum comment text which may contain newlines ('\n'), must not be set */
	char*        serial_number;			/**< Serial number of the instrument that acquired the spectrum, must be set to write a spectrum file */
	char*        firmware_version;      /**< Firmware version of the instrument that acquired the spectrum, must not be set */
	char*        label;					/**< Label of the spectrum (can be mandatory \see hcl_spectrum_read_container, must be set to write a Container file */
	double       spectral_resolution;	/**< Wavelength resolution of the spectral data, must be set to write a spectrum file */
	unsigned int average_count;			/**< Average count that was used for spectrum acquisition, must be set to write a spectrum file */
	int          probe_type;			/**< Probe type that was used for spectrum acquisition. For valid values see in \ref spectrumprobetypes, must be set */
	int          spectrum_type;			/**< Spectrum type. For valid values see in \ref spectrumtypes, must be set */
} hcl_spectrum_t;


/**
	Get the version of the instrument driver
	\remark The function returns a pointer to an unmanaged ANSI string.
*/
__declspec(dllimport) char* hcl_get_driver_version();

/**
Get the name of the instrument driver
\remark The function returns a pointer to an unmanaged ANSI string.
*/
__declspec(dllimport) char* hcl_get_driver_name();

/**
Get the location of the instrument driver
\remark The function returns a pointer to an unmanaged ANSI string.
*/
__declspec(dllimport) char* hcl_get_driver_location();

/** \defgroup finderfunction Finder related functions
* Functions to access Hiperscan's Finder spectrometer with integrated light source and reference standards. Thermal stabilized operation mode of this devices is supported.
* @{ */
/**
    Set callback function for external white reference query.

	While acquisition this callback is used to inform the user to prepare the instrument for external white measurement (reference measurement).
	\param callback pointer to callback function 
    \retval zero If all is fine, otherwise HCL error code
	\see errorcodes
	\see finder_get_last_error
	\see finder_get_last_stacktrace
	\see finder_acquire_reflectance_spectrum
	\see finder_acquire_transflectance_spectrum
*/
__declspec(dllimport) int finder_set_query_external_white_reference_callback(QUERY_CALLBACK callback);

/**
    Set callback function for external dark reference query.
	
	While acquisition this callback is used to inform the user to prepare the instrument for external dark measurement (empty measurement).
	\param callback pointer to callback function 
    \retval zero If all is fine, otherwise HCL error code
	\see errorcodes
	\see finder_get_last_error
	\see finder_get_last_stacktrace
	\see finder_acquire_reflectance_spectrum
	\see finder_acquire_transflectance_spectrum
*/
__declspec(dllimport) int finder_set_query_external_dark_reference_callback(QUERY_CALLBACK callback);

/**
    Set callback function for external transflectance reference query.
	
	While acquisition this callback is used to inform the user to prepare the instrument for external transflectance reference measurement (transflectance inset).
	\param callback pointer to callback function 
    \retval zero If all is fine, otherwise HCL error code
	\see errorcodes
	\see finder_get_last_error
	\see finder_get_last_stacktrace
	\see finder_acquire_transflectance_spectrum
	\remark This function is only needed in order to prepare the instrument for a transflectance measurement, utilizing the transflectance inset.
*/
__declspec(dllimport) int finder_set_query_external_transflectance_reference_callback(QUERY_CALLBACK callback);

/**
    Set callback function for external transflectance specimen query.
	
	While acquisition this callback is used to inform the user to prepare the instrument for the actual specimen/sample measurement (transflectance inset).
	\param callback pointer to callback function 
    \retval zero If all is fine, otherwise HCL error code
	\see errorcodes
	\see finder_get_last_error
	\see finder_get_last_stacktrace
	\see finder_acquire_transflectance_spectrum
	\remark This function is only needed in order to prepare the instrument for a transflectance measurement, utilizing the transflectance inset.
*/
__declspec(dllimport) int finder_set_query_external_transflectance_specimen_callback(QUERY_CALLBACK callback);

/**
	Set callback function for temperature status changed events.

	This callback is used to inform the application about changes in the temperature status of the instrument. The event handler passes the result as JSON string
	which is saved in \a buffer.
	\param callback pointer to callback function
	\param buffer pointer to string buffer, allocated by the application
	\param maxlen length of the string buffer, allocated by the application
	\retval zero If all is fine, otherwise HCL error code
	\see errorcodes
	\see finder_get_last_error
	\see finder_get_last_stacktrace
*/
__declspec(dllimport) int finder_set_temperature_status_callback(TEMPERATURE_STATUS_CALLBACK callback, char buffer[], int maxlen);

/**
    Open a Finder device.
	
	The USB Finder device is opened. All resources are acquired. The type of the device which is opened is determined by its \a product_id.
	Please note that this implementation of HCL C-wrapper can manage only one device at a time. This is not a restriction of the underlying C# class library, but of the C-wrapper.
	\param product_id USB Product ID of the Finder device
    \retval zero If all is fine, otherwise HCL error code
	\see errorcodes
	\see finder_open2
	\see finder_get_last_error
	\see finder_get_last_stacktrace
	\see finder_close
	\see productid
	\remark This function must be called before start interacting with the device.
*/
__declspec(dllimport) int finder_open(unsigned int product_id);

/**
	Open a Finder device with defined Vendor ID.

	The USB Finder device is opened. All resources are acquired. The type of the device which is opened is determined by its \a vendor_id and \a product_id.
	Please note that this implementation of HCL C-wrapper can manage only one device at a time. This is not a restriction of the underlying C# class library, but of the C-wrapper.
	This function replaces \a finder_open and must be used if the required Vendor ID differs from the FTDI default Vendor ID (i.e. Apo-Ident 2.0).
	\param vendor_id USB Vendor ID of the Finder device
	\param product_id USB Product ID of the Finder device
	\retval zero If all is fine, otherwise HCL error code
	\see errorcodes
	\see finder_open
	\see finder_get_last_error
	\see finder_get_last_stacktrace
	\see finder_close
	\see productid
	\remark This function must be called before start interacting with the device.
*/
__declspec(dllimport) int finder_open2(unsigned int vendor_id, unsigned int product_id);

/**
    Initialize a Finder device.

	Self test procedures are performed. If applicable and necessary, the warmup procedure and internal reference measurements are performed.
    \retval zero If all is fine, otherwise HCL error code
	\see errorcodes
	\see finder_get_last_error
	\see finder_get_last_stacktrace
	\see finder_open
	\see finder_get_status
	\remark This function must be called before start acquisition of spectra.
*/
__declspec(dllimport) int finder_init();

/**
    Close a Finder device.
	
	The USB Finder device is closed. All resources are freed.
    \retval zero If all is fine, otherwise HCL error code
	\see errorcodes
	\see finder_get_last_error
	\see finder_get_last_stacktrace
	\see finder_open
	\see finder_open2
	\remark This function must not be called before further interaction with the Finder device.
*/
__declspec(dllimport) int finder_close();

/**
	Set Finder Status to Sleep.

	The instrument is set to Sleep mode. 
	\retval zero If all is fine, otherwise HCL error code
	\see errorcodes
	\see finder_get_last_error
	\see finder_get_last_stacktrace
	\see finder_close
	\see finder_open
	\remark To reconnect the instrument it must be closed and opened again.
*/
__declspec(dllimport) int finder_sleep();


/**
    Set a device parameter.
	
	A hardware or software parameter is set. This function can be used to change acquisition parameters (e.g. "Average"), timeouts or debug options.
	Valid parameter names are:
	  - `"AutoAcquisitionSequence"`: Automatically prepare instrument for acquisition and automatically end sequence (True|False), \see finder_begin_acquisition_sequence(), \see finder_end_acquisition_sequence()
	  - `"AutoRecalibrationMode"`: Set automatic recalibration mode (Auto|Always|Never)
	  - `"AutoReferencing"`: Activate automatic external referencing (True|False)
	  - `"AutoSaveDirectory"`: Set path for output directory for recalibration and referencing data and logfiles (debugging only)
	  - `"AutoSaveReferences"`: Activate automatic saving of recalibration and referencing data (debugging only)
	  - `"Average"`: Set averaging for sample measurements "AverageMin" <= value <= "AverageMax"
	  - `"DisableConsoleOutput"`: Disable output of debug information to console
	  - `"IdleTimeoutMinutes"`: Set timeout for Sleep Mode in minutes
	  - `"LightSourceTimeoutMinutes"`: Set timeout for Sleep Mode in minutes (same as `"IdleTimeoutMinutes"`)
	  - `"LightSourceWaitMinutes"`: Set light source stabilization timespan in minutes
	  - `"PlausibilityChecks"`: Activate plausibility checks for external referencing (True|False)
	  - `"RecalibrationTimeoutMilliseconds"`: Set timeout for automatic recalibration in milliseconds
	  - `"ReferenceTimeoutMilliseconds"`: Set timeout for automatic external referencing in milliseconds
	  - `"RegressionCount"`: Set number of temperature values, used for determination of the temperature gradient (debugging only)
	\param name Name of the parameter to set
	\param param Value to be set to the parameter
    \retval zero If all is fine, otherwise HCL error code
	\see errorcodes
	\see finder_get_last_error
	\see finder_get_last_stacktrace
	\see finder_get_param
	\remark All parameter values are expeted as Null-terminated C strings. For numerical values a dot is expected as decimal separator (e.g. "3.14").
*/
__declspec(dllimport) int finder_set_param(char name[], char param[]);

/**
    Get a device parameter.
	
	A hardware or software parameter is read. This function can be used to determine acquisition parameters (e.g. "Average"), timeouts or debug options.
	Valid parameter names are:
	  - `"AutoAcquisitionSequence"`: Automatically prepare instrument for acquisition and automatically end sequence (True|False), \see finder_begin_acquisition_sequence(), \see finder_end_acquisition_sequence()
	  - `"AutoRecalibrationMode"`: Current automatic recalibration mode (Auto|Always|Never)
	  - `"AutoReferencing"`: Is automatic external referencing active? (True|False)
      - `"AutoSaveDirectory"`: Path to current output directory for recalibration and referencing data and logfiles
      - `"AutoSaveReferences"`: Are recalibration and referencing data saved automatically? (True|False)
      - `"Average"`: Averaging for sample measurements "AverageMin" <= value <= "AverageMax"
      - `"AverageMax"`: Smallest supported value for parameter "Average"
      - `"AverageMin"`: Greatest supported value for parameter "Average"
      - `"BoardVersion"`: Version ID of Finder PCB
      - `"CasingVersion"`: Version ID of Finder casing
      - `"ExternalDarkReferenceTimestamp"`: Timestamp of current external dark reference in Epoch Seconds, "n/a" if information is not available
      - `"ExternalWhiteReferenceTimestamp"`: Timestamp of current external white reference in Epoch Seconds, "n/a" if information is not available
      - `"FirmwareVersion"`: Current firmware version string, "n/a" if information is not available
      - `"IdleTimeoutMinutes"`: Current timeout for Sleep Mode in minutes, "0" if not available
	  - `"InstrumentType"`: Type of the current instrument \see finderinstrumenttype
	  - `"LightSourceOperatingTimeMinutes"`: Current operating time of the light source in minutes, "n/a" if information is not available
      - `"LightSourcePowerCycleCount"`: Power cycle count of the light source, "n/a" if information is not available
      - `"LightSourceTimeoutMinutes"`: Current timeout for Sleep Mode in minutes (same as `"IdleTimeoutMinutes"`), "0" if not available
      - `"LightSourceVersion"`: Version ID of light source
      - `"LightSourceWaitMinutes"`: Current light source stabilization timespan in minutes, "0" if information is not available
	  - `"Name"`: Name (user-defined) of the instrument
	  - `"OemSerialNumber"`: OEM serial number of the instrument, "n/a" if information is not available
	  - `"Peripheral"`: Typ of currently connected peripheral hardware \see finderperipheral
	  - `"PlausibilityChecks"`: Are plausibility checks performed for external referencing? (True|False)
	  - `"RecalibrationTimeoutMilliseconds"`: Timeout for automatic recalibration in milliseconds
	  - `"RecalibrationTimestamp"`: Timestamp of current recalibration in Epoch Seconds, "n/a" if information is not available
	  - `"ReferenceTimeoutMilliseconds"`: Timeout for external referencing in milliseconds
	  - `"ReferenceWheelVersion"`: Version ID of reference wheel
	  - `"RegressionCount"`: Number of temperature values, used for determination of the temperature gradient, "0" if information is not available
	  - `"ReleaseTemperature"`: Temperature that must be reached to release the instrument or to stop the warm-up process
	  - `"SampleSpinnerAcquisitionCount"`: Returns over-all count of measurements with Sample Spinner, "n/a" if information is not available
	  - `"SerialNumber"`: Serial number of the instrument
	  - `"SpectralResolution"`: Spectral resolution of the instrument in nanometers (nm)
	  - `"SystemOperatingTimeMinutes"`: Current operating time of the instrument, "n/a" if information is not available
	  - `"WavelengthInc"`: Current wavelength increment of acquired spectra
	  - `"WavelengthMax"`: Maximum wavelength value of acquired spectra
	  - `"WavelengthMin"`: Minumum wavelength value of acquired spectra
	\param name Name of the parameter to get
	\param maxlen Max length of the return buffer (\a param)
	\param param Return buffer. This must be an allocated C string with a length of \a maxlen
    \retval zero If all is fine, otherwise HCL error code
	\see errorcodes
	\see finder_get_last_error
	\see finder_get_last_stacktrace
	\see finder_set_param
	\remark All parameter values are returned as Null-terminated C strings. For numerical values a dot is expected as decimal separator (e.g. "3.14").
*/
__declspec(dllimport) int finder_get_param(char name[], int maxlen, char param[]);

/**
	Reset light source stats of a Finder device.

	\retval zero If all is fine, otherwise HCL error code
	\see errorcodes
	\see finder_get_param
*/
__declspec(dllimport) int finder_reset_light_source_stats();

/**
    Get status of a device.

	The current status of a Finder device is determined and returned. All parameters are return values.
	\param status Current status of the device.
	\param progress If an operation is in progress (e.g. light source stabilization), the progress as value between 0.0 and 1.0
	\param eta_seconds If an operation is in progress (e.g. light source stabilization), the "estimatet time to arrival" in seconds
	\param temperature Current temperature of the spectral sensor
	\param temperature_gradient Current temperature gradient of the spectral sensor
    \retval zero If all is fine, otherwise HCL error code
	\see finderstatus
	\see errorcodes
	\see finder_get_last_error
	\see finder_get_last_stacktrace
*/
__declspec(dllimport) int finder_get_status(int *status, double *progress, double *eta_seconds, double *temperature, double *temperature_gradient);

/**
    Update recalibration manually.

	An internal recalibration cycle is started. This is a asynchronous function call.
	\retval zero If all is fine, otherwise HCL error code
	\see errorcodes
	\see finder_get_last_error
	\see finder_get_last_stacktrace
	\remark This function should be used in combination with deactivated automatic internal recalibration only. \see finder_set_param
*/
__declspec(dllimport) int finder_update_recalibration();

/**
Get spectral reference data.

All spectral data, defining the current external referencing are returned as binary data. The return buffer must be allocated before the function call.
\param maxlen length of allocated data buffer (maximum length of binary data)
\param data data buffer for binary reference data
\param len actual length of data saved in the buffer
\retval zero If all is fine, otherwise HCL error code
\see errorcodes
\see finder_get_last_error
\see finder_get_last_stacktrace
\see finder_set_reference_data
\see HCL_MAX_REFERENCE_DATA_LEN
*/
__declspec(dllimport) int finder_get_reference_data(int maxlen, unsigned char data[], int *len);

/**
Set spectral reference data.

Set all spectral data, defining an external referencing from binary data.
\param data data buffer containing binary reference data
\param len actual length of data saved in the buffer
\retval zero If all is fine, otherwise HCL error code
\see errorcodes
\see finder_get_last_error
\see finder_get_last_stacktrace
\see finder_get_reference_data
*/
__declspec(dllimport) int finder_set_reference_data(unsigned char data[], int len);

/**
Begin (prepare) an acquisition sequence.

The instrument is prepared for spectrum acquisition (single spectrum or sequence). 
\retval zero If all is fine, otherwise HCL error code
\see errorcodes
\see finder_get_last_error
\see finder_get_last_stacktrace
\remark This function can only be used in combination with parameter `"AutoAcquisitionSequence"` set to `false`
*/
__declspec(dllimport) int finder_begin_acquisition_sequence();

/**

End an acquisition sequence.

A spectrum acquisition (single spectrum or sequence) is finished.
\retval zero If all is fine, otherwise HCL error code
\see errorcodes
\see finder_get_last_error
\see finder_get_last_stacktrace
\remark This function can only be used in combination with parameter `"AutoAcquisitionSequence"` set to `false`
*/
__declspec(dllimport) int finder_end_acquisition_sequence();

/**
    Acquire a reflectance spectrum.

	A spectrum is acquired in reflectance mode and returned as arrays of \a double. The return buffers must be allocated before the function call.
	\param len Length of the result buffers (see \a HCL_DEFAULT_SPECTRUM_LEN)
	\param wavelength Wavelength values (usually 1nm increments)
	\param reflectance Reflectance values
    \retval zero If all is fine, otherwise HCL error code
	\see HCL_DEFAULT_SPECTRUM_LEN
	\see errorcodes
	\see finder_get_last_error
	\see finder_get_last_stacktrace
	\see finder_acquire_external_references
	\see finder_set_param
	\see finder_begin_acquisition_sequence
	\see finder_end_acquisition_sequence
	\remark External referencing must be performed at least once before calling this function.
*/
__declspec(dllimport) int finder_acquire_reflectance_spectrum(int len, double wavelength[], double reflectance[]);

/**
Acquire reflectance and intensity of a spectrum.

A spectrum is acquired in combined reflectance and intensity mode and returned as arrays of \a double. The return buffers must be allocated before the function call.
\param len Length of the result buffers (see \a HCL_DEFAULT_SPECTRUM_LEN)
\param wavelength Wavelength values (usually 1nm increments)
\param reflectance Reflectance values
\param intensity Intensity values
\retval zero If all is fine, otherwise HCL error code
\see HCL_DEFAULT_SPECTRUM_LEN
\see errorcodes
\see finder_get_last_error
\see finder_get_last_stacktrace
\see finder_acquire_external_references
\see finder_set_param
\see finder_begin_acquisition_sequence
\see finder_end_acquisition_sequence
\remark External referencing must be performed at least once before calling this function.
*/
__declspec(dllimport) int finder_acquire_reflectance_intensity_spectrum(int len, double wavelength[], double reflectance[], double intensity[]);

/**
    Acquire a transflectance spectrum.

	A spectrum is acquired in transflectance mode and returned as arrays of \a double. The return buffers must be allocated before the function call.
	\param len Length of the result buffers (see \a HCL_DEFAULT_SPECTRUM_LEN)
	\param wavelength Wavelength values (usually 1nm increments)
	\param transflectance Transflectance values
    \retval zero If all is fine, otherwise HCL error code
	\see HCL_DEFAULT_SPECTRUM_LEN
	\see errorcodes
	\see finder_get_last_error
	\see finder_get_last_stacktrace
	\see finder_acquire_external_references
	\see finder_set_query_external_transflectance_reference_callback
	\see finder_set_query_external_transflectance_specimen_callback
	\see finder_require_transflectance_reference
	\see finder_set_param
	\see finder_begin_acquisition_sequence
	\see finder_end_acquisition_sequence
	\remark External referencing must be performed at least once before calling this function. Callbacks for external transflectance and transflectance reference measurements must be set.
*/
__declspec(dllimport) int finder_acquire_transflectance_spectrum(int len, double wavelength[], double transflectance[]);

/**
    Acquire external reference spectra.

	External reference measurements are performed (white reference, dark/empty reference). While execution, callback functions are used to interact with the user (prepare instrument for reference measurements).
    \retval zero If all is fine, otherwise HCL error code
	\see errorcodes
	\see finder_get_last_error
	\see finder_get_last_stacktrace
	\see finder_set_query_external_white_reference_callback
	\see finder_set_query_external_dark_reference_callback
	\see finder_acquire_reflectance_spectrum
	\see finder_acquire_transflectance_spectrum
	\see finder_get_white_spectrum
	\see finder_get_dark_spectrum
	\remark Callbacks for external reference measurements must be set before calling this function.
*/
__declspec(dllimport) int finder_acquire_external_references();

/**
    Get external white reference spectrum.

	Return current reference spectrum (external white reference) as arrays of \a double. The return buffers must be allocated before the function call.
	\param len Length of the result buffers (see \a HCL_DEFAULT_SPECTRUM_LEN)
	\param wavelength Wavelength values (usually 1nm increments)
	\param intensity Intensity values
    \retval zero If all is fine, otherwise HCL error code
	\see HCL_DEFAULT_SPECTRUM_LEN
	\see errorcodes
	\see finder_get_last_error
	\see finder_get_last_stacktrace
	\see finder_acquire_external_references
	\remark External referencing must be performed at least once before calling this function.
*/
__declspec(dllimport) int finder_get_white_spectrum(int len, double wavelength[], double intensity[]);

/**
    Get external dark reference spectrum.

	Return current dark reference spectrum (external dark/empty reference) as arrays of \a double. The return buffers must be allocated before the function call.
	\param len Length of the result buffers (see \a HCL_DEFAULT_SPECTRUM_LEN)
	\param wavelength Wavelength values (usually 1nm increments)
	\param intensity Intensity values
    \retval zero If all is fine, otherwise HCL error code
	\see HCL_DEFAULT_SPECTRUM_LEN
	\see errorcodes
	\see finder_get_last_error
	\see finder_get_last_stacktrace
	\see finder_acquire_external_references
	\remark External referencing must be performed at least once before calling this function.
*/
__declspec(dllimport) int finder_get_dark_spectrum(int len, double wavelength[], double intensity[]);

/**
    Get external transflectance reference spectrum.

	Return current transflectance reference spectrum (transflectance inset) as arrays of \a double. The return buffers must be allocated before the function call.
	\param len Length of the result buffers (see \a HCL_DEFAULT_SPECTRUM_LEN)
	\param wavelength Wavelength values (usually 1nm increments)
	\param intensity Intensity values
    \retval zero If all is fine, otherwise HCL error code
	\see HCL_DEFAULT_SPECTRUM_LEN
	\see errorcodes
	\see finder_get_last_error
	\see finder_get_last_stacktrace
	\see finder_acquire_transflectance_spectrum
	\remark A transflectance measurement must be performed at least once before calling this function.
*/
__declspec(dllimport) int finder_get_transflectance_reference_spectrum(int len, double wavelength[], double intensity[]);

/**
    Get last error message from HCL.

	Return last error description (exception message) from HCL as C string. The return buffer must be allocated before the function call.
	\param msg Return buffer
	\param maxlen Max length of the result buffer (use \a HCL_MAX_MSG_LEN)
	\see finder_get_last_stacktrace
	\see HCL_MAX_MSG_LEN
*/
__declspec(dllimport) void finder_get_last_error(char msg[], int maxlen);

/**
    Get last stacktrace from HCL.

	Return last error stacktrace (exception stacktrace) from HCL as C string. The return buffer must be allocated before the function call.
	\param stacktrace Return buffer
	\param maxlen Max length of the result buffer (use \a HCL_MAX_STACKTRACE_LEN)
	\see finder_get_last_error
	\see HCL_MAX_STACKTRACE_LEN
*/
__declspec(dllimport) void finder_get_last_stacktrace(char stacktrace[], int maxlen);

/**
    Set transflectance acquisition mode.

	With this function the automatic acquisition of transflectance reference spectra during transflectance mode measurement can be turned off. This can be used to implement subsample measurements without user interaction.
	\param param A value 1 turns automatic acquisition of transflectance reference measurements on, a value 0 off
	\see finder_acquire_transflectance_spectrum
*/
__declspec(dllimport) void finder_require_transflectance_reference(int param);
/* @} end of doxygen finderfunction group */

/** \defgroup sgsfunction SGS related functions
* Functions to access Hiperscan's Scanning Grating Spectrometers (SGS).
* @{ */
/**
    Open an SGS device.
	
	The USB SGS device is opened. All resources are acquired. The type of the device which is opened is determined by its \a product_id.
	Please note that this implementation of HCL C wrapper can manage only one device at a time. This is not a restriction of the underlying C# class library, but of the C wrapper.
	\param product_id USB Product ID of the SGS device
    \retval zero If all is fine, otherwise HCL error code
	\see errorcodes
	\see sgs_open2
	\see sgs_get_last_error
	\see sgs_get_last_stacktrace
	\see sgs_close
	\see productid
	\remark This function must be called before start interacting with the device.
*/
__declspec(dllimport) int sgs_open(unsigned int product_id);

/**
	Open an SGS device with defined Vendor ID.

	The USB SGS device is opened. All resources are acquired. The type of the device which is opened is determined by its \a vendor_id and \a product_id.
	Please note that this implementation of HCL C wrapper can manage only one device at a time. This is not a restriction of the underlying C# class library, but of the C wrapper.
	This function replaces \a sgs_open and must be used if the required Vendor ID differs from the FTDI default Vendor ID (i.e. SGS-NT).
	\param vendor_id USB Vendor ID of the SGS device
	\param product_id USB Product ID of the SGS device
	\retval zero If all is fine, otherwise HCL error code
	\see errorcodes
	\see sgs_open
	\see sgs_get_last_error
	\see sgs_get_last_stacktrace
	\see sgs_close
	\see productid
	\remark This function must be called before start interacting with the device.
*/
__declspec(dllimport) int sgs_open2(unsigned int vendor_id, unsigned int product_id);

/**
    Close an SGS device.
	
	The USB SGS device is closed. All resources are freed.
    \retval zero If all is fine, otherwise HCL error code
	\see errorcodes
	\see sgs_get_last_error
	\see sgs_get_last_stacktrace
	\see sgs_open
	\see sgs_open2
	\remark This function must not be called before further interaction with the SGS device.
*/
__declspec(dllimport) int sgs_close();

/**
    Set a device parameter.
	
	A hardware or software parameter is set. This function can be used to change acquisition parameters
	(e.g. "Average"), timeouts or debug options.
	See HCL documentation for valid parameters. 
	\param name Name of the parameter to set
	\param param Value to be set to the parameter
    \retval zero If all is fine, otherwise HCL error code
	\see errorcodes
	\see sgs_get_last_error
	\see sgs_get_last_stacktrace
	\see sgs_get_param
	\remark All parameter values are expeted as Null-terminated C strings. For numerical values a dot is expected as decimal separator (e.g. "3.14").
*/
__declspec(dllimport) int sgs_set_param(char name[], char param[]);

/**
    Get a device parameter.
	
	A hardware or software parameter is read. This function can be used to determine acquisition parameters (e.g. "Average"), timeouts or debug options.
	See HCL documentation for valid parameters. 
	\param name Name of the parameter to get
	\param maxlen Max length of the return buffer (\a param)
	\param param Return buffer. This must be an allocated C string with a length of \a maxlen
    \retval zero If all is fine, otherwise HCL error code
	\see errorcodes
	\see sgs_get_last_error
	\see sgs_get_last_stacktrace
	\see sgs_set_param
	\remark All parameter values are returned as Null-terminated C strings. For numerical values a dot is expected as decimal separator (e.g. "3.14").
*/
__declspec(dllimport) int sgs_get_param(char name[], int maxlen, char param[]);

/**
    Acquire an intensity spectrum.

	A spectrum is acquired in intensity mode and returned as arrays of \a double. The return buffers must be allocated before the function call.
	\param len Length of the result buffers (see \a HCL_DEFAULT_SPECTRUM_LEN)
	\param wavelength Wavelength values (usually 1nm increments)
	\param intensity Intensity values
    \retval zero If all is fine, otherwise HCL error code
	\see HCL_DEFAULT_SPECTRUM_LEN
	\see errorcodes
	\see sgs_get_last_error
	\see sgs_get_last_stacktrace
	\see sgs_acquire_dark_intensity
	\remark A dark intensity measurement must be performed at least once before calling this function.
*/
__declspec(dllimport) int sgs_acquire_intensity_spectrum(int len, double wavelength[], double intensity[]);

/**
    Acquire a reflectance spectrum.

	A spectrum is acquired in reflectance mode and returned as arrays of \a double. The return buffers must be allocated before the function call.
	\param len Length of the result buffers (see \a HCL_DEFAULT_SPECTRUM_LEN)
	\param wavelength Wavelength values (usually 1nm increments)
	\param reflectance Reflectance values
    \retval zero If all is fine, otherwise HCL error code
	\see HCL_DEFAULT_SPECTRUM_LEN
	\see errorcodes
	\see sgs_get_last_error
	\see sgs_get_last_stacktrace
	\see sgs_acquire_reference_spectrum
	\see sgs_acquire_dark_intensity
	\see sgs_acquire_reference_spectrum
	\remark Both, a dark intensity measurement, as well as a reference measurement must be performed at least once before calling this function.
*/
__declspec(dllimport) int sgs_acquire_reflectance_spectrum(int len, double wavelength[], double reflectance[]);

/**
    Acquire an absorbance spectrum.

	A spectrum is acquired in absorbance mode and returned as arrays of \a double. The return buffers must be allocated before the function call.
	\param len Length of the result buffers (see \a HCL_DEFAULT_SPECTRUM_LEN)
	\param wavelength Wavelength values (usually 1nm increments)
	\param absorbance Absorbance values
    \retval zero If all is fine, otherwise HCL error code
	\see HCL_DEFAULT_SPECTRUM_LEN
	\see errorcodes
	\see sgs_get_last_error
	\see sgs_get_last_stacktrace
	\see sgs_acquire_reference_spectrum
	\see sgs_acquire_dark_intensity
	\see sgs_acquire_reference_spectrum
	\remark Both, a dark intensity measurement, as well as a reference measurement must be performed at least once before calling this function.
*/
__declspec(dllimport) int sgs_acquire_absorbance_spectrum(int len, double wavelength[], double absorbance[]);

/**
    Acquire a reference spectrum.

	A reference measurements is performed (white reference).
    \retval zero If all is fine, otherwise HCL error code
	\see errorcodes
	\see sgs_get_last_error
	\see sgs_get_last_stacktrace
	\see sgs_acquire_reflectance_spectrum
	\see sgs_acquire_absorbance_spectrum
	\remark This function must be called before performing reflectance or absorbance measurements.
*/
__declspec(dllimport) int sgs_acquire_reference_spectrum();

/**
    Acquire dark intensity.

	Dark intensity is determined. Light source must be switched off during this measurement.
    \retval zero If all is fine, otherwise HCL error code
	\see errorcodes
	\see sgs_get_last_error
	\see sgs_get_last_stacktrace
	\see sgs_acquire_intensity_spectrum
	\see sgs_acquire_reflectance_spectrum
	\see sgs_acquire_absorbance_spectrum
	\remark This function must be called before performing intensity, reflectance or absorbance measurements.
*/
__declspec(dllimport) int sgs_acquire_dark_intensity();

/**
    Get reference spectrum.

	Return current reference spectrum (white reference) as arrays of \a double. The return buffers must be allocated before the function call.
	\param len Length of the result buffers (see \a HCL_DEFAULT_SPECTRUM_LEN)
	\param wavelength Wavelength values (usually 1nm increments)
	\param intensity Intensity values
    \retval zero If all is fine, otherwise HCL error code
	\see HCL_DEFAULT_SPECTRUM_LEN
	\see errorcodes
	\see sgs_get_last_error
	\see sgs_get_last_stacktrace
	\see sgs_acquire_reference_spectrum
	\remark Referencing must be performed at least once before calling this function.
*/__declspec(dllimport) int sgs_get_reference_spectrum(int len, double wavelength[], double intensity[]);

/**
    Get dark intensity.

	Return current dark intensity by reference. The return buffers must be allocated before the function call.
	\param intensity Dark intensity
	\param sigma Stamdard deviation of dark intensity (noise measure)
    \retval zero If all is fine, otherwise HCL error code
	\see HCL_DEFAULT_SPECTRUM_LEN
	\see errorcodes
	\see sgs_get_last_error
	\see sgs_get_last_stacktrace
	\see sgs_acquire_dark_intensity
	\remark A dark intensity measurement must be performed at least once before calling this function.
*/
__declspec(dllimport) int sgs_get_dark_intensity(double* intensity, double* sigma);

/**
    Get last error message from HCL.

	Return last error description (exception message) from HCL as C string. The return buffer must be allocated before the function call.
	\param msg Return buffer
	\param maxlen Max length of the result buffer (use \a HCL_MAX_MSG_LEN)
	\see sgs_get_last_stacktrace
	\see HCL_MAX_MSG_LEN
*/
__declspec(dllimport) void sgs_get_last_error(char msg[], int maxlen);

/**
    Get last stacktrace from HCL.

	Return last error stacktrace (exception stacktrace) from HCL as C string. The return buffer must be allocated before the function call.
	\param stacktrace Return buffer
	\param maxlen Max length of the result buffer (use \a HCL_MAX_STACKTRACE_LEN)
	\see sgs_get_last_error
	\see HCL_MAX_STACKTRACE_LEN
*/
__declspec(dllimport) void sgs_get_last_stacktrace(char stacktrace[], int maxlen);
/* @} end of doxygen sgsfunction group */


/** \defgroup spectrumfunction Spectrum related functions
* Functions to access Hiperscan's spectrum (file) formats defined in Hiperscan Class Library (HCL).
* @{ */
/**
Initialize a spectrum structure.

An instance of \a hcl_spectrum_t is initialized. The instance will not be created/allocated, just initialized.
\param spectrum Reference to an instance of \a hcl_spectrum_t
\param lenght Length of the spectrum (number of wavelength, intensity or reflectance values)
\see hcl_spectrum_free
\remark This function must be called for every instance of \a hcl_spectrum_t before values are set and before a call to \a hcl_spectrum_free(). Functions which read spectra (from files or instruments) initialize spectra by themself.
*/
__declspec(dllimport) void hcl_spectrum_init(hcl_spectrum_t* spectrum, int length);


/**
Free a spectrum structure.

An instance of \a hcl_spectrum_t is freed. All internal memory allocations are released.
\param spectrum Reference to an instance of \a hcl_spectrum_t
\see hcl_spectrum_init
\remark This function must be called in order to prevent memory leaks within an application. It must not be called for every for uninitialized instances of \a hcl_spectrum_t.
*/
__declspec(dllimport) void hcl_spectrum_free(hcl_spectrum_t* spectrum);

/**
Read a CSV spectrum file.

A spectrum file in Hiperscan's CSV spectrum format is read from a file and returned as an instance of \a hcl_spectrum_t.
\param fname Path to the spectrum file to be read
\param spectrum Reference to an existing, not initialized instance of \a hcl_spectrum_t
\retval zero If all is fine, otherwise HCL error code
\see errorcodes
\see hcl_spectrum_t::spectrum_type
\see HCL_EINVALIDSPECTRUMTYPE
\see hcl_spectrum_free
\remark This function initializes the the defined instance of \a hcl_spectrum_t by itself.
\remark Only processed spectra can be read by this function. If a spectrum type is not compatible \a HCL_EINVALIDSPECTRUMTYPE is returned.
*/
__declspec(dllimport) int hcl_spectrum_read_csv(char fname[], hcl_spectrum_t* spectrum);


/**
Write a CSV spectrum file.

A spectrum file in Hiperscan's CSV spectrum format is written to a file.
\param fname Path to the specrum file to be written
\param spectrum Reference to an instance of \a hcl_spectrum_t, containing the spectral information to be written
\param salt Salt to calculate a SHA384 checksum for the spectrum, NULL if not used
\param salt_len Length of the Salt, 0 if not used
\retval zero If all is fine, otherwise HCL error code
\see errorcodes
\see hcl_spectrum_t::spectrum_type
\see HCL_EINVALIDSPECTRUMTYPE
\see hcl_spectrum_check_integrity
\remark Only processed spectra can be written by this function. If a spectrum type is not compatible \a HCL_EINVALIDSPECTRUMTYPE is returned.
\remark If no Salt is provided, \a hcl_spectrum_check_integrity() cannot check the integrity of the spectrum file.
*/
__declspec(dllimport) int hcl_spectrum_write_csv(char fname[], hcl_spectrum_t* spectrum, unsigned char salt[], int salt_len);

/**
Read a CSV Container file.

A Container file in Hiperscan's CSVC format is read from a file and returned as an array of instance of \a hcl_spectrum_t.
\param fname Path to the Container file to be read
\param spectra Reference to a pointer to an array of instances of \a hcl_spectrum_t (is set to the resulting list of read spectra)
\param count Reference to an integer value, which is set to the count of read spectra (length of the array \a spectra)
\retval zero If all is fine, otherwise HCL error code
\see errorcodes
\see hcl_spectrum_t::spectrum_type
\see HCL_EINVALIDSPECTRUMTYPE
\see hcl_spectrum_free
\remark This function will allocate and initialize an array of instances of \a hcl_spectrum_t by itself. The length of the array is determined by the count of spectra in the Container file.
\remark All instances of \a hcl_spectrum_t contained in the returned array must be freed manually, using \a hcl_spectrum_free(), to prevent memory leaks.
\remark Only Container files exclusively containing processed spectra can be read by this function. If a spectrum type is not compatible \a HCL_EINVALIDSPECTRUMTYPE is returned.
*/
__declspec(dllimport) int hcl_spectrum_read_container(char fname[], hcl_spectrum_t** spectra[], int* count);

/**
Write a CSV Container file.

A Container file in Hiperscan's CSVC format is written to a file.
\param fname Path to the Container file to be written
\param spectra Reference to an array of instances of \a hcl_spectrum_t, containing the spectral information to be written
\param count Count of spectra to be written into the Container file (length of the array of instances of \a hcl_spectrum_t) 
\param salt Salt to calculate a SHA384 checksum for all spectra, NULL if not used
\param salt_len Length of the Salt, 0 if not used
\retval zero If all is fine, otherwise HCL error code
\see errorcodes
\see hcl_spectrum_t::spectrum_type
\see HCL_EINVALIDSPECTRUMTYPE
\see hcl_spectrum_check_integrity
\remark Only processed spectra can be written by this function. If a spectrum type is not compatible, \a HCL_EINVALIDSPECTRUMTYPE is returned.
\remark If no Salt is provided, \a hcl_spectrum_check_integrity() cannot check the integrity of the spectrum file.
*/
__declspec(dllimport) int hcl_spectrum_write_container(char fname[], hcl_spectrum_t* spectra[], int count, unsigned char salt[], int salt_len);

/**
Check integrity of a CSV spectrum or Container file.

The integrity of a CSV spectrum or Container file is checked, using an optional SHA384 checksum. 
\param fname Path to the file to be checked
\param salt Salt to calculate a SHA384 checksum for the spectrum/for all spectra
\param salt_len Length of the Salt
\param result A reference to an integer which is set to 1 (integrity okay) or 0 (integrity not okay)
\retval zero If all is fine, otherwise HCL error code
\see errorcodes
\see HCL_EMISSING_CHECKSUM
\see hcl_spectrum_write_csv
\see hcl_spectrum_write_container
\remark If a spectrum or Container file has been written without a checksum, \a HCL_EMISSING_CHECKSUM is returned.
*/
__declspec(dllimport) int hcl_spectrum_check_integrity(char fname[], unsigned char salt[], int salt_len, int* result);

/* @} end of doxygen spectrumfunction group */

#ifdef __cplusplus
}
#endif

/* @} end of doxygen cwrapper group */
