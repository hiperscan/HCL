﻿// Created with MonoDevelop
//
//    Copyright (C) 2017 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;

using Hiperscan.Spectroscopy.IdentModule;


namespace Hiperscan.Spectroscopy.IdentServer
{

    [Serializable()]
    public class AuthenticationSet
    {
        public const string NO_SUBSTANCE = "NO_SUBSTANCE";

        public AuthenticationSet()
        {
            this.Serial         = null;
            this.ModelClassName = string.Empty;
            this.SubstanceId    = AuthenticationSet.NO_SUBSTANCE;
            this.AccessDatabase = null;
            this.IsValidationMode = false;
        }

        public string Serial                     { get; set; }
        public string ModelClassName             { get; set; }
        public string SubstanceId                { get; set; }
        public AccessDatabaseInfo AccessDatabase { get; set; }
        public bool IsValidationMode             { get; set; }
    }
}

