﻿// Created with MonoDevelop
//
//    Copyright (C) 2017 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Runtime.Serialization;


namespace Hiperscan.Spectroscopy.IdentServer
{

    [DataContract(Name = "NormalizedPcaMatch", Namespace = "http://www.hiperscan.com/spectroscopy/identserver")] [Serializable()]
    public class NormalizedPcaMatch : System.Runtime.Serialization.IExtensibleDataObject
    {
        [DataMember] public string ClassificationName {get; set;}
        [DataMember] public string SampleId           {get; set;}
        [DataMember] public double Significance       {get; set;}
        [DataMember] public double Correlation        {get; set;}
        [DataMember] public double Confidence         {get; set;}
        [DataMember] public double Distance           {get; set;}

        [NonSerialized] private ExtensionDataObject extension_data;

        public NormalizedPcaMatch()
        {
            this.ClassificationName    = string.Empty;
            this.SampleId            = string.Empty;
            this.Correlation        = 0.0;
            this.Confidence            = 0.0;
            this.Significance        = 0.0;
            this.Distance            = 0.0; 
        }
    
        public ExtensionDataObject ExtensionData
        {
            get { return this.extension_data; }
            set { this.extension_data = value; }
        }
    }
}

