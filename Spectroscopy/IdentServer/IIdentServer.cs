﻿// Created with MonoDevelop
//
//    Copyright (C) 2009 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;


using Hiperscan.Spectroscopy.IdentModule;


namespace Hiperscan.Spectroscopy.IdentServer
{
    public interface IIdentServer
    {
        byte[] GetChallenge();
        List<AccessDatabaseInfo> Initialize(string serial, byte[] master_response);

        List<string> GetModelClassNames(AuthenticationSet authentification);
        ModelClassInfo GetModelClassInfo(AuthenticationSet authentification);
        ModelClassExtensionInfo GetModelClassExtensionInfo(AuthenticationSet authentification);

        ResultSet Classify(AuthenticationSet authentification, Spectrum spectrum);

        IdentModuleInfo GetIdentModuleInfo();
        UpdateCertificateInfo GetUpdateCertificateData();

        byte[] GetValidationDocument(string model_class_name);
        byte[] GetAdditionalTestPdf();
        byte[] GetPznList();

        byte[] GetLicense();
        byte[] GetLogfile();
        byte[] GetUpdateChangesFile();
    }
}
