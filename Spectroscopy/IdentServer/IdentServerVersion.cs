﻿// Created with MonoDevelop
//
//    Copyright (C) 2017 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;

using Hiperscan.Unix;


namespace Hiperscan.Spectroscopy.IdentServer
{

    [DataContract(Name = "IdentServerVersion", Namespace = "http://www.hiperscan.com/spectroscopy/identserver")] [Serializable]
    public class IdentServerVersion : System.Runtime.Serialization.IExtensibleDataObject
    {
        private IdentServerVersion()
        {
        }

        public IdentServerVersion(string full_version)
        {
            this.IdentServerIdRegex = new Regex(@"^\d+\.\d+\.\d+-\d{4}-\d{2}-\d{4}$");

            if (this.IdentServerIdRegex.Match(full_version).Success == false)
                throw new ArgumentException(Catalog.GetString("Ident-Server ID has invalid format."));

            this.IdentServerId = full_version;
        }

        public IdentServerVersion(string assembly_version, string ident_module_version, string ident_module_subversion) :
        this (assembly_version + "-" + ident_module_version + "-" + ident_module_subversion)
        {

        }

        public string IdentModuleVersion
        {
            get
            {
                string[] tmp = this.IdentServerId.Split(new char[] { '-' });
                return this.ShortAssemblyVersion + "-" + tmp[1] + "-" + tmp[2];
            }
        }

        public string IdentModuleSubVersion
        {
            get { return this.IdentServerId.Split('-')[3]; }
        }

        public string AssemblyVersion
        {
            get { return this.IdentServerId.Split('-')[0]; }
        }

        public string ShortAssemblyVersion
        {
            get
            {
                string[] tmp = this.AssemblyVersion.Split(new char[] { '.' });
                return tmp[0] + "." + tmp[1];
            }
        }

        public string AssemblyMajorVersion
        {
            get { return this.ShortAssemblyVersion.Split(new char[] { '.' })[0]; }
        }

        public string AssemblyMinorVersion
        {
            get { return this.ShortAssemblyVersion.Split(new char[] { '.' })[1]; }
        }

        [DataMember]
        public string IdentServerId { get; private set; }

        [NonSerialized] private ExtensionDataObject extension_data;

        public ExtensionDataObject ExtensionData
        {
            get { return this.extension_data; }
            set { this.extension_data = value; }
        }

        private Regex IdentServerIdRegex { get; set; }
    }
}
