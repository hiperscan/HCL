﻿// Created with MonoDevelop
//
//    Copyright (C) 2017 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;


namespace Hiperscan.Spectroscopy.IdentServer
{
    [DataContract(Name = "ApoIdentResultSet", Namespace = "http://www.hiperscan.com/spectroscopy/identserver")]
    [Serializable()]
    public class ApoIdentResultSet : ResultSet
    {
        public ApoIdentResultSet() : base()
        {
            this.Rating = 0.0;
            this.ClassificationMatch = false;
            this.ClassificationId = ResultSet.NO_ID;
            this.SpectrumBackProjection = null;
            this.MatchBackProjection = null;
            this.PcaMatches = new List<NormalizedPcaMatch>();
            this.SignificanceLevel = string.Empty;
        }

        public bool TruePositive
        {
            get { return this.PositiveIdentified && this.ClassificationMatch; }
        }

        [DataMember] public bool ClassificationMatch { get; set; }
        [DataMember] public string ClassificationId { get; set; }

        [DataMember] public DataSet SpectrumBackProjection { get; set; }
        [DataMember] public DataSet MatchBackProjection { get; set; }
        [DataMember] public List<NormalizedPcaMatch> PcaMatches { get; set; }
        [DataMember] public string SignificanceLevel { get; set; }

        [DataMember] public double Rating { get; set; }
    }
}
