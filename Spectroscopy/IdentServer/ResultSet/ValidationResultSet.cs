﻿// Created with MonoDevelop
//
//    Copyright (C) 2017 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;


namespace Hiperscan.Spectroscopy.IdentServer
{
    [DataContract(Name = "ValidationResultSet", Namespace = "http://www.hiperscan.com/spectroscopy/identserver")]
    [Serializable()]
    public class ValidationResultSet : ResultSet
    {
        public ValidationResultSet() : base()
        {
        }

        [DataMember] public string ForeignSampleId { get; set; } = String.Empty;
        [DataMember] public double ForeignDistance { get; set; } = double.NaN;
        [DataMember] public double ForeignCorrelation { get; set; } = double.NaN;

        [DataMember] public double NominalDistance { get; set; } = double.NaN;
        [DataMember] public double NominalCorrelation { get; set; } = double.NaN;
    }
}
