// Created with MonoDevelop
//
//    Copyright (C) 2017 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;


namespace Hiperscan.Spectroscopy.IdentServer
{

    [DataContract(Name = "ResultSet", Namespace = "http://www.hiperscan.com/spectroscopy/identserver")] 
    [Serializable()]
    [KnownType(typeof(ApoIdentResultSet))]
    [KnownType(typeof(ValidationResultSet))]
    public abstract class ResultSet : System.Runtime.Serialization.IExtensibleDataObject
    {
        public const string NO_ID = "NO_ID";

        [NonSerialized] private ExtensionDataObject extension_data;

        public ResultSet()
        {
            this.PositiveIdentified     = false;
            this.MinRating              = 0.0;
            this.Identifier             = string.Empty;
            this.MatchId                = string.Empty;
            this.MatchRating            = 0.0;
            this.Date                   = DateTime.MinValue;
        }

        public ExtensionDataObject ExtensionData
        {
            get { return this.extension_data; }
            set { this.extension_data = value; }
        }

        [DataMember] public bool PositiveIdentified { get; set; }
        [DataMember] public double MinRating { get; set; }
        [DataMember] public string Identifier { get; set; }
        [DataMember] public DateTime Date { get; set; }
        [DataMember] public string MatchId { get; set; }
        [DataMember] public double MatchRating { get; set; }
        [DataMember] public string PcaVersion { get; set; }

    }
}

