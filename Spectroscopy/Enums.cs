// Created with MonoDevelop
//
//    Copyright (C) 2009 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;


namespace Hiperscan.Spectroscopy
{

    public enum SpectrumType : int
    {
        Unknown = 0,
        Single = 1,
        Stream = 2,
        QuickStream = 3,
        Preview = 4,
        InternalWhite = 5,
        InternalDark = 6,
        InternalOxid = 7,
        ExternalWhite = 8,
        ExternalEmpty = 9,
        SolidSpecimen = 10,
        FluidSpecimen = 11,
        ProcessedSolidSpecimen = 12,
        ProcessedFluidSpecimen = 13,
        ExternalFluidWhite = 14,
        ReferenceSet = 15,
        CalibrationReference = 16,
        BandwidthReference = 17,
        ProcessedIntensitySpecimen = 18,
        ExternalWhiteInsert = 19
    }

    public enum ProbeType : int
    {
        Unknown = 0,
        FinderStandard = 1,
        FinderInsert = 2,
        FinderSDStandard = 3,
        FinderSDInsert = 4,
        FinderSDSampleSpinner = 5
    }

    public enum LineStyle : int
    {
        Solid,
        Dash,
        Dot,
        DashDot,
        DashDotDot
    }

    public enum MarkerStyle : int
    {
        Cross1,
        Square,
        Circle,
        Diamond
    }

    [Flags]
    public enum CorrectionMask : int
    {
        None = 0,
        Adc = 1,
        WavelengthCorrection = 2,
        SystemFunction = 4,
        DarkIntensity = 8,
        LinearDistortion = 16,
        WavelengthLimits = 32,
        All = 1 | 2 | 8 | 16 | 32 // TODO: SystemFunction correction is disabled for now!
    }
}
