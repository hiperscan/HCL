﻿// Created with MonoDevelop
//
//    Copyright (C) 2018 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Runtime.Serialization;


namespace Hiperscan.Spectroscopy.Method
{

    [DataContract(Name = "PcaMatch", Namespace = "http://www.hiperscan.com/spectroscopy/method")] [Serializable()]
    public class PcaMatch : System.IComparable<PcaMatch>, System.Runtime.Serialization.IExtensibleDataObject
    {

        [DataMember(Order = 0)] public string Id                 { get; }
        [DataMember(Order = 1)] public int Ranking               { get; private set; }
        [DataMember(Order = 2)] public double Significance       { get; private set; }
        [DataMember(Order = 3)] public double Confidence         { get; private set; }
        [DataMember(Order = 4)] public double Correlation        { get; private set; }
        [DataMember(Order = 5)] public double Distance           { get; private set; }
        [DataMember(Order = 6)] public string ClassificationName { get; set; }
        [DataMember(Order = 7)] public DataSet BackProjection    { get; set; }

        [NonSerialized] private ExtensionDataObject extension_data;

        private PcaMatch()
        {
        }

        public PcaMatch(string name, double significance)
        {
            this.Ranking = 0;
            this.Id = name;
            this.Significance = significance;
            this.Confidence = 0.0;
            this.Correlation = 0.0;
            this.Distance = double.PositiveInfinity;
            this.ClassificationName = string.Empty;
            this.BackProjection = null;
        }

        public int CompareTo(PcaMatch match)
        {
            if (match == null)
                return 1;

            return this.Significance.CompareTo(match.Significance);
        }

        public void SetRanking(int ranking)
        {
            this.Ranking = ranking;
        }

        public void SetSignificance(double significance)
        {
            this.Significance = significance;
        }

        public void SetConfidence(double confidence)
        {
            this.Confidence = confidence;
        }

        public void SetCorrelation(double correlation)
        {
            this.Correlation = correlation;
        }

        public void SetDistance(double distance)
        {
            this.Distance = distance;
        }

        public override string ToString()
        {
            return $"[PCAMatch: Name={Id}, Ranking={Ranking}, Significance={Significance}, Confidence={Confidence}, Correlation={Correlation}, Distance={Distance}]";
        }

        public ExtensionDataObject ExtensionData
        {
            get { return this.extension_data;  }
            set { this.extension_data = value; }
        }
    }
}