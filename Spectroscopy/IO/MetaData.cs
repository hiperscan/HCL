// Created with MonoDevelop
//
//    Copyright (C) 2017 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.IO;
using System.Xml.Serialization;


namespace Hiperscan.Spectroscopy.IO
{

    public class MetaData
    {
        public string Type { get; private set; }
        public byte[] Data { get; private set; }
        
        public MetaData(string type, byte[] data)
        {
            this.Type = type;
            this.Data = data;
        }

        public static byte[] SerializeString(string s)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                XmlSerializer serializer = new XmlSerializer(typeof(string));
                serializer.Serialize(stream, new string(s.ToCharArray()));
                return stream.ToArray();
            }
        }

        public static string DeserializeString(byte[] b)
        {
            using (MemoryStream stream = new MemoryStream(b))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(string));
                return (string)serializer.Deserialize(stream);
            }
        }
    }
}