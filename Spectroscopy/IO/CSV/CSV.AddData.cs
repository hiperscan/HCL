﻿// Created with MonoDevelop
//
//    Copyright (C) 2009 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;


namespace Hiperscan.Spectroscopy.IO
{

    public static partial class CSV
    {
        public class AddData : System.IComparable<AddData>
        {
            internal AddData()
            {
                this.Data = new List<uint>();
            }

            public int CompareTo(AddData add_data)
            {
                if (add_data == null)
                    return 1;

                return this.DateTime.CompareTo(add_data.DateTime);
            }

            public List<uint> Data   { get; set; }
            public DateTime DateTime { get; set; }
            public string Serial     { get; set; }
        }
    }
}