﻿// Created with MonoDevelop
//
//    Copyright (C) 2009 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.IO;
using System.Collections.Generic;
using System.Globalization;


namespace Hiperscan.Spectroscopy.IO
{
    public static partial class CSV
    {
        public static List<double> ReadList(string fname)
        {
            return CSV.ReadList(fname, CSV.DEFAULT_CULTURE);
        }

        public static List<double> ReadList(string fname, CultureInfo culture)
        {
            using (Stream stream = new FileStream(fname, FileMode.Open))
            {
                return CSV.ReadList(stream, culture);
            }
        }

        public static List<double> ReadList(Stream stream, CultureInfo culture)
        {
            using (StreamReader reader = new StreamReader(stream))
            {
                List<double> x = new List<double>();

                while (reader.EndOfStream == false)
                {
                    string line = reader.ReadLine();
                    x.Add(double.Parse(line, culture));
                }

                return x;
            }
        }
    }
}