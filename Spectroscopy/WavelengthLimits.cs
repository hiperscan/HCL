﻿// Created with MonoDevelop
//
//    Copyright (C) 2018 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Runtime.Serialization;

using Hiperscan.Unix;


namespace Hiperscan.Spectroscopy
{

    [DataContract(Name = "Spectrum", Namespace = "http://www.hiperscan.com/spectroscopy")] [Serializable()]
    public class WavelengthLimits: System.Runtime.Serialization.IExtensibleDataObject
    {
        [DataMember] public double Minimum { get; private set; }
        [DataMember] public double Maximum { get; private set; }

        [NonSerialized] private ExtensionDataObject extension_data;


        private WavelengthLimits()
        {
        }

        public WavelengthLimits(double min, double max)
        {
            this.Minimum = min;
            this.Maximum = max;
        }

        public WavelengthLimits(DataSet ds)
        {
            this.Minimum = ds.XMin();
            this.Maximum = ds.XMax();
        }

        public WavelengthLimits(WavelengthLimits limits)
        {
            this.Minimum = limits.Minimum;
            this.Maximum = limits.Maximum;
        }

        public WavelengthLimits(SerializationInfo info, StreamingContext context)
        {
            this.Minimum = (double)info.GetValue("Minimum", typeof(double));
            this.Maximum = (double)info.GetValue("Maximum", typeof(double));
        }

        public void GetObjectData(SerializationInfo info, StreamingContext ctxt)
        {
            info.AddValue("Minimum", this.Minimum);
            info.AddValue("Maximum", this.Maximum);
        }

        public DataSet CorrectedSpectrum(DataSet ds)
        {
            DataSet corrected_ds = new DataSet(ds);

            List<int> indices = corrected_ds.Find('x', '<', this.Minimum);

            if (indices.Count > corrected_ds.Count - 2)
                throw new Exception(Catalog.GetString("Invalid Wavelength Limits or Wavelength Correction."));

            corrected_ds.RemoveAt(indices);
            indices = corrected_ds.Find('x', '>', this.Maximum);
            corrected_ds.RemoveAt(indices);

            return corrected_ds;
        }

        public override string ToString()
        {
            return this.ToString(CultureInfo.CurrentCulture);
        }

        public string ToString(CultureInfo culture)
        {
            return this.Minimum.ToString(culture) + culture.TextInfo.ListSeparator + this.Maximum.ToString(culture);
        }

        public static WavelengthLimits DefaultWavelengthLimits
        {
            get { return new WavelengthLimits(1000.0, 1900.0); }
        }

        public ExtensionDataObject ExtensionData
        {
            get { return this.extension_data; }
            set { this.extension_data = value; }
        }
    }
}