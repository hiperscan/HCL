﻿// Created with MonoDevelop
//
//    Copyright (C) 2018 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Runtime.Serialization;


namespace Hiperscan.Spectroscopy
{

    [DataContract(Name = "DarkIntensity", Namespace = "http://www.hiperscan.com/spectroscopy")] [Serializable()]
    public class DarkIntensity : System.Runtime.Serialization.IExtensibleDataObject
    {
        [DataMember] public double Intensity   { get; private set; }
        [DataMember] public double Sigma       { get; private set; }
        [DataMember] public DateTime Timestamp { get; private set; }

        [NonSerialized] private ExtensionDataObject extension_data;

        private DarkIntensity()
        {
        }

        public DarkIntensity(double intensity, double sigma)
        {
            this.Intensity = intensity;
            this.Sigma = sigma;
            this.Timestamp = DateTime.Now;
        }

        public DarkIntensity(Spectrum spectrum)
        {
            spectrum = spectrum.Clone();
            spectrum.ApplyCorrectionsToRawData(CorrectionMask.WavelengthLimits);

            this.Intensity = spectrum.RawIntensity.YMean();
            this.Sigma     = spectrum.RawIntensity.YSigma();
            this.Timestamp = spectrum.Timestamp;
        }

        public DarkIntensity(DarkIntensity di)
        {
            this.Intensity = di.Intensity;
            this.Sigma     = di.Sigma;
            this.Timestamp = di.Timestamp;
        }

        public void SetIntensity(double val)
        {
            this.Intensity = val;
        }

        public void SetSigma(double val)
        {
            this.Sigma = val;
        }

        public void SetDateTime(DateTime val)
        {
            this.Timestamp = val;
        }

        public ExtensionDataObject ExtensionData
        {
            get { return this.extension_data; }
            set { this.extension_data = value; }
        }
    }
}