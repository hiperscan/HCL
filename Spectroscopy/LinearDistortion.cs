﻿// Created with MonoDevelop
//
//    Copyright (C) 2018 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Runtime.Serialization;


namespace Hiperscan.Spectroscopy
{
    [DataContract(Name = "LinearDistortion", Namespace = "http://www.hiperscan.com/spectroscopy")] [Serializable()]
    public class LinearDistortion : System.Runtime.Serialization.IExtensibleDataObject
    {
        [DataMember] public double Offset      { get; }
        [DataMember] public double Factor      { get; }
        [DataMember] public DateTime Timestamp { get; set; }

        [NonSerialized] private ExtensionDataObject extension_data;


        private LinearDistortion()
        {
        }

        public LinearDistortion(double offset, double factor)
        {
            this.Offset    = offset;
            this.Factor    = factor;
            this.Timestamp = DateTime.Now;
        }

        public LinearDistortion(LinearDistortion ld)
        {
            this.Offset    = ld.Offset;
            this.Factor    = ld.Factor;
            this.Timestamp = ld.Timestamp;
        }

        public override string ToString()
        {
            return $"[LinearDistortion: Offset={Offset}, Factor={Factor}, Timestamp={Timestamp}]";
        }

        public ExtensionDataObject ExtensionData
        {
            get { return this.extension_data; }
            set { this.extension_data = value; }
        }
    }
}