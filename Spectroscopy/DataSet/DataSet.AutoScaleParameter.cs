// Created with MonoDevelop
//
//    Copyright (C) 2008 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;

using Hiperscan.Unix;


namespace Hiperscan.Spectroscopy
{

    public partial class DataSet
    {
        
        public class AutoScaleParameter
        {
            public DataSet Means  { get; private set; }
            public DataSet Sigmas { get; private set; }
        
            public AutoScaleParameter(DataSet[] data_sets, List<double> wavelengths)
            {
                if (data_sets == null || data_sets.Length < 1)
                    throw new ArgumentException(Catalog.GetString("Auto scale parameter must contain at least one data set."));
                
                List<double> means  = new List<double>(data_sets.Length);
                List<double> sigmas = new List<double>(data_sets.Length);
                
                List<List<double>> yis = new List<List<double>>(data_sets.Length);
                
                foreach (DataSet ds in data_sets)
                {
                    yis.Add(ds.FirLowpassFilter(DataSet.GetDefaultSystemFunctionCorrectionCoefficients()).Interpolate(wavelengths).Y);
                }

                for (int ix=0; ix < yis[0].Count; ++ix)
                {
                    List<double> yi = new List<double>(yis.Count);
                    for (int jx=0; jx  < yis.Count; ++jx)
                    {
                        yi.Add(yis[jx][ix]);
                    }
                    DataSet ds = new DataSet(yi, yi);
                    
                    means.Add(ds.YMean());
                    sigmas.Add(ds.YSigma());
                }
                
                this.Means  = new DataSet(wavelengths, means);
                this.Sigmas = new DataSet(wavelengths, sigmas);
            }
        }
    }
}
