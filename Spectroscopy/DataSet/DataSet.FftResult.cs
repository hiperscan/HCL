// Created with MonoDevelop
//
//    Copyright (C) 2008 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Numerics;

using Hiperscan.Unix;


namespace Hiperscan.Spectroscopy
{

    public partial class DataSet
    {

        public class FftResult
        {
            public FftResult(double[] real, double[] imag, double delta)
            {
                if (real.Length != imag.Length)
                    throw new ArgumentException(Catalog.GetString("Real and Imag fields must have same length."));

                double[] omega = new double[real.Length];
                double df = 2.0 / delta / (double)real.Length;
                for (int ix = 0; ix < omega.Length; ++ix)
                {
                    omega[ix] = 0.0 + (double)ix * df;
                }

                double[] amplitude = new double[real.Length];
                double[] phase = new double[real.Length];

                for (int ix = 0; ix < real.Length; ++ix)
                {
                    Complex c = new Complex(real[ix], imag[ix]);

                    amplitude[ix] = c.Magnitude;
                    phase[ix] = c.Phase;
                }

                this.Real = new DataSet(omega, real);
                this.Imag = new DataSet(omega, imag);

                this.Amplitude = new DataSet(omega, amplitude);
                this.Phase     = new DataSet(omega, phase);
            }

            public DataSet Real      { get; }
            public DataSet Imag      { get; }
            public DataSet Amplitude { get; }
            public DataSet Phase     { get; }
        }
        
    }
}
