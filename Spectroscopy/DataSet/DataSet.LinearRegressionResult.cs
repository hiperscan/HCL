// Created with MonoDevelop
//
//    Copyright (C) 2008 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.Collections.Generic;
using System.Globalization;


namespace Hiperscan.Spectroscopy
{

    public partial class DataSet
    {

        public class LinearRegressionResult
        {
            public LinearRegressionResult(double offset, double slope)
            {
                this.Offset = offset;
                this.Slope = slope;
            }
            
            public double GetRegressionValue(double x)
            {
                return x * this.Slope + this.Offset;
            }
            
            public List<double> GetRegressionValues(List<double> x)
            {
                List<double> result = new List<double>(x.Count);
                foreach (double v in x)
                {
                    result.Add(v * this.Slope + this.Offset);
                }
                return result;
            }

            public double Offset { get; }
            public double Slope { get; }

            public override string ToString()
            {
                return string.Format("[LinearRegressionResult: Offset={0}, Slope={1}]", Offset, Slope);
            }

            public string ToCsvString()
            {
                return string.Format(CultureInfo.InvariantCulture, "LinearRegressionResult:,Offset:,{0},Slope:,{1}", Offset, Slope);
            }
        }
    }
}
