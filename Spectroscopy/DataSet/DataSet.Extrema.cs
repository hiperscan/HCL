// Created with MonoDevelop
//
//    Copyright (C) 2008 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.Collections.Generic;


namespace Hiperscan.Spectroscopy
{

    public partial class DataSet
    {

        public class Extrema
        {
            internal Extrema(DataSet minima, DataSet maxima)
            {
                this.Minima = minima;
                this.Maxima = maxima;
            }

            public DataSet Minima { get; }
            public DataSet Maxima { get; }

            public DataSet All()
            {
                DataSet ds = new DataSet(this.Minima.Count)
                {
                    IsContinuous = false
                };
                ds.AddRange(this.Minima);
                ds.AddRange(this.Maxima);
                ds.Sort();
                return ds;
            }

            public void RemoveRange(int index, int count)
            {
                List<double> xarray = this.All().X.GetRange(index, count);

                foreach (double x in xarray)
                {
                    if (this.Minima.X.Contains(x))
                    {
                        int ix = this.Minima.X.IndexOf(x);
                        this.Minima.RemoveAt(ix);
                    }

                    if (this.Maxima.X.Contains(x))
                    {
                        int ix = this.Maxima.X.IndexOf(x);
                        this.Maxima.RemoveAt(ix);
                    }
                }
            }
        }
    }
}
