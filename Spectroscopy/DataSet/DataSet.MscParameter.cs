// Created with MonoDevelop
//
//    Copyright (C) 2008 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;

using Hiperscan.Unix;


namespace Hiperscan.Spectroscopy
{

    public partial class DataSet
    {

        public class MscParameter
        {
            public DataSet Means { get; private set; }

            
            public MscParameter(List<double> wavelengths, List<double> means)
            {
                this.Means = new DataSet(wavelengths, means);
            }
            
            public MscParameter(DataSet[] data_sets)
            {
                if (data_sets == null || data_sets.Length < 1)
                    throw new ArgumentException(Catalog.GetString("MSC parameter must contain at least one data set."));

                List<double> xi = data_sets[0].X;
                double[] means = new double[xi.Count];
                double N = (double)data_sets.Length;
                
                foreach (DataSet ds in data_sets)
                {
                    DataSet dsi = ds.Interpolate(xi);
                    for (int ix=0; ix < dsi.Count; ++ix)
                    {
                        means[ix] += dsi.Y[ix] / N;
                    }
                }
                
                this.Means = new DataSet(xi.ToArray(), means);
            }

            public override int GetHashCode()
            {
                return this.Means.GetHashCode();
            }
        }
    }
}
