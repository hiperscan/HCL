// Created with MonoDevelop
//
//    Copyright (C) 2008 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.


namespace Hiperscan.Spectroscopy
{

    public partial class DataSet
    {

        public class FwhmResult
        {
            public FwhmResult(double x_peak, double y_peak, double fwhm)
            {
                this.XPeak = x_peak;
                this.YPeak = y_peak;
                this.FWHM  = fwhm;
            }

            public FwhmResult() : this(0.0, 0.0, 0.0)
            {
            }

            public double XPeak { get; }
            public double YPeak { get; }
            public double FWHM  { get; }
        }
    }
}
