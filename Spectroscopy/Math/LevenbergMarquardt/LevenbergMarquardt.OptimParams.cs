﻿// Created with MonoDevelop
//
//    Copyright (C) 2017 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;

using Hiperscan.Unix;


namespace Hiperscan.Spectroscopy.Math
{

    public partial class LevenbergMarquardt
    {

        public class OptimParams
        {

            public OptimParams()
            {
                this.MaxIterations = 400;
                this.WarnOnIterationExceeded = false;
                this.ToleranceGradient = 1e-10;
                this.ToleranceFunction = 1e-10;
                this.ToleranceArgument = 1e-10;
            }

            public OptimParams(int max_iterations, double tol_gradient, double tol_function, double tol_argument)
            {
                this.MaxIterations     = max_iterations;
                this.ToleranceGradient = tol_gradient;
                this.ToleranceFunction = tol_function;
                this.ToleranceArgument = tol_argument;

                if (this.MaxIterations     <= 0 || 
                    this.ToleranceArgument <= 0 || 
                    this.ToleranceFunction <= 0 ||
                    this.ToleranceGradient <= 0)
                {
                    throw new ArgumentException(Catalog.GetString("Optimization parameters have to be greater than zero."));
                }
            }

            public int MaxIterations            { get; set; }
            public bool WarnOnIterationExceeded { get; set; }
            public double ToleranceGradient     { get; set; }
            public double ToleranceFunction     { get; set; }
            public double ToleranceArgument     { get; set; }
        }
    }
}

