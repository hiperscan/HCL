// Created with MonoDevelop
//
//    Copyright (C) 2017 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;

using MathNet.Numerics.LinearAlgebra.Double;

using static System.Math;


namespace Hiperscan.Spectroscopy.Math
{

    public partial class LevenbergMarquardt
    {

        public class ParabolicFit : Hiperscan.Spectroscopy.Math.LevenbergMarquardt.BaseFit
        {
            public ParabolicFit(DataSet fit_data, double direction) : base(fit_data, direction)
            {
                this.InitCoefficients = new double[3] { 0.0, 1.0, 0.0 };
            }

            public override Vector FunctionVector(double[] coeff)
            {
                Vector fvec = new DenseVector(this.FitData.X.Count);
                for (int ix = 0; ix < this.FitData.X.Count; ++ix)
                {
                    fvec[ix] = coeff[0] + coeff[1] * Pow((this.FitData.X[ix] - coeff[2]), 2f);
                }
                return fvec;
            }

            public override void NormalizeFitData()
            {
                this.FitData *= this.Direction;
                this.XShift = this.FitData.XMedian();
                this.YShift = this.FitData.YMin();
                this.FitData -= new DataSet.Operand(this.XShift, this.YShift);
                this.XScale = 1.0;
                this.YScale = 1.0 / this.FitData.YMax();
                this.FitData *= new DataSet.Operand(this.XScale, this.YScale);
            }

            public override double[] UnNormalizeCoefficients(double[] coeff)
            {
                coeff[0] = (coeff[0] / this.YScale + this.YShift) / this.Direction;
                coeff[1] = (coeff[1] / this.YScale) / this.Direction;
                coeff[2] = coeff[2] / this.XScale + this.XShift;

                return coeff;
            }
        }
    }
}

