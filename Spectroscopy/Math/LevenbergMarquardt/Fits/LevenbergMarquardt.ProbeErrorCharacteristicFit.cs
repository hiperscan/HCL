// Created with MonoDevelop
//
//    Copyright (C) 2017 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;

using MathNet.Numerics.LinearAlgebra.Double;

using static System.Math;


namespace Hiperscan.Spectroscopy.Math
{

    public partial class LevenbergMarquardt
    {

        public class ProbeErrorCharacteristicFit : Hiperscan.Spectroscopy.Math.LevenbergMarquardt.BaseFit
        {
            public ProbeErrorCharacteristicFit(DataSet fit_data, DataSet ref_data, double direction) : base(fit_data, ref_data, direction)
            {
                this.InitCoefficients = new double[3] { 0.0, 0.0, 0.0 };
                this.FitData = this.FitData;
                this.RefData = this.RefData.Interpolate(this.FitData.X);
            }

            public override Vector FunctionVector(double[] coeff)
            {
                Vector ref_data_vec = new DenseVector(this.RefData.Y.ToArray());
                Vector fvec = new DenseVector(this.RefData.Count);
                for (int ix = 0; ix < this.RefData.Count; ++ix)
                {
                    fvec[ix] = this.RefData.X[ix] * coeff[1] + coeff[2];
                }
                fvec = (Vector)(ref_data_vec * coeff[0]).Add(fvec);

                return fvec;
            }

            public override Vector ObjectiveFunc(double[] coeff)
            {
                Vector fvec = this.FunctionVector(coeff);
                Vector fitvec = new DenseVector(this.FitData.Y.ToArray());

                return (Vector)(fitvec - fvec);
            }

            public override void NormalizeFitData()
            {
                this.FitData *= this.Direction;
            }

            public override double[] UnNormalizeCoefficients(double[] coeff)
            {
                return coeff;
            }
        }
    }
}

