﻿// Created with MonoDevelop
//
//    Copyright (C) 2017 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.


namespace Hiperscan.Spectroscopy.Math
{

    public partial class LevenbergMarquardt
    {

        public class OptimResults
        {

            public OptimResults(double[] initCoeff, TerminationType termination_type)
            {
                this.Coefficients    = initCoeff;
                this.TerminationType = termination_type;
            }

            public TerminationType TerminationType         { get; internal set; }
            public double[] Coefficients                   { get; internal set; }
            public int Iterations                          { get; internal set; } = 0;
            public bool MaxIterationsExceeded              { get; internal set; } = false;
            public double GradientNorm                     { get; internal set; } = 0;
            public double FunctionNorm                     { get; internal set; } = 0;
            public double ArgumentNorm                     { get; internal set; } = 0;
            public double StepBoundToScaledArgumentNorm    { get; internal set; } = 0;
            public double LatestFunctionReduction          { get; internal set; } = 0;
            public double LatestPredictedFunctionReduction { get; internal set; } = 0;
            public double[] ResidualVector                 { get; internal set; }
        }
    }
}