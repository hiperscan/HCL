﻿// Created with MonoDevelop
//
//    Copyright (C) 2017 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

using Hiperscan.Spectroscopy.Method;


namespace Hiperscan.Spectroscopy.IdentModule
{

    [Serializable()]
    [DataContract(Name = "ModelClassInfo", Namespace = "http://www.hiperscan.com/spectroscopy/identmodule")]
    public class ModelClassInfo : System.Runtime.Serialization.IExtensibleDataObject
    {
        [DataMember] public string ModelClassName                    { get; set; }
        [DataMember] public SubstanceType SubstanceType              { get; set; }
        [DataMember] public bool ValidationDocument                  { get; set; }
        [DataMember] public string DefaultSupplierName               { get; set; }
        [DataMember] public List<ClassificationInfo> Classifications { get; set; }

        public AccessDatabaseInfo DatabaseInfo { get; set; }

        [NonSerialized] private ExtensionDataObject extension_data;
        [NonSerialized] private Dictionary<string, SubstanceNameInfo> substance_name_resolver = null;

        private class SubstanceNameInfo
        {
            public ClassificationInfo ClassificationInfo { get; set; }
            public SubstanceInfo SubstanceInfo { get; set; }
        }

        public ModelClassInfo(string model_class_name)
        {
            this.ModelClassName          = model_class_name;
            this.SubstanceType           = SubstanceType.Solid;
            this.ValidationDocument      = false;
            this.DefaultSupplierName     = string.Empty;
            this.Classifications         = new List<ClassificationInfo>();
            this.substance_name_resolver = new Dictionary<string, SubstanceNameInfo>();
        }

        public void InitResolver()
        {
            this.substance_name_resolver = new Dictionary<string, SubstanceNameInfo>();

            foreach (ClassificationInfo classification_info in this.Classifications)
            {
                foreach (SubstanceInfo substance_info in classification_info.Substances)
                {
                    if (substance_info.IsVisible == false)
                        continue;

                    foreach (string name in substance_info.AllNames)
                    {
                        SubstanceNameInfo info = new SubstanceNameInfo
                        {
                            ClassificationInfo = classification_info,
                            SubstanceInfo = substance_info
                        };
                        this.substance_name_resolver.Add(name, info);
                    }
                }
            }
        }

        public ClassificationInfo GetClassificationInfo(string substance_name)
        {
            return this.substance_name_resolver[substance_name].ClassificationInfo;
        }

        public SubstanceInfo GetSubstanceInfo(string substance_name)
        {
            return this.substance_name_resolver[substance_name].SubstanceInfo;
        }

        public ExtensionDataObject ExtensionData
        {
            get { return this.extension_data; }
            set { this.extension_data = value; }
        }
    }
}