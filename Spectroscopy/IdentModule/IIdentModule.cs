﻿// Created with MonoDevelop
//
//    Copyright (C) 2020 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;

using Hiperscan.Common;
using Hiperscan.Spectroscopy.IdentServer;


namespace Hiperscan.Spectroscopy.IdentModule
{

    public interface IIdentModule
    {
        OperationResult StartIdentServer(string host = IdentModuleClient.HOST, int port = IdentModuleClient.TCP_PORT);
        OperationResult StopIdentServer();

        OperationResult GetChallenge(out byte[] challenge);
        OperationResult Initialize(string serial, byte[] master_respons, out List<AccessDatabaseInfo> database_infos);

        OperationResult GetModelClassNames(AuthenticationSet authentification, out List<string> classifier_names);
        OperationResult GetModelClassInfo(AuthenticationSet authentification, out ModelClassInfo model_class_info);
        OperationResult GetModelClassExtensionInfo(AuthenticationSet authentification, out ModelClassExtensionInfo model_class_extension_info);

        OperationResult Classify(AuthenticationSet authentification, Spectrum spectrum, out ResultSet result);

        OperationResult GetIdentModuleInfo(out IdentModuleInfo identmodule_info);
        OperationResult GetUpdateCertificateData(out UpdateCertificateInfo update_certificate_info);

        OperationResult GetValidationDocument(string model_class_name, out byte[] pdf_stream);
        OperationResult GetAdditionalTestPdf(out byte[] pdf_stream);

        OperationResult GetLicense(out byte[] txt_stream);
        OperationResult GetLogfile(out byte[] zip_stream);
        OperationResult GetUpdateChangesFile(out byte[] pdf_stream);
    }
}
