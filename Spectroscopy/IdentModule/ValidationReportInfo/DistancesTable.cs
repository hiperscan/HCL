﻿// Created with MonoDevelop
//
//    SearchIPAddr: find spectrometer in local network
//    Copyright (C) 2020 A. Höpfner, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.



using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Hiperscan.Spectroscopy.IdentModule.ValidationReportInfo
{
    [DataContract(Name = "DistancesTable", Namespace = "http://www.hiperscan.com/spectroscopy/identmodule/validationreportinfo")]
    [Serializable()]
    public class DistancesTable : System.Runtime.Serialization.IExtensibleDataObject
    {
        [Serializable]
        public class DistanceEntry 
        {
            public string MainModel { get; set; } = String.Empty;
            public string SecondModel { get; set; } = String.Empty;
        }

        [DataMember] public Dictionary<string, DistanceEntry> Distances { get; private set; } = new Dictionary<string, DistanceEntry>();

        [NonSerialized] ExtensionDataObject extension_data = null;
        public ExtensionDataObject ExtensionData
        {
            get { return this.extension_data; }
            set { this.extension_data = value; }
        }
    }
}
