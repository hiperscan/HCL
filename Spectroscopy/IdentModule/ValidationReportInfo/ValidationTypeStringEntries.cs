﻿// Created with MonoDevelop
//
//    SearchIPAddr: find spectrometer in local network
//    Copyright (C) 2020 A. Höpfner, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.



using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Hiperscan.Spectroscopy.IdentModule.ValidationReportInfo
{
    [DataContract(Name = "ValidationTypeStringEntries", Namespace = "http://www.hiperscan.com/spectroscopy/identmodule/validationreportinfo")]
    [Serializable()]
    public class ValidationTypeStringEntries : System.Runtime.Serialization.IExtensibleDataObject
    {
        [NonSerialized] ExtensionDataObject extension_data = null;
        public ExtensionDataObject ExtensionData
        {
            get { return this.extension_data; }
            set { this.extension_data = value; }
        }

        public ValidationTypeStringEntries()
        {
        }

        public ValidationTypeStringEntries(bool pre_init)
        {
            if (pre_init == true)
            {
                this.Items.Add(ValidationTypes.A, String.Empty);
                this.Items.Add(ValidationTypes.B, String.Empty);
                this.Items.Add(ValidationTypes.C, String.Empty);
            }
        }

        public string this[ValidationTypes type]
        {
            get
            {
                return this.Items[type];
            }
            set
            {
                this.Items[type] = value;
            }
        }


        private Dictionary<ValidationTypes, string> Items { get; set; } = new Dictionary<ValidationTypes, string>();
    }
}
