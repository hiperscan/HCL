﻿// Created with MonoDevelop
//
//    Copyright (C) 2017 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.IO;
using System.Runtime.Serialization;
using System.Xml;

using Hiperscan.Spectroscopy.Method;


namespace Hiperscan.Spectroscopy.IdentModule
{

    [DataContract(Name = "ModelClassExtensionInfo", Namespace = "http://www.hiperscan.com/spectroscopy/identmodule")] [Serializable()]
    public class ModelClassExtensionInfo : Hiperscan.Spectroscopy.IdentModule.ModelClassInfo, System.Runtime.Serialization.IExtensibleDataObject
    {

        public ModelClassExtensionInfo() : base(string.Empty)
        {
            this.SubstanceType = SubstanceType.Solid;
            this.ValidationDocument = false;
            this.DefaultSupplierName = string.Empty;
        }

        public static void Write(ModelClassExtensionInfo class_info, string fname)
        {
            XmlWriterSettings settings = new XmlWriterSettings
            {
                Indent = true,
                IndentChars = "\t",
                NewLineOnAttributes = true,
                NewLineChars = "\n"
            };

            using (XmlWriter writer = XmlWriter.Create(fname, settings))
            {
                DataContractSerializer serializer = new DataContractSerializer(typeof(ModelClassExtensionInfo));

                // we want to register a global namespace to prevent thousands of repeats
                serializer.WriteStartObject(writer, class_info);
                writer.WriteAttributeString("xmlns", "p", null, "http://schemas.microsoft.com/2003/10/Serialization/Arrays");
                serializer.WriteObjectContent(writer, class_info);
                serializer.WriteEndObject(writer);
            }
        }

        public static ModelClassExtensionInfo Read(Stream stream)
        {
            ModelClassExtensionInfo class_info = null;
            using (XmlTextReader tr = new XmlTextReader(stream))
            {
                DataContractSerializer serializer = new DataContractSerializer(typeof(ModelClassExtensionInfo));
                class_info = (ModelClassExtensionInfo)serializer.ReadObject(tr);
                class_info.InitResolver();
            }

            return class_info;
        }

        public static ModelClassExtensionInfo Read(string file_name)
        {
            ModelClassExtensionInfo class_info = null;
            using (Stream stream = File.Open(file_name, FileMode.Open, FileAccess.Read))
            {
                class_info = ModelClassExtensionInfo.Read(stream);
            }
            return class_info;
        }
    }
}
