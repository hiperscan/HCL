﻿// Created with MonoDevelop
//
//    Copyright (C) 2017 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Xml;

using Hiperscan.Spectroscopy.IdentServer;


namespace Hiperscan.Spectroscopy.IdentModule
{
    [DataContract(Name = "IdentModuleInfo", Namespace = "http://www.hiperscan.com/spectroscopy/identmodule")] [Serializable]
    public class IdentModuleInfo : System.Runtime.Serialization.IExtensibleDataObject
    {
        [DataMember] public string PcaVersion             { get; set; }
        [DataMember] public Common.I18n.Locale.LocaleType Locale { get; set; }
        [DataMember] public IdentServerVersion Version    { get; set; }
        [DataMember] public Guid Guid                     { get; set; }
        [DataMember] public List<string> ModelClassNames  { get; set; }
        [DataMember] public DateTime CreationTimestamp    { get; set; } = DateTime.Now;
        [DataMember] public bool IsRelease                { get; set; }
        [DataMember] public int PortNumber                { get; set; } = IdentModuleClient.TCP_PORT;

        public static IdentModuleInfo Read(string fname)
        {
            IdentModuleInfo info = null;
            using (Stream stream = File.Open(fname, FileMode.Open, FileAccess.Read))
            {
                using (XmlTextReader tr = new XmlTextReader(stream))
                {
                    DataContractSerializer serializer = new DataContractSerializer(typeof(IdentModuleInfo));
                    info = (IdentModuleInfo)serializer.ReadObject(tr);
                }
            }
            return info;
        }

        public static void Write(IdentModuleInfo ident_module_info, string fname)
        {
            XmlWriterSettings settings = new XmlWriterSettings
            {
                Indent = true,
                IndentChars = "\t",
                NewLineOnAttributes = true,
                NewLineChars = "\n"
            };

            using (XmlWriter writer = XmlWriter.Create(fname, settings))
            {
                DataContractSerializer serializer = new DataContractSerializer(typeof(IdentModuleInfo));

                // we want to register a global namespace to prevent thousands of repeats
                serializer.WriteStartObject(writer, ident_module_info);
                writer.WriteAttributeString("xmlns", "p", null, "http://schemas.microsoft.com/2003/10/Serialization/Arrays");
                serializer.WriteObjectContent(writer, ident_module_info);
                serializer.WriteEndObject(writer);
            }
        }

        public override string ToString()
        {
            string info = "IdentModuleInfo:";
            info += string.Format("\nAssembly version:               {0}",    this.Version.AssemblyVersion);
            info += string.Format("\nShort assembly version:         {0}",    this.Version.ShortAssemblyVersion);
            info += string.Format("\nIdentServer version:            {0}",    this.Version.IdentServerId);
            info += string.Format("\nIdentModule version:            {0}",    this.Version.IdentModuleVersion);
            info += string.Format("\nIdentModule subversion:         {0}",    this.Version.IdentModuleSubVersion);
            info += string.Format("\nIs release:                     {0}",    this.IsRelease);
            info += string.Format("\nIdentModule locale:             {0}",    this.Locale);
            info += string.Format("\nIdentModule GUID:               {0}",    this.Guid);
            info += string.Format("\nIdentModule creation timestamp: {0}",    this.CreationTimestamp);
            info += string.Format("\nPCA version:                    {0}",    this.PcaVersion);
            info += string.Format("\nHostname:                       {0}",  Environment.MachineName);
            info += string.Format("\nTCP Port number:                {0}",  this.PortNumber);

            foreach (string model_class_name in this.ModelClassNames)
            {
                info += string.Format("\nModel Class name: {0}",    model_class_name);
            }

            return info;
        }

        [NonSerialized] private ExtensionDataObject extension_data;
        public ExtensionDataObject ExtensionData
        {
            get { return this.extension_data;  }
            set { this.extension_data = value; }
        }
    }
}

