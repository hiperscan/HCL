﻿// Created with MonoDevelop
//
//    Copyright (C) 2017 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace Hiperscan.Spectroscopy.IdentModule
{

    [DataContract(Name = "SubstanceInfo", Namespace = "http://www.hiperscan.com/spectroscopy/identmodule")] [Serializable()]
    public class SubstanceInfo : IComparable<SubstanceInfo>, System.Runtime.Serialization.IExtensibleDataObject
    {
        [DataMember] public string Id                      { get; set; }
        [DataMember] public string SubstanceClassName      { get; set; }
        [DataMember] public string PrimaryDefaultName      { get; set; }
        [DataMember] public string PrimaryLatinName        { get; set; }
        [DataMember] public List<string> OtherDefaultNames { get; set; }
        [DataMember] public List<string> OtherLatinNames   { get; set; }

        [DataMember] public string AdditionalSubstanceInfo { get; set; }
        [DataMember] public bool CanInsert                 { get; set; }
        [DataMember] public bool IsVisible                 { get; set; }
        [DataMember] public List<string> BatchNumbers      { get; set; }
        [DataMember] public string BAKMarking              { private get; set; }

        [NonSerialized] private List<string> all_names = null;
        [NonSerialized] private ExtensionDataObject extension_data;

        public SubstanceInfo(string id)
        {
            this.Id = id;
            this.SubstanceClassName      = string.Empty;
            this.PrimaryDefaultName      = string.Empty;
            this.PrimaryLatinName        = string.Empty;
            this.OtherDefaultNames       = new List<string>();
            this.OtherLatinNames         = new List<string>();
            this.AdditionalSubstanceInfo = string.Empty;
            this.CanInsert               = false;
            this.BatchNumbers            = new List<string>();
            this.IsVisible               = true;
            this.BAKMarking              = String.Empty;
        }

        private bool IsDefaultName(string substance_name)
        {
            if (this.PrimaryDefaultName == substance_name)
                return true;

            return this.OtherDefaultNames.Contains(substance_name);
        }

        public int CompareTo(SubstanceInfo other)
        {
            if (other == null)
                return 1;

            return string.Compare(this.PrimaryDefaultName, other.PrimaryDefaultName, StringComparison.InvariantCulture);
        }

        public List<string> AllNames
        {
            get
            {
                if (this.all_names == null)
                {
                    this.all_names = new List<string>
                    {
                        this.PrimaryDefaultName
                    };

                    foreach (string name in this.OtherDefaultNames)
                    {
                        if (this.all_names.Contains(name) == false)
                            this.all_names.Add(name);
                    }

                    if (string.IsNullOrEmpty(this.PrimaryLatinName) == false)
                    {
                        if (this.all_names.Contains(this.PrimaryLatinName) == false)
                            this.all_names.Add(this.PrimaryLatinName);

                        foreach (string latin_name in this.OtherLatinNames)
                        {
                            if (this.all_names.Contains(latin_name))
                                continue;

                            this.all_names.Add(latin_name);
                        }
                    }
                }

                return this.all_names;
            }
        }

        public bool HasAlternativeNames
        {
            get { return this.OtherLatinNames.Any() || this.OtherDefaultNames.Any(); }
        }

        public BAKMarkingState BAKMarkingState
        {
            get
            {
                string data = (this.BAKMarking ?? String.Empty).ToLower();

                if (String.IsNullOrEmpty(data))
                    return BAKMarkingState.None;

                if (data.Contains("keine"))
                    return BAKMarkingState.None;

                if (data.Contains("rot"))
                    return BAKMarkingState.Red;


                BAKMarkingState state = BAKMarkingState.None;

                if (data.Contains("blau"))
                    state = state | BAKMarkingState.Blue;

                if (data.Contains("gelb"))
                    state = state | BAKMarkingState.Yellow;

                if (data.Contains("orange"))
                    state = state | BAKMarkingState.Orange;

                return state;
            }
        }

        public ExtensionDataObject ExtensionData
        {
            get { return this.extension_data;  }
            set { this.extension_data = value; }
        }
    }
}