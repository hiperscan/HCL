﻿// Created with MonoDevelop
//
//    Copyright (C) 2018 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Runtime.Serialization;


namespace Hiperscan.Spectroscopy.IdentModule
{
    public enum AccessDatabaseType
    {
        None,
        Master,
        Extension,
        UserDefined,
    }

    [DataContract(Name = "AccessDatabaseInfo", Namespace = "http://www.hiperscan.com/spectroscopy/identmodule")] [Serializable()]
    public class AccessDatabaseInfo : System.Runtime.Serialization.IExtensibleDataObject
    {
        [DataMember] public string Name             { get; private set; }
        [DataMember] public AccessDatabaseType Type { get; private set; }
        [DataMember] public byte[] IndividualKey    { get; set; }

        [NonSerialized] private ExtensionDataObject extension_data;

        private AccessDatabaseInfo()
        {
        }

        public AccessDatabaseInfo(string name, AccessDatabaseType type)
        {
            this.Type          = type;
            this.Name          = name;
            this.IndividualKey = null;        
        }

        public override int GetHashCode()
        {
            return (this.Name.GetHashCode() * 211) ^ (this.Type.GetHashCode() * 37);
        }

        public override bool Equals(object obj)
        {
            if (Object.ReferenceEquals(this, obj) == true)
                return true;

            AccessDatabaseInfo other = (AccessDatabaseInfo) obj;
            if (other is null)
                return false;

            return this.Name == other.Name && this.Type == other.Type;                
        }


        public static bool operator==(AccessDatabaseInfo lhs, AccessDatabaseInfo rhs)
        {
            if (lhs is null)
            {
                if (rhs is null)
                    return true;

                return false;
            }

            return lhs.Equals(rhs);
        }

        public static bool operator!=(AccessDatabaseInfo lhs, AccessDatabaseInfo rhs)
        {
            return (lhs == rhs) == false;
        }


        public static AccessDatabaseInfo UserDefined
        {
            get
            {
                return new AccessDatabaseInfo("UserDefined", AccessDatabaseType.UserDefined);
            }
        }

        public static AccessDatabaseInfo Master
        {
            get
            {
                return new AccessDatabaseInfo("Master", AccessDatabaseType.Master);
            }
        }

        public ExtensionDataObject ExtensionData
        {
            get { return this.extension_data; }
            set { this.extension_data = value; }
        }
    }
}