﻿// Created with MonoDevelop
//
//    Copyright (C) 2018 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Hiperscan.Spectroscopy.IdentModule.ValidationReportInfo;

namespace Hiperscan.Spectroscopy.IdentModule
{

    [DataContract(Name = "ClassificationInfo", Namespace = "http://www.hiperscan.com/spectroscopy/identmodule")] [Serializable()]
    public class ClassificationInfo : System.Runtime.Serialization.IExtensibleDataObject
    {

        [DataMember] public string Id                             { get; set; }
        [DataMember] public string ClassificationName             { get; set; }
        [DataMember] public ClassificationType ClassificationType { get; set; }
        [DataMember] public List<SubstanceInfo> Substances        { get; set; }
        [DataMember] public bool IsUserDefined                    { get; set; }
        [DataMember] public bool IsNirMeasurable                  { get; set; }
        [DataMember] public ClassificationReportInfo ReportInfo   { get; set; }

        [NonSerialized] private ExtensionDataObject extension_data;


        public ClassificationInfo(string id)
        {
            this.Id                 = id;
            this.ClassificationName = string.Empty;
            this.ClassificationType = ClassificationType.Single;
            this.Substances         = new List<SubstanceInfo>();   
            this.IsUserDefined      = false;
            this.IsNirMeasurable    = true;
            this.ReportInfo         = null;        
        }

        public Dictionary<string, string> GetSubstanceNames(SubstanceInfo exclude_info)
        {
            Dictionary<string, string> value = new Dictionary<string, string>();

            foreach (SubstanceInfo substance_info in this.Substances)
            {
                if (substance_info.IsVisible == false)
                    continue;
                
                if (exclude_info != null && exclude_info.Id == substance_info.Id)
                    continue;

                value.Add(substance_info.PrimaryDefaultName, substance_info.PrimaryLatinName);
            }

            return value; 
        }

        public ExtensionDataObject ExtensionData
        {
            get { return this.extension_data; }
            set { this.extension_data = value; }
        }
    }
}