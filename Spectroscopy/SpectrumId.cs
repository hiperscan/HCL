﻿// Created with MonoDevelop
//
//    Copyright (C) 2017 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

using Hiperscan.Unix;


namespace Hiperscan.Spectroscopy
{

    public static class SpectrumId
    {
        public static string GetSampleId(Spectrum spectrum)
        {
            return SpectrumId.GetSampleId(spectrum.FileInfo.Name);
        }

        public static string GetSampleId(string fname)
        {
            string basename = Path.GetFileNameWithoutExtension(fname);
            int ix = basename.IndexOf('_');
            if (ix < 0)
                throw new Exception(Catalog.GetString("Spectrum filename:") + " " + fname);
            return basename.Substring(0, ix);
        }

        public static string GetSpectrumId(Spectrum spectrum)
        {
            return SpectrumId.GetSpectrumId(spectrum.FileInfo.Name);
        }

        public static string GetSpectrumId(string fname)
        {
            return Path.GetFileNameWithoutExtension(fname);
        }

        public static string RemoveNoId(string id)
        {
            return Regex.Replace(id, @"^NOID", string.Empty).Trim();
        }

        public static bool IsNoId(string id)
        {
            return Regex.IsMatch(id, @"^NOID");
        }

        public static string AddNoId(string id)
        {
            return "NOID" + id;
        }

        public static bool IsSampleInsert(string id)
        {
            return Regex.IsMatch(id, @"SI$");
        }

        public static string RemoveSampleInsert(string id)
        {
            return Regex.Replace(id, @"SI$", string.Empty).Trim();
        }

        public static string GetSubstanceId(string entry)
        {
            return entry.Split('_').First();
        }
    }
}