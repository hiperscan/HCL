﻿// Created with MonoDevelop
//
//    Copyright (C) 2018 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;


namespace Hiperscan.Common.CommandLineArgsParser
{
    public class MenuComparer : IComparer<IOption>
    {
        public int Compare(IOption x, IOption y)
        {
            if (x != null && y != null)
            {
                string this_string = x.ShortName;
                if (string.IsNullOrEmpty(this_string) == true || this_string == "-?")
                    this_string = x.LongName;

                string other_string = y.ShortName;
                if (string.IsNullOrEmpty(other_string) == true || other_string == "-?")
                    other_string = y.LongName;

                return string.Compare(this_string.ToLower(), other_string.ToLower(), StringComparison.InvariantCulture);
            }
            else
            {
                return x.ParsePriority.CompareTo(y.ParsePriority);
            }
        }
    }
}