﻿// Created with MonoDevelop
//
//    Copyright (C) 2018 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System.Collections.Generic;


namespace Hiperscan.Common.CommandLineArgsParser
{

    public interface IOption
    {
        void Parse(List<PredefinedOption> args);
        void WriteMenu();

        string ParameterName            {get; set;}
        string Explanation              {get; set;}
        string LongName                 {get; set;}
        string ShortName                {get; set;}
        bool HasParameter               {get; set;}
        List<string> PredefinedValues   {get; set;}

        int ParsePriority               {get; set;}

        bool Disabled                   {get; set;}
        string Identifier               {get; set;}
        bool Undocumented               {get; set;}
    }
}
