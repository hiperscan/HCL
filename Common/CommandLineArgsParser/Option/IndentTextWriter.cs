﻿// Created with MonoDevelop
//
//    Copyright (C) 2018 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;


namespace Hiperscan.Common.CommandLineArgsParser
{

    public static class IndentTextWriter
    {
        public static void Write(int start_xposition, string text)
        {
            if (string.IsNullOrEmpty(text) == true)
                return;

            if (IndentTextWriter.CurrentXPosition >= start_xposition)
                Console.WriteLine();

            string[] lines = text.Split(new char[]{'\n'});

            foreach (string line in lines)
            {
                if (string.IsNullOrEmpty(line) == true)
                {
                    Console.WriteLine();
                    continue;
                }

                IndentTextWriter.MoveCursor(start_xposition);

                string[] words = line.Split(new char[]{' '});

                for (int i = 0; i < words.Length; i++)
                {
                    if (string.IsNullOrEmpty(words[i]) == true)
                    {
                        IndentTextWriter.AddSpace(start_xposition);
                        continue;
                    }

                    IndentTextWriter.AddWord(start_xposition, words[i]);
                    if (i < words.Length - 1)
                        AddSpace(start_xposition);
                }
            }
        }

        private static void AddWord(int start_xposition, string word)
        {
            int max_word_length = IndentTextWriter.MaxXPosition - start_xposition + 1;
            int word_lenght = word.Length;

            bool new_line = false;
            if (word_lenght <= max_word_length)
            {
                int available_length = IndentTextWriter.MaxXPosition - IndentTextWriter.CurrentXPosition + 1;
                if (available_length < word_lenght)
                    new_line = true; 
            }
            else
            {
                if (CurrentXPosition > start_xposition + max_word_length * 2 / 3)
                    new_line = true; 
            }

            if (new_line == true)
            {
                Console.WriteLine();
                IndentTextWriter.MoveCursor(start_xposition);
            }

            foreach (char c in word)
            {  
                if (IndentTextWriter.CurrentXPosition == 0)
                    IndentTextWriter.MoveCursor(start_xposition);

                Console.Write(c);
            }
        }

        private static void AddSpace(int start_xposition)
        {
            if (IndentTextWriter.CurrentXPosition < IndentTextWriter.MaxXPosition &&
                IndentTextWriter.CurrentXPosition > 0)
                Console.Write(" ");
            else
                IndentTextWriter.MoveCursor(start_xposition);
        }

        private static void MoveCursor(int start_xposition)
        {
            for(int i = IndentTextWriter.CurrentXPosition; i < start_xposition; i++)
                Console.Write(" ");
        }

        private static int CurrentXPosition
        {
            get
            {
                return Console.CursorLeft;
            }
        }

        private static int MaxXPosition => Console.WindowWidth - 1;

        public static int FirstIndent  {get; set;} = 2;
        public static int SecondIndent {get; set;} = 6;
        public static int ThirdIndent  {get; set;} = 42;
    }
}

