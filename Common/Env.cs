﻿// Created with MonoDevelop
//
//    Copyright (C) 2018 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;


namespace Hiperscan.Common
{

    public static class Env
    {
        public static class ApoIdent
        {
            public const string DEBUG            = "APOIDENT_DEBUG";                         // Debug Mode, if set to 1
            public const string TOUCHSCREEN_MODE = "APOIDENT_TOUCHSCREEN_MODE";              // Switch to experimental touch screen mode, if set to 1
            public const string FORCE_WARMUP     = "APOIDENT_FORCE_WARMUP";                  // Start Warm-Up Mode in any case, if set to 1
            public const string AVERAGE          = "APOIDENT_AVERAGE";                       // Set default averaging for spectrum acquisition
            public const string DISABLE_SPLASH   = "APOIDENT_DISABLE_SPLASH";                // Disables the splash screen, if value set to 1

            public const string LIGHT_SOURCE_DAMAGE_THRESHOLD = "APOIDENT_LIGHT_SOURCE_DAMAGE_THRESHOLD"; // Threshold for light source failure detection
            public const string NO_FINDER_PLAUSIBILTY_CHECK   = "APOIDENT_NO_FINDER_PLAUSIBILTY_CHECK";   // Checking finder calibrations is deactivated   if set to 1
            public const string BLINDCLASSIFICATION           = "APOIDENT_BLINDCLASSIFICATION";           //If set to 1, then this mode is activated
            public const string COLOR                         = "APOIDENT_COLOR";
            public const string CITRIX_SUPPORT                = "APOIDENT_CITRIX_SUPPORT";
            public const string SHOW_LOADED_ASSEMBLIES        = "APOIDENT_SHOW_LOADED_ASSEMBLIES";
            public const string NO_IDENTMODULE_LOG            = "APOIDENT_NO_IDENTMODULE_LOG";
        }

        public static class Finder
        {
            public const string NO_SAMPLE_SPINNER                = "FINDER_NO_SAMPLE_SPINNER";                  // Disable peripheral "Sample Spinner"
            public const string SAMPLE_SPINNER_VMAX              = "FINDER_SAMPLE_SPINNER_VMAX";                // Set max value for angular velocity (VMIN..16)
            public const string SAMPLE_SPINNER_VMIN              = "FINDER_SAMPLE_SPINNER_VMIN";                // Set min value vor angular velocity (1..VMAX)
            public const string SAMPLE_SPINNER_INIT_TIME         = "FINDER_SAMPLE_SPINNER_INIT_TIME";           // Set duration of init cycle in ms
            public const string SAMPLE_SPINNER_REF_ANGLE         = "FINDER_SAMPLE_SPINNER_REF_ANGLE";           // Set angle to turn from Black to White Reference
            public const string NO_SESSION_TIMEOUT               = "FINDER_NO_SESSION_TIMEOUT";                 // Disable USB session timeout
            public const string MIN_TEMP_GRADIENT_LIMIT          = "FINDER_MIN_TEMP_GRADIENT_LIMIT";            // Minimum absolute Temperature Gradient for ready-state
            public const string MAX_TEMP_GRADIENT_LIMIT          = "FINDER_MAX_TEMP_GRADIENT_LIMIT";            // Maximum absolute Temperature Gradient for ready-state
            public const string MIN_TEMP_GRADIENT_GRADIENT_LIMIT = "FINDER_MIN_TEMP_GRADIENT_GRADIENT_LIMIT";   // Minimum absolute Temperature Gradient of Gradient for ready-state
            public const string MAX_TEMP_GRADIENT_GRADIENT_LIMIT = "FINDER_MAX_TEMP_GRADIENT_GRADIENT_LIMIT";   // Maximum absolute Temperature Gradient of Gradient for ready-state
        }

        public static class QuickStep
        {
            public const string NO_DEVICE_HOTPLUG = "QS_NO_DEVICE_AUTOUPDATE";  // Disable USB hot-plugging
            public const string CONFIG_DIR        = "QS_CONFIG_DIR";            // Define a custom configuration profile directory
        }

        public static class HCL
        {
            public const string DEBUG                             = "HCL_DEBUG";                             // Debug mode
            public const string NO_TEMPERATURE_STABILIZATION      = "HCL_NO_TEMPERATURE_STABILIZATION";      // Disable temperature stabilization for Finder Hardware
            public const string DISABLE_TEMP_GRADIENT             = "HCL_DISABLE_TEMP_GRADIENT";             // Disable temperature gradient criterion for Finder Hardware
            public const string NO_LIGHT_SOURCE_STABILIZATION     = "HCL_NO_LIGHT_SOURCE_STABILIZATION";     // Disable light source stabilization for Finder Hardware
            public const string LOG_ACQUISITION_PERIOD            = "HCL_LOG_ACQUISITION_PERIOD";            // Save duration of every spectrum acquisition in spectrum header
            public const string NO_LUT_PLAUSIBILITY_CHECK         = "HCL_NO_LUT_PLAUSIBILITY_CHECK";         // Never check LUT for plausibility
            public const string SET_LIGHT_SOURCE_ERROR            = "HCL_SET_LIGHT_SOURCE_ERROR";            // Simulate a light source error
            public const string SPECTRAL_ENGINES_SUPPORT          = "HCL_SPECTRAL_ENGINES_SUPPORT";          // Enable driver support for Spectral Engines (experimental)
            public const string SERIALIZATION_LEGACY_MODE         = "HCL_SERIALIZATION_LEGACY_MODE";         // Enable support for legacy XML serialization data (files/remoting)
            public const string SET_HEATING_ERROR                 = "HCL_SET_HEATING_ERROR";                 // Simulate a heating error for Finder hardware
            public const string EXPERIMENTAL                      = "HCL_EXPERIMENTAL";                      // Activate API calls which are marked as experimental
            public const string SKIP_LIGHT_SOURCE_STABILIZATION   = "HCL_SKIP_LIGHT_SOURCE_STABILIZATION";   // Waiting time for light source to become stable is skipped (debugging only)
            public const string READ_SPECTRUM_TRY_COUNT           = "HCL_READ_SPECTRUM_TRY_COUNT";           // sets the spectrum read try count by given value
            public const string FAST_WARMUP_TEMP_OFFSET           = "HCL_FAST_WARMUP_TEMP_OFFSET";           // temperature offset for deactivation of Fast Warmup mode (e.g. Finder SD)
            public const string SGSNT_DEFAULT_ACQUISITION_CHANNEL = "HCL_SGSNT_DEFAULT_ACQUISITION_CHANNEL"; // sets the default channel for data sampling 0=ch1, 1=ch1|reverse, 2=Ch2, 3=ch2|reverse...
            public const string NO_WAVELENGTH_DEVIATION_LIMIT     = "HCL_NO_WAVELENGTH_DEVIATION_LIMIT";     // deactivates the plausibility check of wavelength corrections
            public const string DISABLE_RECALIBRATION             = "HCL_DISABLE_RECALIBRATION";             // deactivate automatic recalibration
            public const string DISABLE_WAVELENGTH_CORRECTION     = "HCL_DISABLE_WAVELENGTH_CORRECTION";     // completely deactivate wavelength correction
        }

        public static class Plugin
        {
            public const string ACQUISITION_NO_WLC                 = "ACQUISITION_NO_WLC";                 // Disable wavelength correction in Acquisition Plug-In
            public const string ACQUISITION_NO_PLAUSIBILITY_CHECKS = "ACQUISITION_NO_PLAUSIBILITY_CHECKS"; // Disable all plausibility checks for reference spectra in Acquisition Plug-In
            public const string ACQUISITION_TIME_LIMIT             = "ACQUISITION_TIME_LIMIT";             // Define time limit for references in minutes in Acquisition Plug-In
            public const string EURO_OTC                           = "EURO_OTC";                           // Enable Euro OTC mode in Acquisition Plug-In
            public const string ACQUISITION_DISABLE_PLOT           = "ACQUISITION_DISABLE_PLOT";           // Do not update QuicStep Plot while acquisition in Acquisition Plug-In
            public const string ACQUISITION_ID_POSTFIX             = "ACQUISITION_ID_POSTFIX";             // Define spectrum-ID postfix in Acquisition Plug-In

            public const string ACQUISITION_TIME_LIMIT_TRANSFLECT  = "ACQUISITION_TIME_LIMIT_TRANSFLECT"; // Define time limit for transflect references in seconds in Acquisition Plug-In

        }

        public static class PerformanceQualification
        {
            public const string PERFORMANCEQUALIFICATION_PATH = "PERFORMANCEQUALIFICATION_PATH"; //contains the content of PATH-Variable (windows) before changing PATH
        }

        public static class OperationQualification
        {
            public const string DEBUG                                       = "OQ_DEBUG";           // activated if set to 1
            public const string SPECTRUM_LENGTH                             = "OQ_SPECTRUM_LENGTH"; // define expected data array length in spectrum
            public const string SKIP_WAVELENGTH_ACCURACY_CALCULATION        = "OQ_SKIP_WAVELENGTH_ACCURACY_CALCULATION"; // do not try to calculate wavelength accuracy while OQ
            public const string SKIP_WAVELENGTH_REPRODUCIBILITY_CALCULATION = "OQ_SKIP_WAVELENGTH_REPRODUCIBILITY_CALCULATION"; // do not try to calculate wavelength reproducibility while OQ
        }
    }
}