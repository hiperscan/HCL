﻿// Created with MonoDevelop
//
//    Copyright (C) 2016 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Globalization;
using System.IO;
using System.Reflection;

using Hiperscan.Unix;


namespace Hiperscan.Common.I18n
{

    public static class Locale
    {
        public enum LocaleType
        {
            Unknown,
            de_DE,
            en_GB,
            cs_CZ,
        }

        private const string LANGUAGE         = "LANGUAGE";
        private const string COUNTRY          = "COUNTRY";
        public const string LANGUAGE_COUNTRY  = "LANGUAGE_COUNTRY";
        public const string NO_SUBSTANCE_NAME = "NO_SUBSTANCE_NAME";
        public const string C                 = "C";

        private static CultureInfo current_culture = null;


        public static string FullName
        {
            get
            {
                string full_name = string.Empty;

                switch (Locale.Language)
                {

                case "de":
                    full_name = "Deutsch";
                    break;

                case "en":
                    full_name = "English";
                    break;

                case "cs":
                    full_name = "Česky";
                    break;

                case "C":
                    full_name = "Posix default";
                    break;

                default:
                    throw new ArgumentException(Catalog.GetString("Unknown locale language:") + " " + Locale.Language);

                }

                return full_name;
            }
        }

        public static string Language
        {
            get
            {
                string val = Catalog.GetString("LANGUAGE_COUNTRY").Split(new char[]{'_', '-'})[0];
                if (val == Locale.LANGUAGE)
                    return Locale.C;

                return val;
            }
        }

        public static string Country
        {
            get
            {
                string val = Catalog.GetString("LANGUAGE_COUNTRY").Split(new char[]{'_', '-'})[1];
                if (val == Locale.COUNTRY)
                    return string.Empty;

                return val;
            }
        }

        public static string Info
        {
            get
            {
                if (Locale.Language == Locale.C && string.IsNullOrEmpty(Locale.Country))
                    return string.Format("Internationalization is set to: {0} ({1})", Locale.Language, Locale.FullName);
                return string.Format("Internationalization is set to: {0}-{1} ({2})", Locale.Language, Locale.Country, Locale.FullName);
            }
        }
            
        public static string ResourceFileRelativeToAssembly(Assembly assembly, string path, string file, bool use_country)
        {
            if (assembly == null)
                assembly = Assembly.GetExecutingAssembly();

            string destination = Path.GetDirectoryName(assembly.Location);

            destination = Path.Combine(destination, path, Locale.Language);

            if (use_country == true)
            {
                if (string.IsNullOrEmpty(Locale.Country) == false)
                    destination = Path.Combine(destination, Locale.Country, file);
            }
            else
            {
                destination = Path.Combine(destination, file);
            }

            return destination;
        }

        public static string LocalizedUseByDate(DateTime date)
        {
            return Locale.LocalizedUseByDate( date, Locale.CurrentCulture);
        }

        public static string LocalizedUseByDate(DateTime date, CultureInfo culture)
        {
            return date.ToString("MMMM yyyy", culture);
        }

        public static string LocalizedDate(DateTime date)
        {
            return Locale.LocalizedDate(date, Locale.CurrentCulture);
        }

        public static string LocalizedDate(DateTime date, CultureInfo culture)
        {
            return date.ToString("d", culture);
        }

        public static string LocalizedDateTime(DateTime date)
        {
            return date.ToString(Locale.CurrentCulture);
        }

        public static CultureInfo CurrentCulture
        {
            get
            {
                if (Locale.current_culture != null)
                    return current_culture;

                switch(Locale.Language)
                {

                case "de":
                    Locale.current_culture = new CultureInfo("de-DE");
                    break;

                case "en":
                    Locale.current_culture = new CultureInfo("en-GB");
                    break;

                case "cs":
                    Locale.current_culture = new CultureInfo("cs-CZ");
                    break;

                case "C":
                    Locale.current_culture = CultureInfo.InvariantCulture;
                    break;

                default:
                    throw new ArgumentException(Catalog.GetString("Unknown locale language:") + " " + Locale.Language);

                }

                return Locale.current_culture;
            }
        }
    }
}
