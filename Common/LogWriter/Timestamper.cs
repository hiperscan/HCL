﻿// CInterface.cs created with MonoDevelop
// User: klose at 16:58 21.10.2015
// CVS release: $Id$
//
//    QuickStep: Acquire, view and identify spectra 
//    Copyright (C) 2012  Thomas Klose, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

using System;
using System.Diagnostics;
using System.IO;
using System.Text;


namespace Hiperscan.Common.LogWriter
{
    public class Timestamper : System.IO.TextWriter, IDisposable
    {
        private readonly TextWriter writer;

        public Timestamper(string fname)
        {
            StreamWriter sw = new StreamWriter(fname)
            {
                AutoFlush = true
            };
            this.writer = sw;
        }

        public Timestamper(string fname, bool append, Encoding encoding)
        {
            StreamWriter sw = new StreamWriter(fname, append, encoding)
            {
                AutoFlush = true
            };
            this.writer = sw;
        }

        public Timestamper(TextWriter writer)
        {
            this.writer = writer;
        }

        public override void Write(char[] buffer, int index, int count)
        {
            if (this.SkipWriteLineComand == true)
            {
                this.SkipWriteLineComand = false;
                return;
            }

            if (this.IsWriteLineCommand)
            {
                string tmp = new string(buffer).Substring(index, count);
                if (tmp.EndsWith(this.NewLine, StringComparison.InvariantCulture) == false)
                {
                    tmp += this.NewLine;
                    this.SkipWriteLineComand = true;
                }

                if (this.WriteTimeStamp == true && this.Disabled == false)
                    tmp = this.Timestamp + tmp;

                this.writer.Write(tmp.ToCharArray(), 0, tmp.Length);
                this.WriteTimeStamp = true;
            }
            else
            {
                string tmp = new string(buffer).Substring(index, count);
                if (this.WriteTimeStamp == true && this.Disabled == false)
                    tmp = this.Timestamp + tmp;

                this.writer.Write(tmp.ToCharArray(), 0, tmp.Length);
                this.WriteTimeStamp = false;
            }
        }

        public override void Write(char value)
        {
            if (this.WriteTimeStamp == true && this.Disabled == false)
            {
                this.WriteTimeStamp = false;
                string tmp = this.Timestamp + value.ToString();

                this.writer.Write(tmp.ToCharArray(), 0, tmp.Length);
            }
            else
            {
                this.writer.Write(value);
            }
        }

        public override void Close()
        {
            this.writer.Close();
        }

        public new void Dispose()
        {
            this.writer.Dispose();
        }

        public bool Disabled             { get; set; } = false;
        private bool WriteTimeStamp      { get; set; } = true;
        private bool SkipWriteLineComand { get; set; } = false;

        private bool IsWriteLineCommand   => new StackTrace().ToString().Contains($"{typeof(TextWriter).ToString()}.WriteLine");
        private string Timestamp          => $"[{DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss")}.{DateTime.Now.Millisecond.ToString("d3")}] ";
        public override Encoding Encoding => this.writer.Encoding;
    }
}