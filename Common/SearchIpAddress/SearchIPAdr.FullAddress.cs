﻿// Created with MonoDevelop
//
//    SearchIPAddr: find spectrometer in local network
//    Copyright (C) 2020 A. Höpfner, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.


using System;
namespace Hiperscan.Common.SearchIpAddress
{
    public partial class SearchIPAdr
    {
        public class FullAddress
        {
            private const String INDICATOR = "<< Full Address >> ";

            public FullAddress(string address, uint port)
            {
                this.Address = address;
                this.Port = port;
            }

            public FullAddress(string full_address)
            {
                full_address = full_address ?? String.Empty;
                if (full_address.StartsWith(FullAddress.INDICATOR, StringComparison.InvariantCulture) == false)
                    return;

                string[] splitter = new string[] { FullAddress.INDICATOR, ":" };
                string[] data = full_address.Split(splitter, StringSplitOptions.RemoveEmptyEntries);

                if (data.Length != 2)
                    return;

                try
                {
                    this.Port = UInt32.Parse(data[1]);
                }
                catch (Exception)
                {
                    return;
                }

                this.Address = data[0];
            }

            public override int GetHashCode()
            {
                return (this.Address + this.Port.ToString()).GetHashCode();
            }

            public override bool Equals(object obj)
            {
                if ((obj == null) || this.GetType().Equals(obj.GetType()) == false)
                    return false;

                FullAddress tmp = (FullAddress)obj;

                if (this.Address != tmp.Address)
                    return false;

                if (this.Port != tmp.Port)
                    return false;

                return true;
            }

            public override string ToString()
            {
                if (this.IsValid)
                    return String.Format("{0}{1}:{2}", FullAddress.INDICATOR, this.Address, this.Port);
                else
                    return "No valid address";
            }

            public bool IsValid
            {
                get
                {
                    return String.IsNullOrEmpty(this.Address) == false && this.Port != 0;
                }
            }


            public string Address { get; private set; }
            public uint Port { get; private set; }
        }
    }
}
