﻿// Created with MonoDevelop
//
//    SearchIPAddr: find spectrometer in local network
//    Copyright (C) 2020 A. Höpfner, Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.


namespace Hiperscan.Common.SearchIpAddress
{
    public partial class SearchIPAdr
    {
        public static class ServiceTypes
        {
            public const string MDNS_AILINK = "_ailink._tcp";
            public const string MDNS_QUICKSTEP = "_quickstep._tcp";
            public const string PING_APO_IDENT = "apoident-|dc:a6:32";
            public const string MDNS_FINDERTEST = "_findertest._tcp";

            public static string AddIndexToMdnsServiceName(string serivce_name, int i)
            {
                string[] tmp = serivce_name.Split('.');
                return tmp[0] + i.ToString() + "." + tmp[1];
            }
        }
    }
}
