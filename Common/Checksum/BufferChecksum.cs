﻿// Created with MonoDevelop
//
//    Copyright (C) 2019 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;


namespace Hiperscan.Common.Checksum
{
    public class BufferChecksum : Hiperscan.Common.Checksum.BaseChecksum
    {
        public BufferChecksum(HashType type, byte[] original_buffer, string salt = null, string api_key_hash = null, string api_key = null, string control_hash_value = null)
            : base(type, salt, api_key_hash, api_key)
        {
            this.ControlHashValue = control_hash_value;
            this.Buffer = original_buffer;
        }

        public override string ComputeHash()
        {
            return BitConverter.ToString(this.CurrentHashAlogrithm.ComputeHash(this.Buffer));
        }

        public override bool Verifiy()
        {
            return this.ComputeHash() == this.ControlHashValue;
        }

        protected string ControlHashValue { get; set; }
        protected byte[] Buffer           { get; set; }
    }
}
