﻿// Created with MonoDevelop
//
//    Copyright (C) 2017 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;


namespace Hiperscan.Common.RegularExpressions
{

    public static class GlobalRegex
    {

        public static class SpectrumRegex
        {
            private const string SequenceNumber = @"(([A-Z]{1,4})|([A-Z]{1}\d{7}))";
            private const string SpectrumNumber = @"\d+";


            public const string SampleId = @"[0-9A-Z]+";

            public static readonly string SequenceId = string.Format(@"{0}_{1}",
                                                                     SpectrumRegex.SampleId,
                                                                     SpectrumRegex.SequenceNumber);

            public static readonly string SpectrumId = string.Format(@"{0}_{1}_{2}",
                                                                     GlobalRegex.SpectrumRegex.SampleId,
                                                                     GlobalRegex.SpectrumRegex.SequenceNumber,
                                                                     GlobalRegex.SpectrumRegex.SpectrumNumber);

            public const string CustomerFilename = @".+__.+__.+";
            public const string ReferenceDirname = @"(\.?Referencing)|(\.?Referenzierung)";

            public const string Date = @"\d{4}-\d{2}-\d{2}_\d{2}-\d{2}-\d{2}";
            public const string RawSpecimenSpectrumFilename = @"probe|specimen|sample";

            public static readonly string CustomerValidationFilename = string.Format(@"{0}_{1}_{2}",
                                                                                     GlobalRegex.SpectrumRegex.SampleId,
                                                                                     GlobalRegex.SpectrumRegex.Date,
                                                                                     GlobalRegex.DeviceRegex.SgsNumber);

            public static readonly string Identity = string.Format("{0}:={0}", GlobalRegex.SpectrumRegex.SampleId);

        }

        public static class DeviceRegex
        {
            public const string SgsNumber    = @"[A-Z]\d{7}";
            public const string DeviceNumber = @"[A-Za-z]{1,2}\d\d((0[1-9])|([1-4]\d)|(5[0-3]))( |-)?\d{3,4}";
        }

        public static class ReferenceRegex
        {
            //TODO: add the patterns for white, black and so on
        }

        public static string xComplete(this string value)
        {
            return "^" + value + "$";
        }

        public static string xCompleteWithCsvExtension(this string value)
        {
            return "^" + value + @"\.[Cc][Ss][Vv]$";
        }
    }
}
