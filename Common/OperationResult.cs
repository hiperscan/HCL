﻿// Created with MonoDevelop
//
//    Copyright (C) 2020 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;


namespace Hiperscan.Common
{

    public class OperationResult
    {

        private Exception exception = null;


        public OperationResult(int error_code = 0)
        {
            this.ErrorCode = error_code;
            this.TimeStamp = DateTime.Now;
        }

        public OperationResult(string message, int error_code = 0) : this(error_code)
        {
            if (string.IsNullOrEmpty(message) == false)
                this.exception = new Exception(message);
        }

        public OperationResult(Exception ex, int error_code = 0) : this(error_code)
        {
            if (ex != null)
                this.Exception = ex;
        }

        public bool HasErrorCode
        {
            get { return this.ErrorCode != 0; }
        }

        public Exception Exception 
        { 
            get 
            {
                return this.exception; 
            }

            private set
            {
                this.exception = value;
                if (this.HasErrorCode == false)
                    this.ErrorCode = -1;

                this.StackTrace = value.StackTrace;
                this.ErrorMessage = value.Message;
            }
        }
        public string StackTrace         { get; private set; }
        public string ErrorMessage       { get; private set; }
        public DateTime TimeStamp        { get; private set; }
        public int ErrorCode             { get; private set; } = 0;
        public string UserDefinedMessage { get; set; }
    }
}