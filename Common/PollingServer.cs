﻿// Created with MonoDevelop
//
//    Copyright (C) 2019 Hiperscan GmbH Dresden, Germany
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.

using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;


namespace Hiperscan.Common
{

    public class PollingServer<T> : System.IDisposable
    {

        private T item = default(T);

        private enum PollingState
        {
            IsRunning,
            StoppingRunning,
            RunningStopped,
        }

        private volatile PollingState state = PollingState.RunningStopped;

        public PollingServer(Comparer<T> comparer = null, T default_value = default(T))
        {
            this.Item = default_value;
            this.DefaultValue = default_value;

            if (comparer == null)
                this.Comparer = Comparer<T>.Default;

            if (this.Comparer == null)
                throw new Exception("Cannot find comparer.");
        }

        public async Task<T> WaitForItemAsync()
        {
            return await Task<T>.Run(() => this.WaitForItem());
        }

        public T WaitForItem()
        {
            T tmp = default(T);
            this.state = PollingState.IsRunning;

            while (true)
            {
                tmp = this.Item;
                this.Item = this.DefaultValue;

                if (this.state == PollingState.StoppingRunning)
                    break;

                if (this.Comparer.Compare(tmp, this.DefaultValue) == 0)
                {
                    Thread.Sleep(20);
                    continue;
                }
                
                break;
            }

            this.state = PollingState.RunningStopped;

            return tmp;

        }

        public void Dispose()
        {
            /*
            if (state == PollingState.IsRunning)
                this.state = PollingState.StoppingRunning;

            while (this.state != PollingState.RunningStopped)
                Thread.Sleep(20);
            */
        }

        public T Item
        {
            private get
            {
                lock (this)
                    return this.item;
            }

            set
            {
                lock (this)
                    this.item = value;
            }
        }

        private T DefaultValue       { get; set; }
        private Comparer<T> Comparer { get; set; }
    }
}
